# encoding: utf-8
"""
    Accounts Admin module
"""

from django.contrib import admin
from django.contrib.auth import get_user_model
from django.contrib.auth.admin import UserAdmin
from accounts.models.user_card import UserCard
from accounts.models.employee_card import EmployeeCard

admin.autodiscover()


class EmployeeCardAdminInline(admin.TabularInline):
    """
        UserCard Admin
    """
    model = EmployeeCard


class UserCardAdminInline(admin.TabularInline):
    """
        UserCard Admin
    """
    model = UserCard


class GVTUserAdmin(UserAdmin):
    """
        Override User Admin
    """
    inlines = [EmployeeCardAdminInline, UserCardAdminInline]


admin.site.unregister(get_user_model())
admin.site.register(get_user_model(), GVTUserAdmin)
