"""
Accounts Permissions
"""
from django.views import View
from rest_framework.permissions import BasePermission
from rest_framework.request import Request
from restfw_composed_permissions.base import BasePermissionComponent

from accounts.enums import UserStatuses
from accounts.utils.employees_util import EmployeesUtil


class IsEmployeeManagerOrDriver(BasePermission):
    """
    Check if user is Employee and has Manager role
    """

    @classmethod
    def has_permission(cls, request: Request, view: View) -> bool:
        """
        Check if current session user has Manager role
        :param request: HttpRequest instance
        :param view: View function
        :return: boolean
        """
        return request.user.user_card.status == UserStatuses.active.value and \
            EmployeesUtil.is_manager_or_driver(request.user)


class IsEmployeeManager(BasePermission):
    """
    Check if user is Employee and has Manager role
    """

    @classmethod
    def has_permission(cls, request: Request, view: View) -> bool:
        """
        Check if current session user has Manager role
        :param request: HttpRequest instance
        :param view: View function
        :return: boolean
        """
        return request.user.user_card.status == UserStatuses.active.value and \
            EmployeesUtil.is_manager(request.user)


class IsEmployeeManagerPermissionComponent(BasePermissionComponent):
    """
    Is Employee Manager PermissionComponent
    """

    def has_permission(self, permission: BasePermission, request: Request, view: View) -> bool:
        """
        Check if current logged-in user has permissions
        :param permission: Permission instance
        :param request: HttpRequest instance
        :param view: current view
        :return: boolean
        """
        return IsEmployeeManager.has_permission(request, view)


class IsUserActive(BasePermission):
    """
    Check if UserCard is in active status
    """

    @classmethod
    def has_permission(cls, request: Request, view: View) -> bool:
        """
        Check if current session user has Manager role
        :param request: HttpRequest instance
        :param view: View function
        :return: boolean
        """
        return request.user.user_card.status == UserStatuses.active.value and request.user.is_active
