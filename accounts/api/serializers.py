# encoding: utf-8
"""
Accounts serializers
"""
from typing import Union, Type

from django.contrib.auth import get_user_model
from django.db.models import Model
from rest_framework import serializers
from sorl.thumbnail import get_thumbnail
from accounts.models.user_card import UserCard
from accounts.models.employee_card import EmployeeCard
from accounts.models.employee_geolocation import EmployeeGeolocation
from django.conf import settings

from organisations.api.serializers import OrganisationMemberCardSerializer
from organisations.models.organisation_member_card import OrganisationMemberCard
from organisations.models.organisation import Organisation


class EmployeeGeolocationSerializer(serializers.ModelSerializer):
    """
        Employee Geolocation serializer
    """

    class Meta:
        """ Meta class """
        model: Type[Model] = EmployeeGeolocation
        fields: tuple = ('longitude', 'latitude', 'modified')


class EmployeeCardSerializer(serializers.ModelSerializer):
    """
        Employee Card Serializer
    """
    geolocation: serializers.BaseSerializer = EmployeeGeolocationSerializer()

    class Meta:
        """ Meta class """
        model: Type[Model] = EmployeeCard
        fields: tuple = ('role', 'get_role_display', 'geolocation')


class UserCardSerializer(serializers.ModelSerializer):
    """
        Employee Card Serializer
    """

    photo: serializers.SerializerMethodField = serializers.SerializerMethodField()

    class Meta:
        """ Meta class """
        model: Type[Model] = UserCard
        fields: tuple = ('photo', 'date_of_birth', 'phone_number', 'status', 'about', 'hometown', 'get_status_display')

    @staticmethod
    def get_photo(obj: UserCard) -> Union[str, None]:
        """
        Get photo thumbnail
        :return:
        """
        return (settings.CURRENT_SITE + get_thumbnail(obj.photo, '128x128', crop='center', quality=70).url) \
            if obj.photo else None


class SimpleUserCardSerializer(serializers.ModelSerializer):
    """
    Simple UserCard JSON Serializer
    """
    class Meta:
        """ Meta class """
        model: Model = UserCard
        fields: tuple = ('phone_number', )


class UserSerializer(serializers.ModelSerializer):
    """
        User serializer
    """
    employee_card: serializers.BaseSerializer = EmployeeCardSerializer()
    user_card: serializers.BaseSerializer = UserCardSerializer()

    class Meta:
        """ Meta class """
        model: Model = get_user_model()
        fields: tuple = (
            'id', 'first_name', 'last_name', 'email', 'username', 'is_superuser',
            'date_joined', 'is_staff', 'employee_card', 'user_card'
        )


class SimpleUserSerializer(serializers.ModelSerializer):
    """
    Simple User Serializer
    """
    user_card: Type[Model] = SimpleUserCardSerializer()

    class Meta:
        """ Meta class """
        model: Type[Model] = get_user_model()
        fields: tuple = ('id', 'username', 'first_name', 'last_name', 'email', 'user_card')


class EmployeeListSerializer(serializers.ModelSerializer):
    """
        User serializer
    """
    employee_card: serializers.BaseSerializer = EmployeeCardSerializer()
    user_card: serializers.BaseSerializer = SimpleUserCardSerializer()

    class Meta:
        """ Meta class """
        model: Type[Model] = get_user_model()
        fields: tuple = ('id', 'first_name', 'last_name', 'username', 'date_joined', 'employee_card', 'user_card')


class EmployeeFullListSerializer(serializers.ModelSerializer):
    """
        User serializer
    """
    employee_card: serializers.BaseSerializer = EmployeeCardSerializer()
    user_card: serializers.BaseSerializer = UserCardSerializer()

    class Meta:
        """ Meta class """
        model: Type[Model] = get_user_model()
        fields: tuple = ('username', 'first_name', 'last_name', 'email', 'employee_card', 'user_card')


class OrganisationMemberSerializer(serializers.ModelSerializer):
    """
    Organisation Employee Serializer
    !!!NOTE: org_member_card - is a property from prefetch query. See usage of this Serializer
    """
    user_card: serializers.BaseSerializer = UserCardSerializer()
    org_member_card: serializers.BaseSerializer = serializers.SerializerMethodField()

    organisation: Organisation

    class Meta:
        """ Meta class """
        model: Type[Model] = get_user_model()
        fields: tuple = ('username', 'first_name', 'last_name', 'user_card', 'org_member_card')

    def __init__(self, *args, organisation: Organisation = None, **kwargs):
        """
        Initialize serializer
        :param args: arguments
        :param organisation: Organisation instance or None
        :param kwargs: key-word arguments
        """
        super().__init__(*args, **kwargs)
        self.organisation = organisation

    @staticmethod
    def get_org_member_card(obj: get_user_model()) -> dict:
        """
        Get organisation member card
        :param obj: User instance
        :return: serialized OrganisationMemberCard
        """
        org_card: OrganisationMemberCard = next(iter(obj.org_member_card))

        return OrganisationMemberCardSerializer(org_card, many=False).data
