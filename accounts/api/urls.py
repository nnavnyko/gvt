# encoding: utf-8
"""
API Accounts Urls
"""
from django.conf.urls import include, url

from accounts.api.views import UsersViewSet, EmployeesViewSet, AuthorizationView, LogoutView

app_name = 'accounts_api'

urlpatterns = [
    url(r'^json$', UsersViewSet.as_view({'get': 'get_json'}), name='json'),
    url(r'^log-in/$', AuthorizationView.as_view(), name='login'),
    url(r'^log-out/$', LogoutView.as_view(), name='logout'),
    url(r'^update/$', UsersViewSet.as_view({'post': 'update_profile'}), name='update'),
    url(r'^set_geolocation/$', EmployeesViewSet.as_view({'post': 'set_geolocation'}), name='set_geolocation'),
    url(r'^update-photo/$', UsersViewSet.as_view({'post': 'update_profile_photo'}), name='update_profile_photo'),
    url(r'^employees/list$', EmployeesViewSet.as_view({'get': 'get_employees'}), name='employees_list'),
    url(r'^employees/list/full$', EmployeesViewSet.as_view({'get': 'get_employees_full'}), name='employees_full_list'),
    url(r'^employees/(?P<username>[a-z0-9\-_.]+)$', EmployeesViewSet.as_view({'get': 'retrieve'}), name='employee_profile'),
    url(r'^employees/(?P<username>[a-z0-9\-_.]+)/toggle-status$',
        EmployeesViewSet.as_view({'post': 'toggle_is_activate'}), name='toggle_employee_status'),
    url(r'^employees/(?P<username>[a-z0-9\-_.]+)/change-role$',
        EmployeesViewSet.as_view({'post': 'change_employee_role'}), name='change_employee_role'),
]
