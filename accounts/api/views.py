# encoding: utf-8
"""
    Accounts views
"""
from typing import Union

from django.contrib.auth import get_user_model
from django.core.exceptions import PermissionDenied
from django.db.models import QuerySet
from django_filters import FilterSet
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework.authentication import BasicAuthentication
from rest_framework.decorators import action
from rest_framework.pagination import PageNumberPagination
from rest_framework.permissions import IsAuthenticated
from rest_framework.request import Request
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.viewsets import ModelViewSet
from sorl.thumbnail.shortcuts import get_thumbnail
from rest_framework import filters, serializers
from django.contrib.auth import login as auth_login, logout as auth_logout
from django.conf import settings
import jwt

from accounts.models.employee import Employee
from accounts.models.employee_geolocation import EmployeeGeolocation
from accounts.api.serializers import UserSerializer, EmployeeListSerializer, EmployeeFullListSerializer, \
    EmployeeGeolocationSerializer
from accounts.enums import UserStatuses
from accounts.forms.auth import LoginForm
from accounts.forms.profile import UpdateProfileForm, UpdateUserCardForm, UpdateUserPhotoForm
from accounts.models.user_card import UserCard
from accounts.utils.employees_util import EmployeesUtil
from common.api.auth import SessionAuthentication
from accounts.api.permissions import IsEmployeeManagerOrDriver, IsUserActive, IsEmployeeManager
from common.utilities.common import parse_phone_number


class UsersViewSet(ModelViewSet):
    """
        Users ViewSet
    """
    serializer_class: serializers.BaseSerializer = UserSerializer
    permission_classes: tuple = (IsAuthenticated, IsUserActive)
    authentication_classes: tuple = (BasicAuthentication, SessionAuthentication)

    def get_queryset(self) -> QuerySet:
        """
        Get queryset override
        :return: QuerySet result
        """
        return get_user_model().objects.select_related('employee_card', 'user_card').all()

    @staticmethod
    def get_json(request: Request, *args, **kwargs) -> Response:
        """
        Get JSON of the current user in session
        :param request: HTTP request
        :param args: arguments
        :param kwargs: key-word arguments
        :return: REST Response
        """
        return Response(UserSerializer(request.user).data)

    @staticmethod
    @action(detail=False, methods=['POST'])
    def update_profile_photo(request: Request, *args, **kwargs) -> Union[Response, None]:
        """
        Update User Photo
        Need to be a separated url because content_type=application/json cannot upload files
        :param request: Request instance
        :param args: arguments
        :param kwargs: keyword arguments
        :return: API Response
        """
        user_card: UserCard = request.user.user_card
        files: dict = {'photo': request.FILES['file']}
        user_card_photo_form: UpdateUserPhotoForm = UpdateUserPhotoForm(instance=user_card, data=files, files=files)

        if user_card_photo_form.is_valid():
            request.user.user_card = user_card_photo_form.save()

            return Response({'photo_url': get_thumbnail(request.user.user_card.photo, '128x128',
                                                        crop='center', quality=70).url})

    @staticmethod
    @action(detail=False, methods=['POST'])
    def update_profile(request: Request, *args, **kwargs) -> Response:
        """
        Update User Information
        :param request: Request instance
        :param args: arguments
        :param kwargs: keyword arguments
        :return: API Response
        """
        user_data: dict = request.data.copy()
        user_card_data: dict = user_data.get('user_card', {})
        user_card_data['phone_number']: str = parse_phone_number(str(user_card_data['phone_number']))

        user_form: UpdateProfileForm = UpdateProfileForm(instance=request.user, data=user_data, files=request.FILES)
        user_card: UserCard = request.user.user_card
        user_card_form: UpdateUserCardForm = UpdateUserCardForm(
            instance=user_card,
            data=user_card_data,
            files=request.FILES
        )

        if user_form.is_valid() and user_card_form.is_valid():
            user: get_user_model() = user_form.save()
            user.user_card = user_card_form.save()

            return Response(UserSerializer(user).data)

        return Response({
            'user_errors': user_form.errors,
            'user_card_errors': user_card_form.errors
        }, status=400)


class EmployeeListPaginationClass(PageNumberPagination):
    """
        TransportationPaginationClass
    """
    page_size: int = 10
    page_query_param: str = 'page'


class EmployeesListFilter(FilterSet):
    """
        Transportation Requests List Filter
    """
    class Meta:
        """ Meta class """
        model = get_user_model()
        fields: dict = {
            'employee_card__role': ['exact']
        }


class EmployeesViewSet(ModelViewSet):
    """
    Employees View Set
    """
    serializer_class: serializers.BaseSerializer = EmployeeListSerializer
    permission_classes: tuple = (IsAuthenticated, IsEmployeeManagerOrDriver, IsUserActive)
    authentication_classes: tuple = (BasicAuthentication, SessionAuthentication)
    pagination_class: tuple = EmployeeListPaginationClass
    lookup_field: str = 'username'
    lookup_url_kwarg: str = 'username'
    filter_class: type = EmployeesListFilter
    filter_backends: tuple = (filters.SearchFilter, DjangoFilterBackend)
    search_fields: tuple = ('user_card__phone_number', 'email')

    @action(detail=False, methods=['GET'])
    def get_employees(self, request: Request, *args, **kwargs) -> Response:
        """
        Get employees list
        :param request: Request instance
        :param args: arguments
        :param kwargs: key-word arguments
        :return: REST Response
        :raises: PermissionDenied if the request.user is not an employee (admin, manager or driver)
        """
        employees_list: serializers.BaseSerializer = self.get_serializer(
            Employee.objects.get_list(
                user_card__status=UserStatuses.active.value
            ),
            many=True
        )

        return Response(employees_list.data)

    @action(detail=False, methods=['GET'])
    def get_employees_full(self, request: Request, *args, **kwargs) -> Response:
        """
        Get employees list
        :param request: Request instance
        :param args: arguments
        :param kwargs: key-word arguments
        :return: REST Response
        :raises: PermissionDenied if the request.user is not an employee (admin, manager or driver)
        """
        queryset: QuerySet = self.filter_queryset(Employee.objects.get_list([request.user.id]))

        return self.get_paginated_queryset_response(queryset)

    def get_paginated_queryset_response(self, queryset: QuerySet) -> Response:
        """
        Get paginated queryset
        :param queryset: QuerySet instance
        :return: REST Response
        """
        page: QuerySet = self.paginate_queryset(queryset)

        if page is not None:
            return self.get_paginated_response(EmployeeFullListSerializer(page, many=True).data)

        return Response(EmployeeFullListSerializer(queryset, many=True).data)

    @action(detail=False, methods=['GET'])
    def retrieve(self, request: Request, *args, **kwargs) -> Response:
        """
        Get Employee Profile
        :param request: Request instance
        :param args: arguments
        :param kwargs: key-word arguments
        :return:
        """
        user: get_user_model() = self.get_object()

        return Response(UserSerializer(user).data)

    def get_queryset(self) -> QuerySet:
        """
        Get queryset override
        :return: QuerySet result
        """
        return get_user_model().objects.select_related('user_card', 'employee_card').all()

    @action(detail=False, methods=['POST'])
    def toggle_is_activate(self, request: Request, *args, **kwargs) -> Response:
        """
        Toggle is_active and UserCard.status fields
        Activate or Deactivate account
        :param request: Request instance
        :param args: arguments
        :param kwargs: key-word arguments
        :return: REST Response
        """
        user: get_user_model() = self.get_object()
        if not IsEmployeeManager.has_permission(request, self):
            raise PermissionDenied(u'Только менеджеры (администратор, менеджер) могут менять Заявку')

        user: get_user_model() = EmployeesUtil.toggle_employee_status(user)

        return Response({
            'status': user.user_card.status,
            'get_status_display': user.user_card.get_status_display()
        })

    def change_employee_role(self, request: Request, *args, **kwargs) -> Response:
        """
        Toggle is_active and UserCard.status fields
        Activate or Deactivate account
        :param request: Request instance
        :param args: arguments
        :param kwargs: key-word arguments
        :return: REST Response
        """
        user = self.get_object()
        if not IsEmployeeManager.has_permission(request, self):
            raise PermissionDenied(u'Только менеджеры (администратор, менеджер) могут менять Заявку')

        EmployeesUtil.change_employee_role(user, request.data.get('role'))

        return Response({
            'role': user.employee_card.role,
            'get_role_display':user.employee_card.get_role_display()
        })

    def set_geolocation(self, request: Request, *args, **kwargs) -> Response:
        """
        Set employee geo-position to DB
        Handle POST request
        :param request: Request instance
        :param args: arguments
        :param kwargs: key-word arguments
        :return: REST Response
        """
        employee: get_user_model() = request.user

        EmployeeGeolocation.objects.set_geolocation(employee, request.data)

        return Response({
            'geolocation': EmployeeGeolocationSerializer(employee.employee_card.geolocation).data
        })


class AuthorizationView(APIView):
    """
    Authorization View
    """

    def post(self, request: Request, *args, **kwargs) -> Response:
        """
        Authorization view
        :param request: Request instance
        :param args: arguments
        :param kwargs: key-word arguments
        :return: REST Response
        """
        data: dict = request.data.copy()
        data['request']: Request = request

        form: LoginForm = LoginForm(data=request.data)

        if form.is_valid():
            auth_login(self.request, form.get_user())
            auth_token: str = jwt.encode(
                {'username': request.user.username},
                settings.JWT_AUTH_SECRET,
                algorithm='HS256'
            ).decode()

            return Response({'logged_in': True, 'username': request.user.username, 'auth_token': auth_token})

        return Response({'errors': form.errors}, status=400)


class LogoutView(APIView):
    """
    Authorization View
    """
    authentication_classes: tuple = (BasicAuthentication, SessionAuthentication)

    def post(self, request: Request, *args, **kwargs) -> Response:
        """
        Authorization view
        :param request: Request instance
        :param args: arguments
        :param kwargs: key-word arguments
        :return: REST Response
        """
        auth_logout(request)

        return Response({'logout': True}, status=200)
