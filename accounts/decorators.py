# encoding: utf-8
"""
Accounts decorators
"""
from django.core.exceptions import PermissionDenied
from django.utils.translation import ugettext_lazy as _
from typing import Any, Callable

from accounts.enums import EMPLOYEE_ROLES_DICT, UserStatuses
from accounts.models.user import DjangoUser


def defined_role_required(fn: Callable) -> Callable:
    """
    User role has to be not 'undefined'
    :param fn: function
    :return:
    """
    def wrapper(request, *args, **kwargs) -> Any:
        """ wrapper function """
        user = request.user
        if not user or not user.is_authenticated or \
                not hasattr(user, 'employee_card') or user.employee_card.role == 'undefined':
            raise PermissionDenied(_(u'Вы должны иметь роль, назначенную администратором'))
        return fn(request, *args, **kwargs)
    return wrapper


def role_required(*roles) -> Callable:
    """
    Forbid if user has no required role
    :param roles:
    :return:
    """
    if isinstance(roles, str):
        roles = [roles]

    def actual_decorator(fn) -> Callable:
        """ actual decorator """
        def wrapper(request, *args, **kwargs) -> Any:
            """ wrapper """
            user: DjangoUser = request.user
            for role in roles:
                if role not in EMPLOYEE_ROLES_DICT:
                    raise ValueError(u'{} not in EMPLOYEE_ROLES'.format(role))

            if not hasattr(user, 'employee_card') or user.employee_card.role not in roles:
                raise PermissionDenied(_(u'Не хватает прав. Свяжитесь с администратором '
                                         u'для получения прав на просмотр содержимого'))
            return fn(request, *args, **kwargs)

        return wrapper

    return actual_decorator


def status_active_required(fn) -> Any:
    """
    Active status of the user required
    :param fn: function or method
    :return: function
    """
    def wrapper(request, *args, **kwargs) -> Any:
        """ wrapper function """
        user = request.user
        if not user or not user.is_authenticated or \
                not user.user_card.status == UserStatuses.active.value:
            raise PermissionDenied(_(u'Ваш профиль не активен'))

        return fn(request, *args, **kwargs)

    return wrapper
