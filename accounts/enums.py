# encoding: utf-8
"""
Enumerations of the Accounts app
"""
from django.utils.translation import ugettext_lazy as _
from enum import Enum


EMPLOYEE_ROLES = (
    ('porter', _(u'Грузчик')),
    ('driver', _(u'Водитель')),
    ('customer', _(u'Заказчик')),
    ('manager', _(u'Менеджер')),
    ('admin', _(u'Администратор')),
    ('undefined', _(u'Роль не назначена')),
)
EMPLOYEE_ROLES_DICT = dict(EMPLOYEE_ROLES)

EMPLOYEE_MANAGER_OR_DRIVER_ROLES = ('driver', 'manager', 'admin')
EMPLOYEE_MANAGERS_ROLES = ('manager', 'admin')
EMPLOYEES_ROLES = ('driver', 'manager', 'admin')


class EmployeeRoles(Enum):
    """
    Employee Roles
    """
    porter = 'porter'
    driver = 'driver'
    customer = 'customer'
    manager = 'manager'
    admin = 'admin'
    undefined = 'undefined'


USER_STATUSES = (
    ('active', _(u'Активен')),
    ('pending', _(u'В ожидании')),
    ('disabled', _(u'Заблокирован'))
)


class UserStatuses(Enum):
    """
    User Statuses
    """
    active = 'active'
    pending = 'pending'
    disabled = 'disabled'

