# encoding: utf-8
"""
Accounts Auth forms
"""
from typing import Type

from django.contrib.auth import get_user_model
from django.contrib.auth.forms import AuthenticationForm
from registration.forms import RegistrationFormTermsOfService, RegistrationFormUniqueEmail

from accounts.models.user_card import UserCard
from django import forms
from django.utils.translation import ugettext_lazy as _
from django.db.models import Q, Model

from common.forms.forms import BootstrapControlsForm, GoogleReCaptchaForm
from common.utilities.common import parse_phone_number


class UserCardForm(forms.ModelForm):
    """
    User Card Form
    """
    class Meta:
        """ Meta class """
        model: Type[Model] = UserCard
        exclude: tuple = ('status', )


class RegistrationForm(GoogleReCaptchaForm, RegistrationFormUniqueEmail, RegistrationFormTermsOfService, BootstrapControlsForm):
    """
    Registration form with confirm terms and unique email
    """
    username = forms.CharField(label=_(u'имя пользователя'), max_length=255, widget=forms.TextInput())
    first_name = forms.CharField(label=_(u'имя'), max_length=255, widget=forms.TextInput())
    last_name = forms.CharField(label=_(u'фамилия'), max_length=255, widget=forms.TextInput())
    phone_number = forms.CharField(label=_(u'номер телефона'), max_length=255, required=True,
                                   widget=forms.TextInput(
                                       attrs={'class': 'phone-number-mask', 'placeholder': _(u'Номер телефона (33) 333-33-33')}))

    def save(self, commit: bool = True) -> get_user_model():
        """
        Create user and add UserCard to user
        :param commit: bool
        :return: user instance
        """
        user = super(RegistrationForm, self).save(commit=False)

        user.first_name = self.cleaned_data.get('first_name')
        user.last_name = self.cleaned_data.get('last_name')

        user.save()

        user_card_data: dict = {
            'user': user.id,
            'phone_number': parse_phone_number(self.cleaned_data.get('phone_number'))
        }
        # Add UserCard to the user
        user_card_form: UserCardForm = UserCardForm(data=user_card_data)

        if user_card_form.is_valid():
            user_card_form.save()

        return user


class LoginForm(AuthenticationForm, BootstrapControlsForm):
    """
    Registration form with confirm terms and unique email
    """
    username = forms.CharField(label=_(u'имя пользователя или E-Mail'), max_length=255, widget=forms.TextInput())

    def clean_username(self) -> str:
        """
        Clean username or email
        :return:
        """
        username: str = self.cleaned_data['username']
        username = get_user_model().objects.filter(
            Q(username=username) | Q(email=username)
        ).values_list(
            'username',
            flat=True
        )

        if not len(username):
            raise forms.ValidationError(
                self.error_messages['invalid_login'],
                code='invalid_login',
                params={'username': self.username_field.verbose_name},
            )

        self.cleaned_data['username'] = username[0]

        return self.cleaned_data['username']
