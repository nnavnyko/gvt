"""
    Profile Forms
    Update current user's information
"""
from typing import Type

from django import forms
from django.contrib.auth import get_user_model
from django.db.models import Model

from accounts.models.user_card import UserCard


class UpdateProfileForm(forms.ModelForm):
    """
        Update Profile Form
    """
    class Meta:
        """ Meta class """
        model: Type[Model] = get_user_model()
        exclude: tuple = ('id', 'password', 'is_staff', 'is_active', 'is_superuser',
                   'date_joined', 'username', 'email')


class UpdateUserCardForm(forms.ModelForm):
    """
        Update User Card
    """
    class Meta:
        """ Meta class """
        model: Type[Model] = UserCard
        exclude: tuple = ('id', 'status', 'user', 'photo')


class UpdateUserPhotoForm(forms.ModelForm):
    """
        Update Profile Form
    """
    class Meta:
        """ Meta class """
        model: Type[Model] = UserCard
        fields: tuple = ('photo', )
