"""
GVT Custom Session Authentication Middleware
---
Use custom header to authenticate user
"""
from django.contrib.sessions.middleware import SessionMiddleware
from django.http import HttpRequest

GVT_SESSION_COOKIE_NAME = 'HTTP_GVT_SESSIONID'


class GVTSessionMiddleware(SessionMiddleware):
    """
    Custom GVT Session Middleware
    """

    def process_request(self, request: HttpRequest):
        """
        Process Request
        Append session to request (use custom Cookie to identify session)
        :param request: HttpRequest instance
        :return: None
        """
        super(GVTSessionMiddleware, self).process_request(request)

        if not request.session or not request.session.session_key:
            # Use custom header if no session in cookies
            session_key: str = request.META.get(GVT_SESSION_COOKIE_NAME)
            request.session = self.SessionStore(session_key)
