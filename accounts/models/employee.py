"""
Employee proxy model
"""
from typing import Optional

from django.contrib.auth import get_user_model
from django.db import models

from accounts.enums import EMPLOYEE_ROLES_DICT, EMPLOYEE_MANAGER_OR_DRIVER_ROLES, EMPLOYEE_MANAGERS_ROLES, UserStatuses
from common.managers.base_manager import BaseManager


class EmployeesManager(BaseManager):
    """
        Employee Card manager
    """

    @staticmethod
    def get_list(exclude_ids: Optional[list] = None, **kwargs) -> models.QuerySet:
        """
        Get employees list
        :param exclude_ids: list of ints (User.ids)
        :param kwargs: dict of filtration/pagination
        :return: QuerySet
        """
        queryset = get_user_model().objects.select_related().filter(
            employee_card__role__in=EMPLOYEE_ROLES_DICT.keys(), **kwargs)

        if exclude_ids:
            queryset = queryset.exclude(id__in=exclude_ids)

        return queryset

    @classmethod
    def get_employees_user_names(cls, *exclude_user_names):
        """
        Get Employees usernames
        :param exclude_user_names: list of strings
        :return: list of strings
        """
        return cls._get_employees_user_names(EMPLOYEE_MANAGER_OR_DRIVER_ROLES, *exclude_user_names)

    @classmethod
    def get_managers_user_names(cls, *exclude_user_names):
        """
        Get Managers usernames
        :param exclude_user_names: list of strings
        :return: list of strings
        """
        return cls._get_employees_user_names(EMPLOYEE_MANAGERS_ROLES, *exclude_user_names)

    @staticmethod
    def _get_employees_user_names(roles_list: list, *exclude_user_names):
        """
        Get employees user names filtered by roles list
        :param roles_list: list of strings
        :param exclude_user_names: list of excludes user names
        :return: list of user names (User.username)
        """
        employees_user_names = get_user_model().objects.select_related().filter(
            employee_card__role__in=roles_list,
            user_card__status=UserStatuses.active.value,
            is_active=True
        ).exclude(
            username__in=exclude_user_names
        ).values_list(
            'username',
            flat=True
        )

        return list(employees_user_names)


class Employee(get_user_model()):
    """
    Employee model - proxy auth.User model
    """

    class Meta:
        proxy = True

    objects = EmployeesManager()
