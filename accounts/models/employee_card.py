"""
EmployeeCard model and manager
"""

from django.conf import settings
from django.db import models
from django.utils.translation import ugettext_lazy as _
from django_extensions.db.models import TimeStampedModel

from accounts.enums import (
    EMPLOYEE_ROLES
)
from accounts.models.user import DjangoUser
from common.managers.base_manager import BaseManager


class EmployeeCardManager(BaseManager):
    """
    EmployeeCard manager
    """
    pass


class EmployeeCard(TimeStampedModel):
    """
        Employee card
        User cannot be an employee until one of superusers
        fill his employee card
    """
    class Meta:
        """ Meta class """
        db_table = 'employee_card'
        verbose_name = _('карта сотрудника')
        verbose_name_plural = _('Карты сотрудника')

    user: DjangoUser
    user = models.OneToOneField(settings.AUTH_USER_MODEL, verbose_name=_('пользователь'), related_name='employee_card',
                                on_delete=models.CASCADE)
    role = models.CharField(verbose_name=_('роль'), choices=EMPLOYEE_ROLES, default='undefined', max_length=20)
    rating = models.FloatField(verbose_name=_('рейтинг'), null=True, default=None, blank=True)

    objects = EmployeeCardManager()

    def __unicode__(self) -> str:
        """
        Unicode representation
        :return: unicode string
        """
        return _(u'Карта сотрудника {}').format(self.user.username)
