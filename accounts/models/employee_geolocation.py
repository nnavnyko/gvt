"""
Employee GeoLocation
"""
from typing import TYPE_CHECKING

from django.contrib.auth import get_user_model
from django.db import models
from django.utils.translation import ugettext_lazy as _
from django_extensions.db.models import TimeStampedModel

from common.managers.base_manager import BaseManager

if TYPE_CHECKING:
    UserModel = get_user_model()


class EmployeeGeolocationManager(BaseManager):
    """
        Employee GeoLocation manager
    """

    def set_geolocation(self, employee: 'UserModel', data: dict) -> 'UserModel':
        """
        Set geolocation of the Employee
        :param employee: User instance
        :param data: key-word arguments (dict, {'longitude': 5.5631234, 'latitude': 9.5123323})
        :return: user instance with updated geolocation
        """
        employee_geolocation: EmployeeGeolocation = employee.employee_card.geolocation \
            if hasattr(employee.employee_card, 'geolocation') else EmployeeGeolocation()

        if not employee_geolocation.id:
            employee.employee_card.geolocation = employee_geolocation

        self.update_model(employee_geolocation, **{
            'longitude': data['longitude'],
            'latitude': data['latitude']
        })

        return employee


class EmployeeGeolocation(TimeStampedModel):
    """
        Employee Geolocation model
        Track employee geo-position
    """
    class Meta:
        """ Meta class """
        db_table = 'employee_geolocation'
        verbose_name = _('местоположение сотрудника')
        verbose_name_plural = _('местоположение сотрудников')

    employee_card = models.OneToOneField('accounts.EmployeeCard', verbose_name=_('карта сотрудника'),
                                         related_name='geolocation', on_delete=models.CASCADE)

    latitude = models.FloatField(verbose_name=_(u'широта'), null=True, blank=True, default=None)
    longitude = models.FloatField(verbose_name=_(u'долгота'), null=True, blank=True, default=None)

    objects = EmployeeGeolocationManager()

    def __str__(self) -> str:
        """
        EmployeeGeolocation string representation
        :return: string
        """
        return 'EmployeeGeolocation {}'.format(self.id)
