"""
Define django user proxy model and manager
"""
from django.contrib.auth import get_user_model

from common.managers.base_manager import BaseManager


class UserManager(BaseManager):
    """
        Django User manager
    """


class DjangoUser(get_user_model()):
    """
        Django User model
    """
    class Meta:
        proxy = True

    objects = UserManager()
