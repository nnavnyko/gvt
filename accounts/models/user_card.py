"""
UserCard model + manager
"""
from django.conf import settings
from django.db import models
from django.utils.translation import ugettext_lazy as _
from django_extensions.db.models import TimeStampedModel
from sorl.thumbnail import ImageField

from accounts.enums import USER_STATUSES
from common.managers.base_manager import BaseManager


class UserCardManager(BaseManager):
    """
        UserCard manager
    """


class UserCard(TimeStampedModel):
    """
        User Card
        This model contains additional info about user (date of birth, photo, etc.)
    """
    class Meta:
        """ Meta class """
        db_table = 'user_card'
        verbose_name = _('карта пользователя')
        verbose_name_plural = _('Карты пользователей')

    user = models.OneToOneField(settings.AUTH_USER_MODEL, verbose_name=_(u'пользователь'),
                                related_name='user_card', on_delete=models.CASCADE)
    photo = ImageField(verbose_name=_('фото'), upload_to='accounts',
                       default=None, null=True, blank=True)
    date_of_birth = models.DateField(verbose_name=_('дата рождения'),
                                     default=None, null=True, blank=True)
    phone_number = models.BigIntegerField(verbose_name=_('номер телефона'), null=False, blank=False)
    about = models.TextField(verbose_name=_('о пользователе'),
                             default=None, null=True, blank=True)
    hometown = models.CharField(verbose_name=_('город'), max_length=255,
                                default=None, null=True, blank=True)
    status = models.CharField(verbose_name=_('статус'), max_length=30, choices=USER_STATUSES, default='active')

    objects = UserCardManager()

    def __unicode__(self) -> str:
        """
        Unicode representation
        :return: unicode string
        """
        return _('Карта пользователя {}').format(self.user.username)
