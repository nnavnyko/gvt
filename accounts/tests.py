# encoding: utf-8
"""
    Accounts Test
    Test login
"""
from django.contrib.auth import get_user_model
from django.test import TestCase
from django.test.client import Client
from django.urls import reverse

from accounts.enums import EmployeeRoles
from accounts.models.employee_card import EmployeeCard
from accounts.models.user_card import UserCard

SIMPLE_PASSWORD = '12345678*Aa'
SUCCESS_STATUS = 200
REDIRECT_STATUS = 302
PERMISSION_DENIED_STATUS = 403


class AuthTestCase(TestCase):
    """
        Auth Test case
    """

    def test_login(self):
        """
        Test Log-In user
        :return:
        """
        user_model = get_user_model()

        self.user = user_model(username='simple_user', first_name='Alex',
                               last_name='Bolduing', email='admin_user_aa@yopmail.com')
        self.user.set_password(SIMPLE_PASSWORD)
        self.user.save()
        user_card = UserCard(phone_number=444444444)
        user_card.user_id = self.user.id
        user_card.save()

        client = Client()
        response = client.post(reverse('accounts:auth_login'),
                               data={'username': self.user.username, 'password': SIMPLE_PASSWORD})

        # Login success
        self.assertEqual(response.status_code, REDIRECT_STATUS)

    def test_register(self):
        """
        Test Log-In user
        :return:
        """
        user_phone_number = 445556666

        client = Client()
        response = client.post(reverse('accounts:register'),
                               data={'username': 'simple_user_register', 'password1': SIMPLE_PASSWORD,
                                     'first_name': 'FirstName', 'last_name': 'LastName',
                                     'phone_number': user_phone_number, 'tos': True, 'g-recaptcha-response': '',
                                     'email': 'admin_user_aa@yopmail.com', 'password2': SIMPLE_PASSWORD})

        # Login success
        self.assertEqual(response.status_code, REDIRECT_STATUS)

        user = get_user_model().objects.first()
        # Test user is created
        self.assertIsNotNone(user)
        # Test user phone_number
        self.assertEqual(user.user_card.phone_number, user_phone_number)
        # Test user has no employee card after registration
        self.assertIsNone(getattr(user, 'employee_card', None))
        # Test user is not active after registration
        self.assertFalse(user.is_active)


class ManageEmployeesTestCase(TestCase):
    """
    Test employees management
    """

    def setUp(self):
        """
        Initialize test case
        :return: None
        """
        self.simple_user = self._create_user('simple_user', 'some@email.com', 444444444)
        self.manager_user = self._create_user('manager_user', 'some@email.com', 555555555, EmployeeRoles.manager.value)
        self.driver_user = self._create_user('driver_user', 'some@email.com', 555555555, EmployeeRoles.driver.value)
        self.client = Client()

    def login_user(self, user):
        """
        Login user
        :param user:
        :return:
        """
        # Try to get employees as simple user - has to get 403 response
        response = self.client.post(reverse('accounts:auth_login'),
                                    data={'username': user.username, 'password': SIMPLE_PASSWORD})
        # Login success
        self.assertEqual(response.status_code, REDIRECT_STATUS)

    @classmethod
    def _create_user(cls, user_name, email, phone_number, employee_role=None):
        """
        Create user
        :param user_name: string
        :param email: string
        :param phone_number: number
        :param employee_role: string or None
        :return: auth.User instance
        """
        user = get_user_model()(username=user_name, email=email, first_name='FirstName', last_name='LastName')
        user.set_password(SIMPLE_PASSWORD)
        user.save()

        user_card = UserCard(phone_number=phone_number)
        user_card.user_id = user.id
        user_card.save()

        if employee_role:
            employee_card = EmployeeCard(role=employee_role)
            employee_card.user_id = user.id
            employee_card.save()

        return user

    def test_get_employees_list(self):
        """
        Test get employees list permissions
        :return:
        """
        self.login_user(self.simple_user)

        response = self.client.get(reverse('accounts:api:employees_list'))

        self.assertEqual(response.status_code, PERMISSION_DENIED_STATUS)

        self.login_user(self.driver_user)

        response = self.client.get(reverse('accounts:api:employees_list'))

        self.assertEqual(response.status_code, SUCCESS_STATUS)
        self.assertListEqual([self.manager_user.id, self.driver_user.id],
                             [employee['id'] for employee in response.data])

    def test_disable_employee(self):
        """
        Test disable employee account
        Only managers can disable employee
        :return: None
        """

        self.login_user(self.simple_user)

        # Test toggle employee status as simple user - has to get PermissionDenied
        response = self.client.post(reverse('accounts:api:toggle_employee_status',
                                            args=(self.driver_user.username,)))

        self.assertEqual(response.status_code, PERMISSION_DENIED_STATUS)

        self.login_user(self.driver_user)

        # Test toggle employee status as driver user - has to get PermissionDenied
        response = self.client.post(reverse('accounts:api:toggle_employee_status',
                                            args=(self.driver_user.username,)))

        self.assertEqual(response.status_code, PERMISSION_DENIED_STATUS)

        self.login_user(self.manager_user)

        # Login success

        response = self.client.post(reverse('accounts:api:toggle_employee_status',
                                            args=(self.driver_user.username,)))

        self.assertEqual(response.status_code, SUCCESS_STATUS)

    def test_change_employee_role(self):
        """
        Test change employee role
        Only managers can change employee role
        :return: None
        """
        self.login_user(self.simple_user)

        # Test toggle employee status as simple user - has to get PermissionDenied
        response = self.client.post(reverse('accounts:api:change_employee_role',
                                            args=(self.driver_user.username,)), data={'role': EmployeeRoles.manager.value})

        self.assertEqual(response.status_code, PERMISSION_DENIED_STATUS)

        self.login_user(self.driver_user)

        # Test toggle employee status as driver user - has to get PermissionDenied
        response = self.client.post(reverse('accounts:api:change_employee_role',
                                            args=(self.driver_user.username,)), data={'role': EmployeeRoles.manager.value})

        self.assertEqual(response.status_code, PERMISSION_DENIED_STATUS)

        self.login_user(self.manager_user)

        response = self.client.post(reverse('accounts:api:change_employee_role',
                                            args=(self.driver_user.username,)), data={'role': EmployeeRoles.manager.value})

        self.assertEqual(response.status_code, SUCCESS_STATUS)

    def test_set_employee_geolocation(self):
        """
        Test set employee geolocation
        :return: None
        """
        self.login_user(self.simple_user)

        # Test toggle employee status as simple user - has to get PermissionDenied
        response = self.client.post(reverse('accounts:api:set_geolocation'),
                                    data={'longitude': 5.5555555, 'latitude': 7.777666666})

        self.assertEqual(response.status_code, PERMISSION_DENIED_STATUS)
        self.login_user(self.driver_user)

        # Test toggle employee status as driver user - geolocation set successfully
        response = self.client.post(reverse('accounts:api:set_geolocation'),
                                    data={'longitude': 5.5555555, 'latitude': 7.777666666})

        self.assertEqual(response.status_code, SUCCESS_STATUS)

        self.assertEqual(self.driver_user.employee_card.geolocation.latitude, 7.777666666)
        self.assertEqual(self.driver_user.employee_card.geolocation.longitude, 5.5555555)
