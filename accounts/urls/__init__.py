# encoding: utf-8
"""
Accounts Urls
"""
from django.conf.urls import include, url
from .auth import urlpatterns as auth_urls
from .profile import urlpatterns as profile_urls
from accounts.api import urls as api_urls

app_name = 'accounts'

urlpatterns = [
    url(r'^api/', include(api_urls, namespace='api'))
]

urlpatterns += auth_urls
urlpatterns += profile_urls
