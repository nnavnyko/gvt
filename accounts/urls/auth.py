# encoding: utf-8
"""
Auth Urls
"""
from django.conf.urls import include, url
from django.contrib.auth.views import PasswordResetView, PasswordResetDoneView, PasswordResetCompleteView, \
    PasswordResetConfirmView, LoginView
from django.urls import reverse_lazy

from accounts.forms.auth import RegistrationForm, LoginForm
from accounts.views.auth import RegistrationView, ActivationView

app_name = 'accounts_auth'
urlpatterns = [
    url(r'^register/$', RegistrationView.as_view(
        form_class=RegistrationForm), name='register'),
    url(r'^login/$', LoginView.as_view(
        **{'template_name': 'registration/login.html', 'authentication_form': LoginForm}),
        name='auth_login'),
    url(r'^password/reset/$', PasswordResetView.as_view(**{
        'template_name': 'registration/gvt_password_reset_form.html',
        'email_template_name': 'registration/gvt_password_reset_email.html',
        'html_email_template_name': 'registration/gvt_password_reset_email.html',
        'success_url': reverse_lazy('accounts:password_reset_done'),
        'subject_template_name': 'registration/gvt_password_reset_subject.txt'}),
        name='auth_password_reset'),
    url(r'^password/reset/done$', PasswordResetDoneView.as_view(
        **{'template_name': 'registration/gvt_password_reset_done.html'}),
        name='password_reset_done'),
    url(r'^password/reset/complete$', PasswordResetCompleteView.as_view(
        **{'template_name': 'registration/gvt_password_reset_complete.html'}),
        name='password_reset_complete'),
    url(r'^password/reset/confirm/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>.+)/$', PasswordResetConfirmView.as_view(
        **{
            'template_name': 'registration/gvt_password_reset_confirm.html',
            'success_url': reverse_lazy('accounts:password_reset_complete')
        }),
        name='auth_password_reset_confirm'),
    url(r'^activate/(?P<activation_key>\w+)/$',
        ActivationView.as_view(),
        name='registration_activate'),
    url(r'', include('registration.backends.default.urls')),
]
