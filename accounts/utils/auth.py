"""
Auth Accounts utils
"""
from accounts.enums import EMPLOYEE_MANAGER_OR_DRIVER_ROLES
from accounts.models.user import DjangoUser


def is_manager_or_driver(user: DjangoUser) -> bool:
    """
    Is user Manager or Driver
    :param user: User instance
    :return: boolean
    """
    return hasattr(user, 'employee_card') and \
        user.employee_card.role in EMPLOYEE_MANAGER_OR_DRIVER_ROLES
