"""
Employee Geolocation util
---
Manage employee's geolocation and access to geolocation
"""
from typing import List

from transportation.enums import RequestStatuses as TransportationRequestStatuses
from transportation.models.transportation_request import TransportationRequest


class EmployeeGeolocationUtil:
    """
    Employee Geolocation Util
    """

    @classmethod
    def can_get_geolocation(cls, tr_request: TransportationRequest) -> bool:
        """
        Check if the user can get employee geolocation
        Check if Transportation request has status that can give access to employee's geolocation
        :param tr_request: TransportationRequest instance
        :return: boolean
        """
        allowed_statuses: List[str] = [
            TransportationRequestStatuses.on_the_road.value,
            TransportationRequestStatuses.in_progress.value
        ]

        return tr_request.status in allowed_statuses
