# encoding: utf-8
"""
Employees Utility
"""
from accounts.models.employee_card import EmployeeCard
from accounts.models.user import DjangoUser
from accounts.models.user_card import UserCard
from accounts.enums import UserStatuses, EMPLOYEE_ROLES_DICT, EMPLOYEE_MANAGERS_ROLES
from accounts.utils.auth import is_manager_or_driver


class EmployeesUtil:
    """
    EmployeesUtil class
    Change employee's status, permissions
    """

    @classmethod
    def toggle_employee_status(cls, employee: DjangoUser) -> DjangoUser:
        """
        Toggle employee status
        :param employee: auth.User
        :return: changed user
        """
        user_card = employee.user_card
        status = (
            UserStatuses.disabled.value
            if user_card.status == UserStatuses.active.value
            else UserStatuses.active.value
        )

        UserCard.objects.update_model(user_card, status=status)

        return employee

    @classmethod
    def change_employee_role(cls, employee: DjangoUser, employee_role: str) -> DjangoUser:
        """
        Toggle employee status
        :param employee: auth.User
        :param employee_role: string
        :return: changed user
        """
        employee_card = employee.employee_card
        if employee_role in EMPLOYEE_ROLES_DICT:
            EmployeeCard.objects.update_model(employee_card, role=employee_role)
        else:
            raise ValueError(u'Роль не соответствует набору Ролей Сотрудника')

        return employee

    @classmethod
    def is_manager(cls, user: DjangoUser) -> bool:
        """
        Is user Manager
        :param user: User instance
        :return: boolean
        """
        return hasattr(user, 'employee_card') and \
            user.employee_card.role in EMPLOYEE_MANAGERS_ROLES

    @classmethod
    def is_manager_or_driver(cls, user: DjangoUser) -> bool:
        """
        Is user Manager or Driver
        :param user: User instance
        :return: boolean
        """
        return is_manager_or_driver(user)
