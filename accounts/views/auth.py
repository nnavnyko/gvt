# Accounts Auth views
"""
Accounts Auth/Register views
"""
from typing import Tuple

from registration.backends.default.views import (
    ActivationView as DefaultActivationView,
    RegistrationView as BaseRegistrationView
)

from accounts.models.user import DjangoUser


class ActivationView(DefaultActivationView):
    """
    Override activation view
    """

    def get_success_url(self, user: DjangoUser) -> Tuple[str, tuple, dict]:
        """
        Return success url
        :return: tuple
        """

        return 'accounts:registration_activation_complete', (), {}


class RegistrationView(BaseRegistrationView):

    success_url: str = 'accounts:registration_complete'
