"""
Define common admin settings
---
Override Constance template
"""
from django.contrib import admin

from constance.admin import ConstanceAdmin, Config


class ConfigAdmin(ConstanceAdmin):
    change_list_template = 'admin/config/settings.html'


admin.site.unregister([Config])
admin.site.register([Config], ConfigAdmin)
