# encoding: utf-8
"""
Rest Authentication
"""
from rest_framework.authentication import SessionAuthentication as OriginalSessionAuthentication
from rest_framework.request import Request


class SessionAuthentication(OriginalSessionAuthentication):
    """
        Session Authentication
    """

    def enforce_csrf(self, request: Request):
        return
