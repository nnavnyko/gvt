"""
Serializers
---
Common serializers
"""
from typing import TYPE_CHECKING

from rest_framework import serializers

if TYPE_CHECKING:
    from common.custom_types import StatusModel


class StatusFieldSerializer(serializers.ModelSerializer):
    """
    Status Field Serializer
    """

    status_display: serializers.SerializerMethodField = serializers.SerializerMethodField()

    @staticmethod
    def get_status_display(obj: 'StatusModel') -> str:
        """
        Get status displayed value of the Transportation Request
        :param obj: some django Model instance having "status" column
        :return: string
        """
        return obj.get_status_display()


