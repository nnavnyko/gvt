"""
Common apps
"""
from __future__ import unicode_literals
from django.utils.translation import ugettext_lazy as _
from django.apps import AppConfig
import constance.apps


class CommonConfig(AppConfig):
    name = 'common'


class RenameConstanceConfig(constance.apps.ConstanceConfig):
    verbose_name = _('Setup')
    verbose_name_plural = _('Setup')
