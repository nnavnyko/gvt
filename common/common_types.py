"""
Types for type hints
"""
from decimal import Decimal
from typing import Union

Number = Union[Decimal, float, str, int]
