"""
GVT Common DataBase (DB) expressions
---
Describe all custom DB functions here
"""
from typing import TYPE_CHECKING

from django.db.models import Func

if TYPE_CHECKING:
    from django.db.models import Field


class DatePart(Func):
    """
    PostgreSQL date_part
    PSQL example:
    date_part('hour', timestamp '2001-02-16 20:38:40') => 20
    date_part('month', model.created) => 2
    """
    function: str = 'date_part'
    template: str = '%(function)s(\'%(date_part)s\', %(expressions)s)'

    def __init__(self, date_part: str, expression: 'Field'):
        """
        Initialize instance
        :param date_part: string ('month', 'hour', etc.)
        :param expression: date column of the Model
        """
        super(DatePart, self).__init__(expression, date_part=date_part)
