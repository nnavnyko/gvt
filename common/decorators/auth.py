"""
    Auth decorators
"""
# encoding: utf-8
from django.shortcuts import redirect
from django.urls import reverse


def manager_required(permissions=None):
    """
    Manager rights required
    If logged in user is not manager (or transporter) redirect user to Login page
    :param permissions:
    :return:
    """
    def decorator(f):
        def wrapper(request, *args, **kwargs):
            if not request.user.is_authenticated or not (request.user.is_staff or request.user.is_superuser):
                return redirect('{}?next={}'.format(reverse('accounts:auth:auth_login'), request.get_full_path()))

            return f(request, *args, **kwargs)

        return wrapper

    return decorator
