"""
Helpers decorators
"""
import functools
import warnings
from typing import Callable


def fail_silently(f: Callable) -> Callable:
    """
    Fail silently
    :param f: function
    :return: callable
    """
    def wrapper(*args, **kwargs):
        try:
            return f(*args, *kwargs)
        except Exception as e:
            print(e)
            return

    return wrapper


def deprecated(msg: str):
    """
    Deprecated decorator
    :param msg: message to display
    :return: dunction
    """
    def deprecated_decorator(func: Callable) -> Callable:
        """
        This is a decorator which can be used to mark functions
        as deprecated. It will result in a warning being emitted
        when the function is used.
        """
        @functools.wraps(func)
        def new_func(*args, **kwargs):
            warnings.simplefilter('always', DeprecationWarning)  # turn off filter
            warnings.warn(msg or f'Call to deprecated function {func.__name__}',
                          category=DeprecationWarning,
                          stacklevel=2)
            warnings.simplefilter('default', DeprecationWarning)  # reset filter

            return func(*args, **kwargs)

        return new_func

    return deprecated_decorator
