"""
Email sender
"""
from typing import List, Callable
from django.core import validators
from django.core.exceptions import ValidationError
from django.conf import settings
from django.core.mail import send_mail


class EmailSender:
    """
    Email messages sender
    """

    @classmethod
    def send_message(
            cls,
            subject: str,
            message: str,
            recipients: List[str],
            email_from: str = settings.SERVER_EMAIL,
            onerror: Callable = None
    ) -> None:
        """
        Send Email message
        :param subject: string (Message subject)
        :param message: string (Message body)
        :param recipients: list of strings (list of emails)
        :param email_from: list of strings (list of emails)
        :param onerror: callback on error
        :return: None
        """
        recipients = [r for r in recipients if cls._validate_recipient_email(r)]
        if not cls._validate_email_data(message, recipients) or not settings.SEND_EMAILS:
            return

        try:
            send_mail(subject, message, email_from, recipients, html_message=message)
        except Exception as e:
            print(e)

            if onerror:
                onerror()

    @staticmethod
    def _validate_recipient_email(recipient: str) -> bool:
        """
        Validate recipient email
        :param recipient: string (Email)
        :return: boolean
        """
        try:
            validators.validate_email(recipient)
            return True
        except ValidationError as e:
            print(e)
            return False

    @staticmethod
    def _validate_email_data(message: str, recipients: List[str]) -> bool:
        """
        Validate Email data
        :param message: message
        :param recipients: recipients
        :return: None
        """
        if not recipients:
            print('No recipients found')
            return False

        if not message:
            print('Message body is empty')
            return False

        return True
