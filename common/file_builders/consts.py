# encoding: utf-8
"""
File Builders constants
---
Declare Common file builders constants
"""
from django.utils.translation import ugettext_lazy as _


URL_LABEL_FIELD_POSTFIX = '_url'

DEFAULT_URL_LABEL = _(u'Ссылка')

BELARUS_ROUBLE_NUM_FORMAT = '#,##0 BYN'
