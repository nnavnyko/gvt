"""
File Builders Enums
"""
from enum import Enum


class ValuesTypes(Enum):
    """
    Values Types Enum
    """

    STRING_TYPE = 'string'
    DATE_TYPE = 'date'
    NUMBER_TYPE = 'number'
    MONEY_TYPE = 'money'
    URL_TYPE = 'url'
