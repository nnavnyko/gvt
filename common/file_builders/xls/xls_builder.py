"""
XLS file Builder
---
Build Microsoft Excel file having specific structure
Provide xls file builder.
Provide methods to edit xls file
"""
from datetime import datetime
from decimal import Decimal
from io import BytesIO
from typing import TYPE_CHECKING, List, Union, Dict, Callable

import xlsxwriter
from django.http.response import HttpResponse
from django.utils import formats
from django.utils.translation import ugettext_lazy as _

from common.file_builders.consts import URL_LABEL_FIELD_POSTFIX, \
    BELARUS_ROUBLE_NUM_FORMAT, DEFAULT_URL_LABEL
from common.file_builders.enums import ValuesTypes

if TYPE_CHECKING:
    from xlsxwriter.worksheet import Worksheet

XLS_FILE_EXTENSION: str = '.xlsx'
EXCEL_FILE_CONTENT_TYPE: str = 'application/vnd.ms-excel'
DEFAULT_FILE_NAME = _(u'Отчет.xlsx')


class XlsBuilder(object):
    """
    Xls file Builder
    """
    output: BytesIO
    filename: str
    workbook: xlsxwriter.Workbook
    worksheet: 'Worksheet'
    columns: List[dict]
    values_writers: Dict[str, Callable]
    values_formats: dict
    closed: bool

    def __init__(self, filename: str, columns: List[dict]):
        """
        Initialize builder
        :param filename: string
        :param columns: list of dicts ([{'header': 'Date', 'field_name': 'date', 'field_type': 'date'}]
        """
        self.output = BytesIO()

        self.filename = filename and \
            ''.join([filename, XLS_FILE_EXTENSION]) or DEFAULT_FILE_NAME
        # Initialize Excel workbook and worksheet
        self.workbook = xlsxwriter.Workbook(self.output, {'in_memory': True, 'remove_timezone': True})
        self.worksheet = self.workbook.add_worksheet()
        # Initialize default Excelt formats
        self.bold_format = self.workbook.add_format({'bold': True})

        self.columns = columns

        self.values_writers = {
            ValuesTypes.STRING_TYPE: self.worksheet.write_string,
            ValuesTypes.DATE_TYPE: self.worksheet.write_datetime,
            ValuesTypes.NUMBER_TYPE: self.worksheet.write_number,
            ValuesTypes.MONEY_TYPE: self.worksheet.write_number,
            ValuesTypes.URL_TYPE: self.worksheet.write_url,
            'default': self.worksheet.write
        }

        self.values_formats = {
            ValuesTypes.MONEY_TYPE: self.workbook.add_format({'num_format': BELARUS_ROUBLE_NUM_FORMAT}),
            ValuesTypes.DATE_TYPE: self.workbook.add_format({'num_format': 'dd/mm/yy'})
        }

        self.__write_headers()

        self.closed = False

    def __write_headers(self):
        """
        Write headers
        :return: None
        """
        headers_row: int = 0
        headers_col: int = 0

        for column in self.columns:
            self.write(headers_row, headers_col, column['header'], cell_format=self.bold_format)
            headers_col += 1

    def write_row(self, row: int, row_dict: dict) -> None:
        """
        Write row dict having the same structure as columns described,
        e.g. if columns is:
            [
                {'header': 'Date', 'field_name': 'date', 'field_type': 'date'},
                {'header': 'Name', 'field_name': 'name', 'field_type': 'string'},
                {'header': 'URL', 'field_name': 'url', 'field_type': 'url'},
            ]
        the row_dict must be:
            {'date': datetime(2017, 05, 01), 'name': 'Joe Smith', 'url': 'https://google.com', 'url_string': 'Link'}

        for field type URL need to have url value (e.g. 'value': '', 'value_url': 'https://google.com')
        :param row: number (index of row_dict in list)
        :param row_dict: dict
        :return: None
        """
        col: int = 0

        for column in self.columns:
            cell_format = self.values_formats.get(column['field_type'])

            if column['field_type'] != ValuesTypes.URL_TYPE:
                self.write(row, col, row_dict.get(column['field_name']),
                           cell_format=cell_format, value_type=column['field_type'])
            else:
                url: str = row_dict.get(
                    ''.join([column['field_name'], URL_LABEL_FIELD_POSTFIX]))

                self.write(row, col, url,
                           cell_format=cell_format, value_type=column['field_type'],
                           string=row_dict.get(column['field_name'], DEFAULT_URL_LABEL))
            col += 1

    def write(
            self,
            row: int,
            col: int,
            value: Union[str, int, float, datetime, Decimal, None],
            cell_format: str = None,
            string: str = None,
            value_type: Union[ValuesTypes, str] = ValuesTypes.STRING_TYPE
    ):
        """
        Write value into xls file cell
        :param row: int (number of row)
        :param col: int (number of column)
        :param value: value to write
        :param cell_format: workbook format or None
        :param string: string or None (string representation of URL)
        :param value_type: string
        :return: None
        """
        value_writer = self.values_writers.get(value_type) or self.values_writers['default']

        if isinstance(string, datetime):
            string = formats.date_format(string, "SHORT_DATETIME_FORMAT")

        if value is None:
            value_writer = self.values_writers['default']

        if value_type != ValuesTypes.URL_TYPE:
            value_writer(row, col, value, cell_format)
        else:
            value_writer(row, col, str(value), cell_format, string=str(string))

    def close(self):
        """
        Close worksheet and return file
        :return: tuple of xls file and output thread
        """
        self.workbook.close()
        self.output.seek(0)

        self.closed = True

        return self.workbook, self.output

    def make_http_response(self) -> HttpResponse:
        """
        Return http response for file
        :return: HttpResponse
        """
        if not self.closed:
            self.close()

        response: HttpResponse = HttpResponse(self.output.read(), content_type=EXCEL_FILE_CONTENT_TYPE)
        response['Content-Disposition'] = 'attachment; filename="{}"'.format(self.filename)

        return response

