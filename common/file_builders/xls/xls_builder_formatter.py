"""
Xls Builder Formatter
"""
from common.file_builders.enums import ValuesTypes


class XlsBuilderFormatter(object):
    """
    Xls Builder Formatter class
    """

    @staticmethod
    def get_column(header_string: str, field_name: str, field_type: str = ValuesTypes.STRING_TYPE) -> dict:
        """
        Get xls column dict
        :param header_string: string
        :param field_name: string (dict key)
        :param field_type: ValuesTypes value
        :return: dict
        """
        return {
            'header': header_string,
            'field_name': field_name,
            'field_type': field_type
        }

