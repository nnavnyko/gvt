# encoding: utf-8
"""
Common Forms Fields
"""
from typing import Union

from django.forms import Field

from common.utilities.common import parse_phone_number


class PhoneField(Field):
    """
    Phone Field
    """

    def to_python(self, value: str) -> Union[int, None]:
        """
        Convert to python value
        :param value: string
        :return: int
        """
        return int(parse_phone_number(str(value))) if value else None
