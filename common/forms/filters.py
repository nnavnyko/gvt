"""
GVT Common filters
"""
from typing import Union

from django_filters import Filter
from django import forms


class CheckboxInput(forms.CheckboxInput):
    """ Checkbox input widget """
    def value_from_datadict(self, data: dict, files: list, name: str) -> Union[bool, None]:
        """
        Override Get value from data dict for filtration
        :param data: dict
        :param files: list of files
        :param name: name of field
        :return: bool or None
        """
        if name not in data:
            # A missing value means False because HTML form submission does not
            # send results for unselected checkboxes.
            return
        value = data.get(name)

        # Translate true and false strings to boolean values.
        values = {'true': True, 'false': False}
        if isinstance(value, str):
            value = values.get(value.lower(), value)

        if value is None or value == '':
            return

        return bool(value)


class BooleanField(forms.BooleanField):
    """ Boolean Form field """
    widget = CheckboxInput

    def clean(self, value: Union[str, None]):
        """
        Validate the given value and return its "cleaned" value as an
        appropriate Python object. Raise ValidationError for any errors.
        """
        if value is None or value == '':
            return None

        return super().clean(value)


class BooleanFilter(Filter):
    """ Boolean field filter """
    field_class = BooleanField
