# encoding: utf-8
"""
Utils forms
"""
import json
from typing import Callable

import requests
from django.conf import settings
from django.forms import Form
from django.forms.widgets import CheckboxInput

from common.forms.types import BaseFormMixin


class BootstrapControlsForm(Form):
    """ BootstrapFormControlsForm """

    def __init__(self, *args, **kwargs):
        """
            add 'form-control' class to all inputs
        """
        super(BootstrapControlsForm, self).__init__(*args, **kwargs)
        for field in [
            field for field in self.fields
            if not isinstance(self.fields[field].widget, CheckboxInput) and
            not self.fields[field].widget == CheckboxInput
        ]:

            if not self.fields[field].widget.attrs.get('class'):
                self.fields[field].widget.attrs['class'] = ''

            self.fields[field].widget.attrs['class'] += ' form-control'
            self.fields[field].widget.attrs['placeholder'] = self.fields[field].widget.attrs.get('placeholder') or \
                str(self.fields[field].label).capitalize()


class GoogleReCaptchaForm(BaseFormMixin):
    """
        Google Re-Captcha form
        Must be inherited by django.forms.Form
    """
    is_valid: Callable
    add_error: Callable
    data: dict

    gcaptcha_form_field: str = 'g-recaptcha-response'

    def is_valid(self) -> bool:
        """
        Check if form is valid
        Add google recaptcha
        :return:
        """
        valid: bool = super(GoogleReCaptchaForm, self).is_valid()
        if valid:
            context: dict = {
                'secret': settings.RECAPTCHA_PRIVATE_KEY,
                'response': self.data[self.gcaptcha_form_field]
            }

            response: dict = json.loads(
                requests.post(settings.GOOGLE_CAPTCHA_URL, data=context, allow_redirects=True).text
            )

            if not response['success']:
                valid = False
                self.add_error(None, u'Google re-captcha is not valid!')

        return valid
