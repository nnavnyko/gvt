"""
Common form types - type hints
"""
from typing import Callable


class BaseFormMixin:
    """ Base form mixin to define common methods """
    is_valid: Callable
