"""
Base Model manager
"""
from typing import Any, Union

from django.db import models
from django.http import Http404


class BaseManager(models.Manager):
    """ Base model manager - subclass of Django Model Manager """

    def get_or_404(self, **kwargs) -> models.Model:
        """
        Get instance or raise 404 exception, if instance is not found
        :param kwargs: key-word arguments
        :return: instance of self.model
        :raise:
        """
        instance = self.get_or_none(**kwargs)

        if instance is None:
            raise Http404('Organisation with this UUID doesn\'t exist!')

        return instance

    def get_by_uuid(self, uuid_: str) -> models.Model:
        """
        Filter by key-word arguments
        :param uuid_: string
        :return: result
        """
        return self.get_or_none(uuid=uuid_)

    @staticmethod
    def update_model(instance: models.Model, commit: bool = True, **kwargs) -> models.Model:
        """
        Update model and commit to db
        :param instance: instance of Model
        :param commit: boolean (False if need to prevent of save model)
        :param kwargs: data to set to model
        :return: updated instance
        """
        key: str
        value: Any

        for key, value in kwargs.items():
            if hasattr(instance, key):
                setattr(instance, key, value)

        if commit:
            instance.save()

        return instance

    def get_or_none(self, **kwargs) -> Union[models.Model, None]:
        """
        Find first instance by key-word arguments
        :param kwargs: key-word arguments
        :return: result
        """
        return self.filter(**kwargs).first()
