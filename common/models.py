# encoding: utf-8
"""
Common models
"""
from __future__ import unicode_literals
from django.utils.translation import ugettext_lazy as _
from django.db import models
import uuid


class UUIDModel(models.Model):
    """
        UUID model
        Append uuid field to the table
    """
    class Meta:
        """ Meta class """
        abstract = True

    uuid = models.UUIDField(
        verbose_name=_(u'уникальный идентификатор объекта'),
        default=uuid.uuid4,
        editable=False,
        unique=True
    )
