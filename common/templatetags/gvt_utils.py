# encoding: utf-8
"""
GVT Utils tags
"""
from typing import Any

from django import template

from common.common_types import Number

register = template.Library()


@register.filter()
def js_float(value: Number) -> str:
    """
    Convert value to JavaScript float
    :param value: string
    :return: str
    """
    try:
        value = str(float(value))
    except (TypeError, ValueError):
        value = '0'

    value = value.replace(',', '.')

    return value


@register.filter()
def get_dict_value(dict_: dict, key: str) -> Any:
    """
    Get dict value filter
    :param dict_: dictionary
    :param key: string
    :return: any value from dict
    """
    return dict_.get(key)
