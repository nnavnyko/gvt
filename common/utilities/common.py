# encoding: utf-8
"""
Common utils
"""
import re
from typing import Union

PHONE_REPLACE_REGEXP: str = r'[\s\\(\\)\\-]*'
PHONE_REPLACE_TO: str = ''


def parse_int(value: Union[int, str], default: int = 0) -> int:
    """
    Parse string to int. If Exception catch, use default value
    :param value: string (or number value)
    :param default:
    :return:
    """
    try:
        value = int(value)
    except (TypeError, ValueError):
        value = parse_int(default)

    return value


def parse_phone_number(phone_number: str) -> str:
    """
    Parse phone number
    :param phone_number: string
    :return: string
    """
    return re.sub(PHONE_REPLACE_REGEXP, PHONE_REPLACE_TO, phone_number)


def normalize_filename(value: str) -> str:
    """
    Replace invalid file characters with underscore
    :param value: string
    :return: normalized string
    """
    return re.sub(r'[^\w\-]', '_', value)
