/**
 * Accounts Router
 */

import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { EditAccountComponent } from './edit-account/edit-account.component';
import { EmployeeListComponent } from './employee-list/employee-list.component';
import { EmployeeProfileComponent } from './employee-profile/employee-profile.component';
import {MyAccountComponent} from "./my-account/my-account.component";

const routes: Routes = [
    {
        path: 'accounts',
        children: [
            { path: 'profile', component: MyAccountComponent },
            { path: 'profile/edit', component: EditAccountComponent },
            {
                path: 'employees',
                children: [
                    { path: 'list', component: EmployeeListComponent },
                    { path: ':username', component: EmployeeProfileComponent },
                ]
            }
        ]
    }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AccountsRoutingModule {}
