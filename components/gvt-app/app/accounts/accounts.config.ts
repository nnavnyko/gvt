/**
 * Account Config (Constants)
 **/
import { Config } from '../settings/config';

/**
 * Accounts Common Config
 */
export class AccountsConfig {
    public static BASIC_API_URL = '/accounts/api/';
    public static BASIC_MODULE_URL = Config.APP_URL + 'accounts/';
}

/**
 * Accounts API URLs Config
 */
export class AccountsAPIURLS {
    public static GET_JSON = AccountsConfig.BASIC_API_URL + 'json';
    public static UPDATE_USER = AccountsConfig.BASIC_API_URL + 'update/';
    public static UPDATE_USER_PHOTO = AccountsConfig.BASIC_API_URL + 'update-photo/';
    public static EMPLOYEES_LIST = AccountsConfig.BASIC_API_URL + 'employees/list';
    public static EMPLOYEES_FULL_LIST = AccountsConfig.BASIC_API_URL + 'employees/list/full';
    public static EMPLOYEE_PROFILE = AccountsConfig.BASIC_API_URL + 'employees/{username}';
    public static TOGGLE_EMPLOYEE_STATUS = AccountsConfig.BASIC_API_URL + 'employees/{username}/toggle-status';
    public static CHANGE_EMPLOYEE_ROLE = AccountsConfig.BASIC_API_URL + 'employees/{username}/change-role';
}

export class UserStatuses {
    public static ACTIVE = 'active';
    public static DISABLED = 'disabled';
}
