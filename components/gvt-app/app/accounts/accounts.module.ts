/**
 * Accounts Module
 * Manage account
 * See other users accounts
 */
import { NgModule } from '@angular/core';
import { TextMaskModule } from 'angular2-text-mask';
import { FileUploadModule } from 'ng2-file-upload';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { EmployeeProfileComponent } from './employee-profile/employee-profile.component';
import { AccountsRoutingModule } from './accounts-router.module';
import { EditAccountComponent } from './edit-account/edit-account.component';
import { EmployeeListComponent } from './employee-list/employee-list.component';
import { SharedModule } from '../shared.module';
import {MyAccountComponent} from "./my-account/my-account.component";

@NgModule({
    imports: [
        TextMaskModule, AccountsRoutingModule, FileUploadModule,
        SharedModule, BrowserAnimationsModule
    ],
    declarations: [EditAccountComponent, EmployeeListComponent, EmployeeProfileComponent, MyAccountComponent]
})
export class AccountsModule{}
