/**
 * Edit Account Component
 * View and Edit account's information
 */
import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Title } from '@angular/platform-browser';
import { FileUploader } from 'ng2-file-upload';
import { TranslateService } from '@ngx-translate/core';

import { AccountsConfig, AccountsAPIURLS } from '../accounts.config';
import { AccountService } from '../shared/services/account.service';
import { CommonConstants } from '../../utils/constants/common';
import { Router } from "@angular/router";

@Component({
    selector: '<edit-account></edit-account>',
    templateUrl: AccountsConfig.BASIC_MODULE_URL + 'edit-account/edit-account.html'
})
export class EditAccountComponent implements OnInit {
    private userEditObj: any;
    private formSubmitted: boolean;
    private userForm: NgForm;
    private phoneMask: any[] = CommonConstants.PHONE_MASK;
    private uploader: FileUploader = new FileUploader({url: AccountsAPIURLS.UPDATE_USER_PHOTO});

    constructor(public account: AccountService, private title: Title, private translate: TranslateService, private router: Router) {
        /**
         * Constructor
         */
        this.translate.get('Редактирование Профиля').subscribe((res:string) => {
            this.title.setTitle(res);
        });

        this.uploader.onCompleteItem = (item:any, response:any, status:any, headers:any):void => {
            try {
                this.userEditObj.userCard.photo = JSON.parse(response).photo_url;
                this.account.user.userCard.photo = JSON.parse(response).photo_url;
            } catch(err) {
                console.dir(err);
            }
        }
    }

    ngOnInit(): void {
        /**
         * Initialize Component
         * Copy account.user object to edit
         */
        let $this: any = this;
        this.account.onUserLoaded(() => {
            $this.userEditObj = $this.account.getUserCopy();
        });
    }

    public submitForm(): any{
        /**
         * Submit Form update
         */
        let this_ = this;
        return () => {
            this_.formSubmitted = true;
            this_.account.saveUser(this_.userEditObj)
                .then((args?: any) => this_.handleSuccessOnSave(args), (errors?: any) => this_.handleErrorOnSave(errors));
        }
    }
    
    private handleSuccessOnSave(args?: any) {
        /**
         * Handle succes on submit form
         * Update user Edit Object
         */
        this.formSubmitted = false;
        this.userEditObj = this.account.getUserCopy();
        this.router.navigate(['/accounts/profile/']);
    }

    private handleErrorOnSave(errors?: any) {
        /**
         * Handle errors on submit form
         * Show error messages
         */
        this.formSubmitted = false;

        console.dir(errors);
    }
}
