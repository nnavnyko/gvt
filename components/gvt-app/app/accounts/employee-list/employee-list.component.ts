/**
 * Employees List Component
 */

import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { Title } from '@angular/platform-browser';
import * as _ from 'lodash';
import { PaginationConfig } from 'ngx-bootstrap/pagination';

import { EmployeesService } from '../shared/services/employees.service';
import { AccountService } from '../shared/services/account.service';
import { SimpleUser } from '../shared/models/simple-user.model';
import { AccountsConfig } from '../accounts.config';
import { Config } from '../../settings/config';

@Component({
    selector: '<employees-list></employees-list>',    
    templateUrl: AccountsConfig.BASIC_MODULE_URL + 'employee-list/employee-list.html',
    providers: [AccountService, EmployeesService, PaginationConfig]
})
export class EmployeeListComponent implements OnInit {
    /**
     * Employees List Component Class
     */
    private totalItems: number;
    private managersRoles: string[] = ['driver', 'admin', 'manager'];
    private userStatuses: string[] = ['active', 'disabled'];
    private currentPage: number;
    private employeeList: SimpleUser[];
    private DEFAULT_AVATAR_IMG = Config.DEFAULT_AVATAR_IMG;
    private employeeRoles: object[] = [
        {'value': '', 'display': 'Выбрать Роль'},
        {'value': 'porter', 'display': 'Грузчик'},
        {'value': 'driver', 'display': 'Водитель'},
        {'value': 'manager', 'display': 'Менеджер'},
        {'value': 'admin', 'display': 'Администратор'},
        {'value': 'undefined', 'display': 'Роль не назначена'}
    ];

    private filterSearch: string = '';
    private filterRole: string = '';
    private filterParams: string = '';
    private loadingComplete: boolean = false;

    constructor(private account: AccountService, private employeesService: EmployeesService,
                private title: Title, private translate: TranslateService) {
        /** Constructor **/
        this.currentPage = 1;
        this.translate.get('Сотрудники').subscribe((res:string) => {
            this.title.setTitle(res);
        });
    }

    ngOnInit(): void {
        /**
         * Load list on init
         */
        this.loadList(this.currentPage);
        _.each(this.employeeRoles, (role:any) => {
            this.translate.get(role['display']).subscribe((res:string) => {
                role['display'] = res;
            });
        });
    }

    pageChanged(event: any): void {
        /**
         * Handle change page
         */
        this.loadList(event.page);
    }

    loadList(page: number): void {
        /**
         * Load paged and filtered Employees list
         */
        
        this.employeesService.getFullEmployeeList('?page=' + page + '&' + this.filterParams).then(data => {
            this.employeeList = data.list;
            this.totalItems = data.totalItems;
            this.loadingComplete = true;
        });
    }

    filterByRole(role: string): void {
        /**
         * Filter By Employee Role
         */
        this.setFilterParams(role, this.filterSearch);

        this.applyFiltration();
    }

    filterBySearch():void {
        /**
         * Filter by search string
         * Search in phone and email fields
         */
        this.setFilterParams(this.filterRole, this.filterSearch);

        this.applyFiltration();
    }

    setFilterParams(filterRole: string, filterSearch: string): void {
        /**
         * Set filtration
         */
        let filterParams = '';
        let filterArr:any[] = [
            {key: 'employee_card__role', value: filterRole},
            {key: 'search', value: filterSearch},
        ];
        _.each(filterArr, filterParam => {
            if (filterParam.value && filterParam.value !== '') {
                filterParams += '&' + filterParam.key + '=' + filterParam.value;
            }
        });
        this.filterParams = filterParams;
    }

    applyFiltration(): void {
        /**
         * Update list after filtration applied
         */
        this.currentPage = 1;

        this.loadList(1);
    }

}
