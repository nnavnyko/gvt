/**
 * Employee Account Component
 */


import {switchMap} from 'rxjs/operators';
import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Params }   from '@angular/router';

import { Title } from '@angular/platform-browser';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { TranslateService } from '@ngx-translate/core';
import * as _ from 'lodash';

import { User } from '../shared/models/user.model';
import { AccountService } from '../shared/services/account.service';
import { EmployeesService } from '../shared/services/employees.service';
import { AccountsConfig, UserStatuses } from '../accounts.config';
import { Config } from '../../settings/config';
import {TransportationRequestsService} from "../../transportation/shared/services/transportation-requests.service";


@Component({
    selector: '<employee-profile></employee-profile>',
    templateUrl: AccountsConfig.BASIC_MODULE_URL + 'employee-profile/employee-profile.html',
    providers: [EmployeesService, TransportationRequestsService]
})
export class EmployeeProfileComponent implements OnInit{
    /**
     * Employee Profile Component
     */
    private employee: User;
    private managersRoles: string[] = ['driver', 'admin', 'manager'];
    private userStatuses: string[] = [UserStatuses.ACTIVE, UserStatuses.DISABLED];
    UserStatuses: any = UserStatuses;
    private DEFAULT_AVATAR_IMG = Config.DEFAULT_AVATAR_IMG;
    private employeeUpdating: boolean = false;
    @ViewChild('changeEmployeeRoleModalInstance', {static: false}) public changeEmployeeRoleModalInstance: ModalDirective;
    private employeeRoles: object[] = [
        {'value': 'porter', 'display': 'Грузчик'},
        {'value': 'driver', 'display': 'Водитель'},
        {'value': 'manager', 'display': 'Менеджер'},
        {'value': 'admin', 'display': 'Администратор'},
        {'value': 'undefined', 'display': 'Роль не назначена'}
    ];

    private currentEmployeeRole: string;
    private loadingComplete: boolean = false;
    private employeeStatistics: object[];

    constructor(private account: AccountService, private employeesService: EmployeesService,
                private title: Title, private route: ActivatedRoute, private translate: TranslateService,
                private trRequests: TransportationRequestsService) {
        this.translate.get('Профиль Сотрудника').subscribe((res:string) => {
            this.title.setTitle(res);
        });
    }
    
    ngOnInit():void {
        /**
         * Initialize Component
         * Load Employee Profile
         */
        this.route.params.pipe(
            switchMap((params: Params) => this.employeesService.getEmployeeProfile(params['username']))).subscribe(data => {
                this.employee = data;
                this.currentEmployeeRole = this.employee.employeeCard.role;
                this.loadingComplete = true;

                this.trRequests.getEmployeeStatistics(this.employee.username).then((data:any) => {
                    this.employeeStatistics = data;
                });
            });

        _.each(this.employeeRoles, (role:any) => {
            this.translate.get(role['display']).subscribe((res:string) => {
                role['display'] = res;
            });
        });
    }

    private toggleEmployeeStatus(): any {
        /**
         * Toggle employee Status
         */
        return () => {
            this.employeeUpdating = true;
            let userStatus: string = this.employee.userCard.status === UserStatuses.ACTIVE ?
                UserStatuses.DISABLED : UserStatuses.ACTIVE;
            this.employeesService.toggleEmployeeStatus(this.employee.username, userStatus).then((data:any) => {
                this.employee.userCard.status = data.status;
                this.employee.userCard.statusDisplay = data.get_status_display;
                this.employeeUpdating = false;
            }, (err:any) => {
                console.dir(err);
                this.employeeUpdating = false;
            });
        };
    }

    private changeEmployeeRole():any {
        /**
         * Change Employee Role
         */
        return () => {
            this.employeeUpdating = true;
            this.employeesService.changeEmployeeRole(this.employee.username, this.currentEmployeeRole).then((data:any) => {
                this.employee.employeeCard.role = data.role;
                this.employee.employeeCard.roleDisplay = data.get_role_display;
                this.employeeUpdating = false;
                this.changeEmployeeRoleModalInstance.hide();
            }, (err:any) => {
                console.dir(err);
                this.employeeUpdating = false;
            });
        };
    }
}
