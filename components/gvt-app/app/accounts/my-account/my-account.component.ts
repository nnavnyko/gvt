/**
 * My Account component
 * Show account of the current logged-in user
 **/

import {Component, OnInit, AfterViewInit} from '@angular/core';
import {TranslateService} from "@ngx-translate/core";
import {Title} from "@angular/platform-browser";

import {AccountsConfig} from "../accounts.config";
import {AccountService} from "../shared/services/account.service";
import {Config} from "../../settings/config";
import {TransportationRequestsService} from "../../transportation/shared/services/transportation-requests.service";

@Component({
    selector: 'my-account',
    templateUrl: AccountsConfig.BASIC_MODULE_URL + 'my-account/my-account.html',
    providers: [TransportationRequestsService]
})
export class MyAccountComponent implements OnInit {
    private DEFAULT_AVATAR_IMG = Config.DEFAULT_AVATAR_IMG;
    private needFillEmployeeStats: boolean = false;

    private userStatistics: object[];
    private employeeStatistics: object[];

    constructor(public account: AccountService, private title: Title,
                private translate: TranslateService, private trRequests: TransportationRequestsService) {
        /**
         * Constructor
         */
        this.translate.get('Аккаунт').subscribe((res: string) => {
            this.title.setTitle(res);
        });
    }

    ngOnInit(): void {
        this.trRequests.getPersonStatistics().then((data:any) => {
            this.userStatistics = data;
        });

        let $this: any = this;
        this.account.onUserLoaded(fillEmployeeStats);

        function fillEmployeeStats():void {
            if ($this.account.isManagerOrDriver()) {
                $this.trRequests.getEmployeeStatistics().then((data:any) => {
                    $this.employeeStatistics = data;
                });
            }
        }

    }

}
