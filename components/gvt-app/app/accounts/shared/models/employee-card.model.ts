/**
 * Employee Card Model
 */
export class EmployeeCard{
    /**
     * Employee Card Class
     */
    public role: string;
    public roleDisplay: string;

    constructor(obj?: any){
        /**
         * Create employee card from object or return empty instance
         */
        if (obj) {
            this.role = obj.role;
            this.roleDisplay = obj.get_role_display;
        }
    }
}