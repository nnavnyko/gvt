/**
 * Simple User model
 */
import { EmployeeCard } from './employee-card.model';
import { UserCard } from './user-card.model';

export class SimpleUser {
    /**
     * Simple User Model class
     */
    public id: number;
    public username: string;
    public email: string;
    public firstName: string;
    public lastName: string;
    public employeeCard: EmployeeCard;
    public userCard: UserCard;

    constructor(obj: any){
        /**
         * Model constructor
         * @param obj: object instance, e.g. {first_name: 'FirstName', last_name: 'LastName', email: 'some@example.com'}
         * @param userCard: UserCard instance
         * @param employeeCard: EmployeeCard instance (optional)
         */
        this.id = obj.id;
        this.username = obj.username;
        this.email = obj.email;
        this.firstName = obj.first_name;
        this.lastName = obj.last_name;
        this.userCard = obj.user_card ? new UserCard(obj.user_card) : null;
        this.employeeCard = obj.employee_card ? new EmployeeCard(obj.employee_card) : null;
    }
}
