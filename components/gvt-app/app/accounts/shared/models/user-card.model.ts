/**
 * User Card Model
 */
import * as moment from 'moment';

export class UserCard {
    /**
     * User Card Class
     */
    public status: string;
    public statusDisplay: string;
    public phoneNumber: string;
    public about: string;
    public photo: string;
    public hometown: string;
    public dateOfBirth: string;

    constructor(obj?: any){
        /**
         * Create user card from object or return empty instance
         */
        if (obj) {
            this.status = obj.status;
            this.statusDisplay = obj.get_status_display;
            this.phoneNumber = obj.phone_number;
            this.about = obj.about;
            this.photo = obj.photo;
            this.dateOfBirth = obj.date_of_birth;
            this.hometown = obj.hometown;
        }
    }
}
