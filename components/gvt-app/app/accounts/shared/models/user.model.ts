/**
 * User Model
 * Describe account model with user data
 */
import { EmployeeCard } from './employee-card.model';
import { UserCard } from './user-card.model';

export class User {
    /**
     * User model
     */
    constructor(
        public id: number,
        public email: string,
        public firstName: string,
        public lastName: string,
        public username: string,
        public isSuperuser: boolean,
        public isStaff: boolean,
        public dateJoined: string,
        public employeeCard: EmployeeCard,
        public userCard: UserCard
    ){}
}