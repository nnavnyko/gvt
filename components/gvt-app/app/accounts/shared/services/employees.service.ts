/**
 * Employees users service
 * Manage employees
 */

import { Injectable } from '@angular/core';
import { HTTPRequestsService } from '../../../utils/services/http-requests/http-requests.service';

import { AccountsAPIURLS } from '../../accounts.config';
import { SimpleEmployeeUser } from '../models/simple-employee-user.model';
import { SimpleUser } from '../models/simple-user.model';
import { User } from '../models/user.model';
import { EmployeeCard } from '../models/employee-card.model';
import { UserCard } from '../models/user-card.model';
import * as _ from 'lodash';
import { NotificationsService } from '../../../utils/services/notifications/notifications.service';


@Injectable()
export class EmployeesService {
    /**
     * Employee Service class
     */

    constructor(private http: HTTPRequestsService, private notifications: NotificationsService){}

    public getEmployeeList(): Promise<any> {
        /**
         * Get employees list
         * The current user (account.user) must have permissions to read this list
         * (must be a manager, an admin or a driver)
         */
        return this.http.get(AccountsAPIURLS.EMPLOYEES_LIST).then((data:any) => {
            let employeesList: SimpleEmployeeUser[] = [];

            _.each(data, employee => {
                employeesList.push(new SimpleEmployeeUser(employee));
            });

            return employeesList;
        });
    }

    public getFullEmployeeList(filterData?: string): Promise<any> {
        /**
         * Get employees list
         * The current user (account.user) must have permissions to read this list
         * (must be a manager, an admin or a driver)
         */
        return this.http.get(AccountsAPIURLS.EMPLOYEES_FULL_LIST + filterData).then((data:any) => {
            let employeeList: SimpleUser[] = [];

            _.each(data.results, employee => {
                employeeList.push(new SimpleUser(employee));
            });

            return {
                list: employeeList,
                totalItems: data.count
            };
        });
    }
    
    public getEmployeeProfile(username: string): Promise<any> {
        /**
         * Get employee profile data
         * @param username: string
         */
        return this.http.get(AccountsAPIURLS.EMPLOYEE_PROFILE.replace('{username}', username)).then((data:any) => {
            let user:User = new User(
                data.id,
                data.email,
                data.first_name,
                data.last_name,
                data.username,
                data.is_superuser,
                data.is_staff,
                data.date_joined,
                new EmployeeCard(data.employee_card),
                new UserCard(data.user_card)
            );

            return user;
        });
    }

    public toggleEmployeeStatus(username: string, status?: string): Promise<any> {
        /**
         * Toggle Employee Status
         * Activate or Deactivate user's account
         * @param username: string
         * @param status: string or undefined
         */
        return this.http.post(AccountsAPIURLS.TOGGLE_EMPLOYEE_STATUS.replace('{username}', username), 
            {'status': status}).then((data: any) => {
            this.notifications.showSuccessMessage('Статус сотрудника успешно изменен',
                'Вы успешно изменили статус сотрудника');
            return data;
        });
    }

    public changeEmployeeRole(username: string, role: string): Promise<any> {
        /**
         * Change Employee Role
         * @param username: string
         * @param role: string
         */
        return this.http.post(AccountsAPIURLS.CHANGE_EMPLOYEE_ROLE.replace('{username}', username),
            {'role': role}).then((data: any) => {
            this.notifications.showSuccessMessage('Роль сотрудника успешно изменена',
                'Вы успешно изменили роль сотрудника');
            return data;
        });
    }
}
