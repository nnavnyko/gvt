/**
 * Main App component
 **/
import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { $WebSocket } from 'angular2-websocket/angular2-websocket';

import { Config } from './settings/config';
import { AccountService } from './accounts/shared/services/account.service';
import { TransportationRequestsService } from './transportation/shared/services/transportation-requests.service';
import { NotificationsService } from './utils/services/notifications/notifications.service';
import { GVTWebSocketConfig } from './utils/services/websocket/websocket.config';

declare let locale: string;

@Component({
    selector: 'gvt-app',
    templateUrl: Config.APP_URL + 'templates/index.html',
    providers: [TransportationRequestsService, AccountService]
})
export class AppComponent implements OnInit {
    /**
     * Application Component
     **/
    private ws: $WebSocket;
    private locale: string;
    public DEFAULT_AVATAR_IMG = Config.DEFAULT_AVATAR_IMG;
    public LOGO_IMG = Config.LOGO_IMG;
    public APP_VERSION = Config.APP_VERSION;
    public LOGO_IMG_INVERSED = Config.LOGO_IMG_INVERSED;

    constructor(public account: AccountService, public config: Config,
                public transportationRequests: TransportationRequestsService,
                private notifications: NotificationsService, translate: TranslateService
    ){
        // this language will be used as a fallback when a translation isn't found in the current language
        this.locale = locale.toLowerCase();
        translate.setDefaultLang('ru');

         // the lang to use, if the lang isn't available, it will use the current loader to get them
        translate.use(this.locale);
    }

    ngOnInit(): void {
        /**
         * Load Application data, such as user
         **/
        this.account.loadUser();
        this.transportationRequests.initialize();

        this.account.onUserLoaded(() => {
            let path: string = Config.getWSHost() + Config.STATISTICS_WS_URL + '/' + this.account.user.username;

            this.ws = new $WebSocket(
                path,
                null,
                new GVTWebSocketConfig()
            );
                    let $this = this;
                    this.ws.onMessage(
                        (msg: any)=> {
                            let messages: object = {
                                'tr_created': {
                                    'title': 'Создан новый заказ',
                                    'description': 'Была оформлена заявка'
                                },
                                'tr_request_assigned': {
                                    'title': 'На Вас назначен заказ',
                                    'description': 'Заявка была назначена на Вас. Просмотрите список заявок'
                                }
                            };
                            $this.transportationRequests.loadStatistics();
                            let message = messages[msg.data];

                            if (message) {
                                $this.notifications.showInfoMessage(message.title, message.description);
                            }
                        },
                        {autoApply: false}
                    );
        });

    }
}