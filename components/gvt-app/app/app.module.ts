/**
 * Application Main Module
 */

import { NgModule } from '@angular/core';
import { BrowserModule, Title } from '@angular/platform-browser';
import { ModalModule } from 'ngx-bootstrap/modal';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { FormsModule }   from '@angular/forms';
import { CommonModule } from '@angular/common';
import { ToastyModule } from 'ng2-toasty';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';

import { ReportsModule } from './reports/reports.module';
import { TransportationRequestsService } from './transportation/shared/services/transportation-requests.service';
import { ReviewsModule } from './reviews/reviews.module';
import { AppComponent }  from './app.component';
import { HomeComponent }  from './home.component';
import { AccountsModule } from './accounts/accounts.module';
import { AppRoutingModule } from './app-router.module';
import { TransportationModule } from './transportation/transportation.module';
import { AccountService } from './accounts/shared/services/account.service';
import { Config } from './settings/config';
import { OrganisationsModule } from "./organisations/organisations.module";


// AoT requires an exported function for factories
export function HttpLoaderFactory(http: HttpClient) {
    return new TranslateHttpLoader(http, '/static/gvt-app/app/assets/i18n/', '.json');
}

@NgModule({
    imports: [BrowserModule, FormsModule, AppRoutingModule, HttpClientModule,
        BsDropdownModule.forRoot(), CommonModule, ModalModule.forRoot(),
        ToastyModule.forRoot(), TransportationModule, ReportsModule, AccountsModule,
        ReviewsModule, OrganisationsModule, TranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useFactory: HttpLoaderFactory,
                deps: [HttpClient]
            }
        })
    ],
    declarations: [AppComponent, HomeComponent],
    bootstrap: [AppComponent],
    providers: [AccountService, Config, Title, TransportationRequestsService]
})
export class AppModule {
    /**
     * Main Application module
     * Import modules
     */
}
