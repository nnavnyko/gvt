/**
 * Actions service
 * Get actions
 * Apply Action discount to Transportation Request
 */

import { Injectable, EventEmitter } from '@angular/core';
import { DiscountsAPIURLS } from '../../discounts.config';
import { TransportationRequest } from '../../../transportation/shared/models/transportation-request.model';
import { Action } from '../models/action.model';
import { HTTPRequestsService } from '../../../utils/services/http-requests/http-requests.service';
import * as _ from 'lodash';

@Injectable()
export class ActionsService {
    /**
     * Actions Service class
     */

    constructor(private http: HTTPRequestsService){}

    public getActionsList():Promise<any> {
        /**
         * Load list from server and generate TS-objects
         * (convert python-like TransportationRequests to TS format)
         */
        return this.http.get(DiscountsAPIURLS.ACTIONS_LIST).then(data => {
            let list:Action[] = [];

            _.each(data, action => {
                list.push(new Action(action));
            });

            return list;
        });
    }

    public applyActionDiscount(trRequestUUID: string, actionUUID?: string, ):Promise<any> {
        /**
         * Apply action discount to Transportation Request
         * Set discount amounts
         *
         * If actionUUID is undefined, clear discounts of Transportation Request
         *
         * @param trRequestUUID: string (TransportationRequest.uuid, required)
         * @param actionUUID: string (Action.uuid, optional)
         */
        let url = actionUUID ? DiscountsAPIURLS.APPLY_ACTION_DISCOUNT.replace('{uuid}', actionUUID) :
            DiscountsAPIURLS.CLEAR_ACTION_DISCOUNT;

        return this.http.post(url, {
            tr_request_uuid: trRequestUUID
        }).then(data => {
            return new TransportationRequest(data);
        });
    }
}