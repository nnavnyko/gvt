/**
 *  Home Page Component
 *  Display Home Content
 */

import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { Title } from '@angular/platform-browser';
import * as _ from 'lodash';

import { AccountService } from './accounts/shared/services/account.service';
import { TransportationRequestsService } from './transportation/shared/services/transportation-requests.service';
import { Config } from './settings/config';

@Component({
    selector: '<home></home>',
    templateUrl: Config.APP_URL + 'templates/home.html'
})
export class HomeComponent implements OnInit{

    private totalRequests: number;
    private statisticsProgress: any;

    constructor(private titleService: Title, private account: AccountService,
                private transportationRequests: TransportationRequestsService,
                private translate: TranslateService) {
        /**
         * Set title 
         */
        this.titleService.setTitle('Быстрая Панель');
    }

    ngOnInit():void {
        /**
         * Initialize component
         */
        if (!this.transportationRequests.statistics) {
            this.transportationRequests.statisticsLoaded.subscribe(
                (statisticsLoaded: boolean) => {
                    this.initializeStatistics();
                });
        } else {
            this.initializeStatistics();
        }
    }

    initializeStatistics(): void{
        /**
         * Calculate statistics
         */
        let statuses: any = {
            'new': 'Новых',
            'accepted': 'Принято',
            'delivered': 'Доставлено',
            'inProgress': 'Выполняются'
        };
        _.forOwn(statuses, (value, key) => {
            this.translate.get(value).subscribe((res: string) => {
                statuses[key] = res;
            });
        });
        let statistics:any[] = [];
        let totalRequests:number = 0;

        _.forEach(this.transportationRequests.statistics, (value: any, key: string) => {
            if (key === 'cancelled') {
                return;
            }
            totalRequests += value;
            statistics.push({
                count: value,
                status: key,
                statusDisplay: statuses[key]
            });
        });
        this.statisticsProgress = statistics;
        this.totalRequests = totalRequests;
    }
}
