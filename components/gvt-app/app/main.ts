/**
 * Main File
 */

import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { AppModule }              from './app.module';
import { enableProdMode } from '@angular/core';
// Enable production mode unless running locally
if (!/127.0.0.1/.test(document.location.host)) {
    enableProdMode();
}

platformBrowserDynamic().bootstrapModule(AppModule);
