
import {switchMap} from 'rxjs/operators';
/**
 * Create/Edit Organisation instance
 * Manage common Organisation's Info
 **/
import {Component, OnInit} from '@angular/core';
import {Organisation} from "../shared/models/organisation.model";
import {OrganisationsService} from "../shared/services/organisations.service";
import {AccountService} from "../../accounts/shared/services/account.service";
import {Title} from "@angular/platform-browser";
import {TranslateService} from "@ngx-translate/core";
import {OrganisationConfig} from "../organisations.config";
import {ActivatedRoute, Params, Router} from "@angular/router";
import {CommonConstants} from "../../utils/constants/common";


@Component({
    selector: '<organisation-edit></organisation-edit>',
    templateUrl: OrganisationConfig.BASIC_MODULE_URL + 'organisation-create-or-edit/organisation-create-or-edit.html',
    providers: [OrganisationsService]
})
export class OrganisationCreateOrEditComponent implements OnInit {
    public organisation: Organisation;
    public phoneMask: any[] = CommonConstants.FULL_PHONE_MASK;
    public formSubmitted: boolean = false;


    constructor(private orgService: OrganisationsService, private account: AccountService,
                private title: Title, private translate: TranslateService, private route: ActivatedRoute,
                private router: Router) {
        this.translate.get('Редактирование Организации').subscribe((res:string) => {
            this.title.setTitle(res);
        });
    }

    /** Initialize Component.
     * Initialize Organisation or Load from backend (if Edit organisation and uuid is provided) **/
    ngOnInit(): void {
        this.route.params.pipe(switchMap((params: Params) => {
            if (params['org_uuid']){
                return this.orgService.getOrganisation(params['org_uuid']).then((data: any) => {
                    return data;
                })
            } else {
                return new Promise((resolve, reject) => {
                    return resolve(new Organisation());
                });
            }
        })).subscribe((data:Organisation) => {
            this.organisation = data;
        });
    }

    /**
     * Save Organisation form and send data to Backend.
     * Redirect user to Organisation details
     **/
    submitForm(): any {
        return () => {
            this.formSubmitted = true;
            if (!this.organisation.uuid) {
                this.orgService.createOrganisation(this.organisation).then((org: Organisation) => {
                    this.router.navigate(['/organisations', org.uuid, 'orders']);
                    this.formSubmitted = true;
                }, (err: any) => {
                    console.error(err);
                    this.formSubmitted = false;
                });
            } else {
                this.orgService.updateOrganisation(this.organisation).then((org: Organisation) => {
                    this.router.navigate(['/organisations', this.organisation.uuid, 'orders']);
                    this.formSubmitted = true;
                }, (err: any) => {
                    console.error(err);
                    this.formSubmitted = false;
                });
            }
        };
    }
}
