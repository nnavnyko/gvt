
import {switchMap} from 'rxjs/operators';
/**
 * Organisation details component
 * Display organisation Info, list of Users, Organisation statistics
 **/
import {Component, OnInit} from "@angular/core";
import {AccountService} from "../../accounts/shared/services/account.service";
import {ActivatedRoute, Params, Router} from "@angular/router";
import {Organisation} from "../shared/models/organisation.model";
import {TranslateService} from "@ngx-translate/core";
import {Title} from "@angular/platform-browser";
import {OrganisationsService} from "../shared/services/organisations.service";
import {OrganisationConfig, OrganisationStatuses} from "../organisations.config";
import {Config} from "../../settings/config";
import {TransportationRequestsService} from "../../transportation/shared/services/transportation-requests.service";
import {NotificationsService} from "../../utils/services/notifications/notifications.service";


@Component({
    selector: '<organisation-details></organisation-details>',
    templateUrl: OrganisationConfig.BASIC_MODULE_URL + 'organisation-details/organisation-details.html',
    providers: [OrganisationsService]
})
export class OrganisationDetailsComponent implements OnInit {
    public organisation: Organisation;
    public organisationUUID: string;
    public orgStatistics: any;
    public collapseOrgDetails: boolean;
    public loadingComplete: boolean = false;
    public organisationStatusUpdating: boolean = false;
    private DEFAULT_ORG_AVATAR_IMG: any = Config.DEFAULT_ORG_AVATAR_IMG;
    private OrganisationStatuses: any = OrganisationStatuses;

    constructor(private orgService: OrganisationsService, private account: AccountService,
                private title: Title, private translate: TranslateService, private route: ActivatedRoute,
                private router: Router, private trRequests: TransportationRequestsService,
                private notifications: NotificationsService){
        this.translate.get('Страница Организации').subscribe((res:string) => {
            this.title.setTitle(res);
        });
    }
    /**
     * Initialize Component. Load Organisation's details
     **/
    ngOnInit(): void {
        this.route.params.pipe(switchMap((params: Params) => {
            this.organisationUUID = params['org_uuid'];

            return this.orgService.getOrganisation(this.organisationUUID);
        })).subscribe((data:Organisation) => {
            this.organisation = data;
            this.loadingComplete = true;

            this.trRequests.getOrganisationStatistics(this.organisationUUID).then((data:any) => {
                this.orgStatistics = data;
            });
        }, (err: any) => {
            console.error(err);
            this.loadingComplete = true;
        });
    }

    /** Toggle organisation status */
    toggleOrganisationStatus(): any {
        return () => {
            this.organisationStatusUpdating = true;
            let status: string = this.organisation.status === OrganisationStatuses.ACTIVE ?
                OrganisationStatuses.DISABLED : OrganisationStatuses.ACTIVE;

            this.orgService.updateOrganisationStatus(this.organisationUUID, status).then((org: Organisation) => {
                this.organisation.status = org.status;
                this.organisation.statusDisplay = org.statusDisplay;
                this.organisationStatusUpdating = false;

                this.notifications.showInfoMessage(
                    'Статус изменен',
                    'Статус организации успешно изменен.',
                    3000
                );
            }, (err: any) => {
                this.organisationStatusUpdating = false;
            });
        };
    }
}
