/**
 * Organisation orders
 * Display All Organisation Orders
 **/

import {Component, OnInit} from "@angular/core";
import {OrganisationConfig} from "../../organisations.config";
import {OrganisationsService} from "../../shared/services/organisations.service";
import {Title} from "@angular/platform-browser";
import {TranslateService} from "@ngx-translate/core";
import {ActivatedRoute, Router} from "@angular/router";
import {AccountService} from "../../../accounts/shared/services/account.service";
import {TransportationRequestsService} from "../../../transportation/shared/services/transportation-requests.service";

@Component({
    selector: '<organisation-orders></organisation-orders>',
    templateUrl: OrganisationConfig.BASIC_MODULE_URL + 'organisation-details/organisation-members/organisation-members.html',
    providers: [OrganisationsService]
})
export class OrganisationMembersComponent implements OnInit {
    private memberList: any[] = [];
    private loadingComplete: boolean = false;
    constructor(private orgService: OrganisationsService, private account: AccountService,
                private title: Title, private translate: TranslateService, private route: ActivatedRoute,
                private router: Router){
        this.translate.get('Участники Организации').subscribe((res:string) => {
            this.title.setTitle(res);
        });
    }
    ngOnInit(): void {
        this.loadingComplete = true;
    }
}
