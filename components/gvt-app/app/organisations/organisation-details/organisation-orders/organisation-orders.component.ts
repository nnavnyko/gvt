/**
 * Organisation orders
 * Display All Organisation Orders
 **/


import {switchMap} from 'rxjs/operators';
import {Component, OnInit} from "@angular/core";
import {OrganisationConfig} from "../../organisations.config";
import {OrganisationsService} from "../../shared/services/organisations.service";
import {Title} from "@angular/platform-browser";
import {TranslateService} from "@ngx-translate/core";
import {ActivatedRoute, Params, Router} from "@angular/router";
import {AccountService} from "../../../accounts/shared/services/account.service";
import {TransportationRequestsService} from "../../../transportation/shared/services/transportation-requests.service";
import {SimpleTransportationRequest} from "../../../transportation/shared/models/simple-transportation-request.model";
import {CommonConstants} from "../../../utils/constants/common";
import * as _ from "lodash";
import {PaginationConfig} from "ngx-bootstrap/pagination";
import {TransportationConstants} from "../../../transportation/transportation.config";

@Component({
    selector: '<organisation-orders></organisation-orders>',
    templateUrl: OrganisationConfig.BASIC_MODULE_URL + 'organisation-details/organisation-orders/organisation-orders.html',
    providers: [OrganisationsService, TransportationRequestsService, PaginationConfig]
})
export class OrganisationOrdersComponent implements OnInit {
    private organisationUUID: string;
    private loadingComplete: boolean = false;
    private totalItems: number = 0;
    private currentPage: number = 1;
    private filterParams: string;
    private filterStatus: string;
    private filterDateStart: string;
    private filterDateEnd: string;
    private trRequests: SimpleTransportationRequest[];
    private statuses: any[] = CommonConstants.TRANSPORTATION_REQUESTS_STATUSES;
    private TransportationConstants:any = TransportationConstants;

    constructor(private orgService: OrganisationsService, private account: AccountService,
                private title: Title, private translate: TranslateService, private route: ActivatedRoute,
                private router: Router, private trRequestService: TransportationRequestsService){
        this.translate.get('Заказы Организации').subscribe((res:string) => {
            this.title.setTitle(res);
        });
    }

    /** Initialize Component - Load TransportationRequests of Organisation **/
    ngOnInit(): void {
        this.route.parent.params.pipe(switchMap((params: Params) => {
            this.organisationUUID = params['org_uuid'];

            this.currentPage = 1;
            this.filterParams = '';
            this.filterStatus = '';

            return new Promise((resolve, reject) => { resolve(); })
        })).subscribe(() => {
            this.loadList(1);
        }, (err: any) => {
            console.error(err);
            this.loadingComplete = true;
        });
    }

    /**
     * Load paged list
     * @param {number} page: current page to load
     */
    loadList(page: number): void {
        this.trRequestService.loadOrganisationOrders(
            this.organisationUUID, '?page_size=25&page=' + page + this.filterParams).then(data => {
            this.trRequests = data.list;
            this.totalItems = data.totalItems;

            this.loadingComplete = true;
        }, (err: any) => {
            console.error(err);
            this.loadingComplete = true;
        });
    }

    /**
     * Handle change page event
     * @param {object} event: Page changed event
     */
    pageChanged(event: any): void {
        this.loadList(event.page);
    }

    /** Update list after filtration applied **/
    applyFiltration(): void {
        this.currentPage = 1;

        this.loadList(1);
    }

    /**
     * Set filtration
     * @param {string} filterStatus: status of TransportationRequest to filter
     * @param {string} filterDateStart: filter TransportationRequest since this date
     * @param {string} filterDateEnd: filter TransportationRequest until this date
     */
    setFilterParams(filterStatus: string, filterDateStart: string, filterDateEnd: string): void {
        let filterParams = '';
        let filterArr:any[] = [
            {key: 'status', value: filterStatus},
            {key: 'date__gte', value: filterDateStart},
            {key: 'date__lte', value: filterDateEnd}
        ];
        _.each(filterArr, filterParam => {
            if (filterParam.value && filterParam.value !== '') {
                filterParams += '&' + filterParam.key + '=' + filterParam.value;
            }
        });
        this.filterParams = filterParams;
    }

    /**
     * Filter by date: from start date to end date
     * Apply filtration by 'created' property
     * @param {string} filterDateStart: string (date key, start or end)
     * @param {string} filterDateEnd: string (date key, start or end)
     */
    filterByDate(filterDateStart:string, filterDateEnd:string):void {
        filterDateStart = filterDateStart || null;
        filterDateEnd = filterDateEnd || null;
        this.setFilterParams(this.filterStatus, filterDateStart, filterDateEnd);

        this.applyFiltration();
    }

    /**
     * Filter list by Created Date or Status
     * Reload list after Status Filter changed
     * @param {string} status: string or null
     */
    filterByStatus(status: any): void {
        this.setFilterParams(status, this.filterDateStart, this.filterDateEnd);

        this.applyFiltration();
    }
}
