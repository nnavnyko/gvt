/**
* General Organisation Report
*/


import {switchMap} from 'rxjs/operators';
import {Component, OnInit} from "@angular/core";
import {OrganisationConfig} from "../../../organisations.config";
import {ReportsURLS} from "../../../../reports/reports.config";
import * as _ from "lodash";
import {EmployeesService} from "../../../../accounts/shared/services/employees.service";
import {AccountService} from "../../../../accounts/shared/services/account.service";
import {CommonConstants} from "../../../../utils/constants/common";
import {Title} from "@angular/platform-browser";
import {Config} from "../../../../settings/config";
import {TranslateService} from "@ngx-translate/core";
import {ActivatedRoute, Params} from "@angular/router";

@Component({
    selector: 'organisation-reports',
    templateUrl: OrganisationConfig.BASIC_MODULE_URL + 'organisation-details/organisation-reports/general-report/general-report.html'
})
export class OrganisationGeneralReportComponent implements OnInit{
    private organisationUUID: string = null;
    private filterStatus: string = "";
    private filterDateStart: string = null;
    private filterDateEnd: string = null;
    private filterParams: string = "";
    private downloadURL: string;
    private statuses: any[] = CommonConstants.TRANSPORTATION_REQUESTS_STATUSES;
    private GENERAL_REPORT_EXAMPLE_IMAGE_URL = Config.STATIC_URL + 'images/reports/general_report_example.png';

    constructor(private title: Title, private account: AccountService,
                private translate: TranslateService, private route: ActivatedRoute){
        this.translate.get('Стандартный Отчет Организации').subscribe((res:string) => {
            this.title.setTitle(res);
        });
    }
    ngOnInit():void {
        /**
         * Initialize component. Load employees list
         */
        _.each(this.statuses, (status:any) => {
            this.translate.get(status['display']).subscribe((res:string) => {
                status['display'] = res;
            });
        });


        // Check if TrRequest is in Organisation details scope
        if (this.route.parent && this.route.parent.params) {
            this.route.parent.params.pipe(switchMap((params: Params) => {
                return new Promise((resolve, reject) => {
                    resolve(params['org_uuid'])
                });
            })).subscribe((orgUUID: string) => {
                this.organisationUUID = orgUUID;
                this.downloadURL = ReportsURLS.DOWNLOAD_ORGANISATION_REPORT.replace('{uuid}', this.organisationUUID);
            });
        }

    }

    setFilterParams(filterStatus: string, filterDateStart: string, filterDateEnd: string): void {
        /**
         * Set filtration
         */
        let filterParams = '';
        let filterArr:any[] = [
            {key: 'status', value: filterStatus},
            {key: 'date__gte', value: filterDateStart},
            {key: 'date__lte', value: filterDateEnd}
        ];
        _.each(filterArr, filterParam => {
            if (filterParam.value && filterParam.value !== '') {
                filterParams += '&' + filterParam.key + '=' + filterParam.value;
            }
        });
        this.filterParams = filterParams;
    }
}