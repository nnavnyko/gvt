/**
 * Organisations list component
 **/

import {Component, OnInit} from "@angular/core";
import {OrganisationsService} from "../shared/services/organisations.service";
import {Organisation} from "../shared/models/organisation.model";
import {OrganisationConfig, OrganisationStatuses} from "../organisations.config";
import {CommonConstants} from "../../utils/constants/common";
import {Config} from "../../settings/config";
import {PaginationConfig} from "ngx-bootstrap/pagination";
import {TranslateService} from "@ngx-translate/core";
import {Title} from "@angular/platform-browser";
import * as _ from "lodash";

@Component({
    selector: 'organisations-list',
    templateUrl: OrganisationConfig.BASIC_MODULE_URL + 'organisations-list/organisations-list.html',
    providers: [OrganisationsService, PaginationConfig]
})
export class OrganisationsListComponent implements OnInit {
    private organisationsList: Organisation[] = [];
    private totalItems: number = 0;
    private currentPage: number = 1;
    private filterStatus: string = '';
    private filterSearch: string = '';
    private filterParams: string = '';
    private loadingComplete: boolean = false;
    private orgStatuses: any[] = CommonConstants.ORGANISATION_STATUSES;
    private OrganisationStatuses: any = OrganisationStatuses;
    private DEFAULT_ORG_AVATAR_IMG: any = Config.DEFAULT_ORG_AVATAR_IMG;

    constructor(private title: Title, private translate: TranslateService, private orgService: OrganisationsService) {
        this.translate.get('Организации').subscribe((res:string) => {
            this.title.setTitle(res);
        });
    }

    /**
     * Initialize component. Load list of organisations
     **/
    ngOnInit(): void {
        this.loadOrgList();
        _.each(this.orgStatuses, (status:any) => {
            this.translate.get(status['display']).subscribe((res:string) => {
                status['display'] = res;
            });
        });
    }

    /**
     * Load organisations list on specific page
     * @param {number} [page]: page number, int
     **/
    loadOrgList(page?: number): void {
        page = page || 1;

        this.orgService.getOrganisationsList(page, this.filterParams).then((data) => {
            this.organisationsList = data.list;
            this.totalItems = data.totalItems;
            this.loadingComplete = true;
        }, (err) => {
            console.error(err);
            this.loadingComplete = true;
        });
    }

    /** Handle page changed event **/
    pageChanged(event: any): void {
        this.loadOrgList(event.page);
    }

    /**
     * Filter list by Created Date or Status
     * Reload list after Status Filter changed
     * @param status: string or null
     */
    filterByStatus(status: any): void {
        this.setFilterParams(status, this.filterSearch);

        this.applyFiltration();
    }

    /**
     * Filter by search string
     * Search in phone and email fields
     */
    filterBySearch():void {
        this.setFilterParams(this.filterStatus, this.filterSearch);

        this.applyFiltration();
    }

    applyFiltration(): void {
        /**
         * Update list after filtration applied
         */
        this.currentPage = 1;

        this.loadOrgList(1);
    }

    /**
     * Set filtration
     */
    setFilterParams(filterStatus: string, filterSearch: string): void {
        let filterParams = '';
        let filterArr:any[] = [
            {key: 'status', value: filterStatus},
            {key: 'search', value: filterSearch},
        ];
        _.each(filterArr, filterParam => {
            if (filterParam.value && filterParam.value !== '') {
                filterParams += '&' + filterParam.key + '=' + filterParam.value;
            }
        });
        this.filterParams = filterParams;
    }
}