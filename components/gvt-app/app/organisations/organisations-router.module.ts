/**
 * Organisations router
 **/

import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { OrganisationsListComponent } from './organisations-list/organisations-list.component';
import { OrganisationCreateOrEditComponent } from "./organisation-create-or-edit/organisation-create-or-edit.component";
import { OrganisationDetailsComponent } from "./organisation-details/organisation-details.component";
import { OrganisationOrdersComponent } from "./organisation-details/organisation-orders/organisation-orders.component";
import { OrganisationMembersComponent } from "./organisation-details/organisation-members/organisation-members.component";
import { OrganisationTransportationRequestDetailsComponent } from "../transportation/transportation-request-details/transportation-request-details.component";
import {UpdateOrganisationTransportationRequestComponent} from "../transportation/transportation-request-update/transportation-request-update.component";
import {OrganisationGeneralReportComponent} from "./organisation-details/organisation-reports/general-report/general-report.component";


const routes: Routes = [
    {
        path: 'organisations',
        children: [
            {
                path: 'list',
                component: OrganisationsListComponent
            },
            {
                path: 'create',
                component: OrganisationCreateOrEditComponent
            },
            {
                path: ':org_uuid',
                component: OrganisationDetailsComponent,
                children: [
                    {
                        path: 'orders',
                        component: OrganisationOrdersComponent,
                    },
                    {
                        path: 'orders/create',
                        component: UpdateOrganisationTransportationRequestComponent
                    },
                    {
                        path: 'orders/:uuid',
                        component: OrganisationTransportationRequestDetailsComponent
                    },
                    {
                        path: 'orders/:uuid/edit',
                        component: UpdateOrganisationTransportationRequestComponent
                    },
                    {
                        path: 'members',
                        component: OrganisationMembersComponent
                    },
                    {
                        path: 'reports/general',
                        component: OrganisationGeneralReportComponent
                    }
                ]
            },
            {
                path: ':org_uuid/edit',
                component: OrganisationCreateOrEditComponent
            }
        ]
    }
];

@NgModule({
    imports: [ RouterModule.forRoot(routes) ],
    exports: [ RouterModule ]
})
export class OrganisationsRoutingModule {}
