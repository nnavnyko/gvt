/**
 * Organisations Config file
 * Put all constants and static data related to the Organisations here
 **/
import { Config } from '../settings/config';


export class OrganisationConfig {
    public static BASIC_API_URL = '/organisations/api/';
    public static BASIC_MODULE_URL = Config.APP_URL + 'organisations/';
}


export class OrganisationAPIURLS {
    /**
     * Transportation API URLs config
     */
    // List URLs
    public static ORGANISATIONS_LIST = OrganisationConfig.BASIC_API_URL + 'list';
    public static ORGANISATION_CREATE = OrganisationConfig.BASIC_API_URL + 'create';
    public static ORGANISATION_DETAILS = OrganisationConfig.BASIC_API_URL + '{uuid}';
    public static UPDATE_ORGANISATION_STATUS = OrganisationConfig.BASIC_API_URL + '{uuid}/update-status';
}

export class OrganisationStatuses {
    public static ACTIVE = 'active';
    public static DISABLED = 'disabled';
}
