/**
 * Organisations Module. Include this module in App module
 **/
import {NgModule} from "@angular/core";
import {SharedModule} from "../shared.module";
import {CollapseModule} from "ngx-bootstrap/collapse";
import {TextMaskModule} from "angular2-text-mask";
import {OrganisationsRoutingModule} from "./organisations-router.module";
import {OrganisationsListComponent} from "./organisations-list/organisations-list.component";
import {OrganisationCreateOrEditComponent} from "./organisation-create-or-edit/organisation-create-or-edit.component";
import {OrganisationDetailsComponent} from "./organisation-details/organisation-details.component";
import {OrganisationOrdersComponent} from "./organisation-details/organisation-orders/organisation-orders.component";
import {OrganisationMembersComponent} from "./organisation-details/organisation-members/organisation-members.component";
import {OrganisationGeneralReportComponent} from "./organisation-details/organisation-reports/general-report/general-report.component";


@NgModule({
    imports: [CollapseModule, TextMaskModule, OrganisationsRoutingModule,
        SharedModule],
    declarations: [OrganisationsListComponent, OrganisationCreateOrEditComponent, OrganisationDetailsComponent,
        OrganisationOrdersComponent, OrganisationMembersComponent, OrganisationGeneralReportComponent]
})
export class OrganisationsModule {}
