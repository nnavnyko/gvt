/**
 * Organisation model
 **/

export class Organisation {
    public id: number;
    public uuid: string;
    public name: string;
    public logo: string;
    public status: string;
    public statusDisplay: string;
    public website: string;
    public contactEmail: string;
    public contactPhone: string;
    public description: string;

    constructor(obj?: any){
        if (obj) {
            this.id = obj.id;
            this.uuid = obj.uuid;
            this.name = obj.name;
            this.logo = obj.logo;
            this.status = obj.status;
            this.statusDisplay = obj.get_status_display;
            this.website = obj.website;
            this.contactEmail = obj.contact_email;
            this.contactPhone = obj.contact_phone;
            this.description = obj.description;
        }
    }

}
