/**
 * Simple Organisation model
 * Contains only necessary fields
 *
 * Use it in TransportationRequest details page
 */

export class SimpleOrganisationModel {
    public uuid: string;
    public name: string;
    public logo: string;

    constructor(obj?: any){
        if (obj) {
            this.uuid = obj.uuid;
            this.name = obj.name;
            this.logo = obj.logo;
        }
    }
}