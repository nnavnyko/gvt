/**
 * Organisations service
 * 
 * Manage Organisations in the system (get list of orgs, create/update organisation, disable organisation)
 **/
import { Injectable } from "@angular/core";
import { HTTPRequestsService } from "../../../utils/services/http-requests/http-requests.service";
import * as _ from 'lodash';

import { Organisation } from '../models/organisation.model';
import { OrganisationAPIURLS } from "../../organisations.config";


@Injectable()
export class OrganisationsService {
    constructor(private http: HTTPRequestsService){}

    /**
     * Get list of all Organisations in the System
     * @param {number} [page]: page number, int
     * @param {string} [query]: filtration query
     * @return {Promise}: promise with list of Organisation objects
     **/
    public getOrganisationsList(page?: number, query?: string): Promise<any> {
        page = page || 1;

        return this.http.get(OrganisationAPIURLS.ORGANISATIONS_LIST + '?page=' + page + (query ? query : ''))
            .then((data:any) => {
            let orgs: Organisation[] = [];

            _.each(data.results, (orgData: any) => {
                orgs.push(new Organisation(orgData));
            });

            return {
                list: orgs,
                totalItems: data.count
            };
        });
    }

    /**
     * Submit Organisation data. Return created organisation
     * @param {Organisation} organisation: Organisation instance
     * @return {Promise}: promise with created Organisation instance
     **/
    public createOrganisation(organisation: Organisation): Promise<any> {
        let postData: any = {
            name: organisation.name,
            description: organisation.description,
            website: organisation.website,
            contact_phone: organisation.contactPhone,
            contact_email: organisation.contactEmail
        };
        return this.http.post(OrganisationAPIURLS.ORGANISATION_CREATE, postData).then((data: any) => {
            return new Organisation(data);
        }, (err) => {
            console.error(err);
        });
    }

    /**
     * Update Organisation data. Return created organisation
     * @param {Organisation} organisation: Organisation instance
     * @return {Promise}: promise with created Organisation instance
     **/
    public updateOrganisation(organisation: Organisation): Promise<any> {
        let postData: any = {
            name: organisation.name,
            description: organisation.description,
            website: organisation.website,
            contact_phone: organisation.contactPhone,
            contact_email: organisation.contactEmail
        };
        return this.http.post(OrganisationAPIURLS.ORGANISATION_DETAILS.replace('{uuid}', organisation.uuid), postData).then((data: any) => {
            return new Organisation(data);
        }, (err) => {
            console.error(err);
        });
    }

    /**
     * Get Organisation details.
     * @param {string} uuid: Organisation.uuid
     * @return {Promise}: promise with Organisation instance
     **/
    public getOrganisation(uuid: string): Promise<any> {
        return this.http.get(OrganisationAPIURLS.ORGANISATION_DETAILS.replace('{uuid}', uuid)).then((data: any) => {
            return new Organisation(data);
        }, (err) => {
            console.error(err);
        });
    }

    /**
     * Update Organisation's status
     * @param {string} uuid: Organisation.uuid
     * @param {string} status: one of OrganisationStatuses enum values
     * @return {Promise}: Http request promise
     */
    public updateOrganisationStatus(uuid: string, status: string): Promise<any> {
        return this.http.post(OrganisationAPIURLS.UPDATE_ORGANISATION_STATUS.replace('{uuid}', uuid), {status: status}).then((data: any) => {
            return new Organisation(data);
        }, (err) => {
            console.error(err);

            throw err;
        });
    }
}

