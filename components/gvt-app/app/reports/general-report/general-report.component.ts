/**
 * General Report Component
 */

import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import * as _ from 'lodash';
import { Title } from '@angular/platform-browser';

import { EmployeesService } from '../../accounts/shared/services/employees.service';
import { AccountService } from '../../accounts/shared/services/account.service';
import { SimpleEmployeeUser } from '../../accounts/shared/models/simple-employee-user.model';
import { ReportsConfig, ReportsURLS } from '../reports.config';
import { CommonConstants } from '../../utils/constants/common';
import { Config } from '../../settings/config';


@Component({
    selector: '<general-report></general-report>',
    templateUrl: ReportsConfig.BASIC_MODULE_URL + 'general-report/general-report.html',
    providers: [EmployeesService]
})
export class GeneralReportComponent implements OnInit {
    /**
     * General Repor Component
     */
    private filterStatus: string = "";
    private filterDateStart: string = null;
    private filterDateEnd: string = null;
    private filterEmployeeID: string = "";
    private filterParams: string = "";
    private employeesList: SimpleEmployeeUser[] = [];
    private downloadURL: string;
    private statuses: any[] = CommonConstants.TRANSPORTATION_REQUESTS_STATUSES;
    private GENERAL_REPORT_EXAMPLE_IMAGE_URL = Config.STATIC_URL + 'images/reports/general_report_example.png';

    constructor(private title: Title, private employeesService: EmployeesService,
                private account: AccountService, private translate: TranslateService){
        this.translate.get('Стандартный Отчет').subscribe((res:string) => {
            this.title.setTitle(res);
        });
    }

    ngOnInit():void {
        /**
         * Initialize component. Load employees list
         */
        this.employeesService.getEmployeeList().then(employeesList => {
            this.employeesList = employeesList;
        });

        _.each(this.statuses, (status:any) => {
            this.translate.get(status['display']).subscribe((res:string) => {
                status['display'] = res;
            });
        });
        this.downloadURL = this.account.isManager() ? ReportsURLS.DOWNLOAD_GENERAL_REPORT : ReportsURLS.DOWNLOAD_EMPLOYEE_REPORT;
    }

    setFilterParams(filterStatus: string, filterDateStart: string, filterDateEnd: string, filterEmployeeID: string): void {
        /**
         * Set filtration
         */
        let filterParams = '';
        let filterArr:any[] = [
            {key: 'status', value: filterStatus},
            {key: 'date__gte', value: filterDateStart},
            {key: 'date__lte', value: filterDateEnd},
            {key: 'assignee_id', value: filterEmployeeID},
        ];
        _.each(filterArr, filterParam => {
            if (filterParam.value && filterParam.value !== '') {
                filterParams += '&' + filterParam.key + '=' + filterParam.value;
            }
        });
        this.filterParams = filterParams;
    }
}
