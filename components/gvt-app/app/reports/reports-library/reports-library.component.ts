/**
 * Created by nnavnyko on 09.07.2017.
 */

import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { Title } from '@angular/platform-browser';

import { ReportsConfig } from '../reports.config';

@Component({
    selector: '<reports-library></reports-library>',
    templateUrl: ReportsConfig.BASIC_MODULE_URL + 'reports-library/reports-library.html'
})
export class ReportsLibraryComponent implements OnInit{
    /**
     * Reports Library Component
     */

    constructor(private title: Title, private translate: TranslateService) {
        this.translate.get(' Отчеты').subscribe((res:string) => {
            this.title.setTitle(res);
        });
    }

    ngOnInit(): void {
        /**
         * Initialize component
         */
    }
}
