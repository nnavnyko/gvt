/**
 * Reports Router Module
 */
import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ReportsLibraryComponent } from './reports-library/reports-library.component';
import { GeneralReportComponent } from './general-report/general-report.component';

const routes: Routes = [
   {path: 'reports', redirectTo: '/reports/library', pathMatch: 'full'},
   {path: 'reports/library', component: ReportsLibraryComponent},
   {path: 'reports/general', component: GeneralReportComponent},
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class ReportsRoutingModule {}
