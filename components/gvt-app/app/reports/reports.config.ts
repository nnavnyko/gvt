/**
 * Reports Config
 */
import { Config } from '../settings/config';

export class ReportsConfig {
    /**
     * Reports Common Config
     */
    public static BASIC_MODULE_URL = Config.APP_URL + 'reports/';
}

export class ReportsAPIURLS {
    /**
     * Reports API URLs Config
     */
}

export class ReportsURLS {
    /**
     * Reports URLs Config
     */
    public static DOWNLOAD_GENERAL_REPORT = '/transportation/reports/general/download';
    public static DOWNLOAD_EMPLOYEE_REPORT = '/transportation/reports/general/employee/download';
    public static DOWNLOAD_ORGANISATION_REPORT = '/transportation/reports/general/organisation/{uuid}/download';
}
