/**
 * Reports Module
 */
import { NgModule } from '@angular/core';

import { SharedModule } from '../shared.module';
import { ReportsLibraryComponent } from './reports-library/reports-library.component';
import { ReportsRoutingModule } from './reports-router.module';
import { GeneralReportComponent } from './general-report/general-report.component';


@NgModule({
    imports: [ReportsRoutingModule, SharedModule],
    declarations: [ReportsLibraryComponent, GeneralReportComponent]
})
export class ReportsModule {
    /**
     * Reports Root Module
     */
}
