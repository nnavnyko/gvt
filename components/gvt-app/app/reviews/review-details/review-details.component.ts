
import {switchMap} from 'rxjs/operators';
/**
 * Review Details Component
 */
import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { ActivatedRoute, Params }   from '@angular/router';
import { TranslateService } from '@ngx-translate/core';


import { Review } from '../shared/models/review.model';
import { ReviewsService } from '../shared/services/reviews.service';
import { ReviewsConfig } from '../reviews.config';


@Component({
    selector: '<review-details></review-details>',
    templateUrl: ReviewsConfig.BASIC_MODULE_URL + 'review-details/review-details.html',
    providers: [ReviewsService]
})
export class ReviewDetailsComponent implements OnInit {
    /**
     * Review Details Component
     * View/Manage Review details
     */
    private review: Review;
    private reviewUpdating: boolean = false;
    private loadingComplete: boolean = false;

    constructor(
        private route: ActivatedRoute, private reviews: ReviewsService, private title: Title,
        private translate: TranslateService
    ) {
        /** Constructor **/
        this.translate.get('Просмотр Отзыва').subscribe((res:string) => {
            this.title.setTitle(res);
        });
    }

    ngOnInit(): void {
        /**
         * Load Review after component is initialized
         */
        this.route.params.pipe(
            switchMap((params: Params) => this.reviews
                .getReviewDetails(params['uuid'])))
            .subscribe(data => {
                this.review = data;
                this.loadingComplete = true;
            });
    }

    public changeReviewStatus(status: string) {
        /**
         * Process Review (Set status to Processed)
         * @param: string (status to change)
         */
        return () => {
            this.reviewUpdating = true;
            this.reviews.changeReviewStatus(this.review.uuid, status).then(review => {
                this.reviewUpdating = false;
                this.review = review;
            }, err => {
                console.dir(err);
                this.reviewUpdating = false;
            });
        };
    }
}
