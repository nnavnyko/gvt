/**
 * Reviews List component
 * Display all reviews for managers
 */
import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { Title } from '@angular/platform-browser';
import { PaginationConfig } from 'ngx-bootstrap/pagination';

import { ReviewsConfig } from '../reviews.config';
import { ReviewsService } from '../shared/services/reviews.service';
import { SimpleReview } from '../shared/models/simple-review.model';

@Component({
    selector: '<reviews-list></reviews-list>',
    templateUrl: ReviewsConfig.BASIC_MODULE_URL + 'reviews-list/reviews-list.html',
    providers: [ReviewsService, PaginationConfig]
})
export class ReviewsListComponent implements OnInit {
    /**
     * Reviews List Component
     */
    private totalItems: number;
    private currentPage: number;
    private loadingComplete: boolean = false;
    private reviewsList: SimpleReview[];

    constructor(private reviews: ReviewsService, private title: Title, private translate: TranslateService) {
        /** Constructor **/
        this.currentPage = 1;

        this.translate.get('Отзывы').subscribe((res:string) => {
            this.title.setTitle(res);
        });
    }

    ngOnInit():void {
        /**
         * Load list on component initialized
         */
        this.loadList(this.currentPage);
    }

    pageChanged(event: any): void {
        /**
         * Handle change page
         */
        this.loadList(event.page);
    }

    loadList(page: number): void {
        /**
         * Load paged Reviews list
         */

        this.reviews.getReviewsList(page).then(data => {
            this.reviewsList = data.list;
            this.totalItems = data.totalItems;
            this.loadingComplete = true;
        });
    }
}

