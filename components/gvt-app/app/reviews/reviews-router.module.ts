/**
 * Reviews Router
 */

import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ReviewsListComponent } from './reviews-list/reviews-list.component';
import { ReviewDetailsComponent } from './review-details/review-details.component';

const routes: Routes = [
    {
        path: 'reviews',
        children: [
            { path: 'list', component: ReviewsListComponent },
            {
                path: 'details/:uuid',
                component: ReviewDetailsComponent
            },
        ]
    }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class ReviewsRoutingModule {}
