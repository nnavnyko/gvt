/**
 * Reviews Configurations
 */
import { Config } from '../settings/config';

export class ReviewsConfig {
    /**
     * Reviews Common Config
     */
    public static BASIC_API_URL = '/reviews/api/';
    public static BASIC_MODULE_URL = Config.APP_URL + 'reviews/';
}

export class ReviewsAPIURLS {
    /**
     * Reviews API URLs Config
     */
    public static REVIEWS_LIST = ReviewsConfig.BASIC_API_URL + 'list';
    public static REVIEW_DETAILS = ReviewsConfig.BASIC_API_URL + '{uuid}/details';
    public static CHANGE_STATUS = ReviewsConfig.BASIC_API_URL + '{uuid}/change_status/';
}
