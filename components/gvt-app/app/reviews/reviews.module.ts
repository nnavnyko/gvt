/**
 * Reviews Module
 */
import { NgModule } from '@angular/core';

import { RatingModule } from '../utils/directives/rating/rating.module';
import { ReviewsRoutingModule } from './reviews-router.module';
import { ReviewsListComponent } from './reviews-list/reviews-list.component';
import { ReviewDetailsComponent } from './review-details/review-details.component';
import { RangeModule } from '../utils/pipes/range/range.module';
import { WordsLimitModule } from '../utils/pipes/words-limit/words-limit.module';
import { SharedModule } from '../shared.module';


@NgModule({
    imports: [
        RangeModule, RatingModule, ReviewsRoutingModule,
        SharedModule, WordsLimitModule
    ],
    declarations: [ReviewsListComponent, ReviewDetailsComponent]
})
export class ReviewsModule {
    /**
     * Reviews Module Class
     */
}