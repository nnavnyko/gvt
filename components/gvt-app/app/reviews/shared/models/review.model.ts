/**
 * Review Model
 */

export class Review {
    /**
     * Review model class
     */
    public uuid: string;
    public rating: number;
    public name: string;
    public email: string;
    public phone: string;
    public created: string;
    public text: string;
    public status: string;
    public statusDisplay: string;

    constructor(obj?: any) {
        /**
         * Create instance from object, of obj parameter is passed
         */
        if (obj) {
            this.uuid = obj.uuid;
            this.rating = obj.rating;
            this.name = obj.name;
            this.email = obj.email;
            this.phone = obj.phone;
            this.created = obj.created;
            this.text = obj.text;
            this.status = obj.status;
            this.statusDisplay = obj.status_display;
        }
    }
}
