/**
 * Reviews Service
 */
import { Injectable } from '@angular/core';
import { HTTPRequestsService } from '../../../utils/services/http-requests/http-requests.service';

import * as _ from 'lodash';
import { NotificationsService } from '../../../utils/services/notifications/notifications.service';

import { ReviewsAPIURLS } from '../../reviews.config';
import { SimpleReview } from '../models/simple-review.model';
import { Review } from '../models/review.model';


@Injectable()
export class ReviewsService {
    /**
     * Reviews Service
     */
    constructor(private http: HTTPRequestsService, private notifications: NotificationsService){}

    public getReviewsList(page?: number): Promise<any> {
        /**
         * Get reviews list
         * The current user (account.user) must have permissions to read this list
         * (must be a manager or an admin)
         */
        let pageFiltering: string = page ? ('?page=' + page) : '';

        return this.http.get(ReviewsAPIURLS.REVIEWS_LIST + pageFiltering).then((data:any) => {
            let reviewsList: SimpleReview[] = [];

            _.each(data.results, review => {
                reviewsList.push(new SimpleReview(review));
            });

            return {
                list: reviewsList,
                totalItems: data.count
            };
        });
    }

    public getReviewDetails(uuid: string): Promise<any> {
        /**
         * Get Review Details
         * @param uuid: string (Review.uuid)
         */
        return this.http.get(ReviewsAPIURLS.REVIEW_DETAILS.replace('{uuid}', uuid)).then(data => {
            return new Review(data);
        });
    }

    public changeReviewStatus(uuid: string, status: string): Promise<any> {
        /**
         * Change Review status
         * @param uuid: string (Review.uuid)
         * @param status: string (Review.status to change)
         */
        return this.http.post(ReviewsAPIURLS.CHANGE_STATUS.replace('{uuid}', uuid), {'status': status}).then(data => {
            this.notifications.showSuccessMessage('Отзыв успешно изменен', 'Вы успешно изменили отзыв');

            return new Review(data);
        });
    }
}

