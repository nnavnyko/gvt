/**
 *  Application Config
 *  Describe Constants of the project
 */

export class Config {
    public static STATIC_URL = '/static/';
    public static APP_URL = '/static/gvt-app/app/';
    public static LOGO_IMG = '/static/images/gvt_icon_64.png';
    public static LOGO_IMG_INVERSED = '/static/images/gvt_icon_inversed_64.png';
    public static DEFAULT_AVATAR_IMG = '/static/images/default-avatar.png';
    public static DEFAULT_ORG_AVATAR_IMG = '/static/images/org_default_avatar.png';
    public static DRIVER_ICON = '/static/images/map/driver_icon_map.png';
    public static START_ICON = '/static/images/map/start_icon_map.png';
    public static END_ICON = '/static/images/map/end_icon_map.png';
    public static NEXT_ICON = '/static/images/map/next_icon_map.png';
    public static WEB_SOCKET_PREFIX = 'ws://';
    public static LIVE_WEB_SOCKET_PREFIX = 'wss://';
    public static MAPBOX_STYLE = 'mapbox://styles/nnavnyko/cjpslwb1u07vc2rmhox79bm4h';

    public static STATISTICS_WS_URL = '/ws/transportation_requests_statistics';
    public static TR_REQUEST_WS_URL = '/ws/transportation_requests_details';

    /**
     * Get current host for WebSocket
     */
    public static getWSHost(): string {
        let host:string = document.location.host;

        if (host.indexOf('127.0.0.1') > -1) {
            return Config.WEB_SOCKET_PREFIX + host.replace(':8000', ':8596');
        }

        return Config.LIVE_WEB_SOCKET_PREFIX + host;
    }

    public static APP_VERSION = '0.10.1';
}