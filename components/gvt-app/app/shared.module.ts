/**
 * Shared module to avoid importing multiple modules in each module
 * Import this module to another NgModule to get access to each module described here
 */
import { NgModule } from '@angular/core';
import { FormsModule }   from '@angular/forms';
import { CommonModule } from '@angular/common';
import { ModalModule } from 'ngx-bootstrap/modal';
import { DatepickerModule } from 'ngx-bootstrap/datepicker';
import { Title } from '@angular/platform-browser';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { PaginationModule } from 'ngx-bootstrap/pagination';
import { NguiDatetimePickerModule } from '@ngui/datetime-picker';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { WebStorageModule } from 'ngx-store';

import { ConfirmationModalModule } from './utils/directives/confirmation-modal/confirmation-modal.module';
import { HTTPRequestsModule } from './utils/services/http-requests/http-requests.module';
import { NotificationsModule } from './utils/services/notifications/notifications.module';
import { LoaderModule } from "./utils/directives/loader/loader.module";
import {ChartModule} from "./utils/directives/chart/chart.module";
import {AnimatedCollapseModule} from "./utils/directives/collapse/animated-collapse.module";


// AoT requires an exported function for factories
export function HttpLoaderFactory(http: HttpClient) {
    return new TranslateHttpLoader(http, '/static/gvt-app/app/assets/i18n/', '.json');
}


@NgModule({
    imports: [HttpClientModule, TranslateModule.forChild({
        loader: {
            provide: TranslateLoader,
            useFactory: HttpLoaderFactory,
            deps: [HttpClient]
        }
    })],
    exports: [
        FormsModule, CommonModule, DatepickerModule, ModalModule, TranslateModule,
        PaginationModule, ConfirmationModalModule, HTTPRequestsModule, NotificationsModule,
        NguiDatetimePickerModule, LoaderModule, ChartModule, WebStorageModule, AnimatedCollapseModule
    ],
    providers: [Title]
})
export class SharedModule {
/**
 * Shared Module class
 */
}
