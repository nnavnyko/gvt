/**
 * Transportation Request Model
 */
import { SimpleEmployeeUser } from '../../../accounts/shared/models/simple-employee-user.model';

export class SimpleTransportationRequest {
    id: number;
    public uuid: string;
    public assignee: SimpleEmployeeUser;
    public status: string;
    public date: string;
    public time: string;
    public statusDisplay: string;
    public amount: number;
    public carAmount: number;
    public portersAmount: number;
    public carAmountMax: number;
    public portersAmountMax: number;
    public fullAmount: number;
    public fullAmountMax: number;
    public discountAmount: number;
    public discountAmountMin: number;
    public discountAmountMax: number;
    public actionID: number;
    public organisationID: number;
    public created: any;
    isHourlyRate: boolean;
    isCashlessPayment: boolean;

    constructor(obj?:any) {
        /**
         * Initialize transportation request from python-format object
         */
        if (obj) {
            this.id = obj.id;
            this.uuid = obj.uuid;
            this.assignee = obj.assignee ? new SimpleEmployeeUser(obj.assignee) : null;
            this.status = obj.status;
            this.date = obj.date;
            this.time = obj.time;
            this.amount = obj.amount;
            this.carAmount = obj.car_amount;
            this.portersAmount = obj.porters_amount;
            this.carAmountMax = obj.car_amount_max;
            this.portersAmountMax = obj.porters_amount_max;
            this.statusDisplay = obj.get_status_display;
            this.created = obj.created;
            this.actionID = obj.action_id;
            this.discountAmount = obj.discount_amount;
            this.discountAmountMin = obj.discount_amount_min;
            this.discountAmountMax = obj.discount_amount_max;
            this.fullAmount = this.getFullAmount();
            this.fullAmountMax = this.getFullAmountMax();
            this.isHourlyRate = obj.is_hourly_rate;
            this.isCashlessPayment = obj.is_cashless_payment;
            this.organisationID = obj.organisation_id;
        }
    }



    getFullAmountMax(): number {
        /**
         * Get full Max amount, if amount is not set;
         * If the TransportationRequest complete and there is final amount or no porters Max amount and no car amount Max
         * return 0.
         * @return: number
         */
        if (this.amount || !this.carAmountMax && !this.portersAmountMax) {
            return 0;
        }
        let amount: number;
        amount = ((this.carAmountMax || this.carAmount || 0) +
            (this.portersAmountMax || this.portersAmount || 0)) || 0;
        let discountAmount = this.discountAmountMax || 0;

        return amount - discountAmount;
    }

    getFullAmount(): number {
        /**
         * Get full amount, if amount is not set;
         * @return: number
         */
        let amount = this.amount || ((this.carAmount || 0) + (this.portersAmount || 0)) || 0;
        let discountAmount = this.discountAmount || this.discountAmountMin || 0;

        return amount - discountAmount;
    }
}
