/**
 * Transportation Statistics
 */

export class Statistics {
    /**
     * Transportation Statistics
     */
    public new: number;
    public accepted: number;
    public cancelled: number;
    public delivered: number;
    public inProgress: number;

    constructor(obj?: any) {
        /**
         * Initialize object
         */
        if (obj) {
            this.new = obj.new;
            this.accepted = obj.accepted;
            this.cancelled = obj.cancelled;
            this.delivered = obj.delivered;
            this.inProgress = obj.in_progress;
        }
    }
}
