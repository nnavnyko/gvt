/**
 * TransportationHistory model
 **/
import {TransportationRequest} from "./transportation-request.model";
import {SimpleUser} from "../../../accounts/shared/models/simple-user.model";

export class TransportationHistory {
    public created: string;
    public actionType: string;
    public actionTypeDisplay: string;
    public comment: string;
    public trState: TransportationRequest;
    public user: SimpleUser;
    public actionBy: SimpleUser;

    /**
     * Convert python-like object to TS format
     */
    constructor(obj?: any) {
        if (obj) {
            this.created = obj.created;
            this.actionType = obj.action_type;
            this.actionTypeDisplay = obj.action_type_display;
            this.comment = obj.comment;
            this.trState = new TransportationRequest(obj.tr_state);
            this.user = obj.user ? new SimpleUser(obj.user) : null;
            this.actionBy = obj.action_by ? new SimpleUser(obj.action_by) : null;
        }
    }
}
