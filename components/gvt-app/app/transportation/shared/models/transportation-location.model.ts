/**
 * Transportation Location
 * This model contains information about point location
 * (address, coordinates, floor, sort order)
 */
export class TransportationLocation {
    /**
     * Transportation Location Model Class
     */
    public sortOrder: number;
    public address: string;
    public longitude: number;
    public latitude: number;
    public floor: number;

    constructor(obj?: any) {
        /**
         * Convert python-like object to TS format
         */
        if (obj) {
            this.sortOrder = obj.sort_order;
            this.address = obj.address;
            this.longitude = obj.longitude;
            this.latitude = obj.latitude;
            this.floor = obj.floor;
        }
    }

    public setCoordinates(longitude: number, latitude: number): void {
        this.longitude = longitude;
        this.latitude = latitude;
    }

}
