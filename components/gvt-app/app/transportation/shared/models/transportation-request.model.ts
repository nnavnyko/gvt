/**
 * Transportation Request model
 * Contains most of fields. Use this model only for Detail-View (heavy model)
 */
import { TransportationLocation } from './transportation-location.model';
import { SimpleEmployeeUser } from '../../../accounts/shared/models/simple-employee-user.model';
import { SimpleUser } from '../../../accounts/shared/models/simple-user.model';
import * as _ from 'lodash';
import {SimpleOrganisationModel} from "../../../organisations/shared/models/simple-organisation.model";


export class TransportationRequest {
    /**
     * Transportation Request Model Class
     */
    id: number;
    uuid: string;
    email: string;
    phone: string;
    isNeedPorters: boolean;
    routeLength: number;
    portersNumber: number;
    created: string;
    text: string;
    status: string;
    statusDisplay: string;
    date: string;
    time: string;
    createdBy: SimpleUser;
    assignee: SimpleEmployeeUser;
    assigneeID: number;
    points: TransportationLocation[];
    amount: number;
    carAmount: number;
    portersAmount: number;
    carAmountMax: number;
    portersAmountMax: number;
    actionID: number;
    discountAmount: number;
    discountAmountMin: number;
    discountAmountMax: number;
    finalCarAmount: number;
    finalPortersAmount: number;
    employeeComment: string;
    isHourlyRate: boolean;
    isCashlessPayment: boolean;
    organisationID: number;
    organisationUUID: string;
    organisation: SimpleOrganisationModel;

    constructor(obj?: any) {
        /**
         * Convert python-like object to TS format
         */
        if (obj) {
            this.id = obj.id;
            this.uuid = obj.uuid;
            this.email = obj.email;
            this.phone = obj.phone;
            this.isNeedPorters = obj.is_need_porters;
            this.routeLength = obj.route_length;
            this.portersNumber = obj.porters_number;
            this.created = obj.created;
            this.text = obj.text;
            this.status = obj.status;
            this.statusDisplay = obj.status_display;
            this.date = obj.date;
            this.time = obj.time;
            this.createdBy = obj.created_by ? new SimpleUser(obj.created_by) : null;
            this.assigneeID = obj.assignee_id;
            this.assignee = obj.assignee ? new SimpleEmployeeUser(obj.assignee) : null;
            this.amount = obj.amount;
            this.carAmount = obj.car_amount;
            this.portersAmount = obj.porters_amount;
            this.carAmountMax = obj.car_amount_max;
            this.portersAmountMax = obj.porters_amount_max;
            this.employeeComment = obj.employee_comment;
            this.isHourlyRate = obj.is_hourly_rate;
            this.isCashlessPayment = obj.is_cashless_payment;
            this.actionID = obj.action_id;
            this.discountAmount = obj.discount_amount;
            this.discountAmountMin = obj.discount_amount_min;
            this.discountAmountMax = obj.discount_amount_max;
            this.finalPortersAmount = obj.final_porters_amount;
            this.finalCarAmount = obj.final_car_amount;
            this.organisationID = obj.organisation_id;

            // Append points if need to
            let points:TransportationLocation[] = [];

            if (obj.points.length) {
                _.each(obj.points, point => {
                    points.push(new TransportationLocation(point));
                });
                points = points.sort((a, b) => {
                    return a.sortOrder - b.sortOrder;
                });
            }

            this.points = points;
            if (obj.organisation) {
                this.organisation = new SimpleOrganisationModel(obj.organisation);
            }
        }
    }

    finalAmount(): number {
        /**
         * Get final Amount
         */
        let amount = ((this.carAmount || 0) + (this.portersAmount || 0)) || 0;
        let maxAmount = (this.carAmountMax || this.carAmount || 0) +
            (this.portersAmountMax || this.portersAmount || 0);

        return maxAmount || amount;
    }

    fullAmount(): number {
        /**
         * Get full amount, if amount is not set;
         */
        let amount = this.amount || ((this.carAmount || 0) + (this.portersAmount || 0)) || 0;
        let discountAmount = this.discountAmount || this.discountAmountMin || 0;

        return amount - discountAmount;
    }

    fullAmountMax(): number {
        /**
         * Get full Max amount, if amount is not set;
         * If the TransportationRequest complete and there is final amount or no porters Max amount and no car amount Max
         * return 0.
         * @return: number
         */
        if (this.amount || !this.carAmountMax && !this.portersAmountMax) {
            return 0;
        }
        let amount: number;
        amount = ((this.carAmountMax || this.carAmount || 0) +
            (this.portersAmountMax || this.portersAmount || 0)) || 0;
        let discountAmount = this.discountAmount || this.discountAmountMax || this.discountAmountMin || 0;

        return amount - discountAmount;
    }

    /**
     * Check if the user is request owner
     * @param {User} user: current logged-in user
     **/
    isRequestOwner(user: any): boolean {
        return !!this.createdBy && this.createdBy.id === user.id;
    }
}
