/**
 * TransportationHistory service
 **/
import {Injectable} from "@angular/core";
import * as _ from 'lodash';

import {HTTPRequestsService} from "../../../utils/services/http-requests/http-requests.service";
import {TransportationAPIURLS} from "../../transportation.config";
import {TransportationHistory} from "../models/transportation-history.model";

@Injectable()
export class TransportationHistoryService {
    constructor(private http: HTTPRequestsService) {

    }
    /**
     * Load TransportationRequest history
     * @param {string} trRequestUUID: TransportationRequest.uuid
     * @return: array of TransactionHistoryObjects
     **/
    public loadHistory(trRequestUUID: string): Promise<TransportationHistory[]> {
        return this.http.get(TransportationAPIURLS.TRANSPORTATION_HISTORY.replace('{uuid}', trRequestUUID)).then(data => {
            let history: TransportationHistory[] = [];

            _.each(data, (h: any) => {
                history.push(new TransportationHistory(h));
            });

            return history;
        });
    }
}
