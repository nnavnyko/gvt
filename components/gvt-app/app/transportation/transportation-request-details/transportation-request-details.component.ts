/**
 * Transportation Request Details Component
 * Show Transportation Request's detailed information
 */


import {timer as observableTimer,  Subject } from 'rxjs';

import {switchMap} from 'rxjs/operators';
import { Component, OnInit, ViewChild, OnDestroy  } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { Title } from '@angular/platform-browser';
import { ActivatedRoute, Params }   from '@angular/router';

import { ModalDirective } from 'ngx-bootstrap/modal';
import { $WebSocket } from 'angular2-websocket/angular2-websocket';

import { TransportationConfig, TransportationAPIURLS } from '../transportation.config';
import { TransportationRequestsService } from '../shared/services/transportation-requests.service';
import { TransportationRequest } from '../shared/models/transportation-request.model';
import { AccountService } from '../../accounts/shared/services/account.service';
import { EmployeesService } from '../../accounts/shared/services/employees.service';
import { SimpleEmployeeUser } from '../../accounts/shared/models/simple-employee-user.model';
import { Config } from '../../settings/config';
import { NotificationsService } from '../../utils/services/notifications/notifications.service';
import { TransportationConstants } from '../transportation.config';
import { GVTWebSocketConfig } from '../../utils/services/websocket/websocket.config';
import { ActionsService } from '../../discounts/shared/services/actions.service';
import { Action } from '../../discounts/shared/models/action.model';
import * as _ from 'lodash';
import { HTTPRequestsService } from "../../utils/services/http-requests/http-requests.service";
import {TransportationHistory} from "../shared/models/transportation-history.model";
import {TransportationHistoryService} from "../shared/services/transportation-history.service";
import {DisableBtnDirective} from "../../utils/directives/disable-btn.directive";
import {TransportationLocation} from "../shared/models/transportation-location.model";


@Component({
    selector: '<transportation-request-details></transportation-request-details>',
    templateUrl: TransportationConfig.BASIC_MODULE_URL + 'transportation-request-details/transportation-request-details.html',
    providers: [EmployeesService, ActionsService, TransportationHistoryService, DisableBtnDirective]
})
export class TransportationRequestDetailsComponent implements OnInit, OnDestroy {
    /**
     * Transportation Request Details Component class
     * Load Transportation Request Details from server using Service
     */
    private trRequest: TransportationRequest;
    private trRequestUpdating: boolean;
    private finalCarAmount: number;
    private finalPortersAmount: number;
    private portersPayment: number;
    private carAmount: number;
    private portersAmount: number;
    private carAmountMax: number;
    private portersAmountMax: number;
    private isHourlyRate: boolean;
    private isCashlessPayment: boolean;
    private employeeComment: string;
    private actionsDiscountsList: Action[];
    private isEmployeeCommentEdit: boolean = false;
    @ViewChild('setPriceModalInstance', {static: false}) public setPriceModalInstance: ModalDirective;
    @ViewChild('assignRequestModalInstance', {static: false}) public assignRequestModalInstance: ModalDirective;
    @ViewChild('completeRequestModalInstance', {static: false}) public completeRequestModalInstance: ModalDirective;
    private employeesList: SimpleEmployeeUser[] = [];
    private finishedStatuses: string[] = ['cancelled', 'paid'];
    private inProgressStatuses: string[] = ['in_progress', 'on_the_road'];
    private TransportationConstants:any = TransportationConstants;
    private driverLocationTimer: any;
    private driverLocationTimerSubscription: any;
    private driverLocation: number[] = [];
    private loadingComplete: boolean = false;
    private history: TransportationHistory[] = [];
    private historyTrStateActions: string[] = [TransportationConstants.ACTION_TYPES.ChangePrice, TransportationConstants.ACTION_TYPES.Created];

    public navigatePointsEvents: Subject<any> = new Subject<any>();


    private ws: $WebSocket;

    constructor(private route: ActivatedRoute, private requestsService: TransportationRequestsService,
                private title: Title, public account: AccountService,
                private employeesService: EmployeesService, private notifications: NotificationsService,
                private translate: TranslateService, private actionsService: ActionsService,
                private http: HTTPRequestsService, private trHistoryService: TransportationHistoryService
    ) {
        this.translate.get('Детали Заявки').subscribe((res:string) => {
            this.title.setTitle(res);
        });
    }

    /**
     * Initialize Component
     * Load TransportationRequest,
     */
    ngOnInit():void {
        this.route.params.pipe(
            switchMap((params: Params) => this.requestsService
                .getTransportationRequestDetails(params['uuid'])))
            .subscribe(data => {
                this.trRequest = data;
                this.updateHistory();
                this.loadingComplete = true;
                this.account.onUserLoaded(() => {
                    let path: string = Config.getWSHost() + Config.TR_REQUEST_WS_URL + '/' +
                        this.trRequest.uuid + '/' + this.account.user.username;

                    this.ws = new $WebSocket(
                        path,
                        null,
                        new GVTWebSocketConfig()
                    );
                    let $this = this;
                    this.ws.onMessage(
                        (msg: any)=> {
                            let messages: object = {
                                'tr_changed': {
                                    'title': 'Заказ изменен',
                                    'description': 'Заказ был изменен сотрудником'
                                },
                                'price_changed': {
                                    'title': 'Цена изменена',
                                    'description': 'Цена заказа была изменена'
                                },
                                'delivered': {
                                    'title': 'Заказ доставлен',
                                    'description': 'Заказ был доставлен'
                                },
                                'cancelled': {
                                    'title': 'Заказ отменен',
                                    'description': 'Заказ был отменен'
                                },
                                'default': {
                                    'title': 'Заказ изменен',
                                    'description': 'Заказ был изменен сотрудником'
                                }
                            };
                            $this.requestsService.getTransportationRequestDetails(this.trRequest.uuid).then(function(data){
                                $this.trRequest = data;
                                $this.toggleDriverTracking();
                                $this.updateHistory();
                                let message = messages[msg.data] || messages['default'];
                                $this.notifications.showInfoMessage(message.title, message.description, 3000);
                            });
                        },
                        {autoApply: false}
                    );
                });


                if (this.inProgressStatuses.indexOf(this.trRequest.status) > -1) {
                    this.startTrackDriverLocation();
                }
            });

        let $this: any = this;
        this.account.onUserLoaded(() => {
            $this.loadEmployeeList();
            $this.loadActionsDiscountsList();
        });
    }

    /**
     * Toggle tracking Driver's location
     **/
    private toggleDriverTracking():void {
        if (this.inProgressStatuses.indexOf(this.trRequest.status) > -1 && !this.driverLocationTimer) {
            this.startTrackDriverLocation();
        } else if (this.inProgressStatuses.indexOf(this.trRequest.status) < 0) {
            this.stopTrackDriverLocation();
        }
    }

    /**
     * Start tracking Driver's location. Get driver location and move Driver icon
     * @throws {Error} - if timer is already created
     **/
    private startTrackDriverLocation(): void {
        if (this.driverLocationTimer) {
            return;
        }
        // let index:number = 0;
        this.driverLocationTimer = observableTimer(1000, 10000);
        this.driverLocationTimerSubscription = this.driverLocationTimer.subscribe(() => {
            this.http.get(
                TransportationAPIURLS.DRIVER_GEOLOCATION.replace('{uuid}', this.trRequest.uuid), null, true
            ).then((resp) => {
                if (resp && resp.geolocation) {
                    this.driverLocation = [resp.geolocation.latitude, resp.geolocation.longitude];
                }
            });
        });
    }

    /**
     * Stop to track Driver's location. Remove timer if it exists
     **/
    private stopTrackDriverLocation(): void {
        this.driverLocation = [null, null];
        if (this.driverLocationTimer && this.driverLocationTimerSubscription) {
            this.driverLocationTimerSubscription.unsubscribe();
            this.driverLocationTimer = null;
        }
    }

    /**
     * Load actions discounts list if user has permissions
     */
    private loadActionsDiscountsList(): void {
        if (this.account.isManagerOrDriver()) {
            this.actionsService.getActionsList().then((data:any) => {
                this.actionsDiscountsList = data;
            });
        }
    }

    /**
     * Load employee list if the user has permissions
     */
    private loadEmployeeList(): void {
        if (this.account.isManagerOrDriver()) {
            this.employeesService.getEmployeeList().then(employeesList => {
                this.employeesList = employeesList;
            });
        }
    }

    /**
     * Apply Action Discount
     *
     * @param actionID: number or undefined
     * @param clearAction: boolean or undefined
     */
    applyActionDiscount(actionID?: number, clearAction?: boolean): any {
        return () => {
            let actionIDParam: number = actionID ? actionID : +this.trRequest.actionID;
            if (clearAction) {
                actionIDParam = null;
            }
            this.trRequest.actionID = actionIDParam;
            let selectedAction = _.find(this.actionsDiscountsList, {'id': actionIDParam});

            this.actionsService.applyActionDiscount(this.trRequest.uuid, selectedAction ? selectedAction.uuid : null)
                .then((data: any) => {
                if (data.uuid) {
                    this.trRequest = data;
                }
            });
        }
    }

    /**
     * Change Transportation Request Status
     * @param {string} type: string (one of ['assign', 'cancel', 'finish'])
     * @param {boolean} [immediately]: true if need to execute immediately
     * @return: function
     */
    changeStatus(type: string, immediately?: boolean): any {
        let func: any = () => {
            this.trRequestUpdating = true;

            this.requestsService.changeTransportationRequestStatus(
                this.trRequest.uuid, type).then(data => {
                    this.trRequest = data;
                    this.updateHistory();
                    this.trRequestUpdating = false;
                    this.toggleDriverTracking();
            }, error => { this.trRequestUpdating = false; });
        };

        if (!immediately) {
            return func;
        } else {
            func();
        }
    }

    /**
     * Show Complet Transportation Modal
     */
    showCompleteTransportationRequestModal():void {
        this.finalCarAmount = this.trRequest.carAmountMax || this.trRequest.carAmount;
        this.finalPortersAmount = this.trRequest.portersAmountMax || this.trRequest.portersAmount;
        this.portersPayment = 0;
        this.completeRequestModalInstance.show();
    }

    /**
     * Finish transaction (change status to 'delivered' and set final amount)
     * @return: function
     */
    completeTransportationRequest(): any {

        return () => {
            this.trRequestUpdating = true;

            this.requestsService.changeTransportationRequestStatus(
                this.trRequest.uuid, 'finish', {
                    'final_car_amount': this.finalCarAmount,
                    'final_porters_amount': this.finalPortersAmount,
                    'porters_payment': this.portersPayment
                }).then(data => {
                    this.trRequest = data;
                    this.updateHistory();
                    this.trRequestUpdating = false;
                    this.completeRequestModalInstance.hide();
                    this.toggleDriverTracking();
            }, error => { this.trRequestUpdating = false; });
        }
    }

    /**
     * Assign user if the account.user has permissions
     * @return: function
     */
    assignUser(): any {
        this.trRequestUpdating = true;

        this.requestsService.assignTransportationRequest(
            this.trRequest.uuid, this.trRequest.assigneeID).then(data => {
                this.trRequest = data;
                this.updateHistory();
                this.trRequestUpdating = false;
                this.assignRequestModalInstance.hide();
        }, error => { this.trRequestUpdating = false; });
    }

    /**
     * Set cat and porters amounts and show Modal
     */
    showSetPriceModal(): void {
        this.carAmount = this.trRequest.carAmount;
        this.portersAmount = this.trRequest.portersAmount;
        this.carAmountMax = this.trRequest.carAmountMax;
        this.portersAmountMax = this.trRequest.portersAmountMax;
        this.isHourlyRate = this.trRequest.isHourlyRate;
        this.isCashlessPayment = this.trRequest.isCashlessPayment;
        this.setPriceModalInstance.show();
    }

    /**
     * Assign user if the account.user has permissions
     * @return: function
     */
    setTransportationRequestPrice(): any {
        return () => {
            this.trRequestUpdating = true;

            this.requestsService.setTransportationRequestPrice(this.trRequest.uuid, this.carAmount,
                this.portersAmount, this.carAmountMax, this.portersAmountMax,
                this.isHourlyRate, this.isCashlessPayment).then(data => {
                    this.trRequest = data;
                    this.updateHistory();
                    this.trRequestUpdating = false;
                    this.setPriceModalInstance.hide();
            }, error => { this.trRequestUpdating = false; });
        }
    }

    /**
     * Pre-define employee comment to edit
     */
    editEmployeeComment(): void {
        this.isEmployeeCommentEdit = true;
        this.employeeComment = this.trRequest.employeeComment;

    }

    /**
     * Add employee comment
     * @return: function
     */
    addEmployeeComment(): any {
        this.trRequestUpdating = true;

        this.requestsService.addEmployeeComment(this.trRequest.uuid, this.employeeComment).then(data => {
                this.trRequest = data;
                this.updateHistory();
                this.trRequestUpdating = false;
                this.isEmployeeCommentEdit = false;
        }, error => { this.trRequestUpdating = false; });
    }

    /**
     * Handle Destroy component event
     * Close WebSocket connection
     */
    ngOnDestroy(): void {
        this.stopTrackDriverLocation();
        try {
            this.ws.close(true);
        } catch (err) {
            console.dir(err);
        }
    }

    /**
     * Check if the discount can be applied
     *
     * @return: boolean
     */
    canApplyDiscount():boolean {
        return !!(this.account && this.account.isManagerOrDriver() && this.finishedStatuses.indexOf(this.trRequest.status) < 0 &&
            this.actionsDiscountsList && this.actionsDiscountsList.length && this.trRequest.fullAmount() &&
            this.trRequest.status !== 'delivered');
    }

    /**
     * Update TransportationRequest history
     */
    private updateHistory(): void {
        this.trHistoryService.loadHistory(this.trRequest.uuid).then((data: any) => {
            this.history = data;
        });
    }

    private navigateToPoint(location: TransportationLocation): void {
        this.navigatePointsEvents.next(location);
    }

}

@Component({
    selector: '<organisation-transportation-request-details></organisation-transportation-request-details>',
    templateUrl: TransportationConfig.BASIC_MODULE_URL + 'transportation-request-details/transportation-request-details.html',
    providers: [EmployeesService, ActionsService, TransportationHistoryService]
})
export class OrganisationTransportationRequestDetailsComponent extends TransportationRequestDetailsComponent {
    private isOrganisation: boolean = true;
}
