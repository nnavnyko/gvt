
import {switchMap} from 'rxjs/operators';
/**
 * Create Transportation Request Component
 */
import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { Title } from '@angular/platform-browser';

import { ActivatedRoute, Router, Params } from '@angular/router';
import * as _ from 'lodash';

import { TransportationConfig } from '../transportation.config';
import { TransportationRequestsService } from '../shared/services/transportation-requests.service';
import { TransportationRequest } from '../shared/models/transportation-request.model';
import { TransportationLocation } from '../shared/models/transportation-location.model';
import { AccountService } from '../../accounts/shared/services/account.service';
import { CommonConstants } from '../../utils/constants/common';


@Component({
    selector: '<update-transportation-request></update-transportation-request>',
    templateUrl: TransportationConfig.BASIC_MODULE_URL + 'transportation-request-update/transportation-request-update.html'
})
export class UpdateTransportationRequestComponent implements OnInit {
    /**
     * Create Transportation Request Component Class
     */
    organisationUUID: string = null;
    private trRequest: TransportationRequest;
    private formSubmitted: boolean;
    private portersAmountInitialized: boolean = false;
    private phoneMask: any[] = CommonConstants.PHONE_MASK;
    private timeMask: any[] = CommonConstants.TIME_MASK;
    private portersNumbers:any[] = [
        {'value': 1, display: '1 грузчик'},
        {'value': 2, display: '2 грузчика'},
        {'value': 3, display: '3 грузчика'},
        {'value': 4, display: '4 грузчика'},
        {'value': 5, display: '5 грузчиков'},
        {'value': 6, display: '6 грузчиков'},
        {'value': 7, display: '7 грузчиков'},
        {'value': 8, display: '8 грузчиков'}
    ];

    constructor(private account: AccountService, private requestsService: TransportationRequestsService,
                private title: Title, private router: Router, private route: ActivatedRoute,
                private translate: TranslateService) {
        this.translate.get('Оформление заказа').subscribe((res:string) => {
            this.title.setTitle(res);
        });
        this.formSubmitted = false;
    }

    ngOnInit():void {
        /**
         * Initialize Transportation Request
         * Predefine email and phone using current session user information
         */
        let $this: any = this;
        this.account.onUserLoaded(() => {
            $this.setInitialData();
        });

        _.each(this.portersNumbers, (portersNumber:any) => {
            this.translate.get(portersNumber['display']).subscribe((res:string) => {
                portersNumber['display'] = res;
            });
        });
    }

    portersAmountChanged(value: number): void {
        /**
         * Set isNeedPorters to true if porters Amount changed
         * @param value: number or undefined
         */
        this.trRequest.portersAmount = value;

        if (value && !this.portersAmountInitialized) {
            this.portersAmountInitialized = true;
            this.trRequest.isNeedPorters = true;
            this.trRequest.portersNumber = 2;
        }
    }

    canSetPriceFields(): boolean {
        /** Check if the employee can set price fields
         * The user can't set price fields if he is not an employee and if request already created **/
        return this.account.isManagerOrDriver() && (!this.trRequest || !this.trRequest.uuid);
    }

    setInitialData(): void {
        /**
         * Set initial Transportation Request data
         * Set phone and user email, if the user is not an employee
         */
        let defaultPoints = [
            new TransportationLocation({'sort_order': 1}),
            new TransportationLocation({'sort_order': 2})
        ];

        // Check if TrRequest is in Organisation details scope
        if (this.route.parent && this.route.parent.params) {
            this.route.parent.params.pipe(switchMap((params: Params) => {
                return new Promise((resolve, reject) => {
                    resolve(params['org_uuid'])
                });
            })).subscribe((orgUUID: string) => {
                this.organisationUUID = orgUUID;
            });
        }

        this.route.params.pipe(switchMap((params: Params) => {
            if (params['uuid']){
                return this.requestsService.getTransportationRequestDetails(params['uuid']);
            } else {
                let trRequest:TransportationRequest = new TransportationRequest();
                trRequest.points = defaultPoints;
                if (!this.account.isManagerOrDriver()) {
                    trRequest.phone = this.account.user.userCard.phoneNumber;
                    trRequest.email = this.account.user.email;
                }
                let today = new Date();
                trRequest.date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();

                return new Promise((resolve, reject) => {
                    return resolve(trRequest);
                });
            }
        })).subscribe(data => {
            this.trRequest = data;
            if (!this.trRequest.points.length) {
                this.trRequest.points = defaultPoints;
            }
        });
    }

    submitForm(): any {
        /**
         * Create Transportation Request form
         * @returns: function
         */
        return () => {
            this.formSubmitted = true;
            // Convert date and time to strings
            this.trRequest.date = this.trRequest.date ? this.trRequest.date + '' : null;
            this.trRequest.time = this.trRequest.time ? this.trRequest.time + '' : null;
            this.trRequest.organisationUUID = this.organisationUUID;
            
            if (this.trRequest.uuid) {
                this.requestsService.createOrEditTransportationRequest(this.trRequest)
                    .then(data => {
                        this.router.navigate(this.getSuccessUrl(data.uuid));
                        this.formSubmitted = false;
                    }, err => {
                        console.dir(err);
                        this.formSubmitted = false;
                    });
            } else {
                this.requestsService.createOrEditTransportationRequest(this.trRequest)
                    .then(data => {
                        this.router.navigate(this.getSuccessUrl(data.uuid));
                        this.formSubmitted = false;
                    }, err => {
                        console.dir(err);
                        this.formSubmitted = false;
                    });
            }
        };
    }

    /**
     * Get success URL to redirect user
     */
    getSuccessUrl(uuid: string): string[] {
        return ['transportation', 'requests', uuid];
    }

    isAddressesValid(): boolean {
        /**
         * Check if addresses valid
         * All addresses must be filled
         */
        return this.trRequest && this.trRequest.points && this.trRequest.points.filter(point => !!point.address).length === this.trRequest.points.length;
    }
}

@Component({
    selector: '<update-organisation-transportation-request></update-organisation-transportation-request>',
    templateUrl: TransportationConfig.BASIC_MODULE_URL + 'transportation-request-update/transportation-request-update.html',
})
export class UpdateOrganisationTransportationRequestComponent extends UpdateTransportationRequestComponent {
    private isOrganisation: boolean = true;

    /**
     * Get success URL to redirect user
     */
    getSuccessUrl(uuid: string): string[] {
        return ['organisations', this.organisationUUID, 'orders', uuid];
    }
}