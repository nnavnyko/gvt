/**
 * Transportation requests list component
 * Load list and show table
 */


import {switchMap} from 'rxjs/operators';
import { Component, OnInit, Input } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import * as _ from 'lodash';
import { Title } from '@angular/platform-browser';
import { ActivatedRoute, Params } from '@angular/router';

import { PaginationConfig } from 'ngx-bootstrap/pagination';
import { LocalStorageService } from 'ngx-store';

import { SimpleTransportationRequest } from '../shared/models/simple-transportation-request.model';
import { TransportationRequestsService } from '../shared/services/transportation-requests.service';
import { TransportationAPIURLS, TransportationConfig } from '../transportation.config';
import { EmployeesService } from '../../accounts/shared/services/employees.service';
import { AccountService } from '../../accounts/shared/services/account.service';
import { SimpleEmployeeUser } from '../../accounts/shared/models/simple-employee-user.model';
import { TransportationConstants } from '../transportation.config';
import { CommonConstants } from '../../utils/constants/common';

// Constant to get list URL depending on the List Type in url parameters
const LIST_TYPE = {
    'all': TransportationAPIURLS.REQUESTS_LIST,
    'my': TransportationAPIURLS.MY_REQUESTS_LIST,
    'assigned': TransportationAPIURLS.MY_ASSIGNED_REQUESTS_LIST
};
let LIST_NAME: object = {
    'all': 'Все Заказы',
    'my': 'Мои Заказы',
    'assigned': 'Назначенные На Меня'
};

const FILTRATION_KEY = 'trRequestsListFiltration';

const LIST_TYPES = {
    all: 'all',
    my: 'my',
    assigned: 'assigned'
};

@Component({
    selector: '<transportation-requests-list></transportation-requests-list>',
    templateUrl: TransportationConfig.BASIC_MODULE_URL + 'transportation-requests-list/transportation-requests-list.html',
    providers: [EmployeesService, PaginationConfig]
})
export class TransportationRequestsListComponent implements OnInit {
    /**
     * Transportation Requests List Component
     */
    private trRequests: SimpleTransportationRequest[];
    private totalItems: number;
    private currentPage: number;
    private filterParams: string;
    private listType: string;
    private listTypeUrl: string;
    private listName: string;
    private filterStatus: string;
    private isCashlessPayment: boolean;
    private filterDateStart: string;
    private filterDateEnd: string;
    private filterSearch: string;
    private filterEmployeeID: string = "";
    private isFiltrationCollapsed: boolean = true;
    private loadingComplete: boolean = false;
    private employeesList: SimpleEmployeeUser[] = [];
    private TransportationConstants:any = TransportationConstants;

    private statuses: any[] = CommonConstants.TRANSPORTATION_REQUESTS_STATUSES;

    private a2eOptions: Object = {
        // other options...
        format: 'YYYY-MM-DD HH:mm',
    };
    private LIST_TYPES: any = LIST_TYPES;

    constructor(private route: ActivatedRoute, private requestsService: TransportationRequestsService,
                private title: Title, private employeesService: EmployeesService,
                private account: AccountService, private translate: TranslateService,
                private localStorage: LocalStorageService){
        this.translate.get('Заявки На Перевозку').subscribe((res:string) => {
            this.title.setTitle(res);
        });

        this.listTypeUrl = this.listTypeUrl || LIST_TYPE.my;
    }

    /** Store filtration */
    storeFiltration(): any {

        let filtration: any = {
            currentPage: this.currentPage || 1,
            filterStatus: this.filterStatus || '',
            filterDateStart: this.filterDateStart ? (this.filterDateStart + '').split('T')[0] : '',
            filterDateEnd: this.filterDateEnd ? (this.filterDateEnd + '').split('T')[0] : '',
            filterSearch: this.filterSearch,
            filterEmployeeID: this.filterEmployeeID,
            isCashlessPayment: this.isCashlessPayment
        };
        this.localStorage.set(FILTRATION_KEY + this.listType, filtration);

        return filtration;
    }

    /** Initialize filtration */
    setFiltration(filtration: any): void {
        this.currentPage = filtration.currentPage;
        this.filterStatus = filtration.filterStatus;
        this.filterDateStart = filtration.filterDateStart;
        this.filterDateEnd = filtration.filterDateEnd;
        this.filterSearch = filtration.filterSearch;
        this.filterEmployeeID = filtration.filterEmployeeID;
        this.isCashlessPayment = filtration.isCashlessPayment;

        this.setFilterParams(this.filterStatus, this.filterDateStart,
            this.filterDateEnd, this.filterSearch, this.filterEmployeeID, this.isCashlessPayment);
    }

    ngOnInit(): void {
        this.route.params.pipe(switchMap((params: Params) => {
            this.listType = params['listType'];

            this.listTypeUrl = LIST_TYPE[this.listType] || LIST_TYPE.my;
            this.listName = LIST_NAME[this.listType];

            let filtration: any = this.localStorage.get(FILTRATION_KEY + this.listType);
            if (!filtration) {
                filtration = this.storeFiltration();
            }
            this.setFiltration(filtration);

            return this.requestsService.loadList((this.listTypeUrl) + '?page_size=25&page=' + this.currentPage + this.filterParams);
        })).subscribe(data => {
            this.trRequests = data.list;
            this.totalItems = data.totalItems;
            this.loadingComplete = true;
        });

        let $this: any = this;
        this.account.onUserLoaded(() => {
            $this.loadEmployeeList();
        });

        _.each(this.statuses, (status:any) => {
            this.translate.get(status['display']).subscribe((res:string) => {
                status['display'] = res;
            });
        });
    }


    /**
     * Load employee list if the user has permissions
     */
    private loadEmployeeList(): void {
        if (this.account.isManager()) {
            this.employeesService.getEmployeeList().then(employeesList => {
                this.employeesList = employeesList;
            });
        }
    }

    /**
     * Handle change page event
     * @param {object} event: Page changed event
     */
    pageChanged(event: any): void {
        this.loadList(event.page);
    }

    /**
     * Load paged list
     * @param {number} page: current page to load
     */
    loadList(page: number): void {
        this.requestsService.loadList((this.listTypeUrl || LIST_TYPE.my) + '?page_size=25&page=' + page + this.filterParams).then(data => {
            this.totalItems = data.totalItems;
            this.trRequests = data.list;
            this.storeFiltration();
            this.loadingComplete = true;
        });
    }

    /**
     * Filter list by Created Date or Status
     * Reload list after Status Filter changed
     * @param {string} status: string or null
     */
    filterByStatus(status: any): void {
        this.setFilterParams(status, this.filterDateStart,
            this.filterDateEnd, this.filterSearch, this.filterEmployeeID, this.isCashlessPayment);

        this.applyFiltration();
    }

    /**
     * Filter list by Created Date or Status
     * Reload list after Status Filter changed
     * @param {string} employeeID: string or null
     */
    filterByEmployeeID(employeeID: any): void {
        this.setFilterParams(this.filterStatus, this.filterDateStart,
            this.filterDateEnd, this.filterSearch, employeeID, this.isCashlessPayment);

        this.applyFiltration();
    }

    /**
     * Update list after filtration applied
     */
    applyFiltration(): void {
        this.currentPage = 1;

        this.loadList(1);
    }

    /**
     * Set filtration
     * @param {string} filterStatus: status of TransportationRequest to filter
     * @param {string} filterDateStart: filter TransportationRequest since this date
     * @param {string} filterDateEnd: filter TransportationRequest until this date
     * @param {string} filterSearch: search by Email or Phone
     * @param {string} filterEmployeeID: filter by Employee
     * @param {boolean} isCashlessPayment: filter only cachless payments
     */
    setFilterParams(filterStatus: string, filterDateStart: string, filterDateEnd: string,
                    filterSearch: string, filterEmployeeID: string, isCashlessPayment: boolean): void {
        let filterParams = '';
        let filterArr:any[] = [
            {key: 'status', value: filterStatus},
            {key: 'date__gte', value: filterDateStart},
            {key: 'date__lte', value: filterDateEnd},
            {key: 'search', value: filterSearch},
            {key: 'assignee_id', value: filterEmployeeID},
            {key: 'is_cashless_payment', value: isCashlessPayment}
        ];
        _.each(filterArr, filterParam => {
            if (filterParam.value && filterParam.value !== '') {
                filterParams += '&' + filterParam.key + '=' + filterParam.value;
            }
        });
        this.filterParams = filterParams;
    }

    /**
     * Filter by date: from start date to end date
     * Apply filtration by 'created' property
     * @param {string} filterDateStart: string (date key, start or end)
     * @param {string} filterDateEnd: string (date key, start or end)
     */
    filterByDate(filterDateStart:string, filterDateEnd:string):void {
        filterDateStart = filterDateStart || null;
        filterDateEnd = filterDateEnd || null;
        this.setFilterParams(this.filterStatus, filterDateStart,
            filterDateEnd, this.filterSearch, this.filterEmployeeID, this.isCashlessPayment);

        this.applyFiltration();
    }

    /**
     * Filter by search string
     * Search in phone and email fields
     */
    filterBySearch():void {
        this.setFilterParams(this.filterStatus, this.filterDateStart,
            this.filterDateEnd, this.filterSearch, this.filterEmployeeID, this.isCashlessPayment);

        this.applyFiltration();
    }

    /**
     * Filter By Cashless Payment
     * If cashless payment checkbox inactive, then show all requests
     */
    filterByCashlessPayment(isCashlessPayment: boolean): void {
        this.setFilterParams(this.filterStatus, this.filterDateStart,
            this.filterDateEnd, this.filterSearch, this.filterEmployeeID, isCashlessPayment);

        this.applyFiltration();
    }
}
