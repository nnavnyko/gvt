/**
 * Transportation Router
 */

import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TransportationRequestsListComponent } from './transportation-requests-list/transportation-requests-list.component';
import { TransportationRequestDetailsComponent } from './transportation-request-details/transportation-request-details.component';
import { UpdateTransportationRequestComponent } from './transportation-request-update/transportation-request-update.component';

const routes: Routes = [
    {
        path: 'transportation',
        children: [
            {
                path: 'requests',
                children: [
                    {
                        path: 'list/:listType',
                        component: TransportationRequestsListComponent
                    },
                    {
                        path: 'create',
                        component: UpdateTransportationRequestComponent
                    },
                    {
                        path: ':uuid',
                        component: TransportationRequestDetailsComponent
                    },
                    {
                        path: ':uuid/edit',
                        component: UpdateTransportationRequestComponent
                    }
                ]
            }
        ]
    }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class TransportationRoutingModule {}
