/**
 * Transportation Module Configuration
 */
import { Config } from '../settings/config';

export class TransportationConfig {
    /**
     * Transportation Module Config
     */
    public static BASIC_API_URL = '/transportation/api/';
    public static BASIC_MODULE_URL = Config.APP_URL + 'transportation/';
}

export class TransportationAPIURLS {
    /**
     * Transportation API URLs config
     */
    // List URLs
    public static REQUESTS_LIST = TransportationConfig.BASIC_API_URL + 'requests';
    public static MY_REQUESTS_LIST = TransportationConfig.BASIC_API_URL + 'requests/my';
    public static MY_ASSIGNED_REQUESTS_LIST = TransportationConfig.BASIC_API_URL + 'requests/my/assigned';
    public static ORGANISATION_REQUESTS_LIST = TransportationConfig.BASIC_API_URL + 'requests/organisation/{uuid}';

    // Transportation Request Details URLs
    public static REQUEST_DETAILS = TransportationConfig.BASIC_API_URL + 'requests/{uuid}/';
    public static CHANGE_REQUEST_STATUS = TransportationConfig.BASIC_API_URL + 'requests/{uuid}/change_status/';
    public static ASSIGN_REQUEST = TransportationConfig.BASIC_API_URL + 'requests/{uuid}/assign/';
    public static SET_REQUEST_PRICE = TransportationConfig.BASIC_API_URL + 'requests/{uuid}/set_price/';
    public static ADD_EMPLOYEE_COMMENT = TransportationConfig.BASIC_API_URL + 'requests/{uuid}/add_employee_comment/';
    public static DRIVER_GEOLOCATION = TransportationConfig.BASIC_API_URL + 'requests/{uuid}/driver_geolocation';

    public static CREATE_REQUEST = TransportationConfig.BASIC_API_URL + 'requests/create/';
    
    public static TRANSPORTATION_STATISTICS = TransportationConfig.BASIC_API_URL + 'statistics';
    public static TRANSPORTATION_HISTORY = TransportationConfig.BASIC_API_URL + 'request/{uuid}/history';

    public static TRANSPORTATION_PERSON_STATISTICS = TransportationConfig.BASIC_API_URL + 'person-statistics';
    public static TRANSPORTATION_ASSIGNED_STATISTICS = TransportationConfig.BASIC_API_URL + 'employee-statistics';
    public static TRANSPORTATION_ORGANISATION_STATISTICS = TransportationConfig.BASIC_API_URL + 'organisation-statistics/{uuid}';
    public static TRANSPORTATION_EMPLOYEE_STATISTICS = TransportationConfig.BASIC_API_URL + 'employee-statistics/{username}';
}

export class TransportationConstants {
    /**
     * Transportation Constants
     */
    public static AVAILABLE_STATUSES = ['delivered', 'in_progress', 'new', 'accepted', 'on_the_road', 'paid'];
    public static ACTION_TYPES = {
        'ChangePrice': 'change_price',
        'Created': 'created',
        'Assigned': 'assigned',
    };
}
