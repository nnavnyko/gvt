/**
 * Transportation Main Module
 */

import { NgModule } from '@angular/core';

import { TransportationRoutingModule } from './transportation-router.module';
import { TransportationRequestsListComponent } from './transportation-requests-list/transportation-requests-list.component';
import { UpdateTransportationRequestComponent } from './transportation-request-update/transportation-request-update.component';
import {
    OrganisationTransportationRequestDetailsComponent,
    TransportationRequestDetailsComponent
} from './transportation-request-details/transportation-request-details.component';
import { YandexMapRoutesModule } from '../utils/directives/yandex-map-routes/yandex-map-routes.module';
import { TextMaskModule } from 'angular2-text-mask';
import { CollapseModule } from 'ngx-bootstrap/collapse';
import { SharedModule } from '../shared.module';
import {MapBoxRoutesEditModule} from "../utils/directives/mapbox-map/mapbox-routes-edit/mapbox-routes-edit.module";
import {MapBoxRoutesModule} from "../utils/directives/mapbox-map/mapbox-routes/mapbox-routes.module";
import {DisableBtnDirective} from "../utils/directives/disable-btn.directive";
import {UpdateOrganisationTransportationRequestComponent} from "./transportation-request-update/transportation-request-update.component";


@NgModule({
    imports: [
        CollapseModule, TextMaskModule, TransportationRoutingModule,
        SharedModule, YandexMapRoutesModule, MapBoxRoutesEditModule, MapBoxRoutesModule
    ],
    declarations: [TransportationRequestsListComponent, TransportationRequestDetailsComponent,
        UpdateTransportationRequestComponent, OrganisationTransportationRequestDetailsComponent,
        DisableBtnDirective, UpdateOrganisationTransportationRequestComponent]
})
export class TransportationModule {}
