/**
 * Common Constants
 */

export class CommonConstants {
    /**
     * Common Constants Class
     */
    public static PHONE_MASK = ['(', /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, '-', /\d/, /\d/];
    public static FULL_PHONE_MASK = ['+', /\d/, /\d/, /\d/, '(', /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, '-', /\d/, /\d/];
    public static TIME_MASK = [/\d/, /\d/, ':', /\d/, /\d/,];
    public static TRANSPORTATION_REQUESTS_STATUSES:any[] = [
        {'value': '', 'display': 'Выбрать Статус'},
        {'value': 'new', 'display': 'Новый'},
        {'value': 'accepted', 'display': 'Принят'},
        {'value': 'on_the_road', 'display': 'В Пути'},
        {'value': 'in_progress', 'display': 'В Процессе'},
        {'value': 'delivered', 'display': 'Доставлен'},
        {'value': 'paid', 'display': 'Оплачен'},
        {'value': 'cancelled', 'display': 'Отменен'}
    ];

    public static ORGANISATION_STATUSES: any[] = [
        {'value': '', 'display': 'Выбрать Статус'},
        {'value': 'active', 'display': 'Активна'},
        {'value': 'disabled', 'display': 'Неактивна'}
    ];
}
