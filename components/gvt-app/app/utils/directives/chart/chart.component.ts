/**
 * Chart Component
 *
 * Display chart. You need to pass data in specific format
 **/
import {Component, AfterViewInit, Input} from "@angular/core";
import {Config} from "../../../settings/config";
import {CommonUtil} from "../../services/common.util";
import * as _ from 'lodash';


declare let $: any;
declare let Chart: any;


@Component({
    selector: '[chart]',
    templateUrl: Config.APP_URL + 'utils/directives/chart/chart.html',
    providers: [CommonUtil]
})
export class ChartComponent implements AfterViewInit {
    @Input() data: object[];
    @Input() color?: string = 'rgba(60,141,188,0.9)';
    private uuid: string = CommonUtil.generateUUID();

    constructor() {}

    /**
     * Initialize chart after component initialized
     **/
    ngAfterViewInit(): void {
        let chartOptions:object = ChartComponent.generateChartOptions();
        let chartData:object = this.generateChartData();

        let chartCanvas = $('#chart-' + this.uuid).get(0).getContext('2d');
        let userChart = new Chart(chartCanvas);

        userChart.Line(chartData, chartOptions);
    }


    /**
     * Generate chart data, using 'data' from Input
     * @return {object}:
     **/
    private generateChartData(): object {
        let labels: string[] = [],
            data: number[] = [];

        _.each(this.data, (d:any) => {
            labels.push(d.month);
            data.push(d.orders_count);
        });

        return {
            labels: labels,
            datasets: [
                {
                    label: 'Digital Goods',
                    fillColor: this.color,
                    strokeColor: 'rgba(60,141,188,0.8)',
                    pointColor: '#3b8bba',
                    pointStrokeColor: 'rgba(60,141,188,1)',
                    pointHighlightFill: '#fff',
                    pointHighlightStroke: 'rgba(60,141,188,1)',
                    data: data
                }
            ]
        };
    }

    /** Generate chart options **/
    private static generateChartOptions(): object {

        return {
            //Boolean - If we should show the scale at all
            showScale: true,
            //Boolean - Whether grid lines are shown across the chart
            scaleShowGridLines: false,
            //String - Colour of the grid lines
            scaleGridLineColor: 'rgba(0,0,0,.05)',
            //Number - Width of the grid lines
            scaleGridLineWidth: 1,
            //Boolean - Whether to show horizontal lines (except X axis)
            scaleShowHorizontalLines: true,
            //Boolean - Whether to show vertical lines (except Y axis)
            scaleShowVerticalLines: true,
            //Boolean - Whether the line is curved between points
            bezierCurve: true,
            //Number - Tension of the bezier curve between points
            bezierCurveTension: 0.3,
            //Boolean - Whether to show a dot for each point
            pointDot: false,
            //Number - Radius of each point dot in pixels
            pointDotRadius: 4,
            //Number - Pixel width of point dot stroke
            pointDotStrokeWidth: 1,
            //Number - amount extra to add to the radius to cater for hit detection outside the drawn point
            pointHitDetectionRadius: 20,
            //Boolean - Whether to show a stroke for datasets
            datasetStroke: true,
            //Number - Pixel width of dataset stroke
            datasetStrokeWidth: 2,
            //Boolean - Whether to fill the dataset with a color
            datasetFill: true,
            //String - A legend template
            legendTemplate: '<ul class="<%=name.toLowerCase()%>-legend"><% for (var i=0; i<datasets.length; i++){%><li><span style="background-color:<%=datasets[i].lineColor%>"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>',
            //Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
            maintainAspectRatio: true,
            //Boolean - whether to make the chart responsive to window resizing
            responsive: true
        };

    }

}
