/**
 * Animated collapse module
 **/
import {NgModule} from "@angular/core";
import {AnimatedCollapse} from "./animated-collapse";

@NgModule({
    declarations: [AnimatedCollapse],
    exports: [AnimatedCollapse]
})
export class AnimatedCollapseModule {}
