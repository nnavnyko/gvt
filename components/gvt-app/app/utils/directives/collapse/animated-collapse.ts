/**
 * Animated collapse
 * Provide and ability to collapse element using animation
 *
 * !!! Please pay attention, that height of collapsed element must be less or equal that 100vh;
 *
 * Use this directive to collapse/expand element smoothly
 * @param {boolean} collapse: ngModel to detect the element should be collapsed or expanded
 * @param {number|undefined} [animationDuration]: duration in milliseconds (by default 400 ms)
**/

import {Directive, ElementRef, Input, OnChanges, SimpleChanges, Renderer2} from "@angular/core";

const COLLAPSE_CLASS = 'gvt-collapse';
const COLLAPSED_CLASS = 'gvt-collapsed';

@Directive({
    selector: '[animatedCollapse]',
    inputs: ['collapseModel']
})
export class AnimatedCollapse implements OnChanges {
    // Inputs
    @Input() collapseModel: boolean;

    private collapsed: boolean = false;
    private defaultHeight: number;

    constructor(private el: ElementRef, private renderer: Renderer2) {
        this.renderer.addClass(this.el.nativeElement, COLLAPSE_CLASS);
    }

    ngOnChanges(changes: SimpleChanges): void {
        if (this.collapseModel !== this.collapsed) {
            if (this.collapseModel) {
                this.collapseElem();
            } else {
                this.expandElem();
            }
            this.collapsed = this.collapseModel;
        }
    }

    /** Collapse element by changing the element's height to 0 **/
    private collapseElem(): void {
        this.renderer.setStyle(this.el.nativeElement, 'max-height',  '0px');
        this.addCollapsedClass();
    }

    private addCollapsedClass(): void {
        setTimeout(() => {
            this.renderer.addClass(this.el.nativeElement, COLLAPSED_CLASS);
        }, 500);
    }

    /** Expand element by changing the element's height to initial value **/
    private expandElem(): void {
        this.renderer.removeClass(this.el.nativeElement, COLLAPSED_CLASS);
        setTimeout(() => {
            this.renderer.removeStyle(this.el.nativeElement, 'max-height');
        }, 10);
    }
}