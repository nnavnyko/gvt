/**
 * Confirmation Modal Component
 * Use it to show button and confirmation modal.
 *
 * @example:
 *
    <confirmation-modal
        *ngIf="submitForm"
        [disabledCondition]="!userForm.form.valid"
        [disablesubmitbtncondition]="formSubmitted"
        [confirmationSuccess]="submitForm"
        [confirmationBtnClass]="btn btn-primary"
    ></confirmation-modal>
 */

import { Component, Input, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import * as _ from 'lodash';

import { Config } from '../../../settings/config';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { DisableSubmitBtnDirective } from '../disable-submit-btn.directive';

const NO_SUCCESS_HANDLER_PROVIDED_ERROR = 'No success handler provided! ' +
    'You need to provide method name (e.g., [confirmationModal]="mySuccessHandler").';
const SUCCESS_HANDLER_IS_NOT_FUNC_ERROR = 'Success handler must be a function instance!';

@Component({
    selector: 'confirmation-modal',
    templateUrl: Config.APP_URL + 'utils/directives/confirmation-modal/confirmation-modal.html',
    providers: [ModalDirective, DisableSubmitBtnDirective]
})
export class ConfirmationModalComponent implements OnInit {
    /**
     * Confirmation Modal Component
     * The user can pass header, body (body can be HTML) of the message
     */
    @Input('buttonText') buttonText: string;
    @Input('confirmationBtnClass') confirmationBtnClass: string;
    @Input('confirmationBody') confirmationBody: string;
    @Input('confirmationYes') confirmationYes: string;
    @Input('confirmationNo') confirmationNo: string;
    @Input('confirmationHeader') confirmationHeader: string;
    @Input('confirmationSuccess') confirmationSuccess: Function;
    @Input('disableSubmitBtnCondition') disableSubmitBtnCondition: boolean;
    @Input('disabledCondition') disabledCondition: boolean;

    
    constructor(private translate: TranslateService){
        /**
         * Component constructor. Set native element reference and modal instance reference
         */
    }

    ngOnInit(): void {
        /**
         * Initialize directive
         * Add click handler to show confirmation modal to the element
         * @throws: Error if no success handler provided
         * @throws: Error if success handler is not a function
         */
        if (!this.confirmationSuccess) {
            throw new Error(NO_SUCCESS_HANDLER_PROVIDED_ERROR);
        }
        if (typeof this.confirmationSuccess !== 'function') {
            throw new Error(SUCCESS_HANDLER_IS_NOT_FUNC_ERROR);
        }
        this.confirmationBody = this.confirmationBody || 'Вы хотите продолжить?';
        this.confirmationHeader = this.confirmationHeader || 'Подтверждение';
        this.confirmationYes = this.confirmationYes || 'Да';
        this.confirmationNo = this.confirmationNo || 'Нет';
        this.confirmationBtnClass = this.confirmationBtnClass || 'btn btn-primary';
        this.buttonText = this.buttonText || 'Применить';
        let keys = ['confirmationBody', 'confirmationHeader', 'confirmationYes', 'confirmationNo', 'buttonText'];
        _.each(keys, (key: string) => {
            this.translate.get(this[key]).subscribe((res:string) => {
                this[key] = res;
            });
        });
    }
    
}
