/**
 * Confirmation Modal Module
 */

import { NgModule } from '@angular/core';
import { ModalModule } from 'ngx-bootstrap/modal';
import { ConfirmationModalComponent } from './confirmation-modal.component';
import { DisableSubmitBtnDirective } from '../disable-submit-btn.directive';

@NgModule({
    imports: [ModalModule],
    declarations: [ConfirmationModalComponent, DisableSubmitBtnDirective],
    exports: [ConfirmationModalComponent]
})
export class ConfirmationModalModule {}
