/**
 * Disable Submit Button after click
 * Add loading icon on submit to the <button> tag
 * Use boolean property to set disable attr
 * Use this directive only (!) for <button> tag - to display loading icon inside tag
 *
 * @example:
 * <button class="btn btn-primary" [disableSubmitBtn]="formSubmitted"></button>
 */

import { Directive, ElementRef, Input, OnChanges, OnInit } from '@angular/core';

const BUTTON_TAG_NAME = 'button';
const NOT_BUTTON_TAG_ERROR = 'The \'disableBtn\' directive can be applied only for <button> tag!';
const LOADING_ICON_POSITION = 'afterbegin';
const LOADING_ICON_TAG = '<i class="fa fa-refresh fa-spin"></i>';
const LOADING_ICON_SELECTOR = 'i.fa.fa-refresh.fa-spin';

@Directive({
    selector: '[disableBtn]'
})
export class DisableBtnDirective implements OnChanges, OnInit{

    @Input('disableBtn') isDisabled: boolean;

    constructor(private el: ElementRef){
        /**
         * Directive constructor. Set native element reference.
         */
    }

    ngOnInit():void {
        /**
         * Initialize directive
         * Add onClick handler for button
         * @throws: Error if not <button> tag used
         */
        if (this.el.nativeElement.tagName.toLowerCase() !== BUTTON_TAG_NAME) {
            throw new Error(NOT_BUTTON_TAG_ERROR);
        }
    }

    ngOnChanges(changes: any): void {
        /**
         * Detect isDisabled attribute changes
         * Add loading icon (<i class="fa fa-refresh fa-spin"></i>) and disable button if isDisabled
         * Remove loading icon and disable button if not isDisabled
         * @param changes: changes event object
         */
        let loadingIcon = this.el.nativeElement.querySelector(LOADING_ICON_SELECTOR);

        if (this.isDisabled && !loadingIcon) {
            this.el.nativeElement.disabled = true;
            this.el.nativeElement.insertAdjacentHTML(LOADING_ICON_POSITION, LOADING_ICON_TAG)
        } else if (loadingIcon) {
            this.el.nativeElement.disabled = false;
            this.el.nativeElement.removeChild(loadingIcon);
        }
    }
}
