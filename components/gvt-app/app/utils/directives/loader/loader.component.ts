/**
 * Loader component. Display loader element
 **/
import {Component} from "@angular/core";
import {Config} from "../../../settings/config";


@Component({
    selector: '<loader></loader>',
    templateUrl: Config.APP_URL + 'utils/directives/loader/loader.html',
})
export class LoaderComponent {}
