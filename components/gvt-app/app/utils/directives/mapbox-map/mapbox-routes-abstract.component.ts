/**
 * Mapbox routes abstract component
 * Put common logic for Edit and View modes here
 */
import * as _ from "lodash";
import {MapboxStyleSwitcherControl} from "mapbox-gl-style-switcher";
import {TranslateService} from "@ngx-translate/core";
import {Input} from "@angular/core";

import {MapPoint} from "../models/map-point.model";
import {TransportationLocation} from "../../../transportation/shared/models/transportation-location.model";
import {TransportationRequest} from "../../../transportation/shared/models/transportation-request.model";
import {HTTPRequestsService} from "../../services/http-requests/http-requests.service";
import {Config} from "../../../settings/config";

declare const turf: any;
declare let mapboxgl: any;

const ROUTE_URL: string = 'https://api.mapbox.com/optimized-trips/v1/mapbox/driving/';
const ROUTE_PARAMS: string = '?overview=full&steps=true&geometries=geojson&source=first&roundtrip=false&destination=last&access_token=';

export class MapboxRoutesAbstractComponent {
    @Input() trRequest: TransportationRequest;

    map: any;
    routePoints: any;
    mapLoaded: boolean;
    mapPoints: MapPoint[] = [];
    http: HTTPRequestsService;
    translate: TranslateService;

    texts: any = {
        fromText: 'Откуда',
        toText: 'Куда',
        nextText: 'Далее',
        hourString: 'час',
        minuteString: 'мин',
        driverText: 'Водитель',
        map: 'Карта',
        satellite: 'Спутник'
    };

    constructor(http: HTTPRequestsService, translate: TranslateService) {
        this.http = http;
        this.translate = translate;

        this.setTexts();
    }


    /**
     * Initialize Route layer
     */
    initializeLayers() {
        let nothing = turf.featureCollection([]);
        let mapboxStyle: any = this.map.getStyle();

        this.map.addSource('route', {
            type: 'geojson',
            data: nothing
        });

        let waterwayLayer = mapboxStyle.name === 'Outdoors' ? 'waterway-label' : null;

        this.map.addLayer({
            id: 'dropoffs-symbol',
            type: 'symbol',
            source: {
                data: this.routePoints,
                type: 'geojson'
            },
            layout: {
                'icon-allow-overlap': true,
                'icon-ignore-placement': true,
                'icon-image': 'marker-15',
            }
        });

        this.map.addLayer({
            id: 'routeline-active',
            type: 'line',
            source: 'route',
            layout: {
                'line-join': 'round',
                'line-cap': 'round'
            },
            paint: {
                'line-color': '#3c3b3c',
                'line-width': {
                    base: 2.5,
                    stops: [[12, 3], [22, 12]]
                }
            }
        }, waterwayLayer);

        this.map.addLayer({
            id: 'routearrows',
            type: 'symbol',
            source: 'route',
            layout: {
                'symbol-placement': 'line',
                'text-field': '▶',
                'text-size': {
                    base: 2,
                    stops: [[12, 24], [22, 60]]
                },
                'symbol-spacing': {
                    base: 1,
                    stops: [[12, 30], [22, 160]]
                },
                'text-keep-upright': false
            },
            paint: {
                'text-color': '#3c3b3c',
                'text-halo-color': 'hsl(55, 11%, 96%)',
                'text-halo-width': 3
            }
        }, waterwayLayer);

        this.rebuildRoute();
    }

    /**
     * Load translations for map texts
     */
    setTexts(): void {
        let keys: string[] = Object.keys(this.texts);
        _.forEach(keys, (key) => {
            this.translate.get(this.texts[key]).subscribe((res: string) => {
                this.texts[key] = res;
            });
        });
    }

    /**
     * Rebuild Route on Map
     */
    rebuildRoute(): void {
        if (!this.mapLoaded) {
            return;
        }
        let coordinates: any[] = [];
        _.each(this.mapPoints, (point: MapPoint) => {
            let coords: any = point.getCoordinates();
            if (coords) {
                coordinates.push(coords);
            }
        });
        this.rebuildRoutePoints();
        if (this.routePoints.features.length < 2) {
            return;
        }

        let url: string = ROUTE_URL +
            coordinates.join(';') +
            ROUTE_PARAMS + mapboxgl.accessToken;

        this.http.get(url).then((data: any) => {

            if (!data.trips[0]) {
                let nothing = turf.featureCollection([]);
                this.map.getSource('route')
                    .setData(nothing);
            } else {
                let routeGeoJSON = turf.featureCollection([turf.feature(data.trips[0].geometry)]);
                this.map.getSource('route')
                    .setData(routeGeoJSON);

                this.trRequest.routeLength = data.trips[0].distance;
            }

            this.map.getSource('dropoffs-symbol')
              .setData(this.routePoints);
        });
        this.setMarkersLabels();
    }

    /** Rebuild turf.featureCollection */
    rebuildRoutePoints(): void {
        this.routePoints = turf.featureCollection([]);

        _.each(this.trRequest.points, (location: TransportationLocation) => {
            let point: MapPoint = this.getPointByLocation(location);
            let turfPoint: any = point.getTurfPoint();
            if (turfPoint) {
                this.routePoints.features.push(turfPoint);
            }
        });
    }

    /**
     * Get MapPoint by TransportationLocation
     * @param {TransportationLocation} location: TransportationLocation instance
     * @return {MapPoint}: map point instance
     */
    getPointByLocation(location: TransportationLocation): MapPoint {
        return _.find(this.mapPoints, (point: MapPoint) => {
            return point.isEqualLocation(location);
        });
    }

    /**
     * Set Markers labels
     * Set points text on map
     */
    setMarkersLabels(): void {
        _.each(this.trRequest.points, (location: TransportationLocation, index: number) => {
            let mapPoint: MapPoint = this.getPointByLocation(location);
            let iconLabel: string = this.texts.fromText;

            if (index === (this.trRequest.points.length - 1)) {
                iconLabel = this.texts.toText;
            } else if (this.trRequest.points.length > 2 && index > 0) {
                iconLabel = this.texts.nextText;
            }

            mapPoint.setMarkerLabel(iconLabel);
        });
    }

    /**
     * Initialize Map Style switcher control
     * Add callbacks
     */
    initializeMapStyleSwitcher() {
        let switcherCtrl: MapboxStyleSwitcherControl = new MapboxStyleSwitcherControl([
            {
                title: this.texts.map,
                uri: Config.MAPBOX_STYLE,
            },
            {
                title: this.texts.satellite,
                uri: 'mapbox://styles/mapbox/satellite-v9',
            },
        ]);

        this.map.addControl(switcherCtrl);

        this.map.on('style.load', () => {
            if (this.mapLoaded) {
                this.initializeLayers();
            }
        });
    }

    /**
     * Initialize mapbox Map controls
     */
    initializeMapControls(): void {
        this.map.addControl(new mapboxgl.FullscreenControl());
        this.map.addControl(new mapboxgl.ScaleControl({
            maxWidth: 80
        }));
        this.map.addControl(new mapboxgl.NavigationControl({showCompass: false}));
        this.initializeMapStyleSwitcher();
    }
}