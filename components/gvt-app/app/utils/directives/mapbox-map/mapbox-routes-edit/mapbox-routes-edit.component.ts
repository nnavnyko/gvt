/**
 * MapBox route edit directive
 */

import {AfterViewInit, Component, EventEmitter, NgZone, OnDestroy, OnInit, Output} from "@angular/core";
import {ModalDirective} from "ngx-bootstrap/modal";
import {Subscriber, Observable} from "rxjs";
import * as _ from "lodash";
import {TranslateService} from "@ngx-translate/core";
import {CommonUtil} from "../../../services/common.util";
import {MapPoint} from "../../models/map-point.model";
import {TransportationLocation} from "../../../../transportation/shared/models/transportation-location.model";
import {NotificationsService} from "../../../services/notifications/notifications.service";
import {Config} from "../../../../settings/config";
import {HTTPRequestsService} from "../../../services/http-requests/http-requests.service";
import {TransportationRequest} from "../../../../transportation/shared/models/transportation-request.model";
import {MapboxRoutesAbstractComponent} from "../mapbox-routes-abstract.component";


declare let mapboxgl: any;
declare let turf: any;

const SEARCH_BY_COORDS_URL: string = 'https://nominatim.openstreetmap.org/search/';
const SEARCH_BY_ADDRESS_URL: string = 'https://nominatim.openstreetmap.org/search/?countrycodes=by&';
const SEARCH_ONE_PARAMS: string = 'format=json&addressdetails=1&limit=1&&accept-language=ru';
const SEARCH_AUTOCOMPLETE_PARAMS: string = 'format=json&addressdetails=1&limit=5&namedetails=1&accept-language=ru';

@Component({
    selector: '<mapbox-routes-edit></mapbox-routes-edit>',
    templateUrl: Config.APP_URL + 'utils/directives/mapbox-map/mapbox-routes-edit/mapbox-routes-edit.html',
    providers: [ModalDirective]
})
export class MapboxRoutesEditComponent extends MapboxRoutesAbstractComponent implements OnInit, OnDestroy, AfterViewInit {
    private trRequestObservable: Observable<any>;
    private trRequestObserver: Subscriber<any>;
    @Output() trRequestOutput: EventEmitter<TransportationRequest> = new EventEmitter();

    private currentLocation: TransportationLocation;
    private currentPoint: MapPoint;
    private currentAddress: string;

    private addressesList: string[];

    private mapLoadedObservable: Observable<any>;
    private mapLoadedObserver: Subscriber<any>;
    @Output() mapLoadedOutput: EventEmitter<boolean> = new EventEmitter();

    private addressesListObservable: Observable<any>;
    private addressesListObserver: Subscriber<any>;
    @Output() addressesListOutput: EventEmitter<string[]> = new EventEmitter();

    private Config: Object = Config;
    private uuid: string = CommonUtil.generateUUID();
    private dragHandler: any = null;
    private isMapDraggable: boolean = true;
    private searchingAddress: boolean = false;

    /** Constructor */
    constructor(http: HTTPRequestsService, private zone: NgZone, translate: TranslateService,
                private notifications: NotificationsService) {
        super(http, translate);
        this.addressesListObservable = new Observable((observer: Subscriber<any>) => {
            this.addressesListObserver = observer;
        });

        this.trRequestObservable = new Observable((observer: Subscriber<any>) => {
            this.trRequestObserver = observer;
        });

        this.mapLoadedObservable = new Observable((observer: Subscriber<any>) => {
            this.mapLoadedObserver = observer;
        });

        this.trRequestObservable.subscribe((data: TransportationRequest) => {
            this.trRequest = data;
            this.trRequestOutput.emit(this.trRequest);
        });

        this.mapLoadedObservable.subscribe((data: boolean) => {
            this.mapLoaded = data;
            this.mapLoadedOutput.emit(this.mapLoaded);
        });

        this.addressesListObservable.subscribe((data: string[]) => {
            this.addressesList = data;
            this.addressesListOutput.emit(this.addressesList);
        });
    }

    /** Initialize map after component's HTML is rendered */
    ngAfterViewInit(): void {
        try {
            this.map = new mapboxgl.Map({
                container: 'map-' + this.uuid,
                style: Config.MAPBOX_STYLE,
                center: [29.2223129, 52.0322082],
                zoom: 14,
                minZoom: 5.5,
                maxZoom: 18.5,
                dragRotate: false
            });
            this.map.touchZoomRotate.disableRotation();

            this.routePoints = turf.featureCollection([]);

            this.initializeMapControls();

            this.map.on('load', () => {
                if ('geolocation' in navigator) {
                    navigator.geolocation.getCurrentPosition( (position) => {
                        this.map.flyTo({center: [position.coords.longitude, position.coords.latitude]});
                    });
                }
                this.map.setLayoutProperty('country-label-lg', 'text-field', ['get', 'name_ru']);

                this.addClickHandler();
                this.addDragListener();
                this.initializeMapPoints();

                this.zone.run(() => {
                    this.mapLoadedObserver.next(true);
                    this.mapLoadedObserver.complete();
                    this.initializeLayers();
                });
            });
        } catch (err) {
            console.error(err);
        }
    }

    /** Add click on map Event Listener */
    private addClickHandler(): void {
        this.map.on('click', (e:any) => {
            if (this.currentPoint) {
                let currentPoint = this.currentPoint;
                currentPoint.move(e.lngLat.lng, e.lngLat.lat);
                this.searchByCoordinates(currentPoint, currentPoint.getCoordinates());
                this.rebuildRoute();
            }
        });
    }

    /** Initialize component */
    ngOnInit(): void {
        this.addressesListObserver.next([]);
        this.trRequestObserver.next(this.trRequest);
    }

    /**
     * Initialize map points from TransportationRequest points
     */
    initializeMapPoints(): void {
        _.each(this.trRequest.points, (point: TransportationLocation) => {
            this.addMapPoint(point);
        });

        this.setNextCurrentPoint();
    }

    /**
     * Add MapPoint to map
     * @param {TransportationLocation} location: TransportationLocation point
     */
    addMapPoint(location: TransportationLocation): void {
        let mapPoint: MapPoint = new MapPoint(location, this.map, true);
        this.mapPoints.push(mapPoint);

        mapPoint.setDragHandler(() => {
            mapPoint.setLocationCoordinates();
            mapPoint.setTurfPoint();

            this.searchByCoordinates(mapPoint, mapPoint.getCoordinates());
            this.rebuildRoute();
        });
    }

    /**
     * Add new map point to route and set it as current point to search and set coodrdinates
     */
    addPoint(): void {
        let location: TransportationLocation = new TransportationLocation({'sort_order': this.trRequest.points.length + 1});
        this.trRequest.points.push(location);

        this.addMapPoint(location);
        this.setCurrentPoint(location);
    }

    /**
     * Remove map point from map:
     * param {Object} location: TransportationLocation
     */
    removePoint(location: TransportationLocation): void {
        if (this.trRequest.points.length === 2) {
            throw Error('Cannot be less than 2 points!');
        }
        let mapPoint: any = this.getPointByLocation(location);

        this.mapPoints.splice(this.mapPoints.indexOf(mapPoint), 1);
        mapPoint.remove();

        this.trRequest.points.splice( this.trRequest.points.indexOf(location), 1);

        // Reorder points
        _.each(this.trRequest.points, (point, index) => {
            point.sortOrder = index + 1;
        });

        this.rebuildRoute();
    }

    /**
     * Set Current Point
     * @param {TransportationLocation} location: TransportationLocation instance
     * @param {boolean} isAutocomplete: TransportationLocation instance
     */
    setCurrentPoint(location: TransportationLocation, isAutocomplete?: boolean): void {
        if (this.currentLocation === location) {
            if (!isAutocomplete) {
                this.currentLocation = null;
                this.currentPoint = null;
            }
        } else {
            this.currentPoint = this.getPointByLocation(location);
            this.currentLocation = location;
            this.currentPoint.flyTo();
        }
    }

    /**
     * Search address by Coordinates
     * @param {MapPoint} mapPoint: current Map Point obj
     * @param {number[]} coordinates: coordinates [lng, lat]
     */
    searchByCoordinates(mapPoint: MapPoint, coordinates: number[]): void {
        let coords = [coordinates[1], coordinates[0]].join(',');

        this.http.get(SEARCH_BY_COORDS_URL + coords + '?' + SEARCH_ONE_PARAMS).then((resp: any) => {
            let respObj: any = resp && resp.length ? resp[0] : null;
            mapPoint.setAddress(this.getHumanReadableAddress(respObj, this.currentAddress));

            this.setNextCurrentPoint();
        });
    }

    /**
     * Set Current point address string
     * Build current point
     */
    searchByAddress(): void {
        let currentPoint: any = this.currentPoint;
        let address: string = this.currentAddress.trim();
        if (!address) {
            return;
        }
        currentPoint.setAddress(address + ' ');

        this.http.get(
            SEARCH_BY_ADDRESS_URL + 'q=' + address + '&' + SEARCH_ONE_PARAMS,
            null,
            true
        ).then((resp: any) => {
            try {
                let respObj: any = resp && resp.length ? resp[0] : null;
                if (respObj) {
                    let longitude: number = parseFloat(respObj.lon);
                    let latitude: number = parseFloat(respObj.lat);

                    currentPoint.move(longitude, latitude);
                    currentPoint.flyTo();

                    this.setNextCurrentPoint();
                    this.rebuildRoute();
                } else {
                    this.translate.get('Адрес не найден').subscribe((title:string) => {
                        this.translate.get('Координаты адреса не найдены. Укажите адрес, установив точку на карте.').subscribe((message:string) => {
                            this.notifications.showInfoMessage(title, message, 5000);
                        });
                    });
                }
            } catch (err){
                return;
            }
        });
    }

    /**
     * Search by Address Variants
     * @param {any} currentPoint: object of TransportationLocation
     * @param {string} address: address lookup string
     * @return {Promise}
     */
    searchByAddressVariant(currentPoint: any, address: string, searchResponse: any): Promise<any> {
        return this.http.get(SEARCH_BY_ADDRESS_URL + address + '&' + SEARCH_ONE_PARAMS).then((resp: any) => {
            try {
                let respObj: any = resp && resp.length ? resp[0] : null;
                if (respObj) {
                    searchResponse.response = respObj;
                }
            } catch (err){
                return;
            }
        });
    }

    /**
     * Set human readable address
     * @param {any} resp: Nominatim geosearch response
     * @param {string} defaultAddress: default address (optional, empty string by default)
     * @return {string}: human readable address
     */
    getHumanReadableAddress(resp: any, defaultAddress?: string): string {
        let address: string = defaultAddress || '';
        if (resp && resp.address) {
            let respAddress = resp.address;
            let addressArr: string[] = [];
            let keys = ['country', 'city', 'town', 'road', 'house_number'];

            _.each(keys, (key: string) => {
                let localCountry = ['беларусь', 'belarus'];
                if (respAddress[key] &&
                    (key !== 'country' || localCountry.indexOf(respAddress[key].toLowerCase()) < 0)) {
                    addressArr.push(respAddress[key]);
                }
            });

            address = addressArr.join(' ');
        }
        return address;
    }

    /**
     * Set current address
     */
    setCurrentAddress(address:string, input:any):void {
        this.currentAddress = address.trim() + ' ';
        this.addressesList = [];
        input.focus();
    }

    /**
     * Show address autocomplete modal
     * @param {Object} event: click Event
     * @param {Object} addressModalInstance: object (modal instance)
     * @param {string} currentPointAddress: string
     */
    showAddressAutocompleteModal(event: any, addressModalInstance: any, currentPointAddress: string):void {
        event.preventDefault();
        event.stopPropagation();
        this.addressesList = [];
        this.currentAddress = currentPointAddress;
        addressModalInstance.show();
    }

    /**
     * Build Address Autocomplete list
     * Make HTTP request to Nominatim service
     */
    buildAddressAutocompleteList(query: any): void {
        if (this.searchingAddress) {
            return;
        }
        this.searchingAddress = true;

        setTimeout(() => {
            this.searchingAddress = false;
        }, 500);

        let searchString = query.trim();

        if (!searchString) {
            return;
        }

        let addressVariants: string[] = this.getAddressVariants(searchString);
        if (addressVariants.length) {
            let data:string[] = [];
            let addressLookup: string = addressVariants[0];

            this.getAddressAutocompleteItems(addressLookup, data).then(() => {
                if (addressVariants.length === 2) {
                    addressLookup = addressVariants[1];

                    this.getAddressAutocompleteItems(addressLookup, data).then(() => {
                        this.zone.run(() => {
                            this.addressesListObserver.next(data);
                        });
                    });
                } else {
                    this.zone.run(() => {
                        this.addressesListObserver.next(data);
                    });
                }
            });
        }
    }

    /**
     * Find address autocomplete
     * @param {string} addressLookup: address string
     * @param {string[]} data: autocomplete list
     * @return {Promise}
     */
    getAddressAutocompleteItems(addressLookup: string, data: string[]): Promise<any> {
        return this.http.get(
            SEARCH_BY_ADDRESS_URL + addressLookup + '&' + SEARCH_AUTOCOMPLETE_PARAMS,
            null,
            true
        ).then((resp: any) => {
            try {
                if (resp) {
                    _.each(resp, (respObj) => {
                        let address: string = this.getHumanReadableAddress(respObj).trim();
                        if (address && data.indexOf(address) < 0) {
                            data.push(address);
                        }
                    });
                }
            } catch (err) {
                return;
            }
        });
    }

    /**
     * Get different variants for passed search string
     * @param {string} searchString
     * @return {string[]}
     */
    getAddressVariants(searchString:string): string[] {
        let result: string[] = [];
        searchString = searchString.replace(',', ' ');

        let searchArr: string[] = _.filter(searchString.split(' '), (str:string) => {
            return !!str.trim();
        });
        let city: string = '';
        let street: string = '';
        let streetReverse: string = '';

        if (searchArr.length) {
            city = searchArr[0].trim();

            if (searchArr.length > 1) {
                let streetArgs: string[] = searchArr.slice(1);

                street = streetArgs.join(' ');
                streetReverse = streetArgs.reverse().join(' ');

                result.push(this.buildSearchAddressQuery(city, street));
                result.push(this.buildSearchAddressQuery(city, streetReverse));
            } else {
                result.push(this.buildSearchAddressQuery(city));
            }
        }

        return result;
    }

    /**
     * Build search address query
     * @param {string} city
     * @param {string} [street]
     * @return {string}: URL query string
     */
    buildSearchAddressQuery(city: string, street?: string): string {
        let result: string[] = ['city=', encodeURIComponent(city)];
        if (street) {
            result.push.apply(result, ['&street=', encodeURIComponent(street)]);
        }

        return result.join('');
    }

    /**
     * Find next TransportationLocation object without set coordinates or address
     */
    setNextCurrentPoint(): void {
        this.currentPoint = _.first(_.filter(this.mapPoints, (point: MapPoint) => {
            return !point.getTurfPoint() || !point.getAddress() || !point.getCoordinates();
        }));
        this.currentLocation = this.currentPoint ? this.currentPoint.getLocation() : null;
    }

    /**
     * Unsubscribe observable letiables
     */
    ngOnDestroy(): void {
        try {
            this.trRequestObserver.complete();
            if (this.map) {
                this.map.remove();
            }
            if (this.dragHandler) {
                document.removeEventListener('touchstart', this.dragHandler);
                document.removeEventListener('click', this.dragHandler);
                let mapElements = document.getElementsByClassName('gvt-map');
                if (mapElements.length) {
                    mapElements[0].removeEventListener('mouseover', this.dragHandler);
                }
            }
        } catch (e) {
            console.log('ERROR in destroy mapbox map:', e);
        }
    }

    /** Add Drag listener on document to fix drag issue on mobile*/
    addDragListener(): void {
        let onMove: any = this.map.dragPan._onMove;
        this.map.dragPan._onMove = (e:any) => {
            if (this.isMapDraggable) {
                onMove(e);
            }
        };

        this.dragHandler = (e:any) => {
            try {
                let mapEl: any = this.map.getContainer();
                if (e.path && e.path.indexOf(mapEl) < 0) {
                    this.isMapDraggable = false;
                    this.map.dragPan.disable();
                } else {
                    if (!this.isMapDraggable) {
                        e.preventDefault();
                        e.stopPropagation();
                    }
                    this.isMapDraggable = true;
                    this.map.dragPan.enable();
                }
            } catch(e) {
                console.dir(e);
            }
        };

        document.addEventListener('touchstart', this.dragHandler);

        let mapElements = document.getElementsByClassName('gvt-map');
        if (mapElements.length) {
            mapElements[0].addEventListener('mouseover', this.dragHandler);
        }
    }

}