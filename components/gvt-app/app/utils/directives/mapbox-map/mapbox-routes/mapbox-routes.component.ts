/**
 * MapBox route edit directive
 */

import {AfterViewInit, Component, EventEmitter, Input, NgZone, OnDestroy, OnInit, Output} from "@angular/core";
import {Config} from "../../../../settings/config";
import {ModalDirective} from "ngx-bootstrap/modal";
import {Subscriber, Observable} from "rxjs";
import {TransportationRequest} from "../../../../transportation/shared/models/transportation-request.model";
import {HTTPRequestsService} from "../../../services/http-requests/http-requests.service";
import * as _ from "lodash";
import {TranslateService} from "@ngx-translate/core";
import {CommonUtil} from "../../../services/common.util";
import {TransportationLocation} from "../../../../transportation/shared/models/transportation-location.model";
import {MapPoint} from "../../models/map-point.model";
import {MapboxRoutesAbstractComponent} from "../mapbox-routes-abstract.component";


declare let mapboxgl: any;
declare let turf: any;

@Component({
    selector: '<mapbox-routes></mapbox-routes>',
    templateUrl: Config.APP_URL + 'utils/directives/mapbox-map/mapbox-routes/mapbox-routes.html',
    providers: [ModalDirective]
})
export class MapboxRoutesComponent extends MapboxRoutesAbstractComponent implements OnInit, OnDestroy, AfterViewInit {
    @Input() flyToEvents: Observable<any>;
    private flyToEventsSubscribtion: any;

    private trRequestObservable: Observable<any>;
    private trRequestObserver: Subscriber<any>;
    @Output() trRequestOutput: EventEmitter<TransportationRequest> = new EventEmitter();

    private mapLoadedObservable: Observable<any>;
    private mapLoadedObserver: Subscriber<any>;
    @Output() mapLoadedOutput: EventEmitter<boolean> = new EventEmitter();

    private Config: Object = Config;
    private uuid: string = CommonUtil.generateUUID();
    private dragHandler: any = null;
    private isMapDraggable: boolean = true;
    @Input() routeLength: any;

    /** Constructor */
    constructor(http: HTTPRequestsService, private zone: NgZone, translate: TranslateService) {
        super(http, translate);

        this.trRequestObservable = new Observable((observer: Subscriber<any>) => {
            this.trRequestObserver = observer;
        });

        this.mapLoadedObservable = new Observable((observer: Subscriber<any>) => {
            this.mapLoadedObserver = observer;
        });

        this.trRequestObservable.subscribe((data: TransportationRequest) => {
            this.trRequest = data;
            this.trRequestOutput.emit(this.trRequest);
        });

        this.mapLoadedObservable.subscribe((data: boolean) => {
            this.mapLoaded = data;
            this.mapLoadedOutput.emit(this.mapLoaded);
        });

        let keys: string[] = ['fromText', 'toText', 'nextText', 'hourString', 'minuteString', 'driverText'];
        _.each(keys, (key) => {
            this.translate.get(this.texts[key]).subscribe((res: string) => {
                this.texts[key] = res;
            });
        });
    }

    /** Initialize map after component's HTML is rendered */
    ngAfterViewInit(): void {
        try {
            let map = new mapboxgl.Map({
                container: 'map-' + this.uuid,
                style: Config.MAPBOX_STYLE,
                center: [29.2223129, 52.0322082],
                zoom: 14,
                minZoom: 5.5,
                maxZoom: 18.5,
                dragRotate: false
            });
            this.map = map;
            this.map.touchZoomRotate.disableRotation();

            this.routePoints = turf.featureCollection([]);
            // Create an empty GeoJSON feature collection for drop off locations

            // Create an empty GeoJSON feature collection, which will be used as the data source for the route before users add any new data
            let nothing = turf.featureCollection([]);

            this.initializeMapControls();

            this.map.on('load', () => {
                this.addDragListener();

                this.zone.run(() => {
                    this.initializeMapPoints();
                    this.mapLoadedObserver.next(true);
                    this.mapLoadedObserver.complete();
                    this.initializeLayers();
                });
            });
        } catch (err) {
            console.error(err);
        }
    }

    /** Initialize component */
    ngOnInit(): void {
        this.trRequestObserver.next(this.trRequest);
        this.flyToEventsSubscribtion = this.flyToEvents.subscribe(
        (location: TransportationLocation) => {
            let mapPoint: MapPoint = _.find(this.mapPoints, (m: MapPoint) => {
                return m.getLocation() === location;
            });

            if (mapPoint) {
                mapPoint.flyTo();
            }
        });
    }

    /**
     * Initialize TransportationRequest points and add them to map
     */
    initializeMapPoints(): void {
        _.each(this.trRequest.points, (point: TransportationLocation, i: number) => {
            let mapPoint: MapPoint = this.addMapPoint(point);
            if (i === 0) {
                mapPoint.flyTo();
            }
        });
        
        this.rebuildRoute();
    }

    /**
     * Add MapPoint to map
     * @param {TransportationLocation} location: TransportationLocation point
     * @return {MapPoint}: mapPoint object
     */
    addMapPoint(location: TransportationLocation): MapPoint {
        let mapPoint: MapPoint = new MapPoint(location, this.map);

        this.mapPoints.push(mapPoint);

        return mapPoint;
    }

    /**
     * Unsubscribe observable letiables
     */
    ngOnDestroy(): void {
        try {
            this.trRequestObserver.complete();
            this.flyToEventsSubscribtion.unsubscribe();
            if (this.map) {
                this.map.remove();
            }
            if (this.dragHandler) {
                document.removeEventListener('touchstart', this.dragHandler);
                document.removeEventListener('click', this.dragHandler);
            }
        } catch (e) {
            console.log('ERROR in destroy mapbox map:', e);
        }
    }

    /** Add Drag listener on document to fix drag issue on mobile*/
    addDragListener(): void {
        let onMove: any = this.map.dragPan._onMove;
        this.map.dragPan._onMove = (e:any) => {
            if (this.isMapDraggable) {
                onMove(e);
            }
        };

        this.dragHandler = (e:any) => {
            try {
                let mapEl: any = this.map.getContainer();
                if (e && e.path && e.path.indexOf(mapEl) < 0) {
                    this.isMapDraggable = false;
                    this.map.dragPan.disable();
                } else {
                    this.isMapDraggable = true;
                    this.map.dragPan.enable();
                }
            } catch(err) {
                console.log(err);
            }
        };

        document.addEventListener('touchstart', this.dragHandler);
        document.addEventListener('click', this.dragHandler);
    }



    /**
     * Get route time from Yandex Maps API
     * @param {number} routeTime: number (time in seconds)
     * @return: string (route time humanized, e.g. 1h 30min)
     */
    getRouteTime(routeTime: number): string {
        let routeTimeStr = '';

        if (!routeTime) {
            return routeTimeStr;
        }
        const hourInSeconds = 3600;
        const minuteInSeconds = 60;

        let hours = (routeTime / hourInSeconds) >> 0;
        if (hours > 0) {
            routeTimeStr += hours + ' ' + this.texts.hourString;
        }

        let minutes = ((routeTime - hours * hourInSeconds) / minuteInSeconds) >> 0;
        if (minutes > 0) {
            routeTimeStr += ' ' + minutes + ' ' + this.texts.minuteString;
        }

        return routeTimeStr;
    }

}