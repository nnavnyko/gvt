
// AoT requires an exported function for factories
import {FormsModule} from "@angular/forms";
import {TranslateLoader, TranslateModule} from "@ngx-translate/core";
import {HttpClient, HttpClientModule} from "@angular/common/http";
import {NgModule} from "@angular/core";
import {TranslateHttpLoader} from "@ngx-translate/http-loader";
import {CommonModule} from "@angular/common";
import {ModalModule} from "ngx-bootstrap/modal";
import {MapboxRoutesComponent} from "./mapbox-routes.component";

export function HttpLoaderFactory(http: HttpClient) {
    return new TranslateHttpLoader(http, '/static/gvt-app/app/assets/i18n/', '.json');
}

@NgModule({
    imports: [HttpClientModule, CommonModule, FormsModule, ModalModule, TranslateModule.forChild({
        loader: {
            provide: TranslateLoader,
            useFactory: HttpLoaderFactory,
            deps: [HttpClient]
        }
    })],
    declarations: [MapboxRoutesComponent],
    exports: [MapboxRoutesComponent]
})
export class MapBoxRoutesModule {}