/**
 * MapBox Map Point container
 */
import * as _ from "lodash";

import {TransportationLocation} from "../../../transportation/shared/models/transportation-location.model";

declare let mapboxgl: any;
declare let turf: any;

const LABEL_CLASS: string = 'gvt-map-point-label';

export class MapPoint {
    private location: TransportationLocation;
    private marker: any;
    private turfPoint: any;
    private map: any;
    private draggable: boolean = false;
    private dragCallback: any = null;

    /**
     * Create MapPoint container
     * @param {TransportationLocation} location: TransportationRequest point
     * @param {any} map: MapBox map instance
     * @param {boolean} draggable: MapBox map instance
     */
    constructor(location : TransportationLocation, map: any, draggable?: boolean) {
        this.location = location;
        this.map = map;
        this.draggable = draggable || false;
        this.addMarkerToMap();
    }

    /**
     * Add Marker to Map if coordinates are set
     */
    public addMarkerToMap(): void {
        if (!this.marker && this.location && this.location.longitude && this.location.latitude) {
            let coordinates:number[] = this.getCoordinates();

            this.marker = new mapboxgl.Marker({
                    draggable: this.draggable,
                    color: '#414141'
                })
                .setLngLat(coordinates)
                .addTo(this.map);
            this.setTurfPoint();
            if (this.dragCallback) {
                this.setDragHandler(this.dragCallback);
            }
        }
    }

    /**
     * Set Drag event handler
     */
    public setDragHandler(callback: any): void {
        if (this.marker) {
            this.marker.on('dragend', () => {
                callback();
            });
        } else {
            this.dragCallback = callback;
        }
    }

    /**
     * Move point to coordinates
     * @param {number} longitude: longitude value
     * @param {number} latitude: latitude value
     */
    public move(longitude: number, latitude: number):void {
        this.location.setCoordinates(longitude, latitude);

        if (!this.marker) {
            this.addMarkerToMap();
        } else {
            this.marker.setLngLat([this.location.longitude, this.location.latitude]);
            this.setTurfPoint();
        }
    }

    /** Set Point as map center */
    public flyTo(): void {
        if (!!this.marker) {
            this.map.flyTo({center: [this.location.longitude, this.location.latitude]});
        }
    }

    /** Set turf point. Turf.point is needed to build a map route */
    public setTurfPoint(): void {
        let coordinates:number[] = this.getCoordinates();
        this.turfPoint = turf.point(
            coordinates,
            {
                orderTime: Date.now(),
                key: Math.random()
            }
        );
    }

    /**
     * Get Turf.point to build route
     * @return {object}: Turf.point
     */
    public getTurfPoint(): any {
        return this.turfPoint;
    }

    /**
     * Get coordinates as [longitude, latitude]
     * @return {number[]}: array of coords
     **/
    public getCoordinates(): number[] {
        if (!this.location.longitude || !this.location.latitude) {
            return null;
        }
        return [this.location.longitude, this.location.latitude];
    }

    /**
     * Check if this MapPoint is the same TransportationLocation
     * @param {TransportationLocation} location: Transportation Location from trRequest.points array
     * @return {boolean}: true if the same
     */
    isEqualLocation(location: TransportationLocation): boolean {
        return this.location == location;
    }

    /**
     * Remove marker point from map.
     * Delete related objects
     */
    public remove(): void {
        if (this.marker) {
            this.marker.remove();
        }
        this.marker = null;
        this.turfPoint = null;
        this.location = null;
    }

    /** Set Address string to TransportationLocation */
    public setAddress(address?: string): void {
        this.location.address = address;
    }

    /** Get Address of TransportationLocation
     * @return {string}: location address
     */
    public getAddress(): string {
        return this.location.address;
    }

    /**
     * Get TransportationLocation
     * @return {TransportationLocation}: location object
     */
    public getLocation(): TransportationLocation {
        return this.location;
    }

    /**
     * Set Marker Class
     * @param {string} markerLabel: name of icon's class
     */
    public setMarkerLabel(markerLabel: string): void {
        if (this.marker) {
            let el: any = this.marker.getElement();
            let labelElements = el.getElementsByClassName(LABEL_CLASS);
            let labelEl: any;

            if (labelElements.length) {
                labelEl = labelElements[0];
            } else {
                labelEl = document.createElement('label');
                labelEl.className = LABEL_CLASS;
                el.appendChild(labelEl);
            }

            labelEl.innerText = markerLabel;
        }
    }

    /** Set TransportationLocation coordinates */
    public setLocationCoordinates(): void {
        let lngLat: any = this.marker.getLngLat();
        this.location.setCoordinates(lngLat.lng, lngLat.lat);
    }
}
