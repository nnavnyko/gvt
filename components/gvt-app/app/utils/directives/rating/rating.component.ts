/**
 * Rating Component
 */
import { Component, Input } from '@angular/core';
import { Config } from '../../../settings/config';


@Component({
    selector: '<rating></rating>',
    templateUrl: Config.APP_URL + 'utils/directives/rating/rating.html',
    styles: ['.rating-wrapper { display: inline-block; }']
})
export class RatingComponent {
    /**
     * Rating Component class
     */
    @Input('value') value: string;
}