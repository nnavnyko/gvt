/**
 * Rating Module
 * Usage:
 * <rating value="3"></rating>
 *
 * Will render:
 * <i class="fa fa-star " aria-hidden="true"></i>
 * <i class="fa fa-star " aria-hidden="true"></i>
 * <i class="fa fa-star " aria-hidden="true"></i>
 * <i class="fa fa-star-o " aria-hidden="true"></i>
 * <i class="fa fa-star-o " aria-hidden="true"></i>
 *
 */

import { NgModule } from '@angular/core';
import { RatingComponent } from './rating.component';
import { RangeModule } from '../../pipes/range/range.module'
import { CommonModule } from '@angular/common';


@NgModule({
    imports: [RangeModule, CommonModule],
    declarations: [RatingComponent],
    exports: [RatingComponent]
})
export class RatingModule {
    /**
     * Rating Module class
     */
}
