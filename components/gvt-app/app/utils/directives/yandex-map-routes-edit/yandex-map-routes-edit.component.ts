/**
 * Yandex Map Routes Edit Component
 * Use it while creating Transportation Request
 */

import { Component, Input, OnInit, Output, EventEmitter, OnDestroy, NgZone } from '@angular/core';
import * as _ from 'lodash';
import { TranslateService } from '@ngx-translate/core';
import { Observable ,  Subscriber } from 'rxjs';

import { Config } from '../../../settings/config';
import { TransportationLocation } from '../../../transportation/shared/models/transportation-location.model';
import { TransportationRequest } from '../../../transportation/shared/models/transportation-request.model';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { HTTPRequestsService } from '../../services/http-requests/http-requests.service';

declare let ymaps: any;

const YANDEX_GEOSEARCH_API_URL = 'https://geocode-maps.yandex.ru/1.x/?format=json&geocode=';

@Component({
    selector: '<yandex-map-routes-edit></yandex-map-routes-edit>',
    templateUrl: Config.APP_URL + 'utils/directives/yandex-map-routes-edit/yandex-map-routes-edit.html',
    providers: [ModalDirective]
})
export class YandexMapRoutesEditComponent implements OnInit, OnDestroy {
    /**
     * Yandex Map Routes Component Class
     */

    @Input() trRequest: TransportationRequest;

    private trRequestObservable: Observable<any>;
    private trRequestObserver: Subscriber<any>;
    @Output() trRequestOutput: EventEmitter<TransportationRequest> = new EventEmitter();

    private texts:any = {
        fromText: 'Откуда',
        toText: 'Куда',
        nextText: 'Далее'
    };

    private map: any;
    private route: any;
    private currentPoint: any;
    private currentAddress: string;
    private mapPlacesMapping: any = [];

    private addressesList: string[];
    private yandexLoaded: boolean = false;

    private yandexLoadedObservable: Observable<any>;
    private yandexLoadedObserver: Subscriber<any>;
    @Output() yandexLoadedOutput: EventEmitter<boolean> = new EventEmitter();

    private addressesListObservable: Observable<any>;
    private addressesListObserver: Subscriber<any>;
    @Output() addressesListOutput: EventEmitter<string[]> = new EventEmitter();


    private Config:Object = Config;

    /** Constructor */
    constructor(private http: HTTPRequestsService, private zone: NgZone, private translate: TranslateService) {
        let $this:any = this;

        $this.addressesListObservable = new Observable((observer: Subscriber<any>) => {
            $this.addressesListObserver = observer;
        });

        $this.trRequestObservable = new Observable((observer: Subscriber<any>) => {
            $this.trRequestObserver = observer;
        });

        $this.yandexLoadedObservable = new Observable((observer: Subscriber<any>) => {
            $this.yandexLoadedObserver = observer;
        });

        $this.addressesListObservable.subscribe((data: string[]) => {
            $this.addressesList = data;
            $this.addressesListOutput.emit($this.addressesList);
        });

        $this.trRequestObservable.subscribe((data: TransportationRequest) => {
            $this.trRequest = data;
            $this.trRequestOutput.emit($this.trRequest);
        });

        $this.yandexLoadedObservable.subscribe((data: boolean) => {
            $this.yandexLoaded = data;
            $this.yandexLoadedOutput.emit($this.yandexLoaded);
        });


        let keys:string[] = ['fromText', 'toText', 'nextText'];
        _.each(keys, (key) => {
            $this.translate.get($this.texts[key]).subscribe((res: string) => {
                $this.texts[key] = res;
            });
        });

    }

    /**
     * Initialize TransportationRequest points
     */
    ngOnInit():void {
        this.addressesListObserver.next([]);
        this.trRequestObserver.next(this.trRequest);
        this.currentPoint = this.trRequest.points[0];

        let $this = this;

        try {
            ymaps.ready(() => {
                $this.zone.run(() => {
                    $this.initializeMap();
                    $this.yandexLoadedObserver.next(true);
                    $this.yandexLoadedObserver.complete();
                });
            });
        } catch (err) {
            console.error(err);
        }
    }

    initializeMap():void {
        this.map = new ymaps.Map('map', {
            center: [52.0322082, 29.2223129],
            zoom: 16
        });

        this.map.controls.remove(this.map.controls.get('searchControl'));
        let $this: any = this;

        this.map.events.add('click', (event:any) => {
            let coords:any = event.get('coords');
            let currentPoint: any = this.currentPoint;
            let index: number = $this.trRequest.points.indexOf(currentPoint);
            currentPoint.latitude = coords[0];
            currentPoint.longitude = coords[1];

            let mapPoint: any = $this.mapPlacesMapping[index] || $this.addMapPoint(currentPoint, index);
            $this.mapPlacesMapping[index] = mapPoint;
            mapPoint.geometry.setCoordinates(coords);

            ymaps.geocode(coords).then((res:any) => {
                let geoObject:any = res.geoObjects.get(0);
                let address:string = geoObject.properties.get('text');
                let metadata = geoObject.properties.get('metaDataProperty').GeocoderMetaData;
                address = $this.getAddressFromMetadata(metadata, address);

                $this.map.setCenter(coords);
                currentPoint.address = address;
                $this.zone.run(() => {
                    $this.trRequestObserver.next($this.trRequest);

                    let index:number = $this.trRequest.points.indexOf(currentPoint);
                    if (index < ($this.trRequest.points.length - 1) && !$this.trRequest.points[index + 1].address) {
                        $this.currentPoint = $this.trRequest.points[index + 1];
                    }
                });
            });
            $this.rebuildRoute();
        });

        _.each($this.trRequest.points, (point, index) => {
            let mapPoint = $this.addMapPoint(point, index);

            $this.mapPlacesMapping.push(mapPoint || null);
        });

        this.rebuildRoute();
    }

    /**
     * Add point to map
     * @param {Object} point: Object (TransportationLocation)
     * @param {number} index: integer (index in points list)
     */
    private addMapPoint(point: TransportationLocation, index: number): any {
        if (!point.latitude  || !point.longitude) {
            return;
        }

        let content = index === 0 ? this.texts.fromText : index + 1 === this.trRequest.points.length ? this.texts.toText : this.texts.nextText;
        let mapPoint = new ymaps.Placemark([point.latitude, point.longitude], {
            hintContent: content,
            balloonContent: content
        });

        this.map.setCenter([point.latitude, point.longitude]);

        this.map.geoObjects.add(mapPoint);

        return mapPoint;
    }

    /**
     * Add Point to Map
     */
    addPoint():void {
        let index:number = this.trRequest.points.length;
        let trPoint: TransportationLocation = new TransportationLocation({'sort_order': this.trRequest.points.length + 1});
        this.trRequest.points.push(trPoint);
        this.currentPoint = trPoint;
        let mapPoint = this.addMapPoint(this.trRequest.points[index], index);
        this.mapPlacesMapping.push(mapPoint || null);
    }

    /**
     * Remove LAST point and re-build map route
     * @param {object} point: TransportationPoint object
     */
    removePoint(point: any):void {
        if (this.trRequest.points.length === 2) {
            throw Error('Cannot be less than 2 points!');
        }
        let index:number = this.trRequest.points.indexOf(point);
        let mapPoint: any = this.mapPlacesMapping[index];
        console.log('map point:', mapPoint);
        this.map.geoObjects.remove(mapPoint);
        this.trRequest.points.splice(index, 1);
        this.mapPlacesMapping.splice(index, 1);
        if (index > (this.trRequest.points.length - 1)) {
            this.currentPoint = this.trRequest.points[this.trRequest.points.length - 1];
        } else {
            this.currentPoint = this.trRequest.points[index];
        }
        // Reorder points
        _.each(this.trRequest.points, (point, index) => {
            point.sortOrder = index + 1;
        });
        this.rebuildRoute();
    }

    /**
     * Re-build map route line
     */
    rebuildRoute(): void{
        let routePoints:any[] = [];
        let $this: any = this;
        _.each(this.trRequest.points, (place, index) => {
            let mapPoint:any = $this.mapPlacesMapping[index];

            if (mapPoint) {
                routePoints.push(mapPoint.geometry.getCoordinates());
            }
        });

        ymaps.route(routePoints, {}).then((r:any) => {
            if ($this.route) {
                $this.map.geoObjects.remove($this.route);
            }

            $this.route = r;
            let points = $this.route.getWayPoints();

            $this.route.getPaths().options.set({
                 strokeColor: '1E98FF',
                 opacity: 0.9,
                 strokeStyle: 'longdashdot',
                 strokeWidth: 3
            });

            let lastPointIndex = $this.trRequest.points.length - 1;

            $this.trRequest.routeLength = $this.route.getLength();
            let fromPoint = points.get(0);
            let toPoint = points.get(lastPointIndex);

            fromPoint.properties.set('iconContent', this.texts.fromText);
            toPoint.properties.set('iconContent', this.texts.toText);

            $this.map.geoObjects.add($this.route);

            $this.trRequestOutput.emit($this.trRequest);
        });
    }

    /**
     * Set Current point address string
     * Build current point
     */
    setCurrentPointAddress(): void {
        let $this:any = this;
        let currentPoint: any = $this.currentPoint;
        if (!this.currentAddress.trim()) {
            return;
        }
        currentPoint.address = this.currentAddress;

        this.http.get(YANDEX_GEOSEARCH_API_URL + currentPoint.address).then(data => {
            let geoObject:any = null;
            try {
                geoObject = data.response.GeoObjectCollection.featureMember[0].GeoObject;
                let coords:string[] = geoObject.Point.pos.split(' ').reverse();
                let metadata:any = geoObject.metaDataProperty.GeocoderMetaData;
                let address = $this.getAddressFromMetadata(metadata, currentPoint.address);
                if (address) {
                    currentPoint.address = address;
                }
                currentPoint.latitude = coords[0];
                currentPoint.longitude = coords[1];

                let index:number = $this.trRequest.points.indexOf(currentPoint);
                let mapPoint = $this.mapPlacesMapping[index] ||
                    $this.addMapPoint(currentPoint, index);
                $this.mapPlacesMapping[index] = mapPoint;

                mapPoint.geometry.setCoordinates(coords);

                $this.rebuildRoute();

                $this.zone.run(() => {
                    if (index < ($this.trRequest.points.length - 1)) {
                        $this.currentPoint = $this.trRequest.points[index + 1];
                    }
                });
            } catch (err){
                return;
            }
        });
    }

    /**
     * Build Address Autocomplete list
     * Make HTTP request to Yandex service
     */
    buildAddressAutocompleteList(query: any): void {
        let $this:any = this;

        ymaps.suggest(query, {results: 25, bounedBy:[[56.149286, 22.855384],[51.405343, 32.622230]]})
            .then((response:any) => {
                let data:string[] = response.filter((item:any) => {
                    return item.value.toLowerCase().indexOf('беларусь') > -1;
                }).map((item:any) => { return item.value; });
                $this.zone.run(() => {
                    $this.addressesListObserver.next(data);
                });
        });
    }

    /**
     * Set current address
     */
    setCurrentAddress(address:string, input:any):void {
        this.currentAddress = address;
        this.addressesList = [];
        input.focus();
    }
    /**
     * Show address autocomplete modal
     * @param {Object} event: click Event
     * @param {Object} addressModalInstance: object (modal instance)
     * @param {string} currentPointAddress: string
     */
    
    showAddressAutocompleteModal(event: any, addressModalInstance: any, currentPointAddress: string):void {
        event.preventDefault();
        event.stopPropagation();
        this.addressesList = [];
        this.currentAddress = currentPointAddress;
        addressModalInstance.show();
    }

    /**
     * Unsubscribe observable variables
     */
    ngOnDestroy():void {
        this.addressesListObserver.complete();
        this.trRequestObserver.complete();
    }

    /**
     * Get address from YMaps metadata of geoObject instance. Convert data into format '<town>, <street>, <house>'
     * @param {object} metadata: geoObject metadata
     * @param {string} defaultValue: default address value
     * @return: string
     **/
    getAddressFromMetadata(metadata:any, defaultValue?:string):string {
        let value:any = defaultValue || '';
        let addressComponents:string[] = ['locality', 'street', 'house'];
        if (metadata && addressComponents.indexOf(metadata.kind) > -1) {
            value = [];
            let components = metadata.Address.Components;
            for (let j = 0; j < components.length; j++) {
                if (addressComponents.indexOf(components[j].kind) > -1) {
                    value.push(components[j].name);
                }
            }
            value = value.join(', ');
        }

        return value;
    }

}
