/**
 * Yandex Map Routes Edit Module
 *
 * @requires: yandex maps script
 *
 * @example:
 * <yandex-map-routes-edit [(points)]="points" [(routeLength)]="routeLength"></yandex-map-routes-edit>
 */

import { NgModule } from '@angular/core';
import { ModalModule } from 'ngx-bootstrap/modal';
import { CommonModule } from '@angular/common';
import { FormsModule }   from '@angular/forms';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';

import { YandexMapRoutesEditComponent } from './yandex-map-routes-edit.component';


// AoT requires an exported function for factories
export function HttpLoaderFactory(http: HttpClient) {
    return new TranslateHttpLoader(http, '/static/gvt-app/app/assets/i18n/', '.json');
}

@NgModule({
    imports: [HttpClientModule, CommonModule, FormsModule, ModalModule, TranslateModule.forChild({
        loader: {
            provide: TranslateLoader,
            useFactory: HttpLoaderFactory,
            deps: [HttpClient]
        }
    })],
    declarations: [YandexMapRoutesEditComponent],
    exports: [YandexMapRoutesEditComponent]
})
export class YandexMapRoutesEditModule {}
