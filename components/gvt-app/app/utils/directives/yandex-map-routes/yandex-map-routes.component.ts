/**
 * Yandex Map Routes Component
 */

import { Component, Input, OnInit, NgZone, OnChanges, SimpleChanges } from '@angular/core';
import * as _ from 'lodash';
import { Config } from '../../../settings/config';
import { TranslateService } from '@ngx-translate/core';
import { Observable ,  Subscriber } from 'rxjs';

declare let ymaps: any;

@Component({
    selector: '<yandex-map-routes></yandex-map-routes>',
    templateUrl: Config.APP_URL + 'utils/directives/yandex-map-routes/yandex-map-routes.html',
})
export class YandexMapRoutesComponent implements OnInit, OnChanges {
    /**
     * Yandex Map Routes Component Class
     */

    @Input() points: any[];
    @Input() routeLength: any;
    private routeTime: string;
    private routeTimeObservable: Observable<any>;
    private routeTimeObserver: Subscriber<any>;
    private texts:any = {
        fromText: 'Откуда',
        toText: 'Куда',
        nextText: 'Далее',
        hourString: 'час',
        minuteString: 'мин',
        driverText: 'Водитель',
    };

    private map: any;
    private route: any;
    private mapPlaces: any[];
    private driverCoordinatesList: any[] = [];
    private driverPoint: any;
    private movingDriverIcon:boolean = false;
    @Input() driverLocation: any;

    constructor(private translate: TranslateService, private zone: NgZone) {}

    /**
     * Initialize TransportationRequest points
     */
    ngOnInit():void {
        let $this:any = this;

        try {
            ymaps.ready(() => { $this.initializeMap(); });
        } catch (err) {
            console.error(err);
        }
        let keys:string[] = ['fromText', 'toText', 'nextText', 'hourString', 'minuteString', 'driverText'];
        _.each(keys, (key) => {
            $this.translate.get($this.texts[key]).subscribe((res: string) => {
                $this.texts[key] = res;
            });
        });

        this.routeTimeObservable = new Observable((observer: Subscriber<any>) => {
            this.routeTimeObserver = observer;
        });
        this.routeTimeObservable.subscribe((data: string) => {
            this.routeTime = data;
        });
    }

    initializeMap():void {
        this.map = new ymaps.Map('map', {
            center: [52.0322082, 29.2223129],
            zoom: 16
        });
        this.mapPlaces = [];

        this.map.controls.remove(this.map.controls.get('searchControl'));

        _.each(this.points, (point, index) => {
            let content = index === 0 ? this.texts.fromText : index + 1 === this.points.length ? this.texts.toText : this.texts.nextText;
            let mapPoint = new ymaps.Placemark([point.latitude, point.longitude], {
                hintContent: content,
                balloonContent: content
            });

            if (index === 0) {
                this.map.setCenter([point.latitude, point.longitude])
            }

            this.map.geoObjects.add(mapPoint);
            this.mapPlaces.push(mapPoint);
        });

        this.rebuildRoute();
    }

    /**
     * Re-build map route line
     */
    rebuildRoute(): void{
        let $this:any = this;
        let routePoints:any[] = [];
        _.each($this.mapPlaces, place => {
            routePoints.push(place.geometry.getCoordinates());
        });

        ymaps.route(routePoints, {}).then((r:any) => {
            if ($this.route) {
                $this.map.geoObjects.remove($this.route);
            }

            $this.route = r;
            let points = $this.route.getWayPoints();

            $this.route.getPaths().options.set({
                 strokeColor: '1E98FF',
                 opacity: 0.9,
                 strokeStyle: 'longdashdot',
                 strokeWidth: 3
            });

            let lastPointIndex = $this.mapPlaces.length - 1;

            $this.routeLength = $this.route.getLength();

            let routeTime:string = $this.getRouteTime($this.route.getTime());
            $this.zone.run(() => {
                this.routeTimeObserver.next(routeTime);
            });

            let fromPoint = points.get(0);
            let toPoint = points.get(lastPointIndex);

            fromPoint.properties.set('iconContent', $this.texts.fromText);
            toPoint.properties.set('iconContent', $this.texts.toText);

            $this.map.geoObjects.add($this.route);
        });
    }

    /**
     * Get route time from Yandex Maps API
     * @param {number} routeTime: number (time in seconds)
     * @return: string (route time humanized, e.g. 1h 30min)
     */
    getRouteTime(routeTime: number): string {
        let routeTimeStr = '';

        if (!routeTime) {
            return routeTimeStr;
        }
        const hourInSeconds = 3600;
        const minuteInSeconds = 60;

        let hours = (routeTime / hourInSeconds) >> 0;
        if (hours > 0) {
            routeTimeStr += hours + ' ' + this.texts.hourString;
        }

        let minutes = ((routeTime - hours * hourInSeconds) / minuteInSeconds) >> 0;
        if (minutes > 0) {
            routeTimeStr += ' ' + minutes + ' ' + this.texts.minuteString;
        }

        return routeTimeStr;
    }

    /** Track changes of the @Input values. Add new driver location and move driver's point to new Coordinates **/
    ngOnChanges(changes: SimpleChanges):void {
        if (this.driverLocationValid(changes)) {
            this.driverCoordinatesList.push(changes.driverLocation.currentValue);
            if (!this.movingDriverIcon) {
                this.moveDriverLocation();
            }
        } else if (this.driverPoint && this.isNeedRemoveDriverPoint(changes)) {
            this.map.geoObjects.remove(this.driverPoint);
            this.driverPoint = undefined;
        }
    }

    /**
     * Check if driver location is valid
     * @param {SimpleChanges} changes: object
     **/
    driverLocationValid(changes: SimpleChanges): boolean {
        return changes.driverLocation &&
            changes.driverLocation.currentValue &&
            changes.driverLocation.currentValue.length === 2 &&
            changes.driverLocation.currentValue[0] !== null &&
            changes.driverLocation.currentValue[1] !== null;
    }

    /**
     * Check if driver location is invalid
     * @param {SimpleChanges} changes: object
     **/
    isNeedRemoveDriverPoint(changes: SimpleChanges): boolean {
        return changes.driverLocation &&
            changes.driverLocation.currentValue &&
            changes.driverLocation.currentValue.length === 2 &&
            changes.driverLocation.currentValue[0] === null &&
            changes.driverLocation.currentValue[1] === null;
    }

    /** Move Driver location. Set coordinates to driver Location **/
    moveDriverLocation(): void {
        if (this.driverCoordinatesList.length) {
            this.movingDriverIcon = true;
            let newDriverLocation = this.driverCoordinatesList.shift();
            if (!this.driverPoint) {
                this.driverPoint = new ymaps.Placemark(newDriverLocation, {
                    hintContent: this.texts.driverText,
                    balloonContent: this.texts.driverText
                }, {
                    iconLayout: 'default#imageWithContent',
                    iconImageHref: Config.DRIVER_ICON,
                    iconImageSize: [36, 36],
                    iconImageOffset: [-16, -16]
                });
                this.map.geoObjects.add(this.driverPoint);
            }
            let currentCoordinates: number[] = this.driverPoint.geometry.getCoordinates();
            if (currentCoordinates[0] !== newDriverLocation[0] || currentCoordinates[1] !== newDriverLocation[1]) {
                this.setDriverCoordinates(currentCoordinates, newDriverLocation);
            } else {
                this.movingDriverIcon = false;
            }
        } else {
            this.movingDriverIcon = false;
        }
    }
    /**
     * Set driver coordinates. Smoothly move driver point to new location
     * @param {number[]} startCoords
     * @param {number[]} endCoords
     **/
    setDriverCoordinates(startCoords:number[], endCoords:number[]) {
        let i: number = 0;
        let numDeltas: number = 250;
        let delay: number = 15;
        let position: number[] = [startCoords[0], startCoords[1]];
        let deltaLat: number = (endCoords[0] - startCoords[0]) / numDeltas;
        let deltaLng = (endCoords[1] - startCoords[1]) / numDeltas;
        let _this = this;

        function moveMarker() {
            position[0] += deltaLat;
            position[1] += deltaLng;
            _this.driverPoint.geometry.setCoordinates(position);
            if (i <= numDeltas) {
                i++;
                setTimeout(moveMarker, delay);
            } else {
                console.log('moved', new Date());
                _this.moveDriverLocation();
            }
        }

        moveMarker();
    }
}

