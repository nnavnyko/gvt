/**
 * Yandex Map Routes Module
 *
 * @requires: yandex maps script
 * @requires: element must have id attribute
 *
 * @example:
 * <yandex-map-routes [(points)]="points" [(routeLength)]="routeLength"></yandex-map-routes>
 */

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { YandexMapRoutesComponent } from './yandex-map-routes.component';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';


// AoT requires an exported function for factories
export function HttpLoaderFactory(http: HttpClient) {
    return new TranslateHttpLoader(http, '/static/gvt-app/app/assets/i18n/', '.json');
}

@NgModule({
    imports: [HttpClientModule, CommonModule, TranslateModule.forChild({
        loader: {
            provide: TranslateLoader,
            useFactory: HttpLoaderFactory,
            deps: [HttpClient]
        }
    })],
    declarations: [YandexMapRoutesComponent],
    exports: [YandexMapRoutesComponent]
})
export class YandexMapRoutesModule {}
