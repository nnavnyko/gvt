/**
 * Range Module
 */
import { NgModule } from '@angular/core';

import { RangePipe } from './range.pipe';


@NgModule({
    declarations: [RangePipe],
    exports: [RangePipe],
})
export class RangeModule {
    /**
     * Range for root module
     */
    static forRoot() {
        return {
            ngModule: RangeModule,
        };
    }
}

