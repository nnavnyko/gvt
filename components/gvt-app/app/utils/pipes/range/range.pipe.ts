/**
 * Range pipe
 * Return list of ints in range
 *
 * Usage:
 * *ngFor="let i of 5 | range"
 */

import { Pipe, PipeTransform } from '@angular/core';
import * as _ from 'lodash';

@Pipe({name: 'range'})
export class RangePipe implements PipeTransform {
    /**
     * Range Pipe class
     */
    transform(start: number, end?:number): any {
        if (typeof end !== 'undefined') {
            return _.range(start, end)
        }
        return _.range(start);
    }
}
