/**
 * Words Limit Module
 */
import { NgModule } from '@angular/core';

import { WordsLimitPipe } from './words-limit.pipe';

@NgModule({
    declarations: [WordsLimitPipe],
    exports: [WordsLimitPipe]
})
export class WordsLimitModule {
    /**
     * Words Limit Module
     */
    static forRoot() {
        return {
            ngModule: WordsLimitModule,
        };
    }
}
