/**
 * Words limit pipe
 * Limit words in string and append '...'
 *
 * Usage:
 * {{ str | wordsLimit: 20 }}
 */

import { Pipe, PipeTransform } from '@angular/core';

@Pipe({name: 'wordsLimit'})
export class WordsLimitPipe implements PipeTransform {
    /**
     * Range Pipe class
     */

    transform(str: string, limit:number): any {
        /**
         * Get words number and truncate data if need to
         * 1. Get words length.
         * 2. If words length greater then limit, create new truncated string.
         *
         * @param str: original string
         * @param limit: number (words limit)
         * @returns: string
         */
        let spaceRegexp: any = /\S+/g;
        let spaceRegexpSplit: any = /\s+/;

        let wordsLength:number = str.match(spaceRegexp).length;
        let stringTruncated: string = str;

        if (wordsLength > limit) {
            /** Create new string of max words number items **/
            stringTruncated = str.split(spaceRegexpSplit, limit).join(' ') + '...';
        }

        return stringTruncated;
    }
}
