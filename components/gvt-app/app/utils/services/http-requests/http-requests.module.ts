/**
 * HTTP Requests Module
 * Include Ng2-Toasty (to display growl-messages)
 *
 * @requires: ng2-toasty (https://github.com/akserg/ng2-toasty) for display growl messages
 */

import { NgModule } from '@angular/core';
import { HTTPRequestsService } from './http-requests.service';
import { ToastyModule } from 'ng2-toasty';


@NgModule({
    providers: [HTTPRequestsService],
    imports: [ToastyModule.forRoot()]
})
export class HTTPRequestsModule {
    /**
     * HTTP Requests Module Class
     * Add this class into your modules in imports list
     */
}
