/**
 * HTTP Requests Service
 * This service wraps HTTP requests:
 * 1) to show common growl-messages;
 * 2) to handle common events (errors, exceptions);
 * 3) to append common HTTP Headers (such as Content-Type).
 */

import { Injectable } from '@angular/core';
import { NotificationsService } from '../notifications/notifications.service';
import {HttpClient, HttpHeaders} from "@angular/common/http";

@Injectable()
export class HTTPRequestsService {
    /**
     * HTTP Requests Service Class
     */
    headers: HttpHeaders;

    constructor(private http: HttpClient, private notifications: NotificationsService){
        /**
         *
         */
        this.headers = new HttpHeaders({'Content-Type': 'application/json'});
    }

    public get(url: string, data?: any, notTrackError?: boolean): Promise<any> {
        /**
         * Handle GET request
         * @param url: string (e.g. '/some/url/path')
         * @param data: object or undefined/null/false (need to convert data to url parameters, e.g. '?some_arg=Value')
         * @returns: Promise
         */
        let params = (typeof data === 'object' && !!data) ? '?' : '';

        // Convert object to HTTP URL parameters
        if (typeof data === 'object' && !!data) {
            for(let key in data) {
                if (data.hasOwnProperty(key)) {
                    params += key + '=' + data[key];
                }
            }
        }

        return this.http.get(url + params).toPromise()
            .then(response => { return response; }).catch(err => {
                if (!notTrackError) {
                    this.notifications.showServerErrorMessage();
                }
                console.dir(err);
        });
    }

    public post(url: string, data?: any): Promise<any> {
        /**
         * Handle POST request
         * @param url: string (e.g. '/some/url/path')
         * @param data: object or undefined/null/false (need to convert data to url parameters, e.g. '?some_arg=Value')
         * @returns: Promise
         */
        return this.http.post(url, data, {headers: this.headers}).toPromise()
            .then(response => { return response; }).catch(err => {
            this.notifications.showServerErrorMessage();
            console.dir(err);
            throw err;
        });
    }
}
