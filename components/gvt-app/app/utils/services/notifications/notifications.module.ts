/**
 * Notifications Module
 * @requires: ng2-toasty (https://github.com/akserg/ng2-toasty) for display growl messages
 */
import { NgModule } from '@angular/core';
import { NotificationsService } from './notifications.service';
import { ToastyModule } from 'ng2-toasty';


@NgModule({
    providers: [NotificationsService],
    imports: [ToastyModule.forRoot()]
})
export class NotificationsModule {
    /**
     * Notifications Module Class
     * Add this class into your modules in imports list
     */
}