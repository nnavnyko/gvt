/**
 * Notifications service
 */

import { Injectable } from '@angular/core';
import { ToastyService, ToastyConfig, ToastOptions, ToastData } from 'ng2-toasty';
import { TranslateService } from '@ngx-translate/core';
import * as _ from 'lodash';


@Injectable()
export class NotificationsService {
    /**
     * Notifications Service class
     */
    constructor(private toastyService:ToastyService, private toastyConfig: ToastyConfig,
                private translate: TranslateService){
        /**
         *
         */
        this.toastyConfig.theme = 'bootstrap';
    }

    public showServerErrorMessage():void {
        /**
         * This method show growl error message
         * Use this method when Server Error occurred
         */

        let title: string = 'Произошла ошибка';
        let description = 'Произошла ошибка. Пожалуйста, обратитесь к администратору, отправив сообщение на gvtmozyr@gmail.com. Приносим свои извинения за причененные неудобства';

        this.translate.get(title).subscribe((resTitle:string) => {
            this.translate.get(description).subscribe((resDesc:string) => {
                var toastOptions:ToastOptions = {
                    title: resTitle,
                    msg: resDesc,
                    showClose: true,
                    timeout: 0
                };

                this.toastyService.error(toastOptions);
            });
        });
    }

    public showSuccessMessage(title: string, message: string): void {
        /**
         * Show success message (green background)
         * @param title: title of the message (string)
         * @param message: message text (string)
         */
        this.translate.get(title).subscribe((resTitle:string) => {
            this.translate.get(message).subscribe((resDesc:string) => {
                var toastOptions:ToastOptions = {
                    title: resTitle,
                    msg: resDesc,
                    showClose: true,
                    timeout: 10000
                };

                this.toastyService.success(toastOptions);
            });
        });
    }

    public showInfoMessage(title: string, message: string, timeout?: number): void {
        /**
         * Show Info message (blue background)
         * @param title: title of the message (string)
         * @param message: message text (string)
         */
        this.translate.get(title).subscribe((resTitle:string) => {
            this.translate.get(message).subscribe((resDesc:string) => {
                var toastOptions:ToastOptions = {
                    title: resTitle,
                    msg: resDesc,
                    showClose: true,
                    timeout: timeout || 0
                };

                this.toastyService.info(toastOptions);
            });
        });

    }
}
