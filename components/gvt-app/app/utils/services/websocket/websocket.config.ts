/**
 * WebSocket Default Config
 */
import { WebSocketConfig } from 'angular2-websocket/angular2-websocket';

export class GVTWebSocketConfig implements WebSocketConfig {
    initialTimeout: number;
    maxTimeout: number;
    reconnectIfNotNormalClose: boolean;

    constructor(){
        this.reconnectIfNotNormalClose = true;
    }
}
