/**
 * System configuration for Angular samples
 * Adjust as necessary for your application needs.
 */
(function (global) {
  System.config({
    baseURL: '/static/gvt-app/',
    paths: {
      // paths serve as alias
      'npm:': 'node_modules/'
    },
    // map tells the System loader where to look for things
    map: {
      // our app is within the app folder
      app: 'app',

      // angular bundles
      '@angular/core': 'npm:@angular/core/bundles/core.umd.js',
      '@angular/common': 'npm:@angular/common/bundles/common.umd.js',
      '@angular/compiler': 'npm:@angular/compiler/bundles/compiler.umd.js',
      '@angular/platform-browser': 'npm:@angular/platform-browser/bundles/platform-browser.umd.js',
      '@angular/platform-browser-dynamic': 'npm:@angular/platform-browser-dynamic/bundles/platform-browser-dynamic.umd.js',
      '@angular/http': 'npm:@angular/http/bundles/http.umd.js',
      '@angular/router': 'npm:@angular/router/bundles/router.umd.js',
      '@angular/forms': 'npm:@angular/forms/bundles/forms.umd.js',
      'mydatepicker': 'npm:mydatepicker/bundles/mydatepicker.umd.min.js',
      'ng2-date-time-picker': 'npm:ng2-date-time-picker',
      'ng2-file-upload': 'npm:ng2-file-upload',
      'angular2-text-mask': 'npm:angular2-text-mask/dist/angular2TextMask.js',
      'text-mask-core': 'npm:text-mask-core',
      'angular2-websocket': 'npm:angular2-websocket',
      '@ngx-translate/core': 'node_modules/@ngx-translate/core/bundles',

      // other libraries
      'rxjs': 'npm:rxjs',
      'moment': 'npm:moment',
      'ngx-bootstrap': 'node_modules/ngx-bootstrap/bundles/ngx-bootstrap.umd.js',
      '@ngui/datetime-picker': 'npm:@ngui/datetime-picker/dist',
      'lodash': 'node_modules/lodash',
      'ng2-toasty': 'node_modules/ng2-toasty/bundles/index.umd.js'
    },
    // packages tells the System loader how to load when no filename and/or no extension
    packages: {
      app: {
        main: './main.js',
        defaultExtension: 'js'
      },
      rxjs: {
        defaultExtension: 'js'
      },
      "node_modules/ng2-bootstrap": {
        "defaultExtension": "js"
      },
      '@ngx-translate/core' : { defaultExtension: 'js' },
      'lodash': {main:'index.js', defaultExtension:'js'},
      '@ngui/datetime-picker': {
          main: 'index.js',
          defaultExtension: 'js'
      },
      'moment': {
          main: 'moment.js',
          defaultExtension: 'js'
      },
      'ng2-file-upload': {
          main: 'index.js',
          defaultExtension: 'js'
      },
      'text-mask-core': {
        defaultExtension: 'js'
      },
      'angular2-text-mask': {
        defaultExtension: 'js'
      },
      'angular2-websocket': {
        defaultExtension: 'js'
      }
    }
  });
})(this);
