/**
 * Registration JS
 * Add mask to phone_number field
 */

function activateSubmitBtn(){
    $('#submit_registration').removeAttr('disabled');
}

$(document).ready(function(){
    $('.phone-number-mask').mask('(00) 000-00-00');
    $.each($('input'), function(i, input) {
        var $input = $(input);
        var nonFormControlTypes = ['checkbox', 'submit'];
        if (!$input.hasClass('form-control') && nonFormControlTypes.indexOf($input.attr('type')) < 0) {
            $input.addClass('form-control')
        }
    });
    $.each($('input[type="submit"], button'), function(i, input) {
        var $input = $(input);
        if (!$input.hasClass('btn')) {
            $input.addClass('btn btn-default');
        }
    });
});
