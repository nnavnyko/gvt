/*
* Required jQuery
*/
;

(function(){
    $(document).ready(function(){
        var map;
        var startPlace;
        var places = [];
        var route;
        function init(){
            map = new ymaps.Map("map", {
                center: [52.0322082, 29.2223129],
                zoom: 13
            });
            startPlace = new ymaps.Placemark([52.0322082, 29.2223129], { hintContent: 'Откуда',
                balloonContent: 'Откуда' });
                map.geoObjects.add(startPlace);
            if (startCoords) {
                startPlace.geometry.setCoordinates(startCoords);
                map.setCenter(startCoords);
            }
            if (nextCoords){
                // Made for 2 points
                var routeCoordes = [
                     startPlace.geometry.getCoordinates()];
                for(var i = 0; i < nextCoords.length; i++){
                    places.push(new ymaps.Placemark(nextCoords[i],
                        { hintContent: 'Куда',
                            balloonContent: 'Куда'
                        },
                        {
                            preset: 'islands#icon',
                            iconColor: '#044E20',
                            iconContent: 'Куда',
                        }));
                    map.geoObjects.add(places[i]);
                    routeCoordes.push(places[i].geometry.getCoordinates());
                }

                ymaps.route(routeCoordes, {
                }).then(function (r) {
                    if (route)
                        map.geoObjects.remove(route);
                    route = r;
    //                                route.getPaths().options.set({
    //                                     // в балуне выводим только информацию о времени движения с учетом пробок
    //                                     balloonContentBodyLayout: ymaps.templateLayoutFactory.createClass('$[properties.humanJamsTime]'),
    //                                     // можно выставить настройки графики маршруту
    //                                     strokeColor: '0000ffff',
    //                                     opacity: 0.9
    //                                });

                    var points = route.getWayPoints();
                    var distance = route.getLength();
                    var displayDistance = (distance/1000).toFixed(1);
                    $('#id_route_length').val(distance);
                    $('#routeLength').text(displayDistance);
                    var from = points.get(0);
                    from.properties.set('iconContent', 'Откуда');
                    var to = points.get(routeCoordes.length - 1);
                    to.properties.set('iconContent', 'Куда');
                    to.properties.set('iconColor', '#044E20');
                    map.geoObjects.add(route);
                });
            }
        }
        ymaps.ready(init);
    });
})();