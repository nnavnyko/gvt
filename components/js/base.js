;

var route, places = [], map;

function activateSubmitBtn(){
    $('#reqFormSubmit').removeAttr('disabled');
}
(function(){
    $(document).ready(function(){
        var mapPointsController = new MapPointsController();

        mapPointsController.initializeMap();

        var phoneLength = 14;

        function getSubmitData($form){
            /**
             * Get JSON format of the submitted data
             * @param $form: jQuery object (Order form)
             * @return: object
             */
            var addressSubStr = 'address';
            var floorSubStr = 'floor';

            var data = { points: [] };
            var formArray = $form.serializeArray();

            $.map(formArray, function(field){
                if (field.name.indexOf(addressSubStr) < 0 && field.name.indexOf(floorSubStr) < 0) {
                    data[field.name] = field.value;
                }
            });

            data.points = mapPointsController.toJSONArray();

            return data;
        }

        $('#reqFormSubmit').click(function(e){
            e.preventDefault();
            var $this = $(this);
            try {
                var $form = $('#reqForm');
                var valid = true;

                var $phone = $('#reqForm #id_phone');
                var $text = $('#reqForm #id_text');
                var $date = $('#reqForm #id_date');

                var requiredFields = [$phone, $text, $date];

                requiredFields.forEach(function($field){
                    $field.parent().removeClass('has-error');
                    if (!$field.val()) {
                        $field.parent().addClass('has-error');
                        valid = false;
                    }
                });

                var phoneValue = $phone.val();
                if (phoneValue && phoneValue.length !== phoneLength) {
                    valid = false;
                    $phone.parent().addClass('has-error');
                }

                var $email = $('#reqForm #id_email');
                var emailValue = $email.val();

                if (!!emailValue && !validateEmail(emailValue)) {
                    $email.parent().addClass('has-error');
                    valid = false
                }

                if (!valid){
                    return false;
                }
                $this.attr('disabled', 'disabled');

                var data = getSubmitData($form);

                $('#reqModal').modal('show');
                $('#successTimerWrapper').html('<canvas width="150" height="150" id="successTimer"></canvas>');

                var $reqModalText = $('#reqModalText');
                $reqModalText.text(requestIsSendingText);

                $.ajax({
                    url: $form.attr('action'),
                    type: 'POST',
                    contentType: 'application/json',
                    data: JSON.stringify(data),
                    success: function(data){
                        setTimeout(function(){ animateSuccess('successTimer'); }, 1);
                        grecaptcha.reset();
                        $form.trigger('reset');
                        $form.find('.has-error').removeClass('has-error');
                        mapPointsController.initializePoints();
                        resetTransportationType();
                        $('#porters-number-wrapper').css('display', 'none');
                        $reqModalText.text(successRequestText);
                    },
                    error: function(err){
                        console.log(err);
                        grecaptcha.reset();
                        $reqModalText.text(errorRequestText);
                    }
                });
            } catch (err) {
                console.err(err);
                console.log(err.stack);
            } finally {
                return false;
            }
        });

        $('#reqForm').submit(function(e){
            e.preventDefault();
            return false;
        });

        $('.language-switcher').click(function(e){
            e.preventDefault();
            var $link = $(this);
            var languageCode = $link.data('lang');
            var $form = $link.parents('form').first();
            $form.find('input[name="language"]').val(languageCode);
            $form.submit();
        });

        $('#reqForm #id_phone').mask('(00) 000-00-00');

        $('#id_is_need_porters').change(function(){
            if (this.checked) {
                $('#porters-number-wrapper').css('display', 'block');
            } else {
                $('#porters-number-wrapper').css('display', 'none');
            }
        });

        $('#transportationTypeDescription').popover({
            title: function(){
                return $('#transportationTypeDescription').data('current-title');
            },
            content: function () {
                return $('#transportationTypeDescription').data('current-content');
            }
        });

        $('#id_transportation_type').change(function(){
            var $select = $(this);
            var $selectedOption = $select.find('option[value="' + $select.val() + '"]');

            if (!$selectedOption.val()){
                resetTransportationType();
            } else {
                var $popoverElem = $('#transportationTypeDescription');

                $popoverElem.data('current-title', $selectedOption.text());
                $popoverElem.data('current-content', $selectedOption.data('content'));
            }
        });

        function resetTransportationType(){
            var $popoverElem = $('#transportationTypeDescription');

            $popoverElem.data('current-title', $popoverElem.data('default-title'));
            $popoverElem.data('current-content', $popoverElem.data('default-content'));
        }

        $('a[href="#page-top"]').click(function(){
            $('html, body').stop().animate({
                scrollTop: 0
            }, 1500, 'easeInOutExpo');
        });

        var today = new Date();
        $('.datepicker').val(today.getDate() + '/' + (today.getMonth() + 1) + '/' + today.getFullYear()).datepicker({
            format: 'dd/mm/yyyy',
            startDate: '0',
            autoclose: true,
            language: currentLanguage,
            orientation: 'bottom'
        });
        $('.timepicker').timepicker({
            minuteStep: 5,
            showMeridian: false,
            defaultTime: false
        });
        $('.js-captcha-refresh').click(function(){
            $.getJSON($(this).data('url'), {}, function(json) {
                // This should update your captcha image src and captcha hidden input
            });
            return false;
        });

        $('#addMapPoint').click(function(){
            mapPointsController.addMapPoint();
        });
        $('#removeMapPoint').click(function(){
            mapPointsController.removeLastPoint();
        });

    });
})();

// requestAnimationFrame Shim
(function() {
    var requestAnimationFrame = window.requestAnimationFrame || window.mozRequestAnimationFrame ||
                              window.webkitRequestAnimationFrame || window.msRequestAnimationFrame;
    window.requestAnimationFrame = requestAnimationFrame;
})();
