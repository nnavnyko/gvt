/**
 * Home page base script
 *
 * @requires: jQuery 3.x.x
 **/

$(document).ready(function(){
    /**
     * Switch language on click Language Code in the header menu
     * @param {object} e: JS Event on click
     **/
    $('.language-switcher').click(function(e){
        e.preventDefault();
        var $link = $(this);
        var languageCode = $link.data('lang');

        var $form = $link.parents('form').first();
        $form.find('input[name="language"]').val(languageCode);
        $form.submit();
    });

    $('a.scroll-to').click(function(e){
        e.preventDefault();
        var elem = $(this);
        var id = elem.attr('href');
        var position = $(id).offset();

        if (typeof position !== 'undefined') {
            $('html, body').stop().animate({
                scrollTop: position.top
            }, 1500, 'easeInOutExpo');
        }
        elem.blur();
    });

    $('.call-phone-link').click(function() {
        gtag('event', 'click', {
            'event_category': 'tel',
            'event_label': 'click_tel'
        });
    });
});
