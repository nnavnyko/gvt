"use strict";

require("core-js/modules/es.promise");

require("core-js/modules/es.string.trim");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.MapPoint = void 0;

/**
 * Map point (location, address, order)
 * Put map point functionality here
 *
 * Using ES6
 */
const ACTIVE_CLASS = 'active';
const SEARCH_BY_COORDS_URL = 'https://nominatim.openstreetmap.org/search/';
const SEARCH_ONE_PARAMS = 'format=json&addressdetails=1&limit=1&&accept-language=ru';
const SEARCH_BY_ADDRESS_URL = 'https://nominatim.openstreetmap.org/search/?countrycodes=by&';
const LABEL_CLASS = 'gvt-map-point-label';

class MapPoint {
  /**
   * MapPoint constructor
   */
  constructor(map, order) {
    this.container = $('<div class="row form-group gvt-map-point-container">');
    this.addressWrapper = $('<div class="gvt-map-point col-md-9 col-sm-9 col-xs-7">');
    this.addressInput = $("<input type=\"text\" name=\"address_".concat(order, "\" \n            placeholder=\"").concat(toCompleteText, "\" class=\"map-autocomplete form-control tt-input\">"));
    this.floorInput = $("<input name=\"floor_".concat(order, "\" type=\"number\" \n            min=\"1\" class=\"form-control\" placeholder=\"").concat(floorText, "\">"));
    this.markerIcon = $('<i class="cur-point fa fa-2x fa-map-marker cursor-pointer">');
    this.removeBtn = $('<button type="button" class="btn btn-warning btn-sm remove-map-point">');
    this.map = map;
    this.address = '';
    this.longitude = null;
    this.latitude = null;
    this.sortOrder = order;
    this.isActive = false;
    this.marker = null;
    this.turfPoint = null;
    this.onchangeCallbacks = [];

    let _this = this;

    this.addressInput.on('keyup', e => {
      if (e.keyCode === 13) {
        _this.searchByAddress();
      }
    }).on('blur', () => {
      _this.searchByAddress();
    });
  }
  /**
   * Get jQuery object of Map Point input
   * @return {object}: jQuery object
   */


  render() {
    let addressFormGroup = $('<div class=" gvt-map-point col-md-10 col-sm-10 col-xs-10">');
    let markerWrapper = $('<div class="col-md-2 col-sm-2 col-xs-2 cur-point-wrapper text-center">');
    markerWrapper.append(this.markerIcon);
    addressFormGroup.append(this.addressInput);
    this.addressWrapper.append(markerWrapper);
    this.addressWrapper.append(addressFormGroup);
    let floorWrapper = $('<div class="col-md-2 floor-wrapper col-sm-2 col-xs-3">');
    let removeButtonWrapper = $('<div class="col-md-1 col-sm-1 col-xs-2 remove-btn-wrapper">');
    this.removeBtn.append('<i class="fa fa-trash-o">');
    removeButtonWrapper.append(this.removeBtn);
    floorWrapper.append(this.floorInput);
    return this.container.append(this.addressWrapper).append(floorWrapper).append(removeButtonWrapper);
  }
  /**
   * Add marker to map
   */


  addToMap() {
    let coordinates = [this.longitude, this.latitude];
    this.marker = new mapboxgl.Marker({
      draggable: true,
      color: '#414141'
    }).setLngLat(coordinates).addTo(this.map);

    let _this = this;

    this.marker.on('dragend', () => {
      let lngLat = _this.marker.getLngLat();

      _this.setCoords(lngLat.lng, lngLat.lat);

      _this.searchAddressByCoords();

      _this.triggerChange();
    });
  }
  /**
   * Active map point
   */


  activate() {
    this.isActive = true;
    this.addressWrapper.addClass(ACTIVE_CLASS);
    this.setAsCenter();
  }
  /**
   * Set as center
   */


  setAsCenter() {
    if (this.longitude && this.latitude) {
      this.map.flyTo({
        center: [this.longitude, this.latitude]
      });
    }
  }
  /**
   * deactive map point
   */


  deactivate() {
    this.isActive = false;
    this.addressWrapper.removeClass(ACTIVE_CLASS);
  }
  /**
   * Move point to specific coords
   * @param {number} longitude
   * @param {number} latitude
   */


  move(longitude, latitude) {
    this.setCoords(longitude, latitude);
    this.marker.setLngLat([this.longitude, this.latitude]);
  }
  /**
   * Set coordinates to object
   */


  setCoords(lng, lat) {
    this.longitude = lng;
    this.latitude = lat;

    if (!this.marker) {
      this.addToMap();
    }

    this.setTurfPoint();
  }
  /**
   * Search address by coordinates of map point
   */


  searchAddressByCoords() {
    let coords = [this.latitude, this.longitude].join(',');
    let url = "".concat(SEARCH_BY_COORDS_URL).concat(coords, "?").concat(SEARCH_ONE_PARAMS);

    let _this = this;

    return new Promise(function (resolve, reject) {
      $.ajax({
        url,
        type: 'GET',
        success: resp => {
          let respObj = resp && resp.length ? resp[0] : null;
          _this.address = _this.getHumanReadableAddress(respObj, _this.address);

          _this.addressInput.val(_this.address);

          _this.triggerChange();

          if (resolve) {
            resolve();
          }
        },
        error: reject
      });
    });
  }
  /**
   * Get human-readable address
   * @param {object} resp: Mapbox response
   * @param {string} [defaultAddress]: string
   * @return {string}
   */


  getHumanReadableAddress(resp, defaultAddress) {
    let address = defaultAddress || '';

    if (resp && resp.address) {
      let respAddress = resp.address;
      let addressArr = [];
      let keys = ['country', 'city', 'town', 'road', 'house_number'];
      keys.forEach(key => {
        let localCountry = ['беларусь', 'belarus'];

        if (respAddress[key] && (key !== 'country' || localCountry.indexOf(respAddress[key].toLowerCase()) < 0)) {
          addressArr.push(respAddress[key]);
        }
      });
      address = addressArr.join(' ');
    }

    return address;
  }
  /**
   * Remove map point
   * Remove DOM element, remove marker from map
   */


  remove() {
    if (this.marker) {
      this.marker.remove();
    }

    this.marker = null;
    this.turfPoint = null;
    this.address = null;
    this.container.remove();
  }
  /**
   * Get coordinates as [longitude, latitude]
   * @return {number[]}: array of coords
   **/


  getCoordinates() {
    if (!this.longitude || !this.latitude) {
      return null;
    }

    return [this.longitude, this.latitude];
  }
  /**
   * Set turf.js point
   */


  setTurfPoint() {
    let coordinates = this.getCoordinates();
    this.turfPoint = turf.point(coordinates, {
      orderTime: Date.now(),
      key: Math.random()
    });
  }
  /**
   * Add onchange callback
   */


  onchange(callback) {
    this.onchangeCallbacks.push(callback);
  }
  /**
   * Trigger change and call all callbacks
   */


  triggerChange() {
    this.onchangeCallbacks.forEach(callback => {
      callback();
    });
  }
  /**
   * Search by address
   */


  searchByAddress() {
    this.address = this.addressInput.val() || '';
    let address = this.address.trim();
    let url = "".concat(SEARCH_BY_ADDRESS_URL, "q=").concat(address, "&").concat(SEARCH_ONE_PARAMS);

    let _this = this;

    $.ajax({
      url,
      type: 'GET',
      success: resp => {
        let respObj = resp && resp.length ? resp[0] : null;

        if (respObj) {
          let longitude = parseFloat(respObj.lon);
          let latitude = parseFloat(respObj.lat);

          _this.move(longitude, latitude);

          _this.searchAddressByCoords();

          _this.setAsCenter();

          _this.triggerChange();
        }
      }
    });
  }
  /**
   * Set Marker Class
   * @param {string} markerLabel: name of icon's class
   */


  setMarkerLabel(markerLabel) {
    if (this.marker) {
      let el = this.marker.getElement();
      let labelElements = el.getElementsByClassName(LABEL_CLASS);
      let labelEl;

      if (labelElements.length) {
        labelEl = labelElements[0];
      } else {
        labelEl = document.createElement('label');
        labelEl.className = LABEL_CLASS;
        el.appendChild(labelEl);
      }

      labelEl.innerText = markerLabel;
    }
  }
  /**
   * Validate map point
   * @return {boolean}
   */


  validate() {
    let floor = parseInt(this.floorInput.val());
    return !!this.address && (isNaN(floor) || floor > 0);
  }

}

exports.MapPoint = MapPoint;