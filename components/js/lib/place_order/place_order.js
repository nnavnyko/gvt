"use strict";

require("core-js/modules/es.regexp.to-string");

require("core-js/modules/es.string.replace");

require("core-js/modules/es.string.starts-with");

var _map = require("./map/map.controller");

/**
 * Place order - make Transportation Request
 */
(function () {
  $(document).ready(function () {
    let mapPointsController = new _map.MapController();
    let phoneLength = 14;
    const PHONE_REGEX = /[\s\\(\\)\\-]*/g;

    function getSubmitData($form) {
      /**
       * Get JSON format of the submitted data
       * @param $form: jQuery object (Order form)
       * @return: object
       */
      let addressSubStr = 'address';
      let floorSubStr = 'floor';
      let data = {
        points: []
      };
      let formArray = $form.serializeArray();
      $.map(formArray, function (field) {
        if (field.name.indexOf(addressSubStr) < 0 && field.name.indexOf(floorSubStr) < 0) {
          data[field.name] = field.value;
        }
      });
      data.points = mapPointsController.toJSONArray();
      return data;
    }

    $('#reqFormSubmit').click(function (e) {
      e.preventDefault();
      let $this = $(this);

      try {
        var errorElement = null;
        let $form = $('#reqForm');
        let valid = true;
        let $phone = $('#reqForm #id_phone');
        let $text = $('#reqForm #id_text');
        let $date = $('#reqForm #id_date');
        let requiredFields = [$phone, $text, $date];
        requiredFields.forEach(function ($field) {
          $field.parent().removeClass('has-error');

          if (!$field.val()) {
            $field.parent().addClass('has-error');
            valid = false;
            errorElement = errorElement || $field;
          }
        });
        let phoneValue = $phone.val();

        if (phoneValue && phoneValue.length !== phoneLength) {
          valid = false;
          $phone.parent().addClass('has-error');
          errorElement = errorElement || $phone;
        }

        let $email = $('#reqForm #id_email');
        let emailValue = $email.val();

        if (!!emailValue && !validateEmail(emailValue)) {
          $email.parent().addClass('has-error');
          valid = false;
          errorElement = errorElement || $email;
        }

        if (!valid) {
          if (errorElement) {
            errorElement.get(0).scrollIntoView({
              behavior: 'smooth'
            });
          }

          return false;
        }

        $this.attr('disabled', 'disabled');
        let data = getSubmitData($form);
        $('#reqModal').modal('show');
        $('#successTimerWrapper').html('<canvas width="150" height="150" id="successTimer"></canvas>');
        let $reqModalText = $('#reqModalText');
        $reqModalText.text(requestIsSendingText);
        $.ajax({
          url: $form.attr('action'),
          type: 'POST',
          contentType: 'application/json',
          data: JSON.stringify(data),
          success: function success(data) {
            setTimeout(function () {
              animateSuccess('successTimer');
            }, 1);
            grecaptcha.reset();
            $form.trigger('reset');
            $form.find('.has-error').removeClass('has-error');
            mapPointsController.reinitializeMapPoints();
            resetTransportationType();
            $('#porters-number-wrapper').css('display', 'none');
            $('#routeLength').text();
            $reqModalText.text(successRequestText);
            gtag('event', 'submit', {
              'event_category': 'form',
              'event_label': 'submit_form'
            });
          },
          error: function error(err) {
            console.log(err);
            grecaptcha.reset();
            $reqModalText.text(errorRequestText);
          }
        });
        return false;
      } catch (err) {
        console.err(err);
        console.log(err.stack);
        return false;
      }
    });
    $('#reqForm').submit(function (e) {
      e.preventDefault();
      return false;
    });
    var phone = $('#reqForm #id_phone');
    const PHONE_PREFIX_BY = '375';
    phone.on('keyup', function () {
      var value = phone.val().replace(PHONE_REGEX, '');

      if (value.startsWith(PHONE_PREFIX_BY)) {
        phone.val(value.substr(PHONE_PREFIX_BY.length));
      }
    });
    phone.mask('(00) 000-00-00');
    $('#id_is_need_porters').change(function () {
      if (this.checked) {
        $('#porters-number-wrapper').css('display', 'block');
      } else {
        $('#porters-number-wrapper').css('display', 'none');
      }
    });
    $('#transportationTypeDescription').popover({
      title: function title() {
        return $('#transportationTypeDescription').data('current-title');
      },
      content: function content() {
        return $('#transportationTypeDescription').data('current-content');
      }
    });
    $('#id_transportation_type').change(function () {
      let $select = $(this);
      let $selectedOption = $select.find('option[value="' + $select.val() + '"]');

      if (!$selectedOption.val()) {
        resetTransportationType();
      } else {
        let $popoverElem = $('#transportationTypeDescription');
        $popoverElem.data('current-title', $selectedOption.text());
        $popoverElem.data('current-content', $selectedOption.data('content'));
      }
    });

    function resetTransportationType() {
      let $popoverElem = $('#transportationTypeDescription');
      $popoverElem.data('current-title', $popoverElem.data('default-title'));
      $popoverElem.data('current-content', $popoverElem.data('default-content'));
    }

    $('a[href="#page-top"]').click(function () {
      $('html, body').stop().animate({
        scrollTop: 0
      }, 1500, 'easeInOutExpo');
    });
    let today = new Date();
    let month = (today.getMonth() + 1).toString();

    if (month.length === 1) {
      month = '0' + month;
    }

    let day = today.getDate().toString();

    if (day.length === 1) {
      day = '0' + day;
    }

    $('.datepicker').val(day + '/' + month + '/' + today.getFullYear()).datepicker({
      format: 'dd/mm/yyyy',
      startDate: '0',
      autoclose: true,
      language: currentLanguage,
      orientation: 'bottom'
    });
    $('.timepicker').timepicker({
      minuteStep: 5,
      showMeridian: false,
      defaultTime: false
    });
    $('.js-captcha-refresh').click(function () {
      $.getJSON($(this).data('url'), {}, function (json) {// This should update your captcha image src and captcha hidden input
      });
      return false;
    });
  });
})(); // requestAnimationFrame Shim


(function () {
  let requestAnimationFrame = window.requestAnimationFrame || window.mozRequestAnimationFrame || window.webkitRequestAnimationFrame || window.msRequestAnimationFrame;
  window.requestAnimationFrame = requestAnimationFrame;
})();