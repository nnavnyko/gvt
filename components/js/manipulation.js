$(document).ready(function () {
    $('.gvt-manipulation-images-container').magnificPopup({
        delegate: 'a',
        type: 'image',
        mainClass: 'mfp-img-mobile',
		gallery: {
			enabled: true,
			navigateByImgClick: true
		},
        zoom: {
            enabled: true,
            duration: 300
        }
    });
});