/**
 * Map controller
 * Control adding/remove/move map points
 */


const MAP_STYLE = 'mapbox://styles/nnavnyko/cjpslwb1u07vc2rmhox79bm4h';

const ROUTE_URL = 'https://api.mapbox.com/optimized-trips/v1/mapbox/driving/';
const ROUTE_PARAMS = '?overview=full&steps=true&geometries=geojson&source=first&roundtrip=false&destination=last&access_token=';

import {MapPoint} from "./map-point.model";
import { MapboxStyleSwitcherControl } from "mapbox-gl-style-switcher";


class MapController {
    constructor() {
        this.map = new mapboxgl.Map({
            container: 'gvt-place-order-map',
            style: MAP_STYLE,
            center: [29.2223129, 52.0322082],
            zoom: 14,
            minZoom: 5.5,
            maxZoom: 18.5,
            dragRotate: false
        });

        this.routePoints = turf.featureCollection([]);
        this.mapLoaded = false;

        this.initializeMap();
        this.initializeMapPointBtn();
    }

    /**
     * Initialize mapbox map
     */
    initializeMap() {
        this.addControlsToMap();

        this.map.on('load', () => {
            this.map.setLayoutProperty('country-label-lg', 'text-field', ['get', 'name_ru']);

            if (window && window.navigator && 'geolocation' in navigator) {
                navigator.geolocation.getCurrentPosition( (position) => {
                    this.map.flyTo({center: [position.coords.longitude, position.coords.latitude]});
                });
            }
            this.initializeLayers();
            this.mapLoaded = true;

            this.initializeMapPoints();
        });
    }

    /**
     * Add default controls to map area, such as 'Fullscreen' btn, 'Scale Control', etc.
     */
    addControlsToMap() {
        this.map.addControl(new mapboxgl.FullscreenControl());

        this.map.addControl(new mapboxgl.ScaleControl({
            maxWidth: 80
        }));

        this.map.addControl(new mapboxgl.NavigationControl({showCompass: false}));
        this.initializeMapStyleSwitcher();
    }

    /**
     * Initialize map points
     * Add two map points
     */
    initializeMapPoints() {
        this.mapPoints = [];
        this.mapPointsElem = $('#maps-points');

        this.activatePoint(this.addMapPoint());
        this.addMapPoint();

        this.addClickListener();
    }

    /** Initialize route layers */
    initializeLayers() {
        let nothing = turf.featureCollection([]);
        let mapboxStyle = this.map.getStyle();

        let waterwayLayer = mapboxStyle.name === 'Outdoors' ? 'waterway-label' : null;

        this.map.addLayer({
            id: 'dropoffs-symbol',
            type: 'symbol',
            source: {
                data: this.routePoints,
                type: 'geojson'
            },
            layout: {
                'icon-allow-overlap': true,
                'icon-ignore-placement': true,
                'icon-image': 'marker-15',
            }
        });

        this.map.addSource('route', {
            type: 'geojson',
            data: nothing
        });

        this.map.addLayer({
            id: 'routeline-active',
            type: 'line',
            source: 'route',
            layout: {
                'line-join': 'round',
                'line-cap': 'round'
            },
            paint: {
                'line-color': '#3c3b3c',
                'line-width': {
                    base: 2.5,
                    stops: [[12, 3], [22, 12]]
                }
            }
        }, waterwayLayer);

        this.map.addLayer({
            id: 'routearrows',
            type: 'symbol',
            source: 'route',
            layout: {
                'symbol-placement': 'line',
                'text-field': '▶',
                'text-size': {
                    base: 2,
                    stops: [[12, 24], [22, 60]]
                },
                'symbol-spacing': {
                    base: 1,
                    stops: [[12, 30], [22, 160]]
                },
                'text-keep-upright': false
            },
            paint: {
                'text-color': '#3c3b3c',
                'text-halo-color': 'hsl(55, 11%, 96%)',
                'text-halo-width': 3
            }
        }, waterwayLayer);
        if (this.mapLoaded) {
            this.rebuildRoute();
        }
    }

    /**
     * Add map point to Map
     * Render map point inputs (address + flow)
     * @param {boolean} [activateOnAdd]: true if need to activate on add
     */
    addMapPoint(activateOnAdd) {
        let mapPoint = new MapPoint(this.map, this.mapPoints.length);
        if (activateOnAdd) {
            this.activatePoint(mapPoint);
        }

        mapPoint.onchange(() => {
            this.rebuildRoute();

            let nextMapPoint = this.mapPoints.find((mapPoint) => {
                return !mapPoint.longitude && !mapPoint.latitude;
            });

            if (nextMapPoint) {
                this.activatePoint(nextMapPoint);
                nextMapPoint.addressInput.trigger('focus');
            }

            this.setPointLabels();
        });

        this.mapPoints.push(mapPoint);

        mapPoint.addressInput.on('focus', () => {
            this.activatePoint(mapPoint);
        });
        mapPoint.markerIcon.click(() => {
            this.toggleActivePoint(mapPoint);
        });
        mapPoint.removeBtn.click(() => {
            this.removePoint(mapPoint);
        });

        this.mapPointsElem.append(mapPoint.render());
        this.setContainerClass();

        return mapPoint;
    }

    /**
     * Activate map point. Set center map to point
     * @param {MapPoint} mapPoint
     */
    activatePoint(mapPoint) {
        this.mapPoints.forEach((point) => { point.deactivate(); });

        mapPoint.activate();
    }


    /**
     * Toggle active map point
     * @param {MapPoint} mapPoint
     */
    toggleActivePoint(mapPoint) {
        if (!mapPoint.isActive) {
            this.activatePoint(mapPoint);
        } else {
            mapPoint.deactivate();
        }
    }

    /**
     * Add click listener to map
     * Set map point coordinates and address on click
     */
    addClickListener() {
        this.map.on('click', (e) => {
            let currentPoint = this.getActivePoint();
            if (!currentPoint) {
                return;
            }
            currentPoint.move(e.lngLat.lng, e.lngLat.lat);

            currentPoint.searchAddressByCoords();
        });
    }

    /**
     * Get active map point
     * @return {object} - MapPoint instance
     */
    getActivePoint() {
        return this.mapPoints.find((point) => point.isActive);
    }

    /**
     * Rebuild route - display route line
     */
    rebuildRoute() {
        let coords = [];
        this.mapPoints.forEach((point) => {
            let pointCoords = point.getCoordinates();

            if (pointCoords) {
                coords.push(pointCoords);
            }
        });

        if (coords.length < 2) {
            let nothing = turf.featureCollection([]);

            this.map.getSource('route').setData(nothing);
            this.map.getSource('dropoffs-symbol').setData(nothing);

            return;
        }

        let url = ROUTE_URL + coords.join(';') + ROUTE_PARAMS + mapboxgl.accessToken;
        let _this = this;

        $.ajax({
            url,
            type: 'GET',
            success: (data) => {
                let trips = !data.trips.length || !data.trips[0] ? null : data.trips[0];
                if (!trips) {
                    let nothing = turf.featureCollection([]);
                    _this.map.getSource('route')
                        .setData(nothing);
                    _this.setRouteLength(null);
                } else {
                    let routeGeoJSON = turf.featureCollection([turf.feature(trips.geometry)]);
                    _this.map.getSource('route')
                        .setData(routeGeoJSON);
                    _this.setRouteLength(trips.distance);
                }

                _this.map.getSource('dropoffs-symbol')
                  .setData(_this.routePoints);
            }
        })
    }

    /**
     * Set route length value and display value
     * @param {number|null} distance
     */
    setRouteLength(distance) {
        let routeLengthInput = $('#id_route_length');
        let routeLengthDisplay = $('#routeLength');

        if (distance !== null) {
            let displayDistance = (distance/1000).toFixed(1);

            routeLengthInput.val(distance);
            routeLengthDisplay.text(displayDistance);
        } else {
            routeLengthInput.val(0);
            routeLengthDisplay.text(routeIsNotSet);
        }
    }

    /**
     * Rebuild turf route points collection
     */
    rebuildRoutePoints() {
        this.routePoints = turf.featureCollection([]);

        this.routePoints.forEach((point) => {
            if (point.turfPoint) {
                this.routePoints.features.push(point.turfPoint);
            }
        });
    }

    /**
     * Initialize Add map point
     */
    initializeMapPointBtn() {
        $('#addMapPoint').click(() => this.addMapPoint(true));
    }

    /**
     * Remove Map point
     * @param {MapPoint} mapPoint
     */
    removePoint(mapPoint) {
        if (this.mapPoints.length <= 2) {
            throw Error('Cannot remove Map point - at least 2 map points must present!');
        }
        mapPoint.remove();

        this.mapPoints.splice(this.mapPoints.indexOf(mapPoint), 1);

        this.rebuildRoute();
        this.setContainerClass();
        this.setPointLabels();
        this.setSortOrder();
    }

    /**
     * Set container class to show/hide Remove points
     */
    setContainerClass() {
        let container = $('#maps-points');
        if (this.mapPoints.length > 2 && !container.hasClass('multiple-route-points')) {
            container.addClass('multiple-route-points');
        } else if (this.mapPoints.length < 3) {
            container.removeClass('multiple-route-points');
        }
    }

    /**
     * Set points labels
     */
    setPointLabels() {
        let lastIndex = this.mapPoints.length - 1;
        this.mapPoints.forEach((mapPoint, index) => {
            let label = `${index + 1}`;

            if (index === 0) {
                label = fromText;
            } else if (index === lastIndex) {
                label = toText;
            }

            mapPoint.setMarkerLabel(label);
        });
    }

    /**
     * Convert map points to JSON Array
     */
    toJSONArray(){
        let points = [];

        this.mapPoints.forEach(function(mapPoint) {
            if (mapPoint.validate()) {
                points.push({
                    sort_order: mapPoint.sortOrder,
                    longitude: mapPoint.longitude,
                    latitude: mapPoint.latitude,
                    address: mapPoint.address,
                    floor: mapPoint.floorInput.val()
                });
            }
        });

        return points;
    }

    /** Reinitialize map points */
    reinitializeMapPoints() {
        this.activatePoint(this.addMapPoint());
        this.addMapPoint();

        let lastIndex = this.mapPoints.length - 2;
        for (let i = 0; i < lastIndex; i++) {
            this.removePoint(this.mapPoints[0]);
        }

        this.rebuildRoute();
    }

    /**
     * Set sort order of each map point
     */
    setSortOrder() {
        this.mapPoints.forEach((mapPoint, index) => {
            mapPoint.sortOrder = index;
        });
    }


    /**
     * Initialize Map Style switcher control
     * Add callbacks
     */
    initializeMapStyleSwitcher() {
        let switcherCtrl = new MapboxStyleSwitcherControl([
            {
                title: mapSelectorMap,
                uri: MAP_STYLE,
            },
            {
                title: mapSelectorSatellite,
                uri: 'mapbox://styles/mapbox/satellite-v9',
            },
        ]);

        this.map.addControl(switcherCtrl);

        this.map.on('style.load', () => {
            if (this.mapLoaded) {
                this.initializeLayers();
            }
        });
    }
}

export {MapController};

