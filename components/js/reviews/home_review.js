/**
 * Send Review from Home page
 * Check for Google ReCaptcha
 * Check for required fields
 */
;
function activateReviewBtn(gcaptchaResponse){
    /**
     * Activate Send review button
     */
    $('#sendReview').removeAttr('disabled');
    $('#reviewForm').find('[name="review-g-recaptcha-response"]').val(gcaptchaResponse);
}
$(document).ready(function(){
    var phoneLength = 14;

    $('#showReviewModalBtn').click(function(e){
        e.preventDefault();
        $('#reviewModal').modal('show');

        setRating(0);
        renderReCaptcha('reviewRecaptcha');
        $('#reviewModalDescription').show();
        var $form = $('#reviewForm');
        $form.show();
        $form.trigger('reset');
        $form.find('.has-error').removeClass('has-error');

        $('#sendReview').show();
        $('#reviewIsSendWrapper').hide();
        $('#reviewModalText').text(requestIsSendingText);
        $('#sendReviewTimerWrapper').html('');
    });

    $('#reviewForm #id_phone').mask('(00) 000-00-00');

    $('#sendReview').click(function(e){
        /**
         * Send Review and handle response
         */
        e.preventDefault();
        var $this = $(this); // send review button
        var $form = $('#reviewForm');

        var requiredIDs = ['id_name', 'id_text'];
        var formIsValid = true;

        requiredIDs.forEach(function(requiredFieldID){
            if (!$form.find('#' + requiredFieldID).val()) {
                formIsValid = false;
            }
        });

        var $email = $form.find('input[name="email"]');
        var emailValue = $email.val();

        if (!!emailValue && !validateEmail(emailValue)) {
            $email.parent().addClass('has-error');
            formIsValid = false;
        }

        var phoneValue = $form.find('#id_phone').val();
        if (phoneValue && phoneValue.length !== phoneLength) {
            formIsValid = false;
            $form.find('#id_phone').parent().addClass('has-error');
        }

        renderReCaptcha('reviewRecaptcha');
        if (formIsValid) {
            var $reviewResponseTextWrapper = $('#reviewIsSendWrapper');

            $reviewResponseTextWrapper.show();
            $('#sendReviewTimerWrapper').html('<canvas width="150" height="150" id="sendReviewSuccessTimer"></canvas>');

            $this.attr('disabled', 'disabled').hide();
            $form.find('has-error').removeClass('has-error');
            $form.hide();

            $.ajax({
                url: sendReviewUrl,
                type: 'POST',
                data: $form.serialize(),
                success: function(){
                    $form.trigger('reset');
                    $('#reviewModalDescription').hide();
                    setTimeout(function(){ animateSuccess('sendReviewSuccessTimer'); }, 1);
                    $('#reviewModalText').text(reviewIsSentText);
                    setRating(0);
                },
                error: function(err) {
                    console.dir(err);
                    $('#reviewModalText').text(reviewSendError);
                }
            });
        } else {
            $form.find('.form-group.review-required').addClass('has-error');
        }
    });

    $('.review-rating').click(function(){
        /**
         * Set rating
         */
        var $this = $(this);
        var selectedRaging = $this.data('rating');
        setRating(selectedRaging);
    });

    function setRating(selectedRaging) {
        $('#id_rating').val(selectedRaging);
        var ratingFill = 'fa-star';
        var ratingEmpty = 'fa-star-o';

        $('.review-rating').each(function(){
            var $rating = $(this);

            if ($rating.data('rating') > selectedRaging) {
                $rating.addClass(ratingEmpty);
                $rating.removeClass(ratingFill);
            } else {
                $rating.addClass(ratingFill);
                $rating.removeClass(ratingEmpty);
            }
        });
    }
});
