;
(function(){
    $(document).ready(function(){
        function animateSuccess(){
            var _this = this;
            var canvas, context, x, y, radius,
                endPercent, curPerc, counterClockwise, circ, quart,
                process, curCheckedX, curCheckedY, halfCheckedX, endCheckedX;

            canvas = document.getElementById('successTimer');
            context = canvas.getContext('2d');
            x = canvas.width / 2;
            y = canvas.height / 2;
            radius = 60;
            endPercent = 101;
            curPerc = 0;
            counterClockwise = false;
            circ = Math.PI * 2;
            quart = Math.PI / 2;
            context.lineWidth = 4;
            context.strokeStyle = '#399B5E';
            context.shadowOffsetX = 0;
            context.shadowOffsetY = 0;
            context.shadowBlur = 2;
            context.shadowColor = '#000';
            process = -1;
            context.font = '24px Calibri ';
            context.fillStyle = '#399B5E';
            curCheckedX = startCheckedX = x-38;
            curCheckedY = startCheckedY = y-9;
            halfCheckedX = x-5;
            endCheckedX = x+38;

            function animate(current) {
                context.clearRect(0, 0, canvas.width, canvas.height);
                context.beginPath();
                context.arc(x, y, radius, -(quart), ((circ) * current) - quart, false);
                context.stroke();
                if (process < 99)
                    process++;
                context.fillText(process + '%', x-22, y+8);
                curPerc++;
                if (curPerc < endPercent) {
                    setTimeout(function () {
                     animate(curPerc / 100);
                    }, 3);
                } else {
                    preDrawChecked();
                }
            }

            function preDrawChecked(){
                context.moveTo(startCheckedX, startCheckedY);
                context.clearRect(x-35, y-20, x/2, y/2);
                drawChecked(curCheckedX, curCheckedY);
            }

            function drawChecked(currentX, currentY){
                context.clearRect(0, 0, canvas.width, canvas.height);
                if (currentX < halfCheckedX)
                    currentY++;
                else
                    currentY--;
                currentX++;
                context.lineWidth = 3;
                context.lineTo(currentX, currentY);
                context.stroke();
                if (currentX < endCheckedX){
                    setTimeout(function () {
                     drawChecked(currentX, currentY);
                    }, 3);
                }
            }
            animate();
        }
        $('#reqFormSubmit').click(function(e){
            e.preventDefault();
            try {
                var $form = $('#reqForm');
                var $this = $(this);
                $this.attr('disabled', 'disabled');
                var valid = true;
                var $phone = $('#id_phone');
                var $text = $('#id_text');
                $phone.parent().removeClass('has-error');
                $text.parent().removeClass('has-error');
                if (!$phone.val()){
                    $phone.parent().addClass('has-error');
                    valid = false;
                }
                if (!$text.val()){
                    $text.parent().addClass('has-error');
                    valid = false;
                }
                if (!valid){
                    $this.removeAttr('disabled');
                    return false;
                }
                $('#reqModal').modal('show');
                $('#successTimerWrapper').html('<canvas width="150" height="150" id="successTimer"></canvas>');
                var $reqModalText = $('#reqModalText');
                $reqModalText.text('Запрос отправляется... ');
                console.log('SEND');
                $.ajax({
                    url: $form.attr('action'),
                    type: 'POST',
                    data: $form.serialize(),
                    success: function(data){
                        setTimeout(animateSuccess, 1);
                        $form.trigger('reset');
                        $this.removeAttr('disabled');
                        $reqModalText.text('Ваш запрос отправлен. С вами свяжется наш диспетчер');
                        if (route)
                            map.geoObjects.remove(route);
                        if (places.length){
                            for(var i = 0; i < places.length; i++){
                                map.geoObjects.remove(places[i]);
                            }
                            places = [];
                        }
                    },
                    error: function(err){
                        console.log(err);
                        $this.removeAttr('disabled');
                        $reqModalText.text('Произошла ошибка. Приносим извинения. Повторите заявку');
                    }
                });
            } catch (err) {
                console.log(err);
                $this.removeAttr('disabled');
                console.log(err.stack);
            } finally {
                return false;
            }
        });
        $('#reqForm').submit(function(e){
            e.preventDefault();
            console.log('SUBMIT');
            return false;
        });
    });
})();