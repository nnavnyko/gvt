;
var route, places = [], map;
(function(){
    var typeaheadOptions = {
        limit: 10
    };
    var typeaheadDatasets = {
        name: 'yandex-autocomplete',
        source: function(query, sync, async) {
            ymaps.suggest(query, {results: 25, bounedBy:[[56.149286, 22.855384],[51.405343, 32.622230]]}).then(function(response){
                async(response.filter(function(item){ return item.value.toLowerCase().indexOf('беларусь') > -1; }).map(function(item){ return item.value; }));
            });
        }
    };
    function mapPointClick(){
        var $this = $(this);
        $('.gvt-map-point').removeClass('active');
        setTimeout(function(){
            $this.addClass('active');
            var $input = $this.find('input').first();
            $this.find('.form-group').removeClass('has-error');
            $this.find('.has-error').removeClass('has-error');
            $this.removeClass('has-error');
            var lat = $('[name="' + $input.attr('name') +'_lat"]').val();
            var lng = $('[name="' + $input.first().attr('name') +'_lng"]').val();
            if (lat && lng)
                map.setCenter([lat, lng]);
        }, 0);
    }
    function keyUpMapInput(event){
        if (event.keyCode == 13){
            event.preventDefault();
            $(this).trigger('blur');
            event.stopPropagation();
        }
    }
    function blurMapInput(){
        var $this = $(this);
        $this.parents('.form-control').removeClass('has-error');
        var geoSearch = $this.val();
        if (geoSearch){
            $.ajax({
                url: 'https://geocode-maps.yandex.ru/1.x/?format=json&geocode=' + geoSearch,
                success: function(res){
                    var coords;
                    try{
                        coords =
                            res.response.GeoObjectCollection
                                .featureMember[0].GeoObject.Point.pos.split(' ').reverse();
                    } catch (err){
                        return;
                    }
                    map.setCenter(coords);
                    $('[name="' + $this.attr('name') +'_lat"]').val(coords[0]);
                    $('[name="' + $this.attr('name') +'_lng"]').val(coords[1]);
                    var mapPointIndex = $this.data('map-point');
                    if (mapPointIndex == 0)
                        startPlace.geometry.setCoordinates(coords);
                    else {
                        if (places.length < mapPointIndex){
                            places.push(new ymaps.Placemark(coords,
                                { hintContent: toText,
                                    balloonContent: toText
                                },
                                {
                                    preset: 'islands#icon',
                                    iconColor: '#044E20',
                                    iconContent: toText
                                }));
                            map.geoObjects.add(places[mapPointIndex - 1]);
                        }
                        places[mapPointIndex - 1].geometry.setCoordinates(coords);
                    }
                    rebuildRoute();
                }
            });
        }
    }
    $(document).ready(function(){
        $('a[href="#page-top"]').click(function(){
            $('html, body').stop().animate({
                scrollTop: 0
            }, 1500, 'easeInOutExpo');
        });
        $('.map-autocomplete').typeahead(typeaheadOptions, typeaheadDatasets);
        $('.map-autocomplete').bind('typeahead:select', blurMapInput);
        (function phonePulsing(bigger){
            $('.phone-pulsing').animate({
                width: bigger ? '150px' : '130px',
                height: bigger ? '150px' : '130px'
            }, 1000, function(){
                bigger = !bigger;
                phonePulsing(bigger);
            });
        })(false);
        $('.gvt-map-point').click(mapPointClick);
        $('.gvt-map-point input').blur(blurMapInput).keyup(keyUpMapInput);
        $('.datepicker').datepicker({
            format: 'dd/mm/yyyy',
            startDate: '+1d',
            autoclose: true,
            language: currentLanguage,
            orientation: 'bottom'
        });
        $('.timepicker').timepicker({
            minuteStep: 5,
            showMeridian: false,
            defaultTime: false
        });
        $('.js-captcha-refresh').click(function(){
            $.getJSON($(this).data('url'), {}, function(json) {
                // This should update your captcha image src and captcha hidden input
            });
            return false;
        });
    });

    $('#addMapPoint').click(function(){
        var curMapIndex = $('#next-points-counter');
        var curMapIndexVal = curMapIndex.val();
        var lastInput = $('input#id_point' + curMapIndexVal);
        if (!lastInput.val()){
            lastInput.parent().addClass('has-error');
            return;
        }
        var removeBtn = $('#removeMapPoint')[0];
        if (removeBtn.style.display == 'none'){
            removeBtn.style.display = 'inline-block';
        }
        curMapIndex.val(parseInt(curMapIndexVal) + 1);
        curMapIndex = parseInt(curMapIndex.val()) || 1;
        var addingInput = $('<input class="map-autocomplete form-control" '
            + ' placeholder="' + toCompleteText + '" data-map-point="' + curMapIndex
                + '" name="point' + curMapIndex + '" id="id_point' + curMapIndex + '" type="text">');
        $('#id_point' + (curMapIndex - 1)).attr('placeholder', nextText);
        $('#maps-points')
            .append($('<div class="form-group gvt-map-point col-md-10 col-sm-10">').click(mapPointClick).append(
                $('<div class="row">').append(
                    $('<div class="col-md-2 col-sm-2 cur-point-wrapper">').append($('<img class="cur-point" src="' + mapCheckImg + '">')))
                .append($('<div class="col-md-10 col-sm-10">').append(addingInput)))
                .append($('<input type="hidden" name="point' + curMapIndex + '_lat">'))
                .append($('<input type="hidden" name="point' + curMapIndex + '_lng">'))
            )
            .append($('<div class="form-group col-md-2 floor-wrapper col-sm-2" id="floor_wrapper_' + curMapIndex + '">')
            .append('<input type="number" class="form-control" placeholder="' + floorText + '" name="point' + curMapIndex + '_floor">'));
        addingInput.blur(
                blurMapInput).keyup(keyUpMapInput);
        setTimeout(function(){addingInput.trigger('click').focus();}, 0);
        if (curMapIndex == 4) {
            $('#addMapPoint').hide();
        }
        addingInput.typeahead(typeaheadOptions, typeaheadDatasets);
    });
    $('#removeMapPoint').click(function(){
        var _this = this;
        var curMapIndexInput = $('#next-points-counter');
        curMapIndex = parseInt(curMapIndexInput.val()) || 1;
        if (curMapIndex != 1) {
            $('input#id_point' + curMapIndex).parents('.form-group.gvt-map-point').remove();
            $('#floor_wrapper_' + curMapIndex).remove();
            curMapIndex--;
            curMapIndexInput.val(curMapIndex);
            $('input#id_point' + curMapIndex).parent().trigger('click');
            if (curMapIndex == 1) {
                _this.style.display = 'none';
            }
            if (curMapIndex < 4) {
                $('#addMapPoint').show();
            }
            map.geoObjects.remove(places[places.length - 1]);
            places.pop();
            rebuildRoute();
        }
    });
    try {
        ymaps.ready(init);
    } catch (err) {
        console.error(err);
    }
    var startPlace;
    function init(){
        map = new ymaps.Map("map", {
            center: [52.0322082, 29.2223129],
            zoom: 16
        });
        map.controls.remove(map.controls.get('searchControl'));
        map.events.add('click', function (e) {
            var coords = e.get('coords');
            var activeInput = $('#mapForm .form-group.active input.tt-input[type="text"]');
            var startPoint = true;
            if (activeInput.attr('name') != 'start')
                startPoint = false;
            if (startPoint)
                startPlace.geometry.setCoordinates(coords);
            else {
                var mapPointIndex = parseInt(activeInput.data('map-point'));
                if (places.length <= mapPointIndex - 1) {
                    places.push(new ymaps.Placemark(coords,
                        { hintContent: toText,
                            balloonContent: toText
                        },
                        {
                            preset: 'islands#icon',
                            iconColor: '#044E20',
                            iconContent: toText
                        }));
                    map.geoObjects.add(places[mapPointIndex - 1]);
                }
                places[mapPointIndex - 1].geometry.setCoordinates(coords);
            }
            rebuildRoute();
            ymaps.geocode(coords).then(function (res) {
                var firstGeoObject = res.geoObjects.get(0);
                var value = firstGeoObject.properties.get('text');
                activeInput.val(value);
                activeInput.typeahead('val',value);
                map.setCenter(coords);
                $('[name="' + activeInput.attr('name') +'_lat"]').val(coords[0]);
                $('[name="' + activeInput.attr('name') +'_lng"]').val(coords[1]);
            });
        });
        startPlace = new ymaps.Placemark([52.0322082, 29.2223129], { hintContent: fromText,
            balloonContent: fromText });
        map.geoObjects.add(startPlace);
    }
    $('.gvt-map-link').click(function(){
        var i = $(this).find('i');
        if (i.hasClass('fa-angle-double-down')){
            i.removeClass('fa-angle-double-down');
            i.addClass('fa-angle-double-up');
        } else {
            i.addClass('fa-angle-double-down');
            i.removeClass('fa-angle-double-up');
        }
    });
    function rebuildRoute(){
        if (places.length){
            var routePoints = [
                 startPlace.geometry.getCoordinates()
            ];
            places.forEach(function(item, index){
                routePoints.push(item.geometry.getCoordinates());
            });
            ymaps.route(routePoints, {
            }).then(function (r) {
                if (route)
                    map.geoObjects.remove(route);
                route = r;
//                                route.getPaths().options.set({
//                                     // в балуне выводим только информацию о времени движения с учетом пробок
//                                     balloonContentBodyLayout: ymaps.templateLayoutFactory.createClass('$[properties.humanJamsTime]'),
//                                     // можно выставить настройки графики маршруту
//                                     strokeColor: '0000ffff',
//                                     opacity: 0.9
//                                });
                var points = route.getWayPoints();
                var distance = route.getLength();
                var displayDistance = (distance/1000).toFixed(1);
                $('#id_route_length').val(distance);
                $('#routeLength').text(displayDistance);
                var from = points.get(0);
                from.properties.set('iconContent', fromText);
                var curMapIndex = parseInt($('#next-points-counter').val());
                var to = points.get(curMapIndex);
                to.properties.set('iconContent', toText);
                to.properties.set('iconColor', '#5ACB5A');
                map.geoObjects.add(route);
            });
        }
    }

})();

// requestAnimationFrame Shim
(function() {
    var requestAnimationFrame = window.requestAnimationFrame || window.mozRequestAnimationFrame ||
                              window.webkitRequestAnimationFrame || window.msRequestAnimationFrame;
    window.requestAnimationFrame = requestAnimationFrame;
})();
