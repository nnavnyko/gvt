/**
 * Map Point Model
 * @requires: predefined translations and image links
 * @requires: Yandex Maps API included script (ymaps object)
 */


/**
 * Create Map point model
 * @param {int} sortOrder: int (order of the current point)
 * @param {object} placemark: ymaps.Placemark instance
 * @param {string} text: input start text
 */
function MapPoint(sortOrder, placemark, text) {
    this.sortOrder = sortOrder;
    this.longitude = null;
    this.latitude = null;
    this.addedToMap = false;
    this.placemark = placemark; // Yandex Maps Placemark object
    this.$addressInput = $('<input name="address' + sortOrder + '" type="text" class="map-autocomplete form-control" placeholder="' + text + ' ' + toCompleteText + '">');
    this.$floorInput = $('<input name="floor' + sortOrder + '" type="number" min="1" class="form-control" placeholder="' + floorText + '">');
}

MapPoint.prototype.render = function(){
    /**
     * Render Map Point line
     * @return: jQuery object (Address line with floor and check icon image rendered)
     */
    this.$addressLineWrapper = $('<div class="form-group gvt-map-point col-md-10 col-sm-10 col-xs-9">');
    var $addressInputWrapper = $('<div class="col-md-10 col-sm-10 col-xs-10">').append(this.$addressInput);

    this.$mapPointLine = $('<div class="row">');

    this.$addressLineWrapper.append(
        $('<div class="row">')
            .append(this._renderCurPointImage())
            .append($addressInputWrapper)
    );

    this.$mapPointLine.append(this.$addressLineWrapper, this._renderFoor());
    this.$floorInput.blur(this.validateFloor.bind(this));

    return this.$mapPointLine;
};

MapPoint.prototype.setCoords = function(latitude, longitude) {
    /**
     * Set Coordinates
     * @param latitude: number
     * @param longitude: number
     */
    this.latitude = latitude;
    this.longitude = longitude;
    this.placemark.geometry.setCoordinates([this.latitude, this.longitude]);
};

MapPoint.prototype._renderCurPointImage = function(){
    /**
     * Render current point image with wrapper
     * @return: jQuery object
     */
    return $('<div class="col-md-2 col-sm-2 col-xs-2 cur-point-wrapper text-center">').append(
        $('<i class="cur-point fa fa-map-marker fa-2x cursor-pointer" aria-hidden="true"></i>'));
};

MapPoint.prototype._renderFoor = function(){
    /**
     * Render floor input with wrapper
     * @return: jQuery object
     */

    this.$floorWrapper = $('<div class="form-group col-md-2 floor-wrapper col-sm-2 col-xs-3" id="floor_wrapper_' + this.sortOrder + '">')
        .append(this.$floorInput);

    return this.$floorWrapper;
};

MapPoint.prototype.addAutocompleteToAddress = function(){
    /**
     * Render floor input with wrapper
     * @return: jQuery object
     */
    // Map and paths scripts
    var typeaheadOptions = {
        limit: 10
    };
    var typeaheadDatasets = {
        name: 'yandex-autocomplete',
        source: function(query, sync, async) {
            ymaps.suggest(query, {results: 25, boundedBy:[[56.149286, 22.855384],[51.405343, 32.622230]],
                provider: 'yandex#search'}).then(function(response){
                async(response.filter(function(item){ return item.value.toLowerCase().indexOf('беларусь') > -1; }).map(function(item){
                    return item.value;
                }));
            });
        }
    };
    this.$addressInput.typeahead(typeaheadOptions, typeaheadDatasets);
};

MapPoint.prototype.destroy = function(){
    /**
     * Destroy Map Paing
     * Remove from HTML document tree
     */
    this.$mapPointLine.remove();
};

MapPoint.prototype.validate = function(){
    /**
     * Validate inputs
     * @return: boolean
     */
    this.$addressLineWrapper.removeClass('has-error');
    var isValid = true;
    if (!this.$addressInput.val()) {
        isValid = false;
    }
    this.validateFloor();

    if (!isValid) {
        this.$addressLineWrapper.addClass('has-error');
    }

    return isValid;
};

MapPoint.prototype.validateFloor = function(){
    /**
     * Validate Floor
     */

    var minimalFloor = 1;

    var floorValue = this.$floorInput.val();

    if (floorValue && parseInt(floorValue) < minimalFloor) {
        this.$floorInput.val(minimalFloor);
    }
};

MapPoint.prototype.makeActive = function(){
    /**
     * Make point active
     * Add 'active' class to point wrapper
     */
    $('.gvt-map-point').removeClass('active');
    this.$addressLineWrapper.addClass('active');
};
