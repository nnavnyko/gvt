/**
 * Map Points Controller
 * @requires: MapPoint Model
 * @requires: Yandex Maps API
 * (This script must be included after map-point.model.js)
 */

/** Initialize Map Points Controller **/
function MapPointsController(){
    this.pointsWrapper = $('#maps-points');
    this.currentPoint = null;
    this.route = null;
    this.map = null;
    this.mapPoints = [];
    this.routeLength = $('#id_route_length');
    this.routeLengthDisplay = $('#routeLength');
    this.removeMapPointBtn = $('#removeMapPoint')[0];
    this.addMapPointBtn = $('#addMapPoint')[0];

    this.YMAPS_GEOCODE_URL = 'https://geocode-maps.yandex.ru/1.x/?format=json&geocode=';
}

/** Initialize Map Points (create models, create Yandex Maps Placemarks) **/
MapPointsController.prototype.initializePoints = function(){
    this.removePoints();
    var initialPointsCount = 2;
    this.routeLengthDisplay.text(routeIsNotSet);

    for (var i = 0; i < initialPointsCount; i++) {
        var content = i === 0 ? fromText : toText;
        var placemark = this._createPlaceMark([], content);
        var sortOrder = i + 1;

        this.mapPoints.push(this._addMapPoint(sortOrder, placemark, i === 0));
    }

    this.removeMapPointBtn.style.display = 'none';
    this.addMapPointBtn.style.display = 'inline-block';
};

/** Add map point **/
MapPointsController.prototype.addMapPoint = function(){
    var maximalPointsLength = 5;
    if (this.mapPoints.length === maximalPointsLength) {
        return;
    }

    var isValid = true;

    this.mapPoints.forEach(function(mapPoint){
        var isPointValid = mapPoint.validate();
        if (!isPointValid) {
            isValid = false;
        }
    });
    if (!isValid) {
        return;
    }

    this.removeMapPointBtn.style.display = 'inline-block';

    var placemark = this._createPlaceMark([], toText);

    var sortOrder = this.mapPoints.length + 1;
    this.mapPoints[this.mapPoints.length - 1].$addressInput.attr('placeholder', nextText);
    // Add Map point and set as active
    this.mapPoints.push(this._addMapPoint(sortOrder, placemark, true));

    if (this.mapPoints.length === maximalPointsLength) {
        this.addMapPointBtn.style.display = 'none';
    }
};

/**
 * Add map point.
 * Add click and blur handlers
 * @param {number} sortOrder: number (order of the point)
 * @param {object} placemark: ymaps.Placemark instance
 * @param {boolean} setActive: boolean (set active after adding point)
 * @param {string} text: Input text
 *
 * @return: MapPoint instance
 */
MapPointsController.prototype._addMapPoint = function(sortOrder, placemark, setActive){
    var _this = this;
    var text = sortOrder === 1 ? fromText : toText;
    var mapPoint = new MapPoint(sortOrder, placemark, text);
    var mapPointWrapperTag = mapPoint.render();

    function blurAddressInput(e){
        var geoSearch = $(this).val();
        if (geoSearch) {
            _this._geoSearch(geoSearch);
        } else {
            mapPoint.longitude = mapPoint.latitude = null;
        }
    }

    mapPoint.$addressLineWrapper.click(function(){
        _this.setActivePoint(mapPoint);
        // Set Center map in current point
        if (!!mapPoint.latitude && !!mapPoint.longitude) {
            _this.map.setCenter([mapPoint.latitude, mapPoint.longitude]);
        }
    }).keyup(function($event){
        if ($event.keyCode === 13){
            $event.preventDefault();
            $(this).trigger('blur');
            $event.stopPropagation();
        }
    });

    _this.pointsWrapper.append(mapPointWrapperTag);

    if (setActive) {
        _this.setActivePoint(mapPoint);
    }
    mapPoint.addAutocompleteToAddress();

    mapPoint.$addressInput.blur(blurAddressInput).bind('typeahead:select', blurAddressInput);

    return mapPoint;
};

/** Search address using Yandes Maps GeoSearch @param geoSearch: string (address input value) **/
MapPointsController.prototype._geoSearch = function(geoSearch){
    var _this = this;
    var mapPoint = _this.currentPoint;
    var addressInput = mapPoint.$addressInput;
    $.ajax({
        url: _this.YMAPS_GEOCODE_URL + geoSearch,
        success: function(res){
            var coords;
            var geoObject;
            try{
                geoObject = res.response.GeoObjectCollection.featureMember[0].GeoObject;
                coords = geoObject.Point.pos.split(' ').reverse();
            } catch (err){
                return;
            }
            if (geoObject) {
                var metadata = geoObject.metaDataProperty.GeocoderMetaData;
                var value = getAddressFromMetadata(metadata, addressInput.val());
                if (value) {
                    addressInput.val(value);
                }
            }
            _this.map.setCenter(coords);
            mapPoint.setCoords(coords[0], coords[1]);
            if (!mapPoint.addedToMap) {
                _this.map.geoObjects.add(mapPoint.placemark);
                mapPoint.addedToMap = true;
            }
            _this.rebuildRoute();
        }
    });
};

/** Remove Points from map **/
MapPointsController.prototype.removePoints = function(){
    var pointsLength = this.mapPoints.length;
    if (this.route) {
        this.map.geoObjects.remove(this.route);
    }

    for (var i = 0; i < pointsLength; i++) {
        var mapPoint = this.mapPoints[0];
        this._removePoint(mapPoint);
    }
};

/** Remove Last Map point **/
MapPointsController.prototype.removeLastPoint = function(){
    var minimalPointsLength = 2;
    this.addMapPointBtn.style.display = 'inline-block';
    if (this.mapPoints.length === minimalPointsLength) {
        return;
    }

    this._removePoint(this.mapPoints[this.mapPoints.length - 1]);

    if (this.mapPoints.length === minimalPointsLength) {
        this.removeMapPointBtn.style.display = 'none';
    }
    this.setActivePoint(this.mapPoints[this.mapPoints.length - 1]);
    this.rebuildRoute();
};

/**
 * Set Active Point
 * @param {object} mapPoint: MapPoint instance
 */
MapPointsController.prototype.setActivePoint = function(mapPoint) {
    this.currentPoint = mapPoint;
    this.currentPoint.makeActive();
};

MapPointsController.prototype._removePoint = function(mapPoint){
    /**
     * Remove Map Point from Map and document HTML tree
     * @param mapPoint: MapPoint instance
     */
    this.map.geoObjects.remove(mapPoint.placemark);
    this.mapPoints.splice(this.mapPoints.indexOf(mapPoint), 1);
    mapPoint.destroy();
};

/**
 * Create Yandex Placemark object with content
 * @param {Array} coords: list of numbers (point coordinates)
 * @param {string} content: string (map icon content)
 *
 * @return: ymaps.Placemark instance
 */
MapPointsController.prototype._createPlaceMark = function(coords, content){
    return new ymaps.Placemark(
        coords,
        {
            hintContent: content,
            balloonContent: content
        },
        {
            preset: 'islands#icon',
            iconContent: content
        }
    );
};

/** Initialize Map **/
MapPointsController.prototype.initializeMap = function(){
    var _this = this;

    /** Function to handle ymaps initialized **/
    function init(){
        var mapTagID = 'map';
        var mozyrCoords = [52.0322082, 29.2223129];

        _this.map = new ymaps.Map(mapTagID, {
            center: mozyrCoords,
            zoom: 16
        });
        // Remove Yandex Maps default Search Control
        _this.map.controls.remove(_this.map.controls.get('searchControl'));

        /**
         * Handle Click inside the map area
         * Set coordinates and address text
         */
        _this.map.events.add('click', function (e) {
            var coords = e.get('coords');
            // Remember current point if YMaps response is too slow and current point has been changed
            var mapPoint = _this.currentPoint;

            ymaps.geocode(coords).then(function (res) {
                var geoObject = res.geoObjects.get(0);
                if (!geoObject) {
                    return;
                }
                var value = geoObject.properties.get('text');
                var metadata = geoObject.properties.get('metaDataProperty').GeocoderMetaData;
                value = getAddressFromMetadata(metadata, value);

                mapPoint.$addressInput.val(value);
                mapPoint.$addressInput.typeahead('val',value);

                mapPoint.setCoords(coords[0], coords[1]);
                if (!mapPoint.addedToMap) {
                    _this.map.geoObjects.add(mapPoint.placemark);
                    mapPoint.addedToMap = true;
                }
                // Set map point to next Empty point
                for(var i = _this.mapPoints.indexOf(mapPoint); i < _this.mapPoints.length; i++) {
                    if (!_this.mapPoints[i].$addressInput.val().trim()) {
                        _this.setActivePoint(_this.mapPoints[i]);
                        break;
                    }
                }

                _this.rebuildRoute();
            });
        });

        _this.initializePoints();
    }

    try {
        ymaps.ready(init);
    } catch (err) {
        console.error(err);
    }
};

/** Rebuild Map Route **/
MapPointsController.prototype.rebuildRoute = function(){
    var routePoints = [];
    var _this = this;

    if (_this.route) {
        _this.map.geoObjects.remove(_this.route);
    }

    _this.mapPoints.forEach(function(item, index){
        if (item.longitude && item.latitude) {
            routePoints.push(item.placemark.geometry.getCoordinates());
        }
    });

    ymaps.route(routePoints, {}).then(function (route) {
        _this.route = route;
        _this.route.getPaths().options.set({
            strokeColor: '1E98FF',
            opacity: 0.9,
            strokeStyle: 'longdashdot'
        });

        var distance = _this.route.getLength();
        var displayDistance = (distance/1000).toFixed(1);
        _this.routeLength.val(distance);
        _this.routeLengthDisplay.text(displayDistance);

        var points = route.getWayPoints();

        var from = points.get(0);
        from.properties.set('iconContent', fromText);

        var to = points.get(_this.mapPoints.length - 1);
        to.properties.set('iconContent', toText);

        _this.map.geoObjects.add(_this.route);
    });
};

/**
 * Convert points to json
 * @return: list of object
 */
MapPointsController.prototype.toJSONArray = function(){
    var points = [];

    this.mapPoints.forEach(function(mapPoint) {
        if (mapPoint.validate()) {
            points.push({
                sort_order: mapPoint.sortOrder,
                longitude: mapPoint.longitude,
                latitude: mapPoint.latitude,
                address: mapPoint.$addressInput.val(),
                floor: mapPoint.$floorInput.val()
            });
        }
    });

    return points;
};

/**
 * Get address from YMaps metadata of geoObject instance. Convert data into format '<town>, <street>, <house>'
 * @param {object} metadata: geoObject metadata
 * @param {string} defaultValue: default address value
 * @return: string
 **/
function getAddressFromMetadata(metadata, defaultValue) {
    var value = defaultValue || '';
    var addressComponents = ['locality', 'street', 'house'];
    if (metadata && addressComponents.indexOf(metadata.kind) > -1) {
        value = [];
        var components = metadata.Address.Components;
        for (var j = 0; j < components.length; j++) {
            if (addressComponents.indexOf(components[j].kind) > -1) {
                value.push(components[j].name);
            }
        }
        value = value.join(', ');
    }

    return value;
}
