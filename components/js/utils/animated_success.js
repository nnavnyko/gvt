/**
 * Animated success
 */

function animateSuccess(timerId){
    var _this = this;
    var canvas, context, x, y, radius,
        endPercent, curPerc, counterClockwise, circ, quart,
        process, curCheckedX, curCheckedY, halfCheckedX, endCheckedX;

    canvas = document.getElementById(timerId);
    context = canvas.getContext('2d');
    x = canvas.width / 2;
    y = canvas.height / 2;
    radius = 60;
    endPercent = 101;
    curPerc = 0;
    counterClockwise = false;
    circ = Math.PI * 2;
    quart = Math.PI / 2;
    context.lineWidth = 4;
    context.strokeStyle = '#11a3f7';
    context.shadowOffsetX = 0;
    context.shadowOffsetY = 0;
    context.shadowBlur = 2;
    context.shadowColor = '#000';
    process = -1;
    context.font = '24px Calibri ';
    context.fillStyle = '#11a3f7';
    curCheckedX = startCheckedX = x-38;
    curCheckedY = startCheckedY = y-9;
    halfCheckedX = x-5;
    endCheckedX = x+38;

    function animate(current) {
        context.clearRect(0, 0, canvas.width, canvas.height);
        context.beginPath();
        context.arc(x, y, radius, -(quart), ((circ) * current) - quart, false);
        context.stroke();
        if (process < 99)
            process++;
        context.fillText(process + '%', x-22, y+8);
        curPerc++;
        if (curPerc < endPercent) {
            setTimeout(function () {
             animate(curPerc / 100);
            }, 3);
        } else {
            preDrawChecked();
        }
    }

    function preDrawChecked(){
        context.moveTo(startCheckedX, startCheckedY);
        context.clearRect(x-35, y-20, x/2, y/2);
        drawChecked(curCheckedX, curCheckedY);
    }

    function drawChecked(currentX, currentY){
        context.clearRect(0, 0, canvas.width, canvas.height);
        if (currentX < halfCheckedX)
            currentY++;
        else
            currentY--;
        currentX++;
        context.lineWidth = 3;
        context.lineTo(currentX, currentY);
        context.stroke();
        if (currentX < endCheckedX){
            setTimeout(function () {
             drawChecked(currentX, currentY);
            }, 3);
        }
    }
    animate();
}