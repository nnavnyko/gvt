/**
 * Render Google Re-Captcha
 * @requires: Google ReCaptcha js file
 */

function renderReCaptcha(wrapperID){
    var $wrapper = $('#' + wrapperID);
    var $newWrapper = $('<div>');
    $newWrapper.attr('id', wrapperID).attr('data-sitekey', $wrapper.data('sitekey'))
        .attr('data-callback', $wrapper.data('callback'))
        .data('sitekey', $wrapper.data('sitekey'))
        .data('callback', $wrapper.data('callback'));

    $wrapper.replaceWith($newWrapper); // clear wrapper - Google ReCaptcha expects empty element
    $wrapper.remove();

    grecaptcha.render(wrapperID, {
      'sitekey' : $newWrapper.data('sitekey'), //Replace this with your Site key
      'callback' : $newWrapper.data('callback')
    });
}
