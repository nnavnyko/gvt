"""
Configuration Admin view
"""
from typing import Tuple, Union

from adminsortable.admin import SortableStackedInline, NonSortableParentAdmin
from django.contrib import admin
from django.db.models import QuerySet
from django.http import HttpRequest

from configuration.models import PhoneSettings, PhoneSettingsItem


class PhoneSettingsItemInline(SortableStackedInline):
    """
    PhoneSettingsItem inline
    """
    model = PhoneSettingsItem
    ordering = ('sort_order',)
    exclude = ('sort_order',)
    extra = 0


@admin.register(PhoneSettings)
class PhoneSettingsAdmin(NonSortableParentAdmin):
    """
    PhoneSettings Admin config
    """
    list_display: Tuple[str] = ('id', 'get_location_display')
    list_display_links: Tuple[str] = ('id', 'get_location_display')
    readonly_fields: Tuple[str] = ('location',)
    inlines: Tuple[Union[admin.StackedInline, admin.TabularInline]] = (PhoneSettingsItemInline,)
    sortable_by = ('id',)

    def get_queryset(self, request: HttpRequest) -> QuerySet:
        """
        Get QuerySet
        :param request: HttpRequest
        :return: QuerySet instance
        """
        queryset: QuerySet = super().get_queryset(request)

        return queryset.order_by('id')

    def has_add_permission(self, request: HttpRequest) -> bool:
        """
        Forbid to add PhoneSettings
        :param request: HttpRequest
        :return: boolean
        """
        return False

    def has_delete_permission(self, request: HttpRequest, obj: PhoneSettings = None) -> bool:
        """
        Forbid to delete PhoneSettings
        :param request: HttpRequest
        :param obj: PhoneSettings instance
        :return: boolean
        """
        return False
