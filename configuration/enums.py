"""
Configuration enums
"""
from enum import Enum

from django.utils.translation import ugettext_lazy as _


class PhoneSettingsLocation(Enum):
    """
    Phone settings type
    """
    HOME_HEADER = 'home_header'
    HOME_CONTACTS = 'home_contacts'
    EVACUATION_HEADER = 'evacuation_header'
    EVACUATION_CONTACTS = 'evacuation_contacts'
    CRANE_HEADER = 'crane_header'
    CRANE_CONTACTS = 'crane_contacts'
    PLACE_ORDER_CONTACTS = 'place_order_contacts'
    AUTO_CRANE_HEADER = 'auto_crane_header'
    AUTO_CRANE_CONTACTS = 'auto_crane_contacts'


PHONE_SETTINGS_LOCATION: tuple = (
    (PhoneSettingsLocation.HOME_HEADER.value, _('Главная страница: Заголовок')),
    (PhoneSettingsLocation.HOME_CONTACTS.value, _('Главная страница: Контакты')),
    (PhoneSettingsLocation.EVACUATION_HEADER.value, _('Эвакуация: Заголовок')),
    (PhoneSettingsLocation.EVACUATION_CONTACTS.value, _('Эвакуация: Контакты')),
    (PhoneSettingsLocation.CRANE_HEADER.value, _('Кран-манипулятор: Заголовок')),
    (PhoneSettingsLocation.CRANE_CONTACTS.value, _('Кран-манипулятор: Контакты')),
    (PhoneSettingsLocation.PLACE_ORDER_CONTACTS.value, _('Страница Заказа: Контакты')),
    (PhoneSettingsLocation.AUTO_CRANE_HEADER.value, _('Страница Автокрана: Заголовок')),
    (PhoneSettingsLocation.AUTO_CRANE_CONTACTS.value, _('Страница Автокрана: Контакты')),
)

