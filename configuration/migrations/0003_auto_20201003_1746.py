# Generated by Django 3.0.1 on 2020-10-03 14:46

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('configuration', '0002_load_default_data'),
    ]

    operations = [
        migrations.AlterField(
            model_name='phonesettings',
            name='location',
            field=models.CharField(blank=True, choices=[('home_header', 'Главная страница: Заголовок'), ('home_contacts', 'Главная страница: Контакты'), ('evacuation_header', 'Эвакуация: Заголовок'), ('evacuation_contacts', 'Эвакуация: Контакты'), ('crane_header', 'Кран-манипулятор: Заголовок'), ('crane_contacts', 'Кран-манипулятор: Контакты'), ('place_order_contacts', 'Страница Заказа: Контакты'), ('auto_crane_header', 'Страница Автокрана: Заголовок'), ('auto_crane_contacts', 'Страница Автокрана: Контакты')], default='new', max_length=20, verbose_name='расположение'),
        ),
    ]
