"""
Configuration models
"""
from configuration.models.phone_settings import PhoneSettings
from configuration.models.phone_settings_item import PhoneSettingsItem
