"""
Phone Settings
"""
from typing import List, Dict, TYPE_CHECKING, Type

from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.apps import apps

from common.managers.base_manager import BaseManager
from common.models import UUIDModel
from configuration.enums import PHONE_SETTINGS_LOCATION

if TYPE_CHECKING:
    from configuration.models import PhoneSettingsItem


class PhoneSettingsManager(BaseManager):
    """
    Phone Settings manager
    """

    def get_phones_by_location(self, *locations: List[str]) -> Dict[str, List['PhoneSettingsItem']]:
        """
        Get Phones by specific locations. For example, get phones for Home page Header and Contacts section:
        phones: dict = PhoneSettings.objects.get_phones_by_location(
            PhoneSettingsLocation.HOME_HEADER.value, PhoneSettingsLocation.HOME_CONTACTS.value
        )
        :param locations: list of str
        :return: dict of "location" as a key and list of "PhoneSettingsItem" as values
        """
        phone_settings_item_model: Type[models.Model] = apps.get_model(
            app_label='configuration',
            model_name='PhoneSettingsItem'
        )
        phone_settings: List[PhoneSettings] = self.prefetch_related(
            models.Prefetch(
                'phones',
                queryset=phone_settings_item_model.objects.filter(is_active=True).order_by('sort_order')
            )
        ).filter(
            location__in=locations
        ).all()

        return {
            ps.location: ps.phones.all() for ps in phone_settings
        }


class PhoneSettings(UUIDModel):
    """
    Phone Settings
    """
    class Meta:
        """ Meta class """
        db_table = 'phone_settings'
        verbose_name = _('настройка телефонов')
        verbose_name_plural = _('Настройки телефонов')

    location = models.CharField(
        choices=PHONE_SETTINGS_LOCATION, verbose_name=_('расположение'), default='new', max_length=20, blank=True
    )
    objects = PhoneSettingsManager()

    def __str__(self) -> str:
        """
        String representation
        :return: string
        """
        prefix: str = _('Настройка телефонов на странице')

        return f'{prefix}: {self.get_location_display()}'
