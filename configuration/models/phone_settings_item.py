"""
Phone Settings Item model
---
One of values displayed on the page
"""
from adminsortable.models import SortableMixin
from django.db import models
from django.utils.translation import ugettext_lazy as _

from common.managers.base_manager import BaseManager
from common.models import UUIDModel


class PhoneSettingsItemManager(BaseManager):
    """
    Phone Settings Item manager
    """
    pass


class PhoneSettingsItem(UUIDModel, SortableMixin):
    """
    PhoneSettingsItem model
    """

    class Meta:
        """ Meta class """
        db_table = 'phone_settings_item'
        verbose_name = _('телефон')
        verbose_name_plural = _('Телефоны')
        ordering = ['sort_order']

    value = models.CharField(verbose_name=_('номер телефона (фактический)'), null=False, blank=False, max_length=255)
    display_value = models.CharField(
        verbose_name=_('значение для отображения'),
        null=True,
        blank=True,
        default=None,
        max_length=255
    )
    is_active = models.BooleanField(verbose_name=_('активен'), default=True, blank=False, null=False)
    sort_order = models.PositiveIntegerField(
        verbose_name=_('порядок сортировки'),
        default=0,
        null=False,
        editable=False
    )

    phone_settings = models.ForeignKey(
        'configuration.PhoneSettings',
        on_delete=models.CASCADE,
        blank=False,
        null=False,
        related_name='phones',
        related_query_name='phone',
        verbose_name='настройка телефонов'
    )
    objects = PhoneSettingsItemManager()

    def __str__(self) -> str:
        """
        String representation
        :return:
        """
        prefix: str = _('Настройка номера телефона')

        return f'{prefix}: {self.value}'
