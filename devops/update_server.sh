red=`tput setaf 1`
green=`tput setaf 2`
reset=`tput sgr0`


echo -e "\nSet DJANGO_SETTINGS_MODULE..."
export_result=$(export DJANGO_SETTINGS_MODULE='gvt.settings.production') && echo $export_result && echo -e "${green}OK" || echo -e "${red}ERROR"
echo -e "${reset}"

echo -e "\nUpdate sources..."
git_result=$(cd ~/gvt && git pull) && echo $git_result && echo -e "${green}OK" || echo -e "${red}ERROR"
echo -e "${reset}"

echo -e "\nUpdate requirements..."
pip_result=$(cd ~/gvt && ~/gvtenv36/bin/python3.6 -m pip install -r ~/gvt/requirements.txt) && echo $pip_result && echo -e "${green}OK" || echo -e "${red}ERROR"
echo -e "${reset}"

echo -e "\nMigrate database..."
migrate_result=$(cd ~/gvt && ~/gvtenv36/bin/python3.6 ~/gvt/manage.py migrate) && echo $migrate_result && echo -e "${green}OK" || echo -e "${red}ERROR"
echo -e "${reset}"

echo -e "\nClear cache..."
cache_result=$(cd ~/gvt && ~/gvtenv36/bin/python3.6 ~/gvt/manage.py clearcache) && echo $cache_result && echo -e "${green}OK" || echo -e "${red}ERROR"
echo -e "${reset}"

echo -e "\nCompress static data..."
compress_result=$(cd ~/gvt && ~/gvtenv36/bin/python3.6 ~/gvt/manage.py compress) && echo $compress_result && echo -e "${green}OK" || (echo -e "${red}ERROR:" && $compress_result)
echo -e "${reset}"

echo -e "\nCollect static files after compress..."
static_result=$(cd ~/gvt && ~/gvtenv36/bin/python3.6 ~/gvt/manage.py collectstatic --noinput) && echo $static_result && echo -e "${green}OK" || echo -e "${red}ERROR"
echo -e "${reset}"

#echo -e "\nTouch UWSGI HTTP (restart server)..."
#touch_result=$(touch ~/uwsgi/http.ini) && echo $touch_result && echo -e "${green}OK" || echo -e "${red}ERROR"
#echo -e "${reset}"
#
#echo -e "\nTouch UWSGI WS (restart server)..."
#touch_result=$(touch ~/uwsgi/ws.ini) && echo $touch_result && echo -e "${green}OK" || echo -e "${red}ERROR"
#echo -e "${reset}"

