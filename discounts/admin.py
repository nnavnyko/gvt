# encoding: utf-8
"""
Discounts admin settings
"""
from django.contrib import admin

from discounts.db_models.discount import Discount
from discounts.db_models.discount_attachment import DiscountAttachment


class DiscountAttachmentInline(admin.StackedInline):
    """ TransportationLocationInline """
    model = DiscountAttachment
    extra = 0
    fields = ['image']


class DiscountAdmin(admin.ModelAdmin):
    """
        Action Admin
    """
    list_display = ('id', 'title', 'status', 'tr_requests_count', 'start_date', 'end_date')
    list_display_links = ('id', 'title')
    inlines = [DiscountAttachmentInline]


admin.site.register(Discount, DiscountAdmin)
