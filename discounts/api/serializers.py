# encoding: utf-8
"""
Discounts Serializers
"""
from typing import Type, List

from django.db import models
from rest_framework import serializers

from discounts.db_models.discount import Discount


class DiscountSerializer(serializers.ModelSerializer):
    """
    Action Serializer
    """
    title = serializers.SerializerMethodField()

    class Meta:
        """ Meta class """
        model: Type[models.Model] = Discount
        fields: List[str] = ['id', 'uuid', 'title', 'discount_value', 'discount_symbol']

    @staticmethod
    def get_title(obj: Discount) -> str:
        """
        Get title
        :param obj: Action instance
        :return: string
        """
        return obj.admin_name

