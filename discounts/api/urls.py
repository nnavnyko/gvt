# encodingL utf-8
"""
Discounts API urls
"""
from django.conf.urls import include, url

from discounts.api.views import ActionViewSet

app_name = 'discounts_api'

urlpatterns = [
    url(r'^actions/list$', ActionViewSet.as_view({'get': 'list'}), name='actions_list'),
    url(r'^actions/apply/(?P<uuid>[a-z0-9\-]+)/', ActionViewSet.as_view({'post': 'apply_action_discount'}),
        name='apply_action_discount'),
    url(r'^actions/clear/', ActionViewSet.as_view({'post': 'clear_action_discount'}),
        name='clear_action_discount'),
]
