"""
Discounts Views
"""
from django.db.models import QuerySet
from rest_framework.authentication import BasicAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.request import Request
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet

from accounts.api.permissions import IsEmployeeManagerOrDriver
from common.api.auth import SessionAuthentication
from discounts.api.serializers import DiscountSerializer
from discounts.db_models.discount import Discount
from discounts.utils.action_discount_util import DiscountUtil
from transportation.api.serializers import TransportationRequestSerializer
from transportation.models.transportation_request import TransportationRequest
from transportation.utils.transportation_request_util import TransportationRequestUtil


class ActionViewSet(ModelViewSet):
    """
    Action View Set
    """
    serializer_class = DiscountSerializer
    permission_classes = (IsAuthenticated, IsEmployeeManagerOrDriver)
    authentication_classes = (BasicAuthentication, SessionAuthentication)
    lookup_url_kwarg: str = 'uuid'
    lookup_field: str = 'uuid'

    def get_queryset(self) -> QuerySet:
        """
        Get queryset
        :return: QuerySet
        """
        return Discount.objects.get_available_actions()

    def list(self, request: Request, *args, **kwargs) -> Response:
        """
        Return queryset
        :return: REST Response
        """

        return Response(DiscountSerializer(self.get_queryset(), many=True).data)

    @classmethod
    def clear_action_discount(cls, request: Request, *args, **kwargs) -> Response:
        """
        Clear action discount for Transportation Request
        :param request: HttpRequest instance
        :param args: arguments
        :param kwargs: key-word arguments
        :return: REST Response
        """
        transportation_request: TransportationRequest = TransportationRequest.objects.get_by_uuid(
            request.data.get('tr_request_uuid'))

        transportation_request = TransportationRequest.objects.update_model(
            transportation_request, **{
                'action': None,
                'action_id': None,
                'discount_amount': 0,
                'discount_amount_min': 0,
                'discount_amount_max': 0
            }
        )

        return Response(TransportationRequestSerializer(transportation_request).data)

    def apply_action_discount(self, request: Request, *args, **kwargs) -> Response:
        """
        Apply action discount
        :param request: HttpRequest instance
        :param args: arguments
        :param kwargs: key-word arguments
        :return: REST Response with TransportationRequest json representation
        """
        discount: Discount = self.get_object()
        transportation_request = TransportationRequest.objects.get_by_uuid(
            request.data.get('tr_request_uuid'))

        transportation_request: TransportationRequest = DiscountUtil.apply_action_discount(
            discount,
            transportation_request
        )

        TransportationRequestUtil.send_ws_message(
            transportation_request,
            message='price_changed',
            current_user=request.user
        )

        return Response(TransportationRequestSerializer(transportation_request).data)
