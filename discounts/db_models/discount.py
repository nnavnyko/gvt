"""
Discount model + manager
"""
from datetime import datetime

from django.db import models
from django.db.models import QuerySet
from django.utils.translation import ugettext_lazy as _
from model_utils.models import TimeStampedModel

from common.managers.base_manager import BaseManager
from common.models import UUIDModel
from discounts.enums import DISCOUNT_UNIT, DISCOUNT_STATUS, DiscountStatus, DISCOUNT_UNIT_SYMBOLS, DiscountUnit


class DiscountManager(BaseManager):
    """
    Discount manager
    """

    def get_available_actions(self) -> QuerySet:
        """
        Get available actions (current date is among start date and end date of Action)
        :return: QuerySet of available actions
        """
        cur_date: datetime.date = datetime.now().date()

        return self.model.objects.prefetch_related('attachments').filter(
            start_date__lte=cur_date,
            end_date__gte=cur_date,
            status=DiscountStatus.active.value
        )


class Discount(UUIDModel, TimeStampedModel):
    """
    Action
    """
    class Meta:
        """ Meta class """
        db_table = 'discount'
        verbose_name = _('скидка')
        verbose_name_plural = _('Скидки')

    admin_name = models.CharField(verbose_name=_('Название Для Сотрудников'), max_length=255)
    title = models.CharField(verbose_name=_('Название'), max_length=255, default='Акция')
    description = models.TextField(verbose_name=_('Описание'), default=None, null=True, blank=True)
    discount_unit = models.CharField(verbose_name=_('Тип скидки'), max_length=50, choices=DISCOUNT_UNIT)

    status = models.CharField(
        verbose_name=_('статус'),
        max_length=50,
        choices=DISCOUNT_STATUS,
        default=DiscountStatus.active.value
    )

    discount_value = models.DecimalField(verbose_name=_('Размер скидки'), max_digits=20, decimal_places=2, default=0.00)
    start_date = models.DateField(verbose_name=_('Начало акции'))
    end_date = models.DateField(verbose_name=_('Окончание акции'))

    objects = DiscountManager()

    @property
    def discount_symbol(self) -> str:
        """
        Return discount symbol
        :return: string
        """
        return DISCOUNT_UNIT_SYMBOLS.get(
            self.discount_unit,
            DISCOUNT_UNIT_SYMBOLS[DiscountUnit.percentage.value]
        )

    def __str__(self) -> str:
        """
        String representation
        :return: string
        """
        return '{}: {}{}'.format(
            self.title,
            self.discount_value,
            self.discount_symbol
        )

    def tr_requests_count(self) -> int:
        """
        Tr Requests count with applied action
        :return: Number
        """
        return self.tr_requests.count()

    tr_requests_count.short_description = _('Количество Заказов')
