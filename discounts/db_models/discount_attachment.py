"""
Discount attachment
"""
from django.db import models
from django.utils.translation import ugettext_lazy as _
from model_utils.models import TimeStampedModel

from common.managers.base_manager import BaseManager
from common.models import UUIDModel


class DiscountAttachmentManager(BaseManager):
    """
    DiscountAttachment DB Manager
    """
    pass


class DiscountAttachment(UUIDModel, TimeStampedModel):
    """
    Action Attachment
    """
    class Meta:
        """ Meta class """
        db_table = 'discount_attachment'
        verbose_name = _('прикрепленное изображение скидки')
        verbose_name_plural = _('Прикрепленные изображения скидок')

    discount = models.ForeignKey(
        'discounts.Discount',
        verbose_name=_('Акция'),
        related_name='attachments',
        on_delete=models.CASCADE
    )
    image = models.ImageField(upload_to='actions/attachments', verbose_name=_('Изображение'))
