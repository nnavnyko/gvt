# encoding: utf-8
"""
Discounts enums
"""
from enum import Enum
from django.utils.translation import ugettext_lazy as _


class DiscountUnit(Enum):
    """
    Discount Unit
    """
    percentage = 'percentage'
    currency = 'currency'


class DiscountStatus(Enum):
    """
    Discount Unit
    """
    active = 'active'
    disabled = 'disabled'


DISCOUNT_UNIT = (
    (DiscountUnit.percentage.value, _('Процент')),
    (DiscountUnit.currency.value, _('Валюта')),
)

DISCOUNT_UNIT_SYMBOLS = {
    DiscountUnit.percentage.value: '%',
    DiscountUnit.currency.value: 'BYN'
}

DISCOUNT_STATUS = (
    (DiscountStatus.active.value, _('Активна')),
    (DiscountStatus.disabled.value, _('Не Активна'))
)
