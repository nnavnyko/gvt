# encoding: utf-8
"""
Discounts app
"""
# TODO: Remove this file after delivery feature/typing to Live
from django.db import models
from django.utils.translation import ugettext_lazy as _
from model_utils.models import TimeStampedModel

from common.decorators.helpers import deprecated
from common.models import UUIDModel
from discounts.enums import DISCOUNT_UNIT, DISCOUNT_UNIT_SYMBOLS, DiscountUnit, DISCOUNT_STATUS, DiscountStatus

from discounts.db_models.discount import Discount
from discounts.db_models.discount_attachment import DiscountAttachment


class Action(UUIDModel, TimeStampedModel):
    """
    Action
    """
    class Meta:
        """ Meta class """
        db_table = 'action'
        verbose_name = _('акция')
        verbose_name_plural = _('Акции')

    admin_name = models.CharField(verbose_name=_('Название Для Сотрудников'), max_length=255)
    title = models.CharField(verbose_name=_('Название'), max_length=255, default='Акция')
    description = models.TextField(verbose_name=_('Описание'), default=None, null=True, blank=True)
    discount_unit = models.CharField(verbose_name=_('Тип скидки'), max_length=50, choices=DISCOUNT_UNIT)
    status = models.CharField(verbose_name=_('статус'), max_length=50, choices=DISCOUNT_STATUS,
                              default=DiscountStatus.active.value)
    discount_value = models.DecimalField(verbose_name=_('Размер скидки'), max_digits=20, decimal_places=2, default=0.00)
    start_date = models.DateField(verbose_name=_('Начало акции'))
    end_date = models.DateField(verbose_name=_('Окончание акции'))

    @deprecated('Action model/class was renamed to Discount. Remove it after delivery to Live')
    def __init__(self, *args, **kwargs):
        """
        Initialize class
        :param args:
        :param kwargs:
        """
        super().__init__(*args, **kwargs)

    @property
    def discount_symbol(self):
        """
        Return discount symbol
        :return: string
        """
        return DISCOUNT_UNIT_SYMBOLS.get(self.discount_unit, DISCOUNT_UNIT_SYMBOLS[DiscountUnit.percentage.value])

    def __str__(self):
        """
        String representation
        :return: string
        """
        return f'{self.title}: {self.discount_value}{self.discount_symbol}'

    def tr_requests_count(self):
        """
        Tr Requests count with applied action
        :return: Number
        """
        return self.tr_requests.count()

    tr_requests_count.short_description = _('Количество Заказов')


class ActionAttachment(UUIDModel, TimeStampedModel):
    """
    Action Attachment
    """
    class Meta:
        """ Meta class """
        db_table = 'action_attachment'
        verbose_name = _('прикрепленное изображение акции')
        verbose_name_plural = _('Прикрепленные изображения акции')

    @deprecated('ActionAttachment model/class was renamed to DiscountAttachment. '
                'Remove it after delivery to Live')
    def __init__(self, *args, **kwargs):
        """
        Initialize class
        :param args:
        :param kwargs:
        """
        super().__init__(*args, **kwargs)

    action = models.ForeignKey('discounts.Action', verbose_name=_('Акция'), related_name='attachments',
                               on_delete=models.CASCADE)
    image = models.ImageField(upload_to='actions/attachments', verbose_name=_('Изображение'))
