"""
Discounts URLs
"""
from django.conf.urls import url, include

from discounts.api import urls as api_urls

app_name = 'discounts'

urlpatterns = [
    url('^api/', include(api_urls, namespace='api'))
]
