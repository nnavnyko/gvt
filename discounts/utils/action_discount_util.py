# encoding: utf-8
"""
Action Discount Util
"""
from typing import TYPE_CHECKING

from discounts.utils.discount_calculator import DiscountCalculator

if TYPE_CHECKING:
    from discounts.db_models.discount import Discount
    from transportation.models.transportation_request import TransportationRequest


class DiscountUtil:
    """
    Action Discount Utility
    """

    @classmethod
    def apply_action_discount(
            cls,
            discount: 'Discount',
            transportation_request: 'TransportationRequest'
    ) -> 'TransportationRequest':
        """
        Apply action discounts. Save transportation request after set discount amounts
        :param discount: Action instance
        :param transportation_request: TransportationRequest instance
        :return: None
        """
        transportation_request.discount_amount_min = DiscountCalculator.calculate_action_discount_amount(
            transportation_request.min_amount, discount)

        transportation_request.discount_amount_max = DiscountCalculator.calculate_action_discount_amount(
            transportation_request.max_amount, discount)
        transportation_request.discount_amount_max = transportation_request.discount_amount_max if \
            transportation_request.discount_amount_max > transportation_request.discount_amount_min else\
            None

        transportation_request.discount = discount

        transportation_request.save()

        return transportation_request
