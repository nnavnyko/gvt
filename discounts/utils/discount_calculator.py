"""
Discounts Calculator
"""
from decimal import Decimal, ROUND_DOWN
from typing import TYPE_CHECKING

from common.common_types import Number
from discounts.enums import DiscountUnit

if TYPE_CHECKING:
    from discounts.db_models.discount import Discount


class DiscountCalculator:
    """
    Discount calculator
    """

    @classmethod
    def round_value(cls, value: Number) -> Decimal:
        """
        Round value to 2 decimal places
        :param value: number (float, Decimal, string)
        :return: Decimal
        """
        return Decimal(str(value)).quantize(Decimal('.01'), rounding=ROUND_DOWN)

    @classmethod
    def calculate_action_discount_amount(cls, price: Number, discount: 'Discount') -> Decimal:
        """
        Calculate action discount amount
        :param price: number
        :param discount: action
        :return: number
        """
        price_value: Decimal = cls.round_value(price)
        discount_amount = cls.round_value(
            price_value * discount.discount_value / 100
        ) if discount.discount_unit == DiscountUnit.percentage.value else discount.discount_value

        discount_amount: Decimal = discount_amount if discount_amount <= price_value else price_value

        return discount_amount
