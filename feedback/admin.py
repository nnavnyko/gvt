# encoding: utf-8
"""
Reviews Admin
---
Register models for admin site
"""
from django.contrib import admin

from feedback.models.request_review import RequestReview
from feedback.models.review import Review

admin.autodiscover()


class ReviewAdmin(admin.ModelAdmin):
    """
    Review Admin
    """
    list_display = ('created', 'rating', 'status')
    list_display_links = ('created',)


class RequestReviewAdmin(admin.ModelAdmin):
    """
    Review Admin
    """
    list_display = ('created', 'rating', 'status')
    list_display_links = ('created',)


admin.site.register(Review, ReviewAdmin)
admin.site.register(RequestReview, RequestReviewAdmin)
