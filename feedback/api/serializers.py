# encoding: utf-8
"""
Reviews Serializers
"""
from typing import Type, List

from rest_framework import serializers
from django.db import models

from accounts.api.serializers import SimpleUserSerializer, EmployeeListSerializer
from common.api.serializers import StatusFieldSerializer
from feedback.models.request_review import RequestReview
from feedback.models.review import Review
from transportation.api.serializers import TransportationRequestListSerializer


class SimpleReviewSerializer(StatusFieldSerializer):
    """
    Simple Review Serializer
    ---
    Use in lists
    """
    class Meta:
        """ Meta class """
        model: Type[models.Model] = Review
        fields: List[str] = ['uuid', 'created', 'name', 'text', 'rating', 'status', 'get_status_display']


class ReviewSerializer(StatusFieldSerializer):
    """
    Review Serializer
    ---
    Use in details view/edit
    """
    class Meta:
        """ Meta class """
        model: Type[models.Model] = Review
        exclude: List[str] = ['id']


class SimpleRequestReviewSerializer(StatusFieldSerializer):
    """
    Simple Transportation Request Serializer
    ---
    Use in lists
    """
    transportation_request: Type[models.Model] = TransportationRequestListSerializer()
    created_by: serializers.ModelSerializer = SimpleUserSerializer()

    class Meta:
        """ Meta class """
        model: Type[models.Model] = RequestReview
        fields: List[str] = [
            'uuid', 'created', 'rating', 'transportation_request', 'created_by', 'status', 'get_status_display'
        ]


class RequestReviewSerializer(StatusFieldSerializer):
    """
    Request Review Serializer
    ---
    Use in details view/edit
    """
    transportation_request: Type[models.Model] = TransportationRequestListSerializer()
    created_by: serializers.ModelSerializer = SimpleUserSerializer()
    employee: serializers.ModelSerializer = EmployeeListSerializer()

    class Meta:
        """ Meta class """
        model: Type[models.Model] = RequestReview
        exclude: List[str] = ['id']
