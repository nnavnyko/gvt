# encoding: utf-8
"""
API Reviews Urls
"""
from django.conf.urls import include, url

from feedback.api.views import CreateReviewAPIView, ReviewAPIViewSet

app_name = 'feedback_api'

urlpatterns = [
    url(r'^create/$', CreateReviewAPIView.as_view(), name='create_review'),
    url(r'^list$', ReviewAPIViewSet.as_view({'get': 'list'}), name='reviews_list'),
    url(r'^(?P<uuid>[a-z0-9\-]+)/details$', ReviewAPIViewSet.as_view({'get': 'retrieve'}), name='review_details'),
    url(r'^(?P<uuid>[a-z0-9\-]+)/change_status/$', ReviewAPIViewSet.as_view({'post': 'change_status'}), name='change_status'),
]
