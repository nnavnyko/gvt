# encoding: utf-8
"""
Reviews API Views
---
Create or process feedback
"""
from typing import Type

from django.db.models import QuerySet
from rest_framework.authentication import BasicAuthentication
from rest_framework.pagination import PageNumberPagination
from rest_framework.permissions import IsAuthenticated
from rest_framework.request import Request
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.viewsets import ModelViewSet

from accounts.api.permissions import IsUserActive, IsEmployeeManager
from common.api.auth import SessionAuthentication
from feedback.api.serializers import ReviewSerializer, SimpleReviewSerializer
from feedback.forms.forms import CreateReviewForm
from feedback.models.review import Review
from feedback.utils.review_util import ReviewsUtil


class CreateReviewAPIView(APIView):
    """
    Create Review API View
    """

    def post(self, request: Request, *args, **kwargs) -> Response:
        """
        Create Review from visit site
        :param request: HttpRequest instance
        :param args: arguments
        :param kwargs: key-word arguments
        :return: HttpResponse
        """
        data: dict = request.data.copy()
        data['user_remote_ip'] = request.META.get('REMOTE_ADDR')

        form: CreateReviewForm = CreateReviewForm(data)
        if form.is_valid():
            form.save()
            return Response({'success': True})

        return Response({'errors': form.errors}, status=400)


class ReviewPaginationClass(PageNumberPagination):
    """
        TransportationPaginationClass
    """
    page_size: int = 10
    page_query_param: str = 'page'


class ReviewAPIViewSet(ModelViewSet):
    """
    ReviewAPIViewSet
    """
    permission_classes: tuple = (IsAuthenticated, IsEmployeeManager, IsUserActive)
    authentication_classes: tuple = (BasicAuthentication, SessionAuthentication)
    pagination_class: Type = ReviewPaginationClass
    serializer_class: Type = SimpleReviewSerializer
    lookup_url_kwarg: str = 'uuid'
    lookup_field: str = 'uuid'

    def get_queryset(self) -> QuerySet:
        """
        Get QuerySet method
        :return: QuerySet
        """
        return Review.objects.order_by('-created')

    def retrieve(self, request: Request, *args, **kwargs) -> Response:
        """
        Get Review Details
        :param request: HttpRequest instance
        :param args: arguments
        :param kwargs: key-word arguments
        :return: REST Response
        """
        review: Review = self.get_object()

        return Response(ReviewSerializer(review).data)

    def change_status(self, request: Request, *args, **kwargs) -> Response:
        """
        Update Review
        :param request: HttpRequest instance
        :param args: arguments
        :param kwargs: key-word arguments
        :return: REST Response
        """
        review = self.get_object()
        review = ReviewsUtil.change_status(review, request.data.get('status'))

        return Response(ReviewSerializer(review).data)
