"""
Feedback app
"""
from django.apps import AppConfig

from django.utils.translation import ugettext_lazy as _


class FeedbackConfig(AppConfig):
    """
    Reviews Config
    """
    name = 'feedback'
    verbose_name = _('Отзывы')
