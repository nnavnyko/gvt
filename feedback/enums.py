# encoding: utf-8
"""
Reviews Enums
"""
from enum import Enum
from django.utils.translation import ugettext_lazy as _


class ReviewStatuses(Enum):
    """
    Review Statuses
    """
    created = 'created'
    viewed = 'viewed'
    processed = 'processed'
    cancelled = 'cancelled'
    approved = 'approved'


REVIEW_STATUSES = (
    (ReviewStatuses.created.value, _('Создан')),
    (ReviewStatuses.viewed.value, _('Просмотрен')),
    (ReviewStatuses.processed.value, _('Обработан')),
    (ReviewStatuses.cancelled.value, _('Отменен')),
    (ReviewStatuses.approved.value, _('Одобрен')),
)

REVIEW_STATUSES_DICT = dict(REVIEW_STATUSES)
