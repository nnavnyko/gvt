"""
Reviews Forms
---
Create, Update Forms
"""
from typing import List, Type
from django.db import models

from common.forms.fields import PhoneField
from common.forms.forms import BootstrapControlsForm, GoogleReCaptchaForm
from django import forms
from django.utils.translation import ugettext_lazy as _

from feedback.models.request_review import RequestReview
from feedback.models.review import Review


class CreateReviewForm(GoogleReCaptchaForm, BootstrapControlsForm, forms.ModelForm):
    """
    Create Review Form
    Create review from visit page
    """
    phone = PhoneField(label=_('Номер телефона'), required=False)
    gcaptcha_form_field = 'review-g-recaptcha-response'

    class Meta:
        """
        Meta class
        """
        model: Type[models.Model] = Review
        fields: List[str] = ['phone', 'email', 'name', 'rating', 'text', 'user_remote_ip']
        widgets: dict = {
            'phone': forms.TextInput(attrs={'minlength': 13})
        }


class CreateRequestReviewForm(forms.ModelForm):
    """
    Create Transportation Request Review Form
    Create review from internal site panel
    """
    class Meta:
        """
        Meta class
        """
        model: Type[models.Model] = RequestReview
        fields: List[str] = ['rating', 'text', 'transportation_request', 'employee', 'created_by']
