# -*- coding: utf-8 -*-
# Generated by Django 1.11.3 on 2017-08-05 20:52
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('feedback', '0003_auto_20170803_1920'),
    ]

    operations = [
        migrations.AlterField(
            model_name='requestreview',
            name='status',
            field=models.CharField(choices=[('created', 'Создан'), ('viewed', 'Просмотрен'), ('processed', 'Обработан'), ('cancelled', 'Отменен'), ('approved', 'Одобрен')], default='created', max_length=20, verbose_name='статус'),
        ),
        migrations.AlterField(
            model_name='review',
            name='status',
            field=models.CharField(choices=[('created', 'Создан'), ('viewed', 'Просмотрен'), ('processed', 'Обработан'), ('cancelled', 'Отменен'), ('approved', 'Одобрен')], default='created', max_length=20, verbose_name='статус'),
        ),
    ]
