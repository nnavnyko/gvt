"""
Abstract (Base) Review model
"""
from django.db import models
from django.utils.translation import ugettext_lazy as _
from model_utils.models import TimeStampedModel

from common.models import UUIDModel
from feedback.enums import REVIEW_STATUSES, ReviewStatuses


class AbstractReviewModel(UUIDModel, TimeStampedModel):
    """
    Abstract Review Model
    """

    class Meta:
        """ Meta class """
        abstract = True

    text = models.TextField(verbose_name=_('текст'), )
    status = models.CharField(choices=REVIEW_STATUSES, max_length=20, null=False,
                              default=ReviewStatuses.created.value, verbose_name=_('статус'))
    rating = models.IntegerField(verbose_name=_('рейтинг'), null=True, blank=True, default=None)
