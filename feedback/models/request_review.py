"""
TransportationRequest Review (Feedback)
"""
from django.conf import settings
from django.db import models
from django.db.models import Avg, Q
from django.utils.translation import ugettext_lazy as _

from common.managers.base_manager import BaseManager
from feedback.enums import ReviewStatuses
from feedback.models.abstract_review import AbstractReviewModel


class RequestReviewManager(BaseManager):
    """
    Request Review Manager
    """

    def employee_rating(self, employee_id):
        """
        Get employee rating
        Get Average rating of an employee user, except cancelled statuses

        :param employee_id: int (auth.User.id)
        :return: number (employee's rating)
        """
        return self.model.objects\
            .filter(~Q(status=ReviewStatuses.cancelled.value))\
            .filter(employee_id=employee_id)\
            .aggregate(average_rating=Avg('rating'))['average_rating']


class RequestReview(AbstractReviewModel):
    """
    Transportation Request Review
    """
    class Meta:
        """ Meta class """
        db_table = 'request_review'
        verbose_name = _(u'отзыв на заказ')
        verbose_name_plural = _(u'Отзывы на заказы')

    created_by = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        verbose_name=_(u'создан'),
        related_name='user_reviews',
        on_delete=models.CASCADE

    )
    transportation_request = models.ForeignKey(
        'transportation.TransportationRequest',
        verbose_name=_('заявка на транспортировку'),
        related_name='review',
        on_delete=models.CASCADE)

    employee = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        verbose_name=_('сотрудник'),
        related_name='employee_reviews',
        on_delete=models.CASCADE
    )

    objects = RequestReviewManager()
