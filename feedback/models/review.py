"""
Website Review (Feedback) model
This kind of feedback can be left on WebSite
"""
from django.db import models
from django.utils.translation import ugettext_lazy as _

from common.managers.base_manager import BaseManager
from feedback.models.abstract_review import AbstractReviewModel


class ReviewManager(BaseManager):
    """
    Review model Manager
    """
    pass


class Review(AbstractReviewModel):
    """
    Review Model
    """
    class Meta:
        """ Meta class """
        db_table = 'review'
        verbose_name = _('отзыв')
        verbose_name_plural = _('Отзывы')

    phone = models.BigIntegerField(verbose_name=_('номер телефона'), null=True, blank=True, default=None)
    name = models.CharField(verbose_name=_('имя'), max_length=255)
    email = models.EmailField(verbose_name='email', null=True, blank=True, default=None)
    user_remote_ip = models.CharField(
        verbose_name=_('IP пользователя'), max_length=255, default=None, blank=True, null=True)

    objects = ReviewManager()
