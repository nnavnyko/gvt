"""
Reviews Tests
"""
from django.contrib.auth import get_user_model
from django.test import TestCase, Client
from django.urls.base import reverse

from accounts.models.employee_card import EmployeeCard
from accounts.models.user_card import UserCard
from accounts.enums import EmployeeRoles
from feedback.enums import ReviewStatuses
from feedback.models.review import Review

SIMPLE_PASSWORD = '12345678*Aa'
REQUEST_ERROR = 400
PERMISSION_DENIED_STATUS = 403
SUCCESS_STATUS = 200
REDIRECT_STATUS = 302   # Used for Log-in requests


class ReviewsTestCase(TestCase):
    """
    Reviews Test Case
    ---
    Test permissions, review creation
    """

    request_data = {
        'text': 'Not bad, not good',
        'phone': '333333333',
        'rating': 3,
        'name': 'Suzan',
        'review-g-recaptcha-response': 'GOOGLE_RECAPTCHA_RESPONSE'
    }

    def setUp(self):
        """
        Set up test case
        :return:
        """
        self.client = Client()
        self.simple_user = self._create_user('simple_user', 'client@email.com', 333333333)
        self.driver_user = self._create_user('driver_user', 'driver@email.com', 443333333,
                                              employee_role=EmployeeRoles.driver.value)
        self.manager_user = self._create_user('manager_user', 'manager@email.com', 553333333,
                                              employee_role=EmployeeRoles.manager.value)

    def login_user(self, user):
        """
        Login user
        :param user:
        :return:
        """
        response = self.client.post(reverse('accounts:auth_login'),
                                    data={'username': user.username, 'password': SIMPLE_PASSWORD})

        # Login success
        self.assertEqual(response.status_code, REDIRECT_STATUS)

    @classmethod
    def _create_user(cls, user_name, email, phone_number, employee_role=None):
        """
        Create user
        :param user_name: string
        :param email: string
        :param phone_number: number
        :param employee_role: string or None
        :return: auth.User instance
        """
        user = get_user_model()(username=user_name, email=email, first_name='FirstName', last_name='LastName')
        user.set_password(SIMPLE_PASSWORD)
        user.save()

        user_card = UserCard(phone_number=phone_number)
        user_card.user_id = user.id
        user_card.save()

        if employee_role:
            employee_card = EmployeeCard(role=employee_role)
            employee_card.user_id = user.id
            employee_card.save()

        return user

    def test_create_review(self):
        """
        Test Create Review
        :return:None
        """
        # Test error if empty data
        response = self.client.post(reverse('feedback:api:create_review'), {})
        # Response status is 400, error
        self.assertEqual(response.status_code, REQUEST_ERROR)

        response = self.client.post(reverse('feedback:api:create_review'), self.request_data)
        # Response status is 200, OK
        self.assertEqual(response.status_code, SUCCESS_STATUS)
        review = Review.objects.order_by('-created').first()
        self.assertEqual(review.rating, self.request_data['rating'])

    def test_permissions_to_get_review_details(self):
        """
        Test permissions to get review details
        Only managers (admin or manager) can get review details
        :return: None
        """
        response = self.client.post(reverse('feedback:api:create_review'), self.request_data)
        # Response status is 200, OK
        self.assertEqual(response.status_code, SUCCESS_STATUS)
        review = Review.objects.order_by('-created').first()

        # Try to get review as simple user - Permission Denied
        self.login_user(self.simple_user)
        response = self.client.get(reverse('feedback:api:review_details', args=(review.uuid,)))
        self.assertEqual(response.status_code, PERMISSION_DENIED_STATUS)

        # Try to get review as driver user - Permission Denied
        self.login_user(self.driver_user)
        response = self.client.get(reverse('feedback:api:review_details', args=(review.uuid,)))
        self.assertEqual(response.status_code, PERMISSION_DENIED_STATUS)

        # Try to get review as manager user - Success
        self.login_user(self.manager_user)
        response = self.client.get(reverse('feedback:api:review_details', args=(review.uuid,)))
        self.assertEqual(response.status_code, SUCCESS_STATUS)

    def test_permissions_to_get_reviews_list(self):
        """
        Test permissions to get feedback list
        Only managers (admin or manager) can get feedback list
        :return: None
        """
        # Create one review
        response = self.client.post(reverse('feedback:api:create_review'), self.request_data)
        # Response status is 200, OK
        self.assertEqual(response.status_code, SUCCESS_STATUS)

        # Try to get review as simple user - Permission Denied
        self.login_user(self.simple_user)
        response = self.client.get(reverse('feedback:api:reviews_list'))
        self.assertEqual(response.status_code, PERMISSION_DENIED_STATUS)

        # Try to get review as driver user - Permission Denied
        self.login_user(self.driver_user)
        response = self.client.get(reverse('feedback:api:reviews_list'))
        self.assertEqual(response.status_code, PERMISSION_DENIED_STATUS)

        # Try to get review as manager user - Success
        self.login_user(self.manager_user)
        response = self.client.get(reverse('feedback:api:reviews_list'))
        self.assertEqual(response.status_code, SUCCESS_STATUS)

    def test_permissions_to_change_review(self):
        """
        Test permissions to change review
        Only managers (admin or manager) can change review
        :return:
        """
        response = self.client.post(reverse('feedback:api:create_review'), self.request_data)
        # Response status is 200, OK
        self.assertEqual(response.status_code, SUCCESS_STATUS)
        review = Review.objects.order_by('-created').first()

        # Try to get review as simple user - Permission Denied
        self.login_user(self.simple_user)
        response = self.client.post(reverse('feedback:api:change_status', args=(review.uuid,)),
                                    data={'status': ReviewStatuses.approved.value})
        self.assertEqual(response.status_code, PERMISSION_DENIED_STATUS)

        # Try to get review as driver user - Permission Denied
        self.login_user(self.driver_user)
        response = self.client.post(reverse('feedback:api:change_status', args=(review.uuid,)),
                                    data={'status': ReviewStatuses.approved.value})
        self.assertEqual(response.status_code, PERMISSION_DENIED_STATUS)

        # Try to get review as manager user - Success
        self.login_user(self.manager_user)
        response = self.client.post(reverse('feedback:api:change_status', args=(review.uuid,)),
                                    data={'status': ReviewStatuses.approved.value})
        self.assertEqual(response.status_code, SUCCESS_STATUS)
        review = Review.objects.get_or_none(uuid=review.uuid)
        self.assertEqual(ReviewStatuses.approved.value, review.status)
