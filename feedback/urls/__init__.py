"""
Transportation Urls
"""
from django.conf.urls import url, include
from feedback.api import urls as api_urls

app_name = 'feedback'

urlpatterns = [
    url(r'^api/', include(api_urls, namespace='api')),
]
