# encoding: utf-8
"""
Reviews Utils
"""
from feedback.enums import REVIEW_STATUSES_DICT
from feedback.models.review import Review


class ReviewsUtil:
    """
    Reviews Util
    """

    @classmethod
    def change_status(cls, review: Review, status: str) -> Review:
        """
        Change status of the Review
        :param review: feedback.Review instance
        :param status: string
        :return: updated review
        """
        if status not in REVIEW_STATUSES_DICT:
            raise ValueError('Unknown status!')

        return Review.objects.update_model(review, status=status)
