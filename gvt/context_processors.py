"""
    Project Context Processors
"""
from django.conf import settings
from django.http import HttpRequest


def google_recaptcha_public_key(request: HttpRequest) -> dict:
    """
    Add Google ReCaptcha Public key
    :param request: HttpRequest instance
    :return: dict
    """
    return {
        'RECAPTCHA_PUBLIC_KEY': settings.RECAPTCHA_PUBLIC_KEY,
    }
