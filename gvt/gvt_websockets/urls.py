"""
GVT Websockets URLs
"""


class WebSocketsURLs:
    """
        Container for websockets urls
    """
    STATISTICS = '/ws/transportation_requests_statistics'
    TR_REQUEST_DETAILS = '/ws/transportation_requests_details'
