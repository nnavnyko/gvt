"""
    WebSockets Publisher
    Publish messages to redis
"""
from typing import TYPE_CHECKING, List

import redis
from django.conf import settings

from accounts.models.user import DjangoUser
from gvt.gvt_websockets.urls import WebSocketsURLs

if TYPE_CHECKING:
    from transportation.models.transportation_request import TransportationRequest


class WebSocketsPublisher:
    """
    WebSockets publisher
    """

    @classmethod
    def publish_statistics(cls, msg: str, current_user: DjangoUser = None, user_names: List[str] = None) -> None:
        """
        Publish TransportationRequests statistics message
        :param msg: string
        :param current_user: current logged-in user
        :param user_names: list of recipients
        :return: None
        """
        cls._publish_message(WebSocketsURLs.STATISTICS, msg, current_user, user_names)

    @classmethod
    def publish_tr_request_changed(
            cls,
            tr_request: 'TransportationRequest',
            msg: str,
            current_user: DjangoUser = None
    ) -> None:
        """
        Publish message that TransportationRequest info has been changed
        :param tr_request: TransportationRequest
        :param msg: string
        :param current_user: current logged-in user
        :return: None
        """
        key: str = f'{WebSocketsURLs.TR_REQUEST_DETAILS}/{tr_request.uuid}'

        cls._publish_message(key, msg, current_user=current_user)

    @staticmethod
    def _publish_message(key: str, msg: str, current_user: DjangoUser = None, recipients: List[str] = None) -> None:
        """
        Publish message to redis
        :param key: string (key for Requests statistics or TR details)
        :param msg: string
        :param expire: int value in seconds or None (default 2 minutes)
        :return:
        """
        redis_client: redis.Redis = redis.Redis(host=settings.REDIS_HOST, port=settings.REDIS_PORT, db=0)
        pattern: str = f'{settings.WEBSOCKET_RESID_PREFIX}{key}'

        channels = redis_client.pubsub_channels(f'{pattern}/*')
        if recipients:
            channels = [c for c in channels if channels.decode('utf-8').split('/')[-1] in recipients]

        for channel in channels:
            if not current_user or not channel.decode('utf-8').endswith(f'/{current_user.username}'):
                redis_client.publish(
                    channel,
                    msg
                )
