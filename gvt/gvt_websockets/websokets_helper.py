"""
WebSockets Helper class
"""
from typing import Union

from websockets import WebSocketServerProtocol
from importlib import import_module
from http.cookies import SimpleCookie
from asgiref.sync import sync_to_async

from django.conf import settings
from django.contrib.sessions.models import Session
from django.contrib.auth import get_user_model

from accounts.enums import EMPLOYEE_MANAGERS_ROLES, EMPLOYEE_MANAGER_OR_DRIVER_ROLES
from accounts.models.user import DjangoUser
from gvt.gvt_websockets.urls import WebSocketsURLs
from transportation.models.transportation_request import TransportationRequest


class WebSocketsHelper:
    """
        WebSockets Helper
        1. Use to validate URL and permissions of the logged-in user
    """

    @classmethod
    async def user_has_permission(cls, websocket: WebSocketServerProtocol, path: str) -> bool:
        """
        Check if user has permissions to path
        User must be logged-in to subscribe websockets
        :param websocket: WebSocketServerProtocol
        :param path: string (URL path)
        :return: boolean
        """
        try:
            user: Union[DjangoUser, None] = await cls._get_current_user(websocket)
            if not user:
                return False

            employee_card = getattr(user, 'employee_card', None)

            if path == f'{WebSocketsURLs.STATISTICS}/{user.username}':
                return (
                    employee_card is not None and
                    employee_card.role in EMPLOYEE_MANAGERS_ROLES
                )

            if path.startswith(f'{WebSocketsURLs.TR_REQUEST_DETAILS}') and path.endswith(f'/{user.username}'):
                tr_uuid = path.replace(f'/{user.username}', '').replace(f'{WebSocketsURLs.TR_REQUEST_DETAILS}/', '')
                tr_request = await sync_to_async(TransportationRequest.objects.filter(uuid=tr_uuid).first)()
                if not tr_request:
                    return False

                print('Subscribed on changes of: ', tr_uuid)

                return (
                    employee_card.role in EMPLOYEE_MANAGER_OR_DRIVER_ROLES or
                    tr_request.created_by_id == user.id
                )
        except Exception as e:
            print(e)

        return False

    @staticmethod
    async def _get_current_user(websocket: WebSocketServerProtocol) -> Union[DjangoUser, None]:
        """
        Get logged-in user from request
        :param websocket: WebSocketServerProtocol - from client
        :return: User instance
        """
        engine = import_module(settings.SESSION_ENGINE)
        cookie = SimpleCookie()
        cookie.load(websocket.request_headers['cookie'])

        # Get Django session from cookie by session id
        session_store = engine.SessionStore(cookie['sessionid'])
        session = await sync_to_async(Session.objects.get)(session_key=session_store.session_key.value)

        user_id = session.get_decoded().get('_auth_user_id')
        if not user_id:
            return None

        user = await sync_to_async(get_user_model().objects.select_related('employee_card').get)(id=user_id)

        return user
