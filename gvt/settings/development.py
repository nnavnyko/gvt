# encoding: utf-8
"""
Development settings
"""
from gvt.settings.common import *

DEBUG = True

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'gvt',
        'USER': 'gvt',
        'PASSWORD': '309baa0ffc8e4e3284f24bbe67945876',
        'HOST': 'localhost',
        'PORT': '',
    }
}

CURRENT_SITE = 'http://127.0.0.1:8000'

DEFAULT_FROM_EMAIL = 'gvtmozyr@gmail.com'

SERVER_EMAIL = 'gvtmozyr@gmail.com'
EMAIL_BACKEND = 'django_mailgun.MailgunBackend'
MAILGUN_ACCESS_KEY = 'key-553b56b163218b8597021bb5bc128d37'
MAILGUN_SERVER_NAME = 'gruzi-v-taxi.by'

RECAPTCHA_PROXY = {'http': 'http://127.0.0.1:8000', 'https': 'https://127.0.0.1:8000'}

# RECAPTCHA_PUBLIC_KEY = '6LfQmCgTAAAAAMd5Zw9nHVks7PIdnNekyRjMYbIb'
# RECAPTCHA_PRIVATE_KEY = '6LfQmCgTAAAAACiQbjOl2mTb4ljaHEE438NKlM_n'
RECAPTCHA_PUBLIC_KEY = '6LeIxAcTAAAAAJcZVRqyHh71UMIEGNQ_MXjiZKhI'
RECAPTCHA_PRIVATE_KEY = '6LeIxAcTAAAAAGG-vFI1TnRWxMZNFuojJ4WifJWe'

DEFAULT_INDEX_TABLESPACE = 'tables'

COMPRESS_ENABLED = False

ALLOWED_HOSTS = ['0.0.0.0', 'localhost', '127.0.0.1']

CORS_ORIGIN_ALLOW_ALL = True

JWT_AUTH_SECRET = 'da4e304125dd40aeb1652cef91baa1b7'

LOGGING = {
    'version': 1,
    'handlers': {
        'console': {
            'class': 'logging.StreamHandler',
        },
    },
    'loggers': {
        'django.request': {
            'handlers': ['console'],
            'level': 'DEBUG',
            'propagate': True,
        },
        'django.db.backends': {
            'handlers': ['console'],
            'level': 'DEBUG',
        },
    },
}

SILENCED_SYSTEM_CHECKS = ['captcha.recaptcha_test_key_error']

SEND_EMAILS = False
