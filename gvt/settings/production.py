# encoding: utf-8
"""
Production settings
"""
from gvt.settings.common import *

DEBUG = False
ADMINS = [('Nikolai', 'nnavnyko@gmail.com'), ('Admin', 'gvtmozyr@gmail.com')]
ALLOWED_HOSTS = ['*.gruzi-v-taxi.by', 'www.gruzi-v-taxi.by', 'gruzi-v-taxi.by', '178.62.45.64', 'localhost']

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'gvt',
        'USER': 'gvt',
        'PASSWORD': 'a50e8587365842a7bcf34817956527e0',
        'HOST': 'localhost',
        'PORT': '',
    }
}

CURRENT_SITE = 'https://gruzi-v-taxi.by'

DEFAULT_FROM_EMAIL = 'gvtmozyr@gmail.com'

SERVER_EMAIL = 'gvtmozyr@gmail.com'
EMAIL_BACKEND = 'django_mailgun.MailgunBackend'
MAILGUN_ACCESS_KEY = 'key-553b56b163218b8597021bb5bc128d37'
MAILGUN_SERVER_NAME = 'gruzi-v-taxi.by'

RECAPTCHA_PUBLIC_KEY = '6LfQmCgTAAAAAMd5Zw9nHVks7PIdnNekyRjMYbIb'
RECAPTCHA_PRIVATE_KEY = '6LfQmCgTAAAAACiQbjOl2mTb4ljaHEE438NKlM_n'

COMPRESS_ENABLED = True


LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'handlers': {
        'mail_admins': {
            'level': 'ERROR',
            'filters': [],
            'class': 'django.utils.log.AdminEmailHandler'
        },
        'file': {
            'level': 'ERROR',
            'class': 'logging.FileHandler',
            'filename': '/site_templates/gvt/gvt/django.log',
        },
    },
    'loggers': {
        'django': {
            'handlers': ['file'],
            'level': 'DEBUG',
            'propagate': True,
        },
        'django.request': {
            'handlers': ['file', 'mail_admins'],
            'level': 'ERROR',
            'propagate': True,
        },
    },
}

CORS_ORIGIN_ALLOW_ALL = False
JWT_AUTH_SECRET = 'da4e304125dd40aeb1652cef91baa1b7'

SEND_EMAILS = True
