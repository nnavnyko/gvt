"""
Test Settings
"""
# encoding: utf-8
"""
Development settings
"""
from gvt.settings.common import *

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db_test.sqlite3'),
        'TEST_NAME': os.path.join(BASE_DIR, 'db_test.sqlite3')
    }
}

CURRENT_SITE = 'http://127.0.0.1:8000'

DEFAULT_FROM_EMAIL = 'gvtmozyr@gmail.com'

SERVER_EMAIL = 'gvtmozyr@gmail.com'
EMAIL_BACKEND = 'django_mailgun.MailgunBackend'
MAILGUN_ACCESS_KEY = 'key-553b56b163218b8597021bb5bc128d37'
MAILGUN_SERVER_NAME = 'gruzi-v-taxi.by'

RECAPTCHA_PROXY = {'http': 'http://127.0.0.1:8000', 'https': 'https://127.0.0.1:8000'}
RECAPTCHA_PUBLIC_KEY = '6LeIxAcTAAAAAJcZVRqyHh71UMIEGNQ_MXjiZKhI'
RECAPTCHA_PRIVATE_KEY = '6LeIxAcTAAAAAGG-vFI1TnRWxMZNFuojJ4WifJWe'

DEFAULT_INDEX_TABLESPACE = 'tables'

LOCAL_TEST = True

SILENCED_SYSTEM_CHECKS = ['captcha.recaptcha_test_key_error']
