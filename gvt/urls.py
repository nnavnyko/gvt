"""gvt URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from django.conf.urls.static import static
from django.conf import settings
from django.views.decorators.csrf import csrf_exempt
from django.views.i18n import set_language

from gvt import views as project_views
from gvt.views import app_view
from transportation import urls as transportation_urls
from discounts import urls as discounts_urls
from accounts import urls as accounts_urls
from feedback import urls as reviews_urls
from organisations import urls as orgs_urls

app_name = 'gvt'

urlpatterns = [
    url(r'^$', project_views.home_view, name='home'),
    url(r'^place-order$', project_views.place_order_view, name='place_order'),
    url(r'^feedback$', project_views.feedback_view, name='feedback'),
    url(r'^evacuation', project_views.evacuation_view, name='evacuation'),
    url(r'^crane-truck', project_views.manipulator_view, name='crane_manipulator'),
    url(r'^auto-crane', project_views.auto_crane, name='auto_crane'),
    url(r'^admin/', admin.site.urls),
    url(r'^transportation/', include(transportation_urls, namespace='transportation')),
    # url(r'^ulogin/', include(ulogin_urls)),
    url(r'^accounts/', include(accounts_urls, namespace='accounts')),
    url(r'^reviews/', include(reviews_urls, namespace='feedback')),
    url(r'^discounts/', include(discounts_urls, namespace='discounts')),
    url(r'^organisations/', include(orgs_urls, namespace='organisations')),
    url(r'^app/', app_view, name='app'),
    url(r'^i18n/setlang/', csrf_exempt(set_language), name='set_language'),
    url(r'^tos', project_views.tos_view, name='terms_of_service'),
    url(r'', include('accounts.urls.auth')),

] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

handler404 = 'gvt.views.handler404'
handler500 = 'gvt.views.handler500'
