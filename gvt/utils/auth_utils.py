"""
Auth Utils
"""
from django.conf import settings
import jwt
from django.contrib.auth import get_user_model
from django.utils.functional import SimpleLazyObject


class JWTAuthUtil(object):
    """
    WS Utility
    """
    @classmethod
    def get_user(cls, auth_token):
        """
        Get user for request usint JWT auth
        :param auth_token:
        :return: User instance
        """
        if auth_token:
            auth_info = jwt.decode(auth_token, settings.JWT_AUTH_SECRET, algorithms=['HS256'])

            user = None
            if auth_info:
                user = get_user_model().objects.filter(username=auth_info.get('username')).first()

            return SimpleLazyObject(lambda: user)


