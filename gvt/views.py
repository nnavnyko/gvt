# encoding: utf-8
"""
Project Views
"""
from typing import Dict, Any, List, Callable

from constance import config
from django.conf import settings
from django.contrib.auth.decorators import login_required
from django.http import HttpRequest, HttpResponse
from django.shortcuts import redirect, render
from django.urls import reverse, reverse_lazy
from django.utils.decorators import method_decorator
from django.utils.translation import get_language
from django.views.generic.base import TemplateView

from accounts.enums import EMPLOYEE_MANAGER_OR_DRIVER_ROLES
from configuration.enums import PhoneSettingsLocation
from configuration.models import PhoneSettings, PhoneSettingsItem
from discounts.db_models.discount import Discount
from feedback.enums import ReviewStatuses
from feedback.forms.forms import CreateReviewForm
from feedback.models.review import Review
from transportation.forms.forms import TransportationRequestForm

CONSTANCE_VARS_BY_LANGUAGE = [
    'title_price_text',
    'work_time',
    'mozyr_price'
]


class BasePhoneSettingsMixin:
    """
    Base template view
    """
    phone_locations: tuple = ()

    def get_context_data(self, **kwargs) -> dict:
        """
        Get context data
        :param kwargs: key-word args
        :return: dict
        """
        context: dict = super().get_context_data(**kwargs)
        context['phone_settings'] = self._get_phone_settings()

        return context

    def _get_phone_settings(self) -> Dict[str, List[PhoneSettingsItem]]:
        """
        Get phone settings for page
        :return: dict
        """
        if not len(self.phone_locations):
            return {}

        phone_settings: Dict[str, List[PhoneSettingsItem]] = PhoneSettings.objects.get_phones_by_location(
            *[l.value for l in self.phone_locations]
        )

        return phone_settings


class BaseWebSiteView(BasePhoneSettingsMixin, TemplateView):
    """
    Base WebSite part view
    """
    phone_locations: tuple = ()

    def get_context_data(self, **kwargs) -> dict:
        """
        Get Request Context
        :param kwargs:
        :return:
        """
        context: dict = super().get_context_data(**kwargs)
        context.update(
            {
                **self.get_constance_translations()
            }
        )

        return context

    @staticmethod
    def get_constance_translations() -> Dict[str, dict]:
        """
        Get contance config with translated strings
        :return: dict
        """
        current_language: str = get_language()
        translated_config: dict = dict(
            (
                key,
                getattr(config, '_'.join([key, current_language]), getattr(config, key, None))
            ) for key in CONSTANCE_VARS_BY_LANGUAGE
        )

        return {'translated_config': translated_config}


class HomeView(BaseWebSiteView):
    """ HomeView """
    template_name: str = 'site_templates/home.html'
    phone_locations: tuple = (PhoneSettingsLocation.HOME_HEADER, PhoneSettingsLocation.HOME_CONTACTS)

    def get_context_data(self, **kwargs) -> dict:
        """
        Append Discounts if there are some
        :param kwargs: key-word argumetnts
        :return: dict
        """
        context: dict = super(HomeView, self).get_context_data(**kwargs)
        discounts = None

        if getattr(config, 'show_actions', False):
            discounts = Discount.objects.get_available_actions()

        context.update({
            'actions': discounts
        })

        return context


class PlaceOrderView(BaseWebSiteView):
    """ Place Order View """
    template_name = 'site_templates/place_order.html'
    phone_locations: tuple = (PhoneSettingsLocation.PLACE_ORDER_CONTACTS,)

    def get_context_data(self, **kwargs) -> dict:
        """
        Get context. Append additional data to context
        :param kwargs: key-word arguments
        :return: None
        """
        context: dict = super(PlaceOrderView, self).get_context_data(**kwargs)

        context.update({
            'tr_req_form': TransportationRequestForm(),
            'MAPBOX_ACCESS_TOKEN': settings.MAPBOX_ACCESS_TOKEN
        })

        return context


class FeedbackView(BaseWebSiteView):
    """ Place Order View """
    template_name = 'site_templates/feedback.html'

    def get_context_data(self, **kwargs) -> dict:
        """
        Append additional data
        :param kwargs: key-word arguments
        :return: dict
        """
        context: dict = super(FeedbackView, self).get_context_data(**kwargs)

        context.update({
            'review_form': CreateReviewForm(),
            'reviews': Review.objects.filter(
                status=ReviewStatuses.approved.value
            ).order_by('-created').all()[:9],
        })

        return context


class EvacuationView(BasePhoneSettingsMixin, TemplateView):
    """ Place Order View """
    template_name = 'site_templates/evacuation.html'
    phone_locations: tuple = (PhoneSettingsLocation.EVACUATION_HEADER, PhoneSettingsLocation.EVACUATION_CONTACTS)


class ManipulatorView(BasePhoneSettingsMixin, TemplateView):
    """ Crane-manipulator View """
    template_name = 'site_templates/mounted_crane_truck.html'
    phone_locations: tuple = (PhoneSettingsLocation.CRANE_HEADER, PhoneSettingsLocation.CRANE_CONTACTS)

    def get_context_data(self, **kwargs) -> dict:
        """
        Get Context data
        :param kwargs: key-word arguments
        :return: context dict
        """
        context: dict = super().get_context_data(**kwargs)

        context['images'] = [
            {
                'source_image': 'images/manipulator_02.jpg',
                'thumbnail': 'images/manipulator_02_thumb.jpg'
            },
            {
                'source_image': 'images/manipulator_01.jpg',
                'thumbnail': 'images/manipulator_01_thumb.jpg'
            },
            {
                'source_image': 'images/manipulator_03.jpg',
                'thumbnail': 'images/manipulator_03_thumb.jpg'
            },
            {
                'source_image': 'images/manipulator_04.jpg',
                'thumbnail': 'images/manipulator_04_thumb.jpg'
            },
            {
                'source_image': 'images/manipulator_05.jpg',
                'thumbnail': 'images/manipulator_05_thumb.jpg'
            },
            {
                'source_image': 'images/manipulator_06.jpg',
                'thumbnail': 'images/manipulator_06_thumb.jpg'
            },
            {
                'source_image': 'images/manipulator_07.jpg',
                'thumbnail': 'images/manipulator_07_thumb.jpg'
            },
            {
                'source_image': 'images/manipulator_08.jpg',
                'thumbnail': 'images/manipulator_08_thumb.jpg'
            }
        ]

        return context


class AutoCraneView(BasePhoneSettingsMixin, TemplateView):
    """ Auto-crane view """
    template_name = 'site_templates/auto_crane.html'
    phone_locations: tuple = (PhoneSettingsLocation.AUTO_CRANE_HEADER, PhoneSettingsLocation.AUTO_CRANE_CONTACTS)

    def get_context_data(self, **kwargs) -> dict:
        """
        Get Context data
        :param kwargs: key-word arguments
        :return: context dict
        """
        context: dict = super().get_context_data(**kwargs)

        context['images'] = [
            {
                'source_image': 'images/auto_crane/auto_crane_01.jpg',
                'thumbnail': 'images/auto_crane/auto_crane_01_thumb.jpg'
            },
            {
                'source_image': 'images/auto_crane/auto_crane_02.jpg',
                'thumbnail': 'images/auto_crane/auto_crane_02_thumb.jpg'
            },
            {
                'source_image': 'images/auto_crane/auto_crane_03.jpg',
                'thumbnail': 'images/auto_crane/auto_crane_03_thumb.jpg'
            }
        ]

        return context


class GVTAppView(TemplateView):
    """
        Gvt app view
    """
    template_name = 'index.html'

    def get_context_data(self, **kwargs) -> dict:
        """
        Get Context data
        :param kwargs: key-word arguments
        :return: context dict
        """
        context: dict = super().get_context_data(**kwargs)
        context['MAPBOX_ACCESS_TOKEN'] = settings.MAPBOX_ACCESS_TOKEN

        return context

    @method_decorator(login_required(login_url=reverse_lazy('accounts:auth_login')))
    def dispatch(self, request, *args, **kwargs):
        """
        Dispatch method
        :param request: HttpRequest instance
        :param args: arguments (list)
        :param kwargs: key-word arguments (dict)
        :return: supermethod result
        """
        # TODO: temporary disable general users
        if not hasattr(request.user, 'employee_card') or \
                request.user.employee_card.role not in EMPLOYEE_MANAGER_OR_DRIVER_ROLES:
            return redirect(reverse('accounts:auth_login'))

        return super(GVTAppView, self).dispatch(request, *args, **kwargs)


class GVTTermsOfServiceView(TemplateView):
    """
        GVT Terms Of Service page
    """
    template_name = 'tos.html'


def handler404(request: HttpRequest, exception: Any = None, template_name: str = '404.html') -> HttpResponse:
    """
    Handle 404 exception - show correct template
    :param request: HttpRequest
    :param exception: Exception
    :param template_name: string
    :return: HttpResponse
    """
    response: HttpResponse = render(request, template_name, context={}, status=404)

    return response


def handler500(request: HttpResponse, exception: Any = None, template_name: str = '500.html'):
    """
    Handle 500 error
    :param request: HttpRequest
    :param exception: Exception
    :param template_name: string
    :return: HttpResponse
    """
    response = render(request, template_name, context={}, status=500)

    return response


home_view = HomeView.as_view()
place_order_view = PlaceOrderView.as_view()
feedback_view = FeedbackView.as_view()
evacuation_view = EvacuationView.as_view()
manipulator_view = ManipulatorView.as_view()
auto_crane = AutoCraneView.as_view()
tos_view = GVTTermsOfServiceView.as_view()
app_view = GVTAppView.as_view()
