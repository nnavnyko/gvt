"""
WebSockets Producer
---
Publish messages tp redis

From documentation:
    https://yeti.co/blog/establishing-a-websocket-pubsub-server-with-redis-and-asyncio-for-the-light-sensor/
"""
# producer.py

import django
import os
import sys

DEFAULT_PORT = 8596

DEBUG = True
if not DEBUG:
    DEFAULT_PORT = 8595

    path = '/home/gvt/gvt'
    if path not in sys.path:
        sys.path.append(path)

# Need to execute it before you can use Django models
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'gvt.settings.development')
django.setup()

import asyncio
import websockets
from aioredis import create_connection, Channel
from django.conf import settings

from gvt.gvt_websockets.websokets_helper import WebSocketsHelper


async def subscribe_to_redis(path: str):
    """
    Subscribe redis channel
    When WebSocketsPublisher publishes the message this subscriber will receive message
    :param path: string
    :return: tuple of channel and connection
    """
    conn = await create_connection((settings.REDIS_HOST, settings.REDIS_PORT))

    # Set up a subscribe channel
    channel: Channel = Channel(f'{settings.WEBSOCKET_RESID_PREFIX}{path}', is_pattern=False)
    await conn.execute_pubsub('subscribe', channel)

    return channel, conn


async def browser_server(websocket, path):
    """
    WebSocket server - send messages to client
    :param websocket: WebSocketServerProtocol
    :param path: string (WS URL path)
    :return: None
    """
    user_has_permission: bool = await WebSocketsHelper.user_has_permission(websocket, path)
    if not user_has_permission:
        print('User has no permissions to this channel')
        return

    channel, conn = await subscribe_to_redis(path)
    try:
        while True:
            # Wait until data is published to this channel
            message = await channel.get()
            print(message)
            # Send unicode decoded data over to the websocket client
            await websocket.send(message.decode('utf-8'))

    except websockets.exceptions.ConnectionClosed:
        # Free up channel if websocket goes down
        await conn.execute_pubsub('unsubscribe', channel)
        conn.close()

if __name__ == '__main__':
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "gvt.settings.development")
    # Runs a server process on 8596. Just do 'python producer.py'
    loop = asyncio.get_event_loop()

    loop.set_debug(True)
    application = websockets.serve(browser_server, 'localhost', DEFAULT_PORT)

    loop.run_until_complete(application)
    loop.run_forever()
