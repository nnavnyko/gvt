��    �     �          �!  $  �!  I  �"  �    $  �   %  a  �&  �   (  2  �(  s  �)  �   W+  �  N,  =  �-  �   /  �   �/     �0    �1  �   �2  �  ~3  �   85  �   �5    �6  %  �7    �8  �   �9  �   �:  �   B;  u   �;  �   c<  �   �<  p  �=  �   ,?  �  �?  ~   sA  8   �A  �   +B  �  �B  �   �D  �   �E  A   5F  @   wF  B   �F     �F     G     G     2G     EG     ZG     oG  "   �G  ?   �G     �G     H  I   	H  Y   SH     �H  "   �H     �H     �H      I     1I     HI  %   cI  5   �I     �I     �I  
   �I  
   �I  #   �I  H   J  r   `J     �J  ,   �J      K     4K     HK     TK  ^   aK     �K  )   �K  g   �K     dL     |L  
  �L  
   �M  
   �M     �M  \   �M  E   &N  2   lN  P   �N     �N  �   O     �O  3   �O  1   �O     P     !P     0P     LP     eP     ~P  )   �P     �P  
   �P     �P     �P     Q  6   Q  "   UQ  #   xQ  �   �Q     BR  ^   YR  ]   �R     S     6S  !   IS  �   kS     PT     iT  
   zT     �T     �T     �T     �T     �T     U     U     3U     DU     UU  9   dU  %   �U  )   �U  $   �U  .   V  0   BV  �   sV     �V     W     -W     KW  I   bW     �W  -   �W     �W     X  
   +X  &   6X  "   ]X  4   �X  %   �X     �X  @   �X  #   <Y  '   `Y  '   �Y     �Y  0   �Y     �Y     	Z  ,   )Z  /   VZ  3   �Z  1   �Z  !   �Z     [     [  )   ([  3   R[  #   �[     �[  B   �[  3   �[  �   2\     �\  .   �\  7   �\  C   +]  0   o]  ;   �]     �]  $   �]  %   ^     :^     R^      f^  �   �^     *_     =_  !   P_  
   r_     }_     �_  *   �_  0   �_     `     '`     E`  &   X`  0   `     �`     �`  d   �`     Da     ba     sa     �a     �a  |   �a  ]   2b  �   �b     <c     Ic      hc     �c  +   �c     �c     �c  
   �c     �c  !   �c  �   !d  .   �d     �d  >   �d     8e  i   Xe  R   �e  3   f     If  (   df  5   �f  �   �f  }   {g  n   �g  v   hh  �   �h  <   �i  >   �i  !   	j     +j     @j  e   Mj  r   �j  h   &k     �k     �k     �k     �k  <   �k     l     7l  ?   Nl     �l     �l      �l  u   �l     Gm     ^m     qm  +   zm  +   �m     �m     �m  7   n     :n     Xn     xn     �n     �n     �n  c   �n  @   o     ]o     to     �o  r   �o     p     p  !   )p  +   Kp  v   wp  M   �p  7   <q  5   tq  /   �q  $   �q  $   �q  �   $r  ?   �r     �r     �r     s  <   -s     js  '   ~s     �s     �s     �s     �s  0   t  R   ?t  %   �t  &   �t  <   �t     u  "   /u     Ru     eu     �u  �   �u  &   Kv  $   rv     �v  #   �v  
   �v     �v  
   �v  #   �v  <   w     Jw     \w  
   yw     �w     �w     �w  
   �w     �w  0   �w  .   "x  !   Qx     sx     zx  -   �x  -   �x  #   �x     y  2   :y  '   my     �y  +   �y     �y  0   �y     %z  B   4z  6   wz  4   �z  1   �z  3   {  @   I{     �{     �{  %   �{     �{  4   �{     (|     D|     `|  8   �|  6   �|  0   �|     !}     2}     R}  
   i}     t}     �}     �}  A   �}  #   �}     ~     >~  <   ^~  >   �~     �~     �~  &        6     E     N     h     u     �     �  #   �     �  )   �  
   �     �     �  %   3�     Y�  >   s�     ��     ��     ʀ     �     �     �    �  �   2�  �   �  �   �    ܄    �  h   ��  �   e�  )  ?�  �   i�    �  �   �  d   ǋ  �   ,�  �   ��     =�  r   ��    0�  r   @�  m   ��  �   !�  �   ̐  �   ~�  o   '�  [   ��  i   �  N   ]�  X   ��  i   �  �   o�  {   _�  �   ە  p   ؖ  $   I�  Z   n�  �   ɗ    Ř  z   �  3   _�  -   ��  3   ��     ��  	   ��  	   �  	   �  	   �  	   &�  	   0�     :�  6   L�  	   ��     ��  Q   ��  M   �     3�     @�     Y�     f�     |�     ��     ��     ��     ��     ߜ     �     �     ��     �  #   �  9   :�     t�  !   ��     ��     ��     ��     ŝ  ;   Ν  
   
�     �  @   0�     q�     y�  �   ��     �     �     �  2   .�  )   a�  *   ��  1   ��     �  O   ��     M�     R�     d�     x�     }�     ��     ��     ��     Ġ  )   ˠ     ��     ��     �     �     �     $�     ;�     T�  X   m�     ơ  0   ͡  @   ��     ?�  	   M�     W�  k   g�     Ӣ     ܢ     �     �     ��  
   �     �     "�     0�     A�     O�     [�     d�     j�     ��     ��     ��  $   ��  $   ݣ  C   �     F�     U�     d�     s�  !   y�     ��     ��     Ĥ     ͤ     �     �     �     �     #�     /�      ?�     `�     r�     ��     ��     ��     ��     ȥ     ܥ     ��     �     /�     M�     U�     X�     `�     r�     ��  
   ��  "   ��     ֦  -   �     �     �     2�  =   L�     ��     ��     ��     ŧ     ܧ     �  
   �     �  O   $�  
   t�  
   �     ��     ��     ��     ��     ��     Ԩ     �     ��  	   �     �     !�     3�     F�  J   O�     ��     ��     ��     ��     ˩  U   ک  3   0�  m   d�     Ҫ     ۪     �     �  %   �  	   -�     7�     <�     C�     O�  E   [�     ��     ��     ̫     �  8   �  :   <�     w�     ��     ��     ��  f   ��  B   �  5   a�  C   ��  `   ۭ     <�     U�     n�     ��     ��  0   ��  -   ɮ  B   ��     :�     A�     I�     U�     e�     ��     ��  6   ��     ί  
   կ     �  C   ��  
   1�     <�     C�  !   H�  (   j�     ��     ��  %   ��     װ     �     ��      �     �     �  2   �  *   P�     {�  	   ��     ��  <   ��     ٱ     �     �     ��  +   �  +   0�     \�     v�     ��     ��     ��  H   Ʋ  )   �     9�     E�     S�     `�     t�     ��     ��     ��     ��     г     �  &   �     *�     :�     H�  
   h�     s�  
   ��     ��     ��  M   ��     �     �     3�     9�     N�     V�     ]�     d�  '   u�  	   ��     ��     ��  
   ��     ��  	   ǵ     ѵ     ׵  $   �     
�     �  
   '�     2�     ;�     Y�  	   p�     z�     ��     ��     ��     ��     ζ     ܶ     �     ��     �     �     0�     B�     \�     n�     s�     �     ��     ��     ��     ŷ     η     �     ��     �     �     *�     ?�     L�     S�     f�     o�     t�     }�     ��     ��     ��     ��     θ     ׸     �     ��     ��     �     �     %�     -�     6�     =�     Q�     V�     p�     u�     ��     ��     ��     ��  	   ̹     ֹ     ܹ     ��     �     �         �       �   ]  �   n   W  �       �  d   �     *   l       7       �   �         !                   �   �   �   F  e   �   |      {  �       �   D   8      $  5  M  (          9   2      e  \  �         �   R   h  }   -   Q  �       �       �   �       �        �      �   )   �       �       c   x  N   B  7  4     r      z  �   �   �   Q       �   �   
  y       �   �       J  C  �                     H    �   O          �   8  n  C   m      �   �   ;      &     �      �       h   �    �  &       G      �        �   B   <  y  |   '  t      {   6   �  A   s   �   �   �   �   �        f  R  �   V   f   +                       �         Y   �   v  �  �       �   a   U  �       @  <          E  x   Z      �   �   w      �       �   .   �               z   �   }      =  ~      b         q  �         H       �       '   p   �   �           c      :       �   k          �   $      �   J   `       P   �                             S      �   i  �   �   5          ^      Z   �   p  �   �   -        �       L   �   �   �   N  T   @           ,   "   �      �           V  :              �   =   I  i                 %  �   1  #     �     �   �   >  j  g       u  �   �       S   D  1   �   �               %   �       ^   *  �   M             g   �   I   #   U       �   �          l                �   (   3   _  d  �                            E   u   ,  X       �      W   6  �   �   �           �   �          r       �     �   o                 ?  v       0     4   �   �  F       �  �       \   �       G   �      �       9     ]       ~  �                �   )      j   K            b   .      _   /  �  �   �   �       t  �  �   �   �       �       K             L  a  [   �   o       s  ?     k   
                       �   �   q   P  �  +      �      ;       �   �   �   A      �   `  �   T             �   �      �   X  !  �   [    �         �   �   3      �   >   Y  0   m   O  �  w   2       	          �             �   "  /   	   
                                    Мы занимаемся грузоперевозками уже более 6 лет.
                                    В нашей команде профессиональные водители и грузчики.
                                 
                                    Собственный автопарк. Подбор размера
                                    машины и количества грузчиков
                                    к каждому заказу индивидуально.
                                     
                                    Экономим ваше время и деньги.
                                    Для нас очень важно, чтобы клиент остался доволен.
                                     
                                Вы лишь даете нам возможность более точно подобрать автомобиль под ваш заказ.
                                Также вы избегаете необходимости отвечать на уточняющие вопросы оператора, тем самым экономите свое время
                             
                                Из года в год стремимся улучшить качество наших услуг,
                                повышая профессиональность водителей, вежливость операторов, аккуратность грузчиков.
                             
                                Мы ценим каждого клиента, который обращается к нам.
                             
                                После заполнения заявки с Вами свяжется наш менеджер для уточнения стоимости перевозки, и Вы сможете подтвердить или отменить заказ.
                             
                                Постоянно расширяем спектр услуг грузового такси:
                                от переездов до вывоза строительного мусора, от перевозки стройматериалов до эвакуации вашего авто.
                             
                                Предоставляем услуги эвакуации авто, грузоперевозки с помощью крана-манипулятора, услуги автокрана
                             
                                Развиваем услуги грузоперевозок в небольших городах, расширяя охват нашей деятельности,
                                чтобы каждому жителю РБ был предоставлен удобный сервис грузового такси.
                             
                        Автомобиль с краном-манипулятором - универсальное средство для перевозки стройматериалов,
                        крупногабаритного груза и многого другого.
                     
                        Грузоподъемность автомобиля составляет 15 тонн, крана-манипулятора - 4.5 тонны.
                     
                        Грузоподъемность установки автокрана составляет 12.5 тонн, длина стрелы - 14 метров.
                     
                        Предоставляем услуги аренды автомобильного крана по г. Мозырю, Калинковичам,
                        Гомельской области и РБ.
                     
                        Предоставляем услуги грузового автомобиля с краном-манипулятором по г. Мозырю, Калинковичам,
                        Гомельской области и РБ.
                     
                        Предоставляем услуги эвакуатора по г. Мозырю, Калинковичам, Гомельской области и РБ.
                     
                        С помощью автокрана возможно выполнение таких работ,
                        как разгрузка грузовых автомобилей, железнодорожных вагонов,
                        монтажные/строительные работы, подъем строительных материалов на высоту.
                     
                        Эвакуируем авто из близлежащего зарубежья (Украина, РФ).
                     
                    Если Ваше транспортное средство оказалось неисправным, Вы можете воспользоваться услугой автопомощь на дороге:
                 
                    Заказать автокран вы можете, позвонив по одному из телефонов, указанных ниже,
                    либо оформив заявку на странице
                 
                    Заказать автомобиль кран-манипулятор вы можете позвонив по одному из телефонов, указанных ниже,
                    либо оформив заявку на странице
                 
                    Заказать услугу эвакуации вы можете позвонив по одному из телефонов, указанных ниже,
                    либо оформив заявку на странице
                 
                    Мы предоставим Вам автомобиль с краном-манипулятором, если Вам необходимо перевезти:
                 
                    Перевозим и эвакуируем различные виды транспорта:
                 
                    Примеры наших работ вы можете увидеть ниже либо посетив страницу нашего
                 
                    Примеры наших работ вы можете увидеть ниже.
                 
                    Список работ, осуществляемых с помощью автомобильного крана:
                 
            Вам было отправлено сообщение, содержащее ссылку для сброса пароля. Проверьте указанный E-Mail.
         
            Вы успешно активировали аккаунт.<br/>
            В ближайшее время наши администраторы проверят ваши данные и дадут вам доступ к внутренним ресурсам.<br/>
            Если вы уже имеете доступ, вы можете
         
            Произошла ОШИБКА при активации аккаунта.<br/>
            Свяжитесь с администратором
         
            Регистрация завершена. Для активации аккаута проверьте свой e-mail.
            После подтверждения вашего почтового ящика вам будет дан доступ к внутренним ресурсам.
            Если вы уже одобрены администратором, вы можете
         
            Регистрация на сайте <a href="http://www.gruzi-v-taxi.by">Грузи в ТАКСИ</a>
         
        Введите новый пароль
         
        Для активации аккаунта перейдите по ссылке в течение
        %(expiration_days)s дней:
         
        Добрый день!<br/>
        Вы (или кто-то от вашего имени) зарегистрировались на сайте
        Грузи в ТАКСИ.<br/>
        Если это были не вы, пожалуйста, игнорируйте это сообщение
        и ваш e-mail автоматически будет удален из нашей базы в течение нескольких дней.
         
        Добрый день!<br>
        Вы получили это сообщение потому что Вы (или кто-то от Вашего имени) сделал запрос на сброс пароля на сайте
         
        Забыли пароль? Введите свой E-Mail и Вам будут высланы инструкции для сброса пароля.
         
Регистрация на сайте Грузи В ТАКСИ
 
С уважением,
Команда Груси в ТАКСИ
 
Сброс пароля на сайте Грузи В ТАКСИ
 1 грузчик 2 грузчика 3 грузчика 4 грузчика 5 грузчиков 6 грузчиков 7 грузчиков 8 и более грузчиков Cброс пароля на сайте Грузи В ТАКСИ IP пользователя Setup [Грузи в ТАКСИ] Была оформлена заявка №%s [Грузи в ТАКСИ] Оформлена заявка на перевоз груза АВТОКРАН Авто-мото запчасти Автокран Автокран Мозырь Автокран в Мозыре Авторизация Администратор Аккаунт Активирован Аккаунт успешно активирован. Активен Активна Акции Акция Безналичный расчет Благодарим Вас за выбор нашей компании. Благодарим Вас за оставленный отзыв и за выбор нашей компании. Быстрая доставка В Пути к начальной точке В ожидании В процессе В пути Валюта Ваш запрос отправлен. С вами свяжется наш диспетчер Ваш логин Ваш профиль не активен Введите адреса или нажмите на карте, выбрав нужную точку Вернуться на Водитель Возможность оформить заявку on-line, используя удобную форму заказа. Опишите подробно Ваш груз, и мы сможем подобрать для Вас подходящий автомобиль. Войти Время Вход В Систему Вы должны иметь роль, назначенную администратором Вы оформили заявку на перевозку груза Вы успешно сбросили пароль! Вывоз старой мебели и строительного мусора. ГРУЗОПЕРЕВОЗКИ Гараж, беседку, контейнер, небольшую постройку, предназначенную для перевозки Главная Главная страница: Заголовок Главная страница: Контакты Главной Главную Груз Доставлен Грузи В ТАКСИ Грузи в ТАКСИ Грузовой авто Грузоперевозки Мозырь Грузчик Далее Данные по заявке Дата Дата Создания Джип / Внедорожник / Кроссовер Длина Маршрута (км) Длина маршрута  (км) Для заказа доставки заполните заявку или позвоните по одному из телефонов, указанных ниже Добрый день! Доставка ваших грузов по торговым точкам и складам. Доставка мебели, стройматериалов, бытовой техники. Доставка топлива Доставлен Другая автопомощь Если сумма заказа Вас не устроит, после оформления заявки вы сможете отменить заказ по телефону, когда оператор позвонит Вам Заблокирован Загрузка Заказ Заказ Изменен Заказ Назначен Заказ Оплачен Заказ Отменен Заказ Создан Заказа Заказано На Дату Заказать Заказчик Закрыть Запрашиваемый ресурс не найден Запрос отправляется Запрос отправляется...  Зарегистрироваться Заявка на грузоперевозку Заявки на транспортировку Здесь вы можете прочитать отзывы о нашей компании и оставить свой отзыв Изменен Статус Изменена Цена Изменить Пароль Изображение Индивидуальный подход к каждому заказу. Инстаграм канала История Транспортировки КОНТАКТЫ КРАН-МАНИПУЛЯТОР Карта Карта пользователя {} Карта сотрудника {} Карточки членов организации Карты пользователей Карты сотрудника Качество, пунктуальность, гарантии Количество Заказов Количество грузчиков Команда Грузи В ТАКСИ Комментарий Контактный номер телефона Контакты Кран-манипулятор Кран-манипулятор Мозырь Кран-манипулятор в Мозыре Кран-манипулятор: Заголовок Кран-манипулятор: Контакты Кровельные работы Куда Менеджер Минивен / Микроавтобус Мозырь, Республика Беларусь Монтаж конструкций Мотоцикл На данный момент отзывы отсутствуют На сервере произошла ошибка На сервере произошла ошибка. Наши администраторы исправляют проблему Название Название Для Сотрудников Наличный и безналичный расчет Например, Мозырь, Социалистическая 72 Настройка номера телефона Настройка телефонов на странице Настройки Настройки Веб Сайта Настройки телефонов Начало акции Не Активна Не выбран маршрут Не хватает прав. Свяжитесь с администратором для получения прав на просмотр содержимого Неактивен Неактивна Негабаритный груз Новый Номер Телефона Номер телефона Номер телефона (33) 333-33-33 ОБЩАЯ СТОИМОСТЬ ПЕРЕВОЗОК ОБЩАЯ СУММА ОФОРМИТЬ ЗАЯВКУ Обработан Общая стоимость авто Общая стоимость грузчиков Общий километраж Одобрен Оказываем широкий спектр услуг в сфере грузоперевозок Окончание акции Описание Оплачен Организации Оставить Отзыв Оставьте свой отзыв о работе компании. Обязательные поля отмечены *. Осуществляем междугородние перевозки по Беларуси. Осуществляем услуги грузоперевозок по городу Мозырю, Калинковичам, междугородние перевозки. Отзывы Отзывы на заказы Отзывы о компании Откуда Отличные цены на услуги Отменен Отправить Отчет Отчет.xlsx Оформление заявки Офромление заявки on-line не обязывает Вас пользоваться именно нашими услугами Оцените качество сервиса Ошибка Сервера ПОЧЕМУ ВАМ СТОИТ ОБРАТИТЬСЯ К НАМ? Перевозка Начата Перевозка пианино, длинномеров, крупногабаритных грузов. Переезд квартир, офисов, дач любой сложности. Перемещние груза на складах Пиломатериалы Пользователи и Группы Пользовательское Соглашение Предоставляем услуги автокрана для перевозки крупногабаритного строительного груза большой массы Предоставляем услуги крана-манипулятора и машины открытой погрузки Предоставляем услуги эвакуатора и машины открытой погрузки Предоставляем услуги эвакуатора, крана-манипулятора, автокрана. При безналичном расчете Вам понадобится оплатить путем перевода средств на счет, указанный на Прикрепленные изображения акции Прикрепленные изображения скидок Прикуривание авто Приложение Принят Произошла ошибка. Приносим извинения. Повторите заявку Произошла ошибка. Приносим извинения. Повторите попытку позже Промежуточная точка (Например, Мозырь, Нефтестроителей 1) Просмотрен Профиль Процент Пункты доставки Разгрузка грузовых авто, вагонов Размер скидки Регистрация Регистрация на сайте Грузи в ТАКСИ Рейтинг Реквизиты Роль не назначена С Вами свяжется наш менеджер для уточнения стоимости перевозки. С уважением, СТОИМОСТЬ Сайт Сайт основан на шаблоне Сборка/разборка мебели. Сброс Пароля Сброс пароля Сброс пароля успешно завершен Сбросить пароль Седан / Универсал Скидка (BYN) Скидки Создан Сотрудник Сотрудничество с фирмами, предприятиями и магазинами. Спецтехника / Строительная техника Спецтехнику Спутник Ссылка Ссылка для сброса пароля больше не валидна. Попробуйте еще раз Статус Стоимость (BYN) Стоимость Авто (BYN) Стоимость Грузчиков (BYN) Стоимость перевозки рассчитывается в зависимости от расстояния Стоимость работы грузчика рассчитывается Страница Автокрана: Заголовок Страница Автокрана: Контакты Страница Заказа: Контакты Страница Не Найдена Страница не найдена Строительные услуги: подача цемента, укладка плит перекрытия, фундамента Стройматериалы (блоки, плиты и т.д.) Сумма (BYN) Телефоны Теперь вы можете  Теперь вы можете войти в систему. Тип скидки Типы транспортировки Транспортировка УСЛУГИ Указать адреса Услуги автокрана Услуги крана-манипулятора Услуги эвакуатора. Машина открытой погрузки. Финансовый менеджер Форма обратной связи Форма обратной связи и Заказ on-line ЭВАКУАЦИЯ Эвакуатор в Мозыре Эвакуация Эвакуация Авто Эвакуация Мозырь Эвакуация легковых авто, грузовиков, микроавтобусов (вытаскивание из грязи, песка, водоемов) Эвакуация: Заголовок Эвакуация: Контакты Этаж Я забыл свой пароль адрес активен акция безналичный расчет в зависимости от сложности работ в систему войти в систему город данные заказа дата рождения долгота заказ заказать на дату заявка на транспортировку значение для отображения изменение статуса имя имя пользователя имя пользователя или E-Mail история транспортировки карта пользователя карта сотрудника карточка члена организации количество грузчиков комментарий комментарий сотрудника контактный E-Mail контактный номер телефона логотип максимальная сумма скидки по заказу максимальная цена автомобиля максимальная цена грузчиков местоположение сотрудника местоположение сотрудников минимальная сумма скидки по заказу название назначен на настройка телефонов номер телефона номер телефона (фактический) нужны грузчики о пользователе одобрен клиентом окончательная цена автомобиля окончательная цена грузчиков окончательная цена заказа описание оплата грузчикам организация отзыв отзыв на заказ по акции пользователь пользователь, совершивший действие порядок сортировки почасавая оплата предыдущая точка прикрепленное изображение акции прикрепленное изображение скидки пункт доставки расположение расстояние (в метрах) рейтинг роль сайт компании скидка создан сотрудник статус статус организации странице сумма скидки по заказу текст телефон тип действия тип транспортировки указать время уникальный идентификатор объекта фамилия фото цена автомобиля цена грузчиков широта этаж Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
 
                                    We have been engaged in cargo transportation for more than 6 years.
                                    We have a team of professional drivers and loaders..
                                     
                                    Own car park. Selection of the size 
                                    of the machine and the number of loaders
                                    for each order individually.
                                     
                                    We save your time and money.
                                    It is very important for us that  the client is satisfied.
                                     
                                You just give us the opportunity to more accurately  pick up the car for your order.
                                Also, you avoid the need to answer operator’s clarifying questions, thereby saving your time
                             
                                From year to year we are striving to improve the quality of our services, 
                                increasing the professionalism of drivers, the politeness of operators, and the accuracy of porters.
                             
                                We value every customer who appeals to us.
                             
                                After completing the application, our manager will contact you to clarify the cost of transportation, and you will be able to confirm  or cancel the order.
                             
                                We are constantly expanding the range of cargo такси:
                                taxi services: from crossings to the removal of construction waste, from transportation of construction materials to the evacuation of your car.
                             
                    We provide auto evacuation services, cargo transportation using a loader crane,truck crane services
                 
                                We are developing freight services in small towns, expanding the scope of our activities 
                                 so that every citizen of the Republic of Belarus is provided with a convenient cargo taxi service.
                             
                        A mounted crane truck is a universal vehicle for transporting building materials,
                        large size cargo and much more.
                     
                        Car loading capacity is 15 tons, the crane - 4.5 tons.
                     
                        Loading capacity of the truck crane installation is 12.5 tons, the boom length is 14 meters.
                     
                    We provide services for renting a truck crane in Mozyr, Kalinkovichi, Gomel region and Belarus.
                 
                    We provide mounted crane truck services in Mozyr, Kalinkovichi, Gomel region and Belarus.
                 
                    We provide towing services in Mozyr, Kalinkovichi, Gomel region and Belarus.
                 
                        With the help of a truck crane it is possible to perform such works
                        as unloading trucks, railway wagons,
                        assembly / construction work, lifting of building materials to a height.
                     
                    Evacuating cars from nearby foreign countries (Ukraine, Russian Federation).
                 
                    If your vehicle is faulty, you can use the roadside assistance service:
                 
                    You can order the loader crane service by calling one of the phones listed below,
                    or by placing an order on the 
                 
                    You can order the mounted crane truck service by calling one of the phones listed below,
                    or by placing an order on the 
                 
                    You can order the evacuation service by calling one of the phones listed below,
                    or by placing an order on the 
                 
                    We will provide you with a mounted crane truck, if you need to transport:
                 
                    We transport and evacuate various types of transport:
                 
                    Examples of our work, you can see below or by visiting page of our 
                 
                    Examples of our work, you can see below.
                 
                    List of works carried out using the loader 
crane:
                 
            You have been sent a message containing a link to reset password. Check the E-Mail.
         
            You have successfully activated your account.<br/>
            In the near future our administrators will check your data and give you access to internal resources.<br/>
            If you already have access, you can
         
            There was an ERROR when the account was activated.<br/>
            Please, contact the administrator
         
            Registration is complete. To activate the checkout, check your E-Mail.
            After confirming your mailbox, you will be given accessto internal resources.
            If you are already approved by the administrator, you can
         
            Registration on the site <a href="http://www.gruzi-v-taxi.by">Грузи в ТАКСИ</a>
         
        Enter new password
         
    To activate your account, click on the link within
    %(expiration_days)s days:
     
    Hello!<br/>
    You (or someone on your behalf) registered on the site
    Грузи в ТАКСИ.<br/>
    If it was not you, please ignore this message
    and your e-mail will be automatically deleted from our database withina few days.
     
Hello!
You received this message because you (or someone on your behalf) madea request to reset the password on the site
<a href="http://gruzi-v-taxi.by">Грузи В ТАКСИ</a>.
    If it was not you or you do not want to reset the password, please, ignore this message.
         
        Forgot your password? Enter your E-Mail and instructions will be sent to you for resetting your password
         
Registration on the site Грузи в ТАКСИ
 
Best regards,
Team Груси в ТАКСИ
 
Registration on the site Грузи в ТАКСИ
 1 loader 2 loaders 3 loaders 4 loaders 5 loaders 6 loaders 7 loaders 8 loaders or more Registration on the site Грузи в ТАКСИ team user's IP Setup [Грузи в ТАКСИ] An application for cargo transportation #%s was issued [Грузи в ТАКСИ] An application for cargo transportation was issued LOADER CRANE Auto-moto parts, details Loader crane Loader crane in Mozyr Loader crane in Mozyr Authorization Administrator Account Activated Account successfully activated Active Active Actions Action/Discount non-cash payment Thank you for choosing our company. Thank you for your feedback and for choosing our company. Quick delivery In the Path to the starting point Pending In progress On the road Currency Your request has been sent. Our dispatcher will contact you Your login Your profile is not active Enter addresses or click on the map, selecting the desired point Bach to Driver Ability to apply online, using the convenient order form. Describe in detail your cargo, and we will be able to choose the right car for you. Log in Time Login To The System You must have a role assigned by the administrator You have applied for cargo transportation You have successfully reset your password! Removal of old furniture and construction debris. CARGO TRANSPORTATION Garage, garden furniture, container, small building designed for transportation Home Home page: Header Home page: Contacts Home Home Cargo Delivered Грузи В ТАКСИ Грузи В ТАКСИ Trucks Грузоперевозки Мозырь Loader Next Application info Date Created Date Jeep / SUV / Crossover Length of the route (km) Length of the route (km) To order a delivery, fill out the application form or call one of the phoneslisted below Hello! Delivery of your goods to stores and warehouses. Delivery of furniture, building materials, household appliances. Fuel delivery Delivered Other auto help If the order amount does not suit you, you will be able tocancel the order by phone when the operator calls Disabled Loading Order Order Changed Order assigned Order Paid Order Cancelled Order created Place Order page Order by date Place Order Customer Close Requested resource not found Request is sending Request is sending... Sign Up Application for cargo transportation Application for cargo transportation Here you can read reviews about our company and leave your feedback Status Changed status changed Reset password Image Individual approach to each order Instagram account Transportation History CONTACTS MOUNTED CRANE TRUCK Map User card {} and proven employees Organisation members' cards Users cards Employees cards Quality, punctuality, guarantees number of loaders number of loaders Грузи В ТАКСИ Team Comment Contact phone number Contacts Mounted crane truck Mounted-Crane truck Mozyr Mounted crane truck in Mozyr Mounted crane truck: Header Mounted-Crane truck: Contacts Roofing To Manager Minivan / Minibus Mozyr, Republic of Belarus Installation of structures Motorcycle There is no feedback at the moment Server error occurred An error has occurred. Please, Repeat request name Name for employees Cash and non-cash payment (For example, Мозырь Нефтестроителей 10) Phone number settings Phone settings on page Configuration Web-site Configuration Phone numbers settings Discount start date Not Active No route selected Not enough rights. Contact the administrator for permission to view the content not active Not Active Oversized cargo New Phone number Phone number Phone number (33) 333-33-33 TOTAL VALUE OF TRANSPORTATION TOTAL PLACE ORDER Processed Total Route Length number of loaders Total Route Length Approved We provide a wide range of services in the field of freight transportation Discount end date description Paid Organisations Leave feedback Leave your feedback about the work of the company. Required fields are marked with *. We provide long-distance transportation in Belarus. We carry out cargo transportation services in the city of Mozyr, Kalinkovichi,  long-distance transportation. Feedback Orders feedback Feedback about company From We have excellent prices for services Cancelled Send Report Report.xlsx Place Order Making an application on-line does not oblige you to use our services Assess the quality of service Server Error WHY DO YOU NEED TO APPLY TO US? Transportation Started Transportation of pianos, big length cargo, bulky cargo. Moving of apartments, offices, cottages of any complexity. Moving cargo in warehouses Lumber Users and Groups Terms Of Use We provide truck crane services for the transportation of large-sized construction cargo of large mass We provide mounted crane truck services and open loading vehicles. We provide towing services and open loading vehicles. We provide services of a tow truck, manipulator crane, truck crane. In case of bank transfer, you will need to pay by transferring funds to the account indicated on Attached discount images Attached discount images Accumulator charging Application Accepted An error has occurred. Apologize. Repeat request An error has occurred. Please, Repeat request Next (For example, Мозырь Нефтестроителей 10) Viewed Profile In progress Delivery points Unloading of trucks, wagons End date Registration Registration on the site Грузи в ТАКСИ team Rating Requisites Not assigned Our manager will contact you to clarify the cost of transportation. Sincerely, PRICES Site This website is based on template Assembling / disassembling of furniture. Password reset Password reset Password reset successfully completed Reset password Sedan / Wagon Price (BYN) Price (BYN) Created Employee Cooperation with companies, enterprises and shops. Special machinery / Construction equipment Special machinery Satellite Link The link to reset the password is no longer valid. try again Status Price (BYN) Price (BYN) Price (BYN) The cost of the loader's work is calculated The cost of the loader's work is calculated Loader crane page: Header Loader crane page: Contacts Place Order: Contacts Page not found Page not found Construction services: cement supply, laying of floor slabs, foundations Building materials (blocks, plates, etc.) Price (BYN) Phone numbers Now you can  Now you can log in. Transportation type Transportation types Transportation type SERVICES Specify the addresses Loader crane services Mounted crane truck services Towing services. Open loading machine. Finance manager Callback form Callback form and on-line order EVACUATION Car Evacuation in Mozyr Evacuation Car Evacuation Car Evacuation in Mozyr Towage of passenger cars, trucks, minibuses (pulling out of mud, sand, water) Evacuation: Header Evacuation: Contacts Floor I forgot my password address active action non-cash payment depending on the complexity of the work in system log in city order info birthday longitude order Order by date Application for cargo transportation display value status changed first name username username (nickname) or E-Mail transportation history user card employee card organisation member's card number of loaders comment employee comment contact email contact phone number logo maximal car price maximal car price maximal porters price employee location location of the employees maximal car price name assigned by phone number settings phone number phone number (actual) need loaders about me approved by customer maximal car price maximal porters price final price amount description porters price amount organisation review final price amount discount user an actor order hourly rate Intermediate point attached action image attached action image delivery location distance (in meters) rating role company website Transportation type created employee status organisation status page discount amount for order text phone number action type transportation type specify the time unique object identifier last name photo car price amount porters price amount latitude floor 