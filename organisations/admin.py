"""
Organisations Admin settings
"""
from django.contrib import admin

from organisations.models.organisation_member_card import OrganisationMemberCard
from organisations.models.organisation import Organisation

admin.autodiscover()


class OrganisationMemberInline(admin.StackedInline):
    """ TransportationLocationInline """
    model = OrganisationMemberCard
    extra = 0


class OrganisationAdmin(admin.ModelAdmin):
    """ Organisation Admin class"""
    list_display = ('id', 'name', 'created', 'status')
    list_display_links = ('id', 'name')
    search_fields = ('name',)
    inlines = [OrganisationMemberInline]


admin.site.register(Organisation, OrganisationAdmin)
