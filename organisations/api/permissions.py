"""
Organisation API Permissions
"""
from typing import List, Union

from rest_framework.permissions import BasePermission
from rest_framework.request import Request
from rest_framework.viewsets import ViewSet
from restfw_composed_permissions.base import BasePermissionComponent, BaseComposedPermission, Or

from accounts.api.permissions import IsEmployeeManagerPermissionComponent
from accounts.enums import UserStatuses
from organisations.enums import OrganisationStatuses
from organisations.models.organisation import Organisation
from organisations.models.organisation_member_card import OrganisationMemberCard

PermissionArg = Union[None, BasePermission]


class IsOrganisationUser(BasePermission):
    """
    Is Organisation User
    """

    @classmethod
    def has_permission(cls, request: Request, view: ViewSet) -> bool:
        """
        Validate request
        :param request: HttpRequest instance (contains current user)
        :param view: view instance
        :return: boolean
        """
        organisations_card: List[OrganisationMemberCard] = request.user.org_cards.all()

        requirements = [
            request.user.user_card.status == UserStatuses.active.value,
            len([
                c for c in organisations_card if c.organisation.status == OrganisationStatuses.active.value
            ]) > 0 if len(organisations_card) else False
        ]

        return all(requirements)


class IsOrganisationUserPermissionComponent(BasePermissionComponent):
    """
    Check if current logged-in user is Organisation user (Organisation Admin or Manager or Finance manager)
    """

    @classmethod
    def has_permission(cls, permission: PermissionArg, request: Request, view: ViewSet) -> bool:
        """
        Validate request
        :param permission: Permission object
        :param request: HttpRequest instance (contains current user)
        :param view: view instance
        :return: boolean
        """
        return IsOrganisationUser.has_permission(request, view)

    @classmethod
    def has_object_permission(
            cls,
            permission: PermissionArg,
            request: Request,
            view: ViewSet,
            obj: Organisation
    ) -> bool:
        """
        Validate request
        :param permission: Permission object
        :param request: HttpRequest instance
        :param view: View instance
        :param obj: Organisation instance
        :return: boolean
        """
        organisations_card: OrganisationMemberCard = request.user.org_cards.filter(organisation_id=obj.id).first()

        return cls.has_permission(permission, request, view) and organisations_card is not None


class OrganisationComplexPermission(BaseComposedPermission):
    """
    Organisation Complex Permission
    """

    def global_permission_set(self):
        """
        Global Permission Set
        :return:
        """
        return Or(IsOrganisationUserPermissionComponent, IsEmployeeManagerPermissionComponent)

    def object_permission_set(self):
        """
        Return object's permissions
        :return:
        """
        return Or(IsOrganisationUserPermissionComponent, IsEmployeeManagerPermissionComponent)