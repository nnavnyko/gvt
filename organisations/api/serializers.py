"""
Organisations Serializers
"""
from typing import List, Type

from django.db import models
from rest_framework import serializers
from sorl.thumbnail import get_thumbnail

from organisations.models.organisation import Organisation
from organisations.models.organisation_member_card import OrganisationMemberCard


class OrganisationBaseSerializer(serializers.ModelSerializer):
    """
    Organisation Base JSON Serializer
    """
    logo = serializers.SerializerMethodField()

    @staticmethod
    def get_logo(obj: Organisation) -> str:
        """
        Get photo thumbnail
        :return:
        """
        return get_thumbnail(obj.logo, 'x128', quality=90).url \
            if obj.logo else None


class OrganisationListSerializer(OrganisationBaseSerializer):
    """
    Organisations Serializer
    """

    class Meta:
        """ Meta class """
        model = Organisation
        fields = ['id', 'uuid', 'name', 'website', 'status', 'get_status_display',
                  'contact_phone', 'contact_email', 'logo']


class OrganisationSerializer(OrganisationBaseSerializer):
    """
    Organisation Serializer
    """

    class Meta:
        """ Meta class """
        model = Organisation
        fields: List[str] = [
            'id', 'uuid', 'name', 'website', 'status', 'get_status_display',
            'contact_phone', 'contact_email', 'logo', 'description'
        ]


class OrganisationMemberCardSerializer(serializers.ModelSerializer):
    """
    OrganisationMemberCard JSON Serializer
    """
    class Meta:
        """ Meta class """
        model: Type[models.Model] = OrganisationMemberCard
        fields: List[str] = ['status', 'get_status_display', 'role', 'get_role_display']


class SimpleOrganisationSerializer(OrganisationBaseSerializer):
    """
    Simple Organisation serializer
    Use this serializer in TransportationRequest details page
    """
    class Meta:
        """ Meta class """
        model: Type[models.Model] = Organisation
        fields: List[str] = ['uuid', 'name', 'logo']
