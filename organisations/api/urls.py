"""
Organisations API URLs
"""
from django.conf.urls import url
from organisations.api import views


app_name = 'organisations'

urlpatterns = [
    url('^list', views.OrganisationsAdminViewSet.as_view({'get': 'list'}), name='organisations_list'),
    url('^create', views.OrganisationsAdminViewSet.as_view({'post': 'create'}), name='create_organisation'),
    url('^(?P<uuid>[a-z0-9\-]+)/members', views.OrganisationViewSet.as_view({'get': 'get_members'}),
        name='get_organisation_members'),
    url('^(?P<uuid>[a-z0-9\-]+)/update-status', views.OrganisationsAdminViewSet.as_view(
        {'post': 'update_organisation_status'}), name='update_organisation_status'),
    url('^(?P<uuid>[a-z0-9\-]+)', views.OrganisationViewSet.as_view({'get': 'retrieve', 'post': 'update'}),
        name='manage_organisation'),
]
