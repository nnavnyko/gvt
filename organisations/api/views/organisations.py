"""
Organisations API
---
Put all API to manage Organisations here (CREATE, UPDATE, GET LIST,
"""
from typing import List, Type

from django.db import transaction, models
from django.db.models import QuerySet
from django_filters import FilterSet
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import filters
from rest_framework.authentication import BasicAuthentication
from rest_framework.pagination import PageNumberPagination
from rest_framework.permissions import IsAuthenticated
from rest_framework.request import Request
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet

from accounts.api.permissions import IsEmployeeManager
from accounts.api.serializers import OrganisationMemberSerializer
from accounts.models.user import DjangoUser
from common.api.auth import SessionAuthentication
from organisations.api.permissions import OrganisationComplexPermission
from organisations.api.serializers import OrganisationListSerializer, OrganisationSerializer
from organisations.forms import OrganisationCreateForm, OrganisationEditForm
from organisations.models.organisation import Organisation
from organisations.models.organisation_member_card import OrganisationMemberCard


class OrganisationsPaginationClass(PageNumberPagination):
    """
        TransportationPaginationClass
    """
    page_size: int = 10
    page_query_param: str = 'page'
    page_size_query_param: str = 'page_size'


class OrganisationListFilter(FilterSet):
    """
        Transportation Requests List Filter
    """
    class Meta:
        """ Meta class """
        model: Type[models.Model] = Organisation
        fields: dict = {
            'status': ['exact'],
        }


class OrganisationsAdminViewSet(ModelViewSet):
    """
    Organisations ViewSet for Admins
    """
    serializer_class = OrganisationListSerializer
    authentication_classes = (BasicAuthentication, SessionAuthentication)
    permission_classes = (IsAuthenticated, IsEmployeeManager)
    pagination_class = OrganisationsPaginationClass
    filter_class = OrganisationListFilter
    filter_backends = (filters.SearchFilter, DjangoFilterBackend, filters.OrderingFilter)
    search_fields = ('name',)
    ordering_fields = ('name', 'status')
    lookup_url_kwarg = 'uuid'
    lookup_field = 'uuid'

    def get_queryset(self) -> QuerySet:
        """
        Get ordered queryset
        :return: QuerySet
        """
        return Organisation.objects.order_by('name').all()

    def create(self, request: Request, *args, **kwargs) -> Response:
        """
        Create Organisation from App
        :param request: HttpRequest instance
        :param args: arguments
        :param kwargs: key-word arguments
        :return: REST Response
        """
        form: OrganisationCreateForm = OrganisationCreateForm(request.data)

        if form.is_valid():
            with transaction.atomic():
                organisation: Organisation = form.save()

            return Response(OrganisationListSerializer(organisation, many=False).data)

        return Response(form.errors, status=400)

    def update_organisation_status(self, request: Request, *args, **kwargs) -> Response:
        """
        Update Organisation's status
        :param request: HttpRequest instnace
        :param args: arguments
        :param kwargs: key-word arguments
        :return: HttpResponse instance
        """
        organisation: Organisation = self.get_object()

        organisation = Organisation.objects.update_status(organisation, request.data.get('status'))

        return Response(OrganisationSerializer(organisation).data)


class OrganisationViewSet(ModelViewSet):
    """
    Organisation ViewSet
    """
    authentication_classes: tuple = (BasicAuthentication, SessionAuthentication)
    permission_classes: tuple = (IsAuthenticated, OrganisationComplexPermission)
    lookup_url_kwarg: str = 'uuid'
    lookup_field: str = 'uuid'

    def get_queryset(self) -> QuerySet:
        """
        Get ordered queryset
        :return: QuerySet
        """
        return Organisation.objects.order_by('name').all()

    def retrieve(self, request: Request, *args, **kwargs) -> Response:
        """
        Get Organisation details
        :param request: HttpRequest instance
        :param args: arguments
        :param kwargs: key-word arguments
        :return: API Response
        """
        organisation: Organisation = self.get_object()

        return Response(OrganisationSerializer(organisation, many=False).data)

    def update(self, request: Response, *args, **kwargs) -> Response:
        """
        Update Organisation instance
        :param request: HttpRequest instance
        :param args: arguments (list)
        :param kwargs: key-word arguments (dict)
        :return: Rest Response with organisation or Error
        """
        organisation: Organisation = self.get_object()

        form: OrganisationEditForm = OrganisationEditForm(instance=organisation, data=request.data)
        if form.is_valid():
            with transaction.atomic():
                organisation = form.save()

            return Response(OrganisationListSerializer(organisation, many=False).data)

        return Response(form.errors, status=400)

    def get_members(self, *args, **kwargs) -> Response:
        """
        Get Organisation's Members(employees) list
        :param args: arguments
        :param kwargs: key-word arguments
        :return: API Response
        """
        organisation: Organisation = self.get_object()
        org_members: List[DjangoUser] = OrganisationMemberCard.objects.get_organisation_members(
            organisation.id
        )

        return Response(OrganisationMemberSerializer(org_members, many=True).data)
