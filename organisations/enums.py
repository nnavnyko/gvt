"""
Organisation Enums
"""
from django.utils.translation import ugettext_lazy as _
from enum import Enum


class OrganisationStatuses(Enum):
    """
    Request Statuses class
    """
    active = 'active'
    disabled = 'disabled'


ORGANISATION_STATUSES = (
    (OrganisationStatuses.active.value, _('Активна')),
    (OrganisationStatuses.disabled.value, _('Неактивна'))
)


class OrganisationMemberStatus(Enum):
    """
    Organisation Member Status
    """
    active = 'active'
    disabled = 'disabled'


ORGANISATION_MEMBER_STATUSES = (
    (OrganisationMemberStatus.active.value, _('Активен')),
    (OrganisationMemberStatus.disabled.value, _('Неактивен'))
)


class OrganisationMemberRole(Enum):
    """
    Organisation Member role
    """
    admin = 'admin'
    finance = 'finance'
    manager = 'manager'


ORGANISATION_MEMBER_ROLES = (
    (OrganisationMemberRole.admin.value, _('Администратор')),
    (OrganisationMemberRole.finance.value, _('Финансовый менеджер')),
    (OrganisationMemberRole.manager.value, _('Менеджер'))
)
