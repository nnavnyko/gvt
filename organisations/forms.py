"""
Organisations forms
---
Put forms of Organisations models here
"""
from django import forms
from django.utils.translation import ugettext_lazy as _

from common.forms.fields import PhoneField
from organisations.models.organisation import Organisation


class OrganisationCreateForm(forms.ModelForm):
    """
    Organisation Create Form
    """
    class Meta:
        """ Metaclass """
        model = Organisation
        exclude = ['uuid', 'status', 'logo']

    contact_phone = PhoneField(label=_('Контактный номер телефона'), required=False)


class OrganisationEditForm(forms.ModelForm):
    """
    Organisation Edit Form
    """

    class Meta:
        """ Metaclass """
        model = Organisation
        exclude = ['status', 'logo']

    contact_phone = PhoneField(label=_('Контактный номер телефона'), required=False)
