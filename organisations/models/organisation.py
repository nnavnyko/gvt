"""
Organisation
"""
from django.core.exceptions import SuspiciousOperation
from django.db import models
from django.utils.translation import ugettext_lazy as _
from django_extensions.db.models import TimeStampedModel
from sorl.thumbnail import ImageField

from common.managers.base_manager import BaseManager
from common.models import UUIDModel
from organisations.enums import ORGANISATION_STATUSES, OrganisationStatuses


class OrganisationManager(BaseManager):
    """
    Organisation model manager
    """

    def update_status(self, organisation: 'Organisation', status: str) -> models.Model:
        """
        Update Organisation's status
        :param organisation: Organisation instance
        :param status: string (one of OrganisationStatuses enum values)
        :return: updated Organisation instance
        """
        if status is None:
            raise SuspiciousOperation('Organisation status is required! You need to pass status to update.')

        if status not in [s.value for s in OrganisationStatuses]:
            raise SuspiciousOperation('Status is not among available statuses')

        return self.update_model(organisation, status=status)


class Organisation(UUIDModel, TimeStampedModel):
    """
    Organisation Model
    """
    class Meta:
        """ Meta class """
        db_table = 'organisation'
        verbose_name = _(u'организация')
        verbose_name_plural = _(u'Организации')

    logo = ImageField(verbose_name=_('логотип'), upload_to='organisations',
                      default=None, null=True, blank=True)
    name = models.CharField(verbose_name=_('название'), max_length=256, null=False, blank=False)
    description = models.TextField(verbose_name=_('описание'), null=True, blank=True, default=None)
    status = models.CharField(verbose_name=_('статус организации'), max_length=8, choices=ORGANISATION_STATUSES,
                              null=False, blank=False, default=OrganisationStatuses.active.value)
    # Contact Info
    website = models.URLField(verbose_name=_('сайт компании'), null=True, blank=True, default=None)
    contact_phone = models.BigIntegerField(verbose_name=_('контактный номер телефона'),
                                           null=True, blank=True, default=None)
    contact_email = models.EmailField(verbose_name=_('контактный E-Mail'), null=True, blank=True, default=None)

    objects = OrganisationManager()

    def __str__(self) -> str:
        """ String representation """
        return 'Организация {}'.format(self.name)
