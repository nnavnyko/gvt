"""
Organisation member card
"""
from typing import List

from django.conf import settings
from django.db import models
from django.db.models import Prefetch
from django.utils.translation import ugettext_lazy as _
from django_extensions.db.models import TimeStampedModel

from accounts.models.user import DjangoUser
from common.managers.base_manager import BaseManager
from common.models import UUIDModel
from organisations.enums import ORGANISATION_MEMBER_STATUSES, OrganisationMemberStatus, ORGANISATION_MEMBER_ROLES, \
    OrganisationMemberRole
from organisations.models.organisation import Organisation


class OrganisationMemberCardManager(BaseManager):
    """
    Organisation member card model manager
    """

    def get_organisation_members(self, organisation_id: int) -> List[DjangoUser]:
        """
        Get organisation members
        :param organisation_id: int (Organisation.id)
        :return: list of User instance with loaded UserCard and OrganisationMemberCard
        """
        member_cards_queryset = self.filter(organisation_id=organisation_id)

        members = DjangoUser.objects.filter(
            org_cards__organisation_id=organisation_id
        ).select_related(
            'user_card'
        ).prefetch_related(
            Prefetch('org_cards', queryset=member_cards_queryset, to_attr='org_member_card')
        ).all()

        return members


class OrganisationMemberCard(UUIDModel, TimeStampedModel):
    """
    Organisation Member Card
    """
    class Meta:
        """ Meta class """
        db_table = 'organisation_member_card'
        verbose_name = _('карточка члена организации')
        verbose_name_plural = _('Карточки членов организации')
        unique_together = ('organisation', 'user')

    status = models.CharField(verbose_name=_('статус'), max_length=8, choices=ORGANISATION_MEMBER_STATUSES,
                              null=False, blank=False, default=OrganisationMemberStatus.active.value)
    role = models.CharField(verbose_name=_('роль'), max_length=10, choices=ORGANISATION_MEMBER_ROLES,
                            null=False, blank=False, default=OrganisationMemberRole.admin.value)
    organisation = models.ForeignKey(Organisation, verbose_name=_('организация'), null=False, blank=False,
                                     on_delete=models.CASCADE, related_name='members')
    user = models.ForeignKey(settings.AUTH_USER_MODEL, verbose_name=_('пользователь'), related_name='org_cards',
                             on_delete=models.CASCADE)

    objects = OrganisationMemberCardManager()

    def __str__(self):
        """
        String representation
        :return: string
        """
        return 'Карточка члена организации'
