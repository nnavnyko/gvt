"""
Organisations test cases
"""
from django.contrib.auth import get_user_model
from django.test import TestCase, Client
import time
from django.urls import reverse

from accounts.models.employee_card import EmployeeCard
from accounts.models.user import DjangoUser
from accounts.models.user_card import UserCard
from accounts.enums import EmployeeRoles
from organisations.models.organisation import Organisation
from organisations.enums import OrganisationStatuses

REQUEST_ERROR = 400
REQUEST_SUCCESS = 200
PERMISSION_DENIED_STATUS = 403

SIMPLE_PASSWORD = '12345678*Aa'


class OrganisationTestCase(TestCase):
    """
    Organisation test case
    """
    def set_manager_user(self):
        """
        Get manager user
        :return:
        """
        user_model = get_user_model()

        self.manager = DjangoUser.objects.get_or_none(email='admin_user_aa@yopmail.com')

        if not self.manager:
            self.manager = user_model(
                username='manager_user', first_name='Alex', last_name='Bolduing', email='admin_user_aa@yopmail.com')
            self.manager.set_password(SIMPLE_PASSWORD)
            self.manager.save()
            user_card = UserCard(phone_number=444444444)
            employee_card = EmployeeCard(role=EmployeeRoles.manager.value)

            user_card.user_id = self.manager.id
            employee_card.user_id = self.manager.id

            self.manager.user_card = user_card
            self.manager.employee_card = employee_card

            user_card.save()
            employee_card.save()

    def set_driver_user(self):
        """
        Set Driver user
        :return: None
        """
        user_model = get_user_model()

        self.driver = DjangoUser.objects.get_or_none(email='driver@yopmail.com')

        if not self.driver:
            self.driver = user_model(
                username='driver_user', first_name='Walter', last_name='White', email='driver@yopmail.com')
            self.driver.set_password(SIMPLE_PASSWORD)
            self.driver.save()
            user_card = UserCard(phone_number=555555555)
            employee_card = EmployeeCard(role=EmployeeRoles.driver.value)

            user_card.user_id = self.driver.id
            employee_card.user_id = self.driver.id

            self.driver.user_card = user_card
            self.driver.employee_card = employee_card

            user_card.save()
            employee_card.save()

    def set_simple_user(self):
        """
        Set Driver user
        :return: None
        """
        user_model = get_user_model()

        self.simple_user = DjangoUser.objects.get_or_none(email='user1@yopmail.com')

        if not self.simple_user:
            self.simple_user = user_model(
                username='simple_user', first_name='Walter', last_name='White', email='user1@yopmail.com')
            self.simple_user.set_password(SIMPLE_PASSWORD)
            self.simple_user.save()
            user_card = UserCard(phone_number=666666666)

            user_card.user_id = self.simple_user.id

            self.simple_user.user_card = user_card

            user_card.save()

    def test_create_organisation(self):
        """
        Test create organisaiton
        :return:
        """
        client = Client()
        self.set_manager_user()
        # Login manager
        client.post(reverse('accounts:auth_login'),
                    data={'username': self.manager.username, 'password': SIMPLE_PASSWORD})

        response = client.post(reverse('organisations:api:create_organisation'), {
            'name': 'Test Organisation',
            'description': 'Some Test Organisations',
            'website': 'https://example.com'
        })
        time.sleep(3)
        # Response status is 200, success
        self.assertEqual(response.status_code, REQUEST_SUCCESS, 'Response code is not 200')
        # 'phone' and 'text' are required params and missing
        organisations = Organisation.objects.all()
        self.assertEqual(1, len(organisations))

        self.assertEqual('Test Organisation', organisations[0].name)
        self.assertEqual(OrganisationStatuses.active.value, organisations[0].status)

    def test_create_organisation_permission_errors(self):
        """
        Test than only Managers can create organisation
        :return:
        """
        client = Client()
        # Test Driver has no permissions
        self.set_driver_user()
        # Login manager
        client.post(reverse('accounts:auth_login'),
                    data={'username': self.driver.username, 'password': SIMPLE_PASSWORD})

        response = client.post(reverse('organisations:api:create_organisation'), {
            'name': 'Test Organisation',
            'description': 'Some Test Organisations',
            'website': 'https://example.com'
        })
        time.sleep(3)
        # Response status is 200, success
        self.assertEqual(response.status_code, PERMISSION_DENIED_STATUS, 'Response code is not 403')

        # Test Simple user has no permissions
        self.set_simple_user()
        # Login manager
        client.post(reverse('accounts:auth_login'),
                    data={'username': self.simple_user.username, 'password': SIMPLE_PASSWORD})

        response = client.post(reverse('organisations:api:create_organisation'), {
            'name': 'Test Organisation',
            'description': 'Some Test Organisations',
            'website': 'https://example.com'
        })
        time.sleep(3)
        # Response status is 200, success
        self.assertEqual(response.status_code, PERMISSION_DENIED_STATUS, 'Response code is not 403')

    def test_create_organisation_form_errors(self):
        """
        Test create organisation form errors
        :return:
        """
        client = Client()
        self.set_manager_user()
        # Login manager
        client.post(reverse('accounts:auth_login'),
                    data={'username': self.manager.username, 'password': SIMPLE_PASSWORD})

        response = client.post(reverse('organisations:api:create_organisation'), {})
        time.sleep(3)
        # Response status is 200, success
        self.assertEqual(response.status_code, REQUEST_ERROR, 'Response code is not 400')

    def test_toggle_organisation_status(self):
        """
        Test Toggle organisation's status
        :return:
        """
        client = Client()
        self.set_manager_user()
        # Login manager
        client.post(reverse('accounts:auth_login'),
                    data={'username': self.manager.username, 'password': SIMPLE_PASSWORD})

        organisation = Organisation.objects.create(name='Test Organisation')
        organisation.save()

        response = client.post(reverse('organisations:api:update_organisation_status', args=(organisation.uuid,)),
                               {'status': OrganisationStatuses.disabled.value})

        time.sleep(3)
        self.assertEqual(response.status_code, REQUEST_SUCCESS, 'Response code is not 200')

        organisation = Organisation.objects.get_by_uuid(organisation.uuid)
        self.assertEqual(OrganisationStatuses.disabled.value, organisation.status)

        response = client.post(reverse('organisations:api:update_organisation_status', args=(organisation.uuid,)),
                               {'status': OrganisationStatuses.active.value})

        time.sleep(3)
        self.assertEqual(response.status_code, REQUEST_SUCCESS, 'Response code is not 200')

        organisation = Organisation.objects.get_by_uuid(organisation.uuid)
        self.assertEqual(OrganisationStatuses.active.value, organisation.status)

    def test_toggle_organisation_status_permission_errors(self):
        """
        Test Toggle organisation status permission errors
        :return:
        """
        client = Client()

        organisation = Organisation.objects.create(name='Test Organisation')
        organisation.save()

        self.set_driver_user()
        # Login driver
        client.post(reverse('accounts:auth_login'),
                    data={'username': self.driver.username, 'password': SIMPLE_PASSWORD})

        response = client.post(reverse('organisations:api:update_organisation_status', args=(organisation.uuid,)),
                               {'status': OrganisationStatuses.disabled.value})

        time.sleep(3)
        self.assertEqual(response.status_code, PERMISSION_DENIED_STATUS, 'Response code is not 403')

        self.set_simple_user()
        # Login driver
        client.post(reverse('accounts:auth_login'),
                    data={'username': self.simple_user.username, 'password': SIMPLE_PASSWORD})

        response = client.post(reverse('organisations:api:update_organisation_status', args=(organisation.uuid,)),
                               {'status': OrganisationStatuses.disabled.value})

        time.sleep(3)
        self.assertEqual(response.status_code, PERMISSION_DENIED_STATUS, 'Response code is not 403')

    def test_toggle_organisation_status_validation_errors(self):
        """
        Test toggle organisation validation errors
        :return:
        """
        client = Client()
        self.set_manager_user()
        # Login manager
        client.post(reverse('accounts:auth_login'),
                    data={'username': self.manager.username, 'password': SIMPLE_PASSWORD})

        organisation = Organisation.objects.create(name='Test Organisation')
        organisation.save()

        response = client.post(reverse('organisations:api:update_organisation_status', args=(organisation.uuid,)),
                               {'status': 'some_invalid_status'})

        time.sleep(3)
        self.assertEqual(response.status_code, REQUEST_ERROR, 'Response code is not 400')

    def test_update_organisation_details(self):
        """
        Test update Organisation's details
        :return:
        """
        client = Client()
        self.set_manager_user()
        # Login manager
        client.post(reverse('accounts:auth_login'),
                    data={'username': self.manager.username, 'password': SIMPLE_PASSWORD})

        organisation = Organisation.objects.create(name='Test Organisation')
        organisation.save()

        response = client.post(
            reverse('organisations:api:manage_organisation', args=(organisation.uuid,)),
            {
                'name': 'Updated Organisation name',
                'description': 'Updated Organisation description'
            })

        time.sleep(3)
        self.assertEqual(response.status_code, REQUEST_SUCCESS, 'Response code is not 200')
        organisation = Organisation.objects.get_by_uuid(organisation.uuid)
        self.assertEqual('Updated Organisation name', organisation.name)
        self.assertEqual('Updated Organisation description', organisation.description)

    def test_update_organisation_details_form_errors(self):
        """
        Test update Organisation's details
        :return:
        """
        client = Client()
        self.set_manager_user()
        # Login manager
        client.post(reverse('accounts:auth_login'),
                    data={'username': self.manager.username, 'password': SIMPLE_PASSWORD})

        organisation = Organisation.objects.create(name='Test Organisation')
        organisation.save()

        response = client.post(
            reverse('organisations:api:manage_organisation', args=(organisation.uuid,)), {})

        time.sleep(3)
        self.assertEqual(response.status_code, REQUEST_ERROR, 'Response code is not 400')

    def test_update_organisation_details_permission_errors(self):
        """
        Test Update organisation's details permission errors
        :return:
        """
        client = Client()

        organisation = Organisation.objects.create(name='Test Organisation')
        organisation.save()

        self.set_driver_user()
        # Login driver
        client.post(reverse('accounts:auth_login'),
                    data={'username': self.driver.username, 'password': SIMPLE_PASSWORD})

        response = client.post(reverse('organisations:api:manage_organisation', args=(organisation.uuid,)),
                               {'name': 'Test organisation name update', 'description': 'Updated description'})

        time.sleep(3)
        self.assertEqual(response.status_code, PERMISSION_DENIED_STATUS, 'Response code is not 403')

        self.set_simple_user()
        # Login driver
        client.post(reverse('accounts:auth_login'),
                    data={'username': self.simple_user.username, 'password': SIMPLE_PASSWORD})

        response = client.post(reverse('organisations:api:manage_organisation', args=(organisation.uuid,)),
                               {'name': 'Test organisation name update', 'description': 'Updated description'})

        time.sleep(3)
        self.assertEqual(response.status_code, PERMISSION_DENIED_STATUS, 'Response code is not 403')
