"""
Organisation URLs
"""
from django.conf.urls import url, include

from organisations.api import urls as api_urls

app_name = 'organisations_api'

urlpatterns = [
    url('^api/', include(api_urls, namespace='api'))
]
