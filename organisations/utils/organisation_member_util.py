"""
Organisation Member Util
"""
from typing import Dict, Union

from accounts.models.user import DjangoUser
from organisations.enums import OrganisationMemberStatus
from organisations.models.organisation_member_card import OrganisationMemberCard


class OrganisationMemberUtil(object):
    """
    Organisation member utility class
    """

    @classmethod
    def is_organisation_member(cls, user: DjangoUser, organisation_id: int) -> bool:
        """
        Check if the user is a member of organisation
        :param user: auth.User instance
        :param organisation_id: int (Organisation.id)
        :return: boolean
        """
        filtration: Dict[str, Union[str, int]] = {
            'user_id': user.id,
            'organisation_id': organisation_id,
            'status': OrganisationMemberStatus.active.value
        }

        return OrganisationMemberCard.objects.filter(**filtration).count() != 0
