# encoding: utf-8
"""
Transportation config
"""
from django.contrib import admin
from django.utils.translation import ugettext_lazy as _

from transportation.models.transportation_request import TransportationRequest
from transportation.models.transportation_type import TransportationType
from transportation.models.transportation_location import TransportationLocation

admin.autodiscover()


class TransportationLocationInline(admin.StackedInline):
    """ TransportationLocationInline """
    model = TransportationLocation
    extra = 0
    fields = ['address', 'sort_order', 'floor']
    readonly_fields = ['address', 'sort_order', 'floor']


class TransportationRequestAdmin(admin.ModelAdmin):
    """ TransportationRequestAdmin """
    list_display = ('id', 'created', 'date', 'get_assignee', 'phone', 'status')
    list_display_links = ('id', 'created')
    list_filter = ('status', 'created', 'date', 'assignee')
    search_fields = ('uuid', 'phone')
    inlines = [TransportationLocationInline]
    change_form_template = 'admin/transportation/tr_req_change_form.html'

    def get_assignee(self, obj):
        return ' '.join([obj.assignee.first_name, obj.assignee.last_name]) if obj.assignee else '-'

    get_assignee.short_description = _('назначен на')
    get_assignee.admin_order_field = 'assignee__first_name'

    def change_view(self, request, object_id, form_url='', extra_context=None):
        """
        Change view method
        :param request:
        :param object_id:
        :param form_url:
        :param extra_context:
        :return:
        """
        if not isinstance(extra_context, dict):
            extra_context = {}
        obj = TransportationRequest.objects.filter(pk=object_id).first()
        extra_context.update({
            'obj': obj,
            'start_coords': obj.points.filter(previous__isnull=True).first(),
            'next_coords': obj.points.filter(previous__isnull=False).order_by('sort_order'),
        })
        return super(TransportationRequestAdmin,
                     self).change_view(request, object_id, form_url, extra_context)


class TransportationTypeAdmin(admin.ModelAdmin):
    """
        TransportationTypeAdmin
    """
    list_display = ('id', 'name')
    list_display_links = ('id', 'name')


admin.site.register(TransportationRequest, TransportationRequestAdmin)
admin.site.register(TransportationType, TransportationTypeAdmin)
