# encoding: utf-8
"""
Transportation Permissions
"""
from typing import List

from rest_framework.permissions import BasePermission
from rest_framework.request import Request
from rest_framework.viewsets import ViewSet

from accounts.enums import EMPLOYEE_MANAGER_OR_DRIVER_ROLES
from transportation.models.transportation_request import TransportationRequest


class IsManagerOrRequestOwner(BasePermission):
    """
    Is Manager/Driver Or Transportation Request Owner
    Check if current user in session has
    """

    @classmethod
    def has_object_permission(cls, request: Request, view: ViewSet, obj: TransportationRequest) -> bool:
        """
        Check if current session user has
        permissions to change Transportation Request
        :param request: HttpRequest instance
        :param view: View function
        :param obj: TransportationRequest instance
        :return: bool
        """
        requirements: List[bool] = [
            hasattr(request.user, 'employee_card') and
            request.user.employee_card.role in EMPLOYEE_MANAGER_OR_DRIVER_ROLES,
            obj.created_by_id == request.user.id
        ]

        return any(requirements)
