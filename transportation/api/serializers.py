# encoding: utf-8
"""
    Transportation API Serializers
"""
from typing import Type, List

from django.db import models
from rest_framework import serializers

from accounts.api.serializers import EmployeeListSerializer, SimpleUserSerializer
from accounts.utils.auth import is_manager_or_driver
from common.api.serializers import StatusFieldSerializer
from organisations.api.serializers import SimpleOrganisationSerializer
from transportation.models.transportation_history import TransportationHistory
from transportation.models.transportation_location import TransportationLocation
from transportation.models.transportation_request import TransportationRequest


class TransportationRequestListSerializer(serializers.ModelSerializer):
    """
        Transportation Request List Serializer
        List serializer has simplified structure
    """

    assignee = EmployeeListSerializer()
    created_by = SimpleUserSerializer()
    time = serializers.SerializerMethodField()

    class Meta:
        """ Meta class """
        model: Type[models.Model] = TransportationRequest
        fields: List[str] = [
            'id', 'uuid', 'status', 'date', 'time', 'created', 'assignee', 'get_status_display',
            'amount', 'car_amount', 'porters_amount', 'car_amount_max',
            'porters_amount_max', 'created_by', 'action_id', 'is_hourly_rate', 'is_cashless_payment',
            'discount_amount', 'discount_amount_min', 'discount_amount_max', 'organisation_id'
        ]

    @staticmethod
    def get_time(obj: TransportationRequest) -> str:
        """
        Return parsed time
        :param obj: TransportationRequest instance
        :return: string
        """
        return obj.time.strftime('%H:%M') if obj.time else None


class TransportationLocationSerializer(serializers.ModelSerializer):
    """
        TransportationLocation Serializer
        Use it in all-fields TransportationRequestSerializer
    """
    class Meta:
        """ Meta class """
        model: Type[models.Model] = TransportationLocation
        fields: List[str] = ['id', 'address', 'latitude', 'longitude', 'floor', 'sort_order']


class TransportationRequestSerializer(StatusFieldSerializer):
    """
        Transportation Request Serializer
        This serializer contains most of fields
        Use this serializer only to view TransportationRequest Details
    """

    assignee = EmployeeListSerializer()
    created_by = EmployeeListSerializer()
    points = TransportationLocationSerializer(many=True)
    assignee_id = serializers.SerializerMethodField()
    action_id = serializers.SerializerMethodField()
    organisation = SimpleOrganisationSerializer()

    class Meta:
        """ Meta class """
        model: Type[models.Model] = TransportationRequest
        exclude: List[str] = ['action']

    @staticmethod
    def get_assignee_id(obj: TransportationRequest) -> int:
        """
        Get status displayed value of the Transportation Request
        :param obj: TransportationRequest instance
        :return: int (User.id)
        """
        return obj.assignee_id

    @staticmethod
    def get_action_id(obj: TransportationRequest) -> int:
        """
        Get status displayed value of the Transportation Request
        :param obj: TransportationRequest instance
        :return: int (
        """
        return obj.discount_id


class TransportationRequestHistorySerializer(StatusFieldSerializer):
    """
    Transportation Request History Serializer
    """
    points = TransportationLocationSerializer(many=True)

    class Meta:
        """ Meta class """
        model: Type[models.Model] = TransportationRequest
        fields: List[str] = [
            'phone', 'text', 'email', 'status', 'date', 'time', 'is_need_porters', 'porters_number',
            'transportation_type_id', 'route_length', 'action_id', 'is_hourly_rate', 'is_cashless_payment',
            'amount', 'discount_amount', 'discount_amount_min', 'discount_amount_max',
            'car_amount', 'porters_amount', 'car_amount_max', 'porters_amount_max', 'final_car_amount',
            'final_porters_amount', 'porters_payment', 'employee_comment', 'points'
        ]


class BaseTransportationHistorySerializer(serializers.ModelSerializer):
    """
    Base Transportation History Serializer
    """
    is_system = serializers.SerializerMethodField()
    action_type_display = serializers.SerializerMethodField()
    tr_state = serializers.SerializerMethodField()

    @staticmethod
    def get_tr_state(obj: TransportationHistory) -> dict:
        """
        Get TransportationRequest state dict
        :param obj: TransportationHistory instance
        :return: dict
        """
        return dict(obj.tr_state)

    @staticmethod
    def get_is_system(obj: TransportationHistory) -> bool:
        """
        Check if action was applied by System or Manager
        :param obj: TransportationHistory instance
        :return: bool
        """
        return not obj.action_by_id or is_manager_or_driver(obj.action_by)

    @staticmethod
    def get_action_type_display(obj: TransportationHistory) -> str:
        """
        Get status displayed value of the Transportation Request
        :param obj:
        :return:
        """
        return obj.get_action_type_display()


class TransportationHistorySerializer(BaseTransportationHistorySerializer):
    """
    TransportationHistory Serializer
    """
    action_by = EmployeeListSerializer()
    user = EmployeeListSerializer()

    class Meta:
        """ Meta class """
        model: Type[models.Model] = TransportationHistory
        exclude: List[str] = ['tr_request']


class SimpleTransportationHistorySerializer(BaseTransportationHistorySerializer):
    """
    TransportationHistory Serializer
    """

    class Meta:
        """ Meta class """
        model: Type[models.Model] = TransportationHistory
        exclude: List[str] = ['tr_request', 'action_by', 'user']
