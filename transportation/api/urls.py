# encoding: utf-8
"""
Transportation API Urls
"""
from django.conf.urls import url
from transportation.api import views

app_name = 'transportation_api'

urlpatterns = [
    url(r'^statistics$', views.TransportationStatisticsAPIView.as_view(),
        name='get_transportation_statistics'),
    url(r'^person-statistics$', views.TransportationRequestStatistics.as_view({'get': 'get_person_statistics'}),
        name='get_person_transportation_statistics'),
    url(r'^employee-statistics$', views.TransportationRequestStatistics.as_view(
        {'get': 'get_assigned_requests_statistics'}),
        name='get_person_transportation_statistics'),
    url(r'^employee-statistics/(?P<username>[a-z0-9\-_.]+)$', views.TransportationRequestStatistics.as_view(
        {'get': 'get_employee_statistics'}),
        name='get_person_transportation_statistics'),
    url(r'^organisation-statistics/(?P<org_uuid>[a-z0-9\-]+)$', views.TransportationRequestStatistics.as_view(
        {'get': 'get_organisation_statistics'}),
        name='get_organisation_transportation_statistics'),
    url(r'^requests/add/$', views.CreateTransportationRequestView.as_view(),
        name='add_transportation_request'),
    url(r'^requests/create/$', views.CreateTransportationRequestAPIView.as_view(),
        name='create_transportation_request_api'),
    url(r'^requests$', views.TransportationRequestListAPIViewSet.as_view({'get': 'list'}),
        name='requests_list'),
    url(r'^requests/my$', views.TransportationRequestListAPIViewSet.as_view({'get': 'my_requests'}),
        name='^my_requests_list'),
    url(r'^requests/my/assigned$', views.TransportationRequestListAPIViewSet.as_view({'get': 'my_assigned_requests'}),
        name='my_assigned_requests_list'),
    url(r'^requests/organisation/(?P<org_uuid>[a-z0-9\-]+)$',
        views.OrganisationTransportRequestsView.as_view({'get': 'org_requests'}),
        name='my_assigned_requests_list'),
    url(r'^requests/(?P<uuid>[a-z0-9\-]+)/$', views.TransportationRequestDetailsViewSet.as_view(
        {'get': 'retrieve', 'post': 'edit'}),
        name='request_details'),
    url(r'^requests/(?P<uuid>[a-z0-9\-]+)/change_status/$', views.TransportationRequestDetailsViewSet.as_view(
        {'post': 'change_status'}),
        name='change_request_status'),
    url(r'^requests/(?P<uuid>[a-z0-9\-]+)/assign/$', views.TransportationRequestDetailsViewSet.as_view(
        {'post': 'assign_request'}),
        name='assign_request'),
    url(r'^requests/(?P<uuid>[a-z0-9\-]+)/set_price/$', views.TransportationRequestDetailsViewSet.as_view(
        {'post': 'set_price_amount'}),
        name='set_request_price'),
    url(r'^requests/(?P<uuid>[a-z0-9\-]+)/add_employee_comment/$', views.TransportationRequestDetailsViewSet.as_view(
        {'post': 'add_employee_comment'}),
        name='add_employee_comment'),
    url(r'^requests/(?P<uuid>[a-z0-9\-]+)/driver_geolocation$', views.TransportationRequestDetailsViewSet.as_view(
        {'get': 'get_driver_geolocation'}),
        name='get_driver_geolocation'),
    url(r'^request/(?P<uuid>[a-z0-9\-]+)/history$', views.TransportationHistoryListAPIViewSet.as_view(
        {'get': 'list'}),
        name='tr_request_list')
]
