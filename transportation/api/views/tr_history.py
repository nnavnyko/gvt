"""
TransportationHistory API views
"""
from typing import Type

from django.db.models import QuerySet
from rest_framework.authentication import BasicAuthentication
from rest_framework.exceptions import PermissionDenied, NotFound
from rest_framework.permissions import IsAuthenticated
from rest_framework.request import Request
from rest_framework.response import Response
from rest_framework.serializers import ModelSerializer
from rest_framework.viewsets import ModelViewSet

from accounts.api.permissions import IsUserActive
from accounts.utils.employees_util import EmployeesUtil
from common.api.auth import SessionAuthentication
from transportation.api.permissions import IsManagerOrRequestOwner
from transportation.api.serializers import TransportationHistorySerializer, SimpleTransportationHistorySerializer
from transportation.models.transportation_history import TransportationHistory
from transportation.models.transportation_request import TransportationRequest


class TransportationHistoryListAPIViewSet(ModelViewSet):
    """
    TransportationHistory list serializer
    """
    permission_classes = (IsAuthenticated, IsUserActive)
    authentication_classes = (BasicAuthentication, SessionAuthentication)

    def list(self, request: Request, *args, **kwargs) -> Response:
        """
        Get list of TransportationRequest
        :param request: HttpRequest instance
        :param args: arguments
        :param kwargs: key-word arguments
        :return: serializer data
        """
        tr_request_uuid: str = kwargs.get('uuid')
        tr_request: TransportationRequest = TransportationRequest.objects.get_by_uuid(tr_request_uuid)

        if not tr_request_uuid or not tr_request:
            raise NotFound('Закан не найден')

        if not IsManagerOrRequestOwner.has_object_permission(request, self, tr_request):
            raise PermissionDenied('Только менеджеры (водитель, администратор, менеджер) '
                                   'или заявитель могут просмотреть историю Заказа')

        serializer: Type[ModelSerializer]
        if EmployeesUtil.is_manager_or_driver(request.user):
            serializer = TransportationHistorySerializer
        else:
            serializer = SimpleTransportationHistorySerializer

        history = TransportationHistory.objects.filter(
            tr_request_id=tr_request.id
        ).order_by(
            '-created'
        ).all()

        return Response(serializer(history, many=True).data)

    def get_queryset(self) -> QuerySet:
        """
        Get QuerySet of Transportation History
        :return: 
        """
        return TransportationHistory.objects.order_by('-created')
