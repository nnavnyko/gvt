# encoding: utf-8
"""
TransportationRequests API
"""
from typing import Dict, List, Union, Type

from django.contrib.auth import get_user_model
from django.db import transaction, models
from django.db.models import QuerySet
from django.forms import ModelForm
from django_filters import FilterSet
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import filters
from rest_framework.authentication import BasicAuthentication
from rest_framework.decorators import action
from rest_framework.exceptions import PermissionDenied
from rest_framework.generics import get_object_or_404
from rest_framework.pagination import PageNumberPagination
from rest_framework.permissions import IsAuthenticated
from rest_framework.request import Request
from rest_framework.response import Response
from rest_framework.serializers import ModelSerializer
from rest_framework.views import APIView
from rest_framework.viewsets import ModelViewSet, ViewSet

from accounts.api.permissions import IsEmployeeManagerOrDriver, IsUserActive, IsEmployeeManager
from accounts.api.serializers import EmployeeGeolocationSerializer
from accounts.models.employee_geolocation import EmployeeGeolocation
from accounts.models.user import DjangoUser
from accounts.utils.employee_geolocation_util import EmployeeGeolocationUtil
from accounts.utils.employees_util import EmployeesUtil
from common.api.auth import SessionAuthentication
from common.forms.filters import BooleanFilter, CheckboxInput
from common.utilities.common import parse_phone_number
from organisations.api.permissions import IsOrganisationUserPermissionComponent, OrganisationComplexPermission
from organisations.models.organisation import Organisation
from organisations.utils.organisation_member_util import OrganisationMemberUtil
from transportation.api.permissions import IsManagerOrRequestOwner
from transportation.api.serializers import TransportationRequestListSerializer, TransportationRequestSerializer
from transportation.enums import ActionTypes
from transportation.forms.forms import TransportationRequestForm, CreateTransportationRequestForm, \
    EditTransportationRequestForm, CreateTransportationRequestAsCustomerForm
from transportation.models.transportation_history import TransportationHistory
from transportation.models.transportation_request import TransportationRequest
from transportation.signals import request_created_signal
from transportation.utils.transportation_request_util import TransportationRequestUtil

CANCEL_ACTION_TYPE = 'cancel'
ASSIGN_ACTION_TYPE = 'assign'


class CreateTransportationRequestView(APIView):
    """ CreateTransportationRequestView """
    authentication_classes = (SessionAuthentication,)

    def post(self, request: Request, *args, **kwargs) -> Response:
        """
        POST request handler
        :param request: REST Request
        :param args: arguments
        :param kwargs: key-word arguments
        :return: Response
        """
        data = request.data.copy()
        data['user_remote_ip'] = request.META.get('REMOTE_ADDR')

        form: TransportationRequestForm = TransportationRequestForm(data)

        if form.is_valid():
            with transaction.atomic():
                tr_req: TransportationRequest = form.save()

                TransportationRequestUtil.append_request_points(tr_req, data.get('points', []))

                tr_req.save()

                TransportationHistory.objects.log_action(
                    tr_req,
                    ActionTypes.created.value,
                    comment='Заказ создан через форму на веб-сайте'
                )
                request_created_signal.send(sender=type(tr_req), instance=tr_req)

            TransportationRequestUtil.send_ws_statistics_message('tr_created')

            return Response(data={'tr_request': 'created', 'status': 'success'}, status=200)

        return Response(data={'errors': form.errors}, status=400)


class TransportationPaginationClass(PageNumberPagination):
    """
        TransportationPaginationClass
    """
    page_size: int = 10
    page_query_param: str = 'page'
    page_size_query_param: str = 'page_size'


class TransportationRequestListFilter(FilterSet):
    """
        Transportation Requests List Filter
    """
    class Meta:
        """ Meta class """
        model = TransportationRequest
        fields: Dict[str, List[str]] = {
            'status': ['exact'],
            'assignee_id': ['exact'],
            'date': ['gte', 'lte'],
            'is_cashless_payment': ['exact']
        }
        filter_overrides: dict = {
            models.BooleanField: {
                'filter_class': BooleanFilter,
                'extra': lambda f: {
                     'widget': CheckboxInput,
                 },
            },
        }


class BaseTransportationRequestListView(ModelViewSet):
    """
    Base TransportRequest List API View
    """
    filter_backends = (filters.SearchFilter, DjangoFilterBackend)
    pagination_class = TransportationPaginationClass
    authentication_classes = (BasicAuthentication, SessionAuthentication)

    def get_paginated_queryset_response(self, queryset: QuerySet) -> Response:
        """
        Get paginated queryset
        :param queryset: QuerySet instance
        :return: REST Response
        """
        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)

            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)

        return Response(serializer.data)


class TransportationRequestListAPIViewSet(BaseTransportationRequestListView):
    """
        Transportation API ViewSet
    """
    serializer_class = TransportationRequestListSerializer
    permission_classes = (IsAuthenticated, IsEmployeeManagerOrDriver)
    filter_class = TransportationRequestListFilter
    search_fields = ('phone', 'email')

    def list(self, request: Request, *args, **kwargs) -> Response:
        """
        Get All transportation requests (only staff or superadmin users can get this list)
        :param request: HttpRequest instance
        :param args: arguments
        :param kwargs: key-word arguments
        :return: Response
        """
        if not IsEmployeeManager.has_permission(request, self):
            raise PermissionDenied()

        return super(TransportationRequestListAPIViewSet, self).list(request, *args, **kwargs)

    def my_requests(self, request: Request, *args, **kwargs) -> Response:
        """
        Get current user's transportation requests
        :param request: HttpRequest instance
        :param args: arguments
        :param kwargs: key-word arguments
        :return: REST Response
        """
        queryset: QuerySet = self.filter_queryset(self.get_queryset().filter(created_by_id=request.user.id))

        return self.get_paginated_queryset_response(queryset)

    def my_assigned_requests(self, request: Request, *args, **kwargs) -> Response:
        """
        Get transportation requests assigned to current user (Only for staff or superadmin users)
        :param request: HttpRequest instance
        :param args: arguments
        :param kwargs: key-word arguments
        :return: REST Response
        :raises: PermissionDenied
        """
        if not IsEmployeeManagerOrDriver.has_permission(request, self):
            raise PermissionDenied()

        queryset: QuerySet = self.filter_queryset(self.get_queryset().filter(assignee_id=request.user.id))

        return self.get_paginated_queryset_response(queryset)

    def get_queryset(self) -> QuerySet:
        """
        Get ordered queryset
        :return: QuerySet
        """
        related: List[str] = [
            'assignee', 'organisation', 'assignee__user_card', 'assignee__employee_card',
            'assignee__employee_card__geolocation'
        ]

        return TransportationRequest.objects.prefetch_related(*related).order_by('-date', '-time', '-created')


class OrganisationTransportationRequestListFilter(FilterSet):
    """
        Transportation Requests List Filter
    """
    class Meta:
        """ Meta class """
        model = TransportationRequest
        fields: Dict[str, List[str]] = {
            'status': ['exact'],
            'date': ['gte', 'lte']
        }


class OrganisationTransportRequestsView(BaseTransportationRequestListView):
    """
    TransportationRequests related to Organisation
    """
    serializer_class = TransportationRequestListSerializer
    permission_classes = (IsAuthenticated, OrganisationComplexPermission)
    filter_class = OrganisationTransportationRequestListFilter

    def org_requests(self, request: Request, org_uuid: str, *args, **kwargs) -> Response:
        """
        Get list of Organisation's TransportationRequests
        :param request: HttpRequest instance
        :param org_uuid: string (Organisation.uuid)
        :param args: arguments
        :param kwargs: key-word arguments
        :return: API Response
        """
        organisation: Organisation = Organisation.objects.get_or_404(uuid=org_uuid)

        queryset: QuerySet = self.filter_queryset(self.get_queryset().filter(organisation_id=organisation.id))

        return self.get_paginated_queryset_response(queryset)

    def get_queryset(self) -> QuerySet:
        """
        Get ordered queryset
        :return: QuerySet
        """
        return TransportationRequest.objects.select_related(
            'assignee'
        ).order_by(
            '-date', '-time', '-created'
        )


class TransportationRequestDetailsViewSet(ModelViewSet):
    """
        TransportationRequest Details ViewSet
        Manage TransactionRequest data (view/update)
    """
    serializer_class = TransportationRequestSerializer
    permission_classes = (IsAuthenticated, IsManagerOrRequestOwner, IsUserActive)
    authentication_classes = (BasicAuthentication, SessionAuthentication)
    lookup_url_kwarg = 'uuid'
    lookup_field = 'uuid'

    def get_queryset(self) -> QuerySet:
        """
        Get ordered queryset
        :return: QuerySet
        """
        return TransportationRequest.objects.select_related().order_by('points__sort_order')

    def retrieve(self, request: Request, *args, **kwargs) -> Response:
        """
        Get Transportation Request Details
        Check permissions:
            - the user has to be a staff-member or
            - the user has to be an owner of the transportation request
        Otherwise throw PermissionDenied exception
        :param request: HttpRequest instance
        :param args: uuid
        :param kwargs: key-word arguments
        :return: REST Response
        :raises: PermissionDenied
        """
        instance: TransportationRequest = self.get_object()
        serializer: ModelSerializer = self.get_serializer(instance)

        return Response(serializer.data)

    @action(detail=False, methods=['POST'])
    def change_status(self, request: Request, *args, **kwargs) -> Response:
        """
        Change TransportationRequest status and assign it to the current session user
        :param request: HttpRequest instance
        :param args: arguments
        :param kwargs: key-word arguments
        :return: REST Response
        :raises: PermissionDenied, MethodNotAllowed
        """
        action_type: str = request.data.get('type')
        ws_messages: dict = {
            CANCEL_ACTION_TYPE: 'cancelled',
            'finish': 'delivered'
        }
        instance: TransportationRequest
        instance = self.get_object()
        if action_type == CANCEL_ACTION_TYPE:
            if not IsManagerOrRequestOwner.has_object_permission(request, self, instance):
                raise PermissionDenied('Только менеджеры (водитель, администратор, менеджер) '
                                       'или заявитель могут отменить Заявку')
        else:
            if not IsEmployeeManagerOrDriver.has_permission(request, self):
                raise PermissionDenied('Только менеджеры (водитель, администратор, менеджер) могут менять Заявку')

        instance = TransportationRequestUtil.change_status(instance, request.user, action_type, request.data)
        TransportationRequestUtil.send_ws_statistics_message('update_statistics', current_user=request.user)

        serializer: ModelSerializer = self.get_serializer(instance)

        TransportationRequestUtil.send_ws_message(
            instance,
            message=ws_messages.get(action_type, 'tr_changed'),
            current_user=request.user
        )

        return Response(serializer.data)

    @action(detail=False, methods=['POST'])
    def assign_request(self, request: Request, *args, **kwargs) -> Response:
        """
        Assign TransportationRequest to manager or driver
        The current user must have permissions
        :param request: HttpRequest instance
        :param args: arguments
        :param kwargs: key-word arguments
        :return: REST Response
        :raises: PermissionDenied
        """
        if not IsEmployeeManagerOrDriver.has_permission(request, self):
            raise PermissionDenied(u'Только менеджеры (водитель, администратор, менеджер) могут менять Заявку')

        instance: TransportationRequest = self.get_object()
        user: DjangoUser = get_object_or_404(get_user_model(), id=request.data.get('assignee_id'))
        data: dict = {'current_user': request.user}
        instance = TransportationRequestUtil.change_status(instance, user, ASSIGN_ACTION_TYPE, data)

        serializer: ModelSerializer = self.get_serializer(instance)

        TransportationRequestUtil.send_ws_message(instance, current_user=request.user)

        TransportationRequestUtil.send_ws_statistics_message(
            'tr_request_assigned', current_user=request.user, recipients=[user.username])
        TransportationRequestUtil.send_ws_statistics_message(
            'update_statistics', current_user=request.user)

        return Response(serializer.data)

    @action(detail=False, methods=['POST'])
    def set_price_amount(self, request: TransportationRequest, *args, **kwargs) -> Response:
        """
        Set TransportationRequest price for car and porters
        :param request: HttpRequest instance
        :param args: arguments
        :param kwargs: key-word arguments
        :return: REST Response
        :raises: PermissionDenied
        """
        if not IsEmployeeManagerOrDriver.has_permission(request, self):
            raise PermissionDenied(u'Только менеджеры (водитель, администратор, менеджер) могут менять Заявку')

        instance: TransportationRequest
        instance = self.get_object()

        instance = TransportationRequestUtil.set_price_amount(instance, request.user, request.data)

        serializer: ModelSerializer = self.get_serializer(instance)

        TransportationRequestUtil.send_ws_message(instance, message='price_changed', current_user=request.user)

        return Response(serializer.data)

    @action(detail=False, methods=['POST'])
    def add_employee_comment(self, request: Request, *args, **kwargs) -> Response:
        """
        Set TransportationRequest price for car and porters
        :param request: HttpRequest instance
        :param args: arguments
        :param kwargs: key-word arguments
        :return: REST Response
        :raises: PermissionDenied
        """
        if not IsEmployeeManagerOrDriver.has_permission(request, self):
            raise PermissionDenied('Только менеджеры (водитель, администратор, менеджер) могут менять Заявку')

        instance: TransportationRequest
        instance = self.get_object()

        with transaction.atomic():
            instance = TransportationRequest.objects.update_model(
                instance,
                employee_comment=request.data.get('employee_comment')
            )

            TransportationHistory.objects.log_action(
                instance,
                ActionTypes.change_info.value,
                user_by_id=request.user.id,
                comment='Сотрудник добавил комментарий'
            )
        serializer: ModelSerializer = self.get_serializer(instance)

        TransportationRequestUtil.send_ws_message(instance, current_user=request.user)

        return Response(serializer.data)

    def edit(self, request: Request, *args, **kwargs) -> Response:
        """
        Edit TransportationRequest
        :param request: HttpRequest instance
        :param args: arguments
        :param kwargs: key-word arguments
        :return: REST Response
        :raises: PermissionDenied
        """
        if not IsEmployeeManagerOrDriver.has_permission(request, self):
            raise PermissionDenied('Только менеджеры (водитель, администратор, менеджер) могут менять Заявку')
        data: dict = request.data.copy()

        data['phone'] = parse_phone_number(str(data.get('phone')))

        instance: TransportationRequest
        instance = self.get_object()

        form: EditTransportationRequestForm = EditTransportationRequestForm(instance=instance, data=data)
        if form.is_valid():
            with transaction.atomic():
                instance.points.clear()
                instance = form.save(commit=False)

                serializer = self.get_serializer(instance)
                TransportationRequestUtil.append_request_points(
                    instance, data.get('points', []))

                instance.save()

                TransportationHistory.objects.log_action(
                    instance, ActionTypes.change_info.value, user_by_id=request.user.id,
                    comment='Сотрудник изменил данные заказа')

            TransportationRequestUtil.send_ws_message(instance, current_user=request.user)

            return Response(serializer.data)

        return Response(data={'errors': form.errors}, status=400)

    def get_driver_geolocation(self, request: Request, *args, **kwargs) -> Response:
        """
        Get Driver geolocation
        :param request: HttpRequest instance
        :param args: arguments
        :param kwargs: key-word arguments
        :return: REST Response
        """
        instance: TransportationRequest = self.get_object()

        if not EmployeeGeolocationUtil.can_get_geolocation(instance):
            raise PermissionDenied(u'Местоположение водителя разрешено отслеживать только в выполняемом заказе')

        geolocation: Union[EmployeeGeolocation, None] = instance.assignee.employee_card \
            if hasattr(instance.assignee.employee_card, 'geolocation') else None

        return Response({
            'geolocation': EmployeeGeolocationSerializer(
                instance.assignee.employee_card.geolocation
            ).data if geolocation else None
        })


class CreateTransportationRequestAPIView(APIView):
    """
        CreateTransportationRequestView
    """
    permission_classes = (IsAuthenticated, IsEmployeeManagerOrDriver, IsUserActive)
    authentication_classes = (BasicAuthentication, SessionAuthentication)

    def post(self, request: Request, *args, **kwargs) -> Response:
        """
        Create Transportation Request
        :param request: HttpRequest instance
        :param args: arguments
        :param kwargs: key-word arguments
        :return: REST Response
        """
        data: dict = request.data.copy()

        data['phone'] = parse_phone_number(str(data.get('phone')))
        data['created_by'] = request.user.id

        organisation_uuid: str = data.pop('organisation_uuid', None)

        if organisation_uuid:
            organisation: Organisation = Organisation.objects.get_by_uuid(organisation_uuid)

            if EmployeesUtil.is_manager(request.user) or OrganisationMemberUtil.is_organisation_member(
                    request.user, organisation.id):
                data['organisation'] = organisation.id

        form_class: Type[ModelForm]

        if EmployeesUtil.is_manager_or_driver(request.user):
            form_class = CreateTransportationRequestForm
        else:
            form_class = CreateTransportationRequestAsCustomerForm

        form = form_class(data)

        if form.is_valid():
            with transaction.atomic():
                tr_req: TransportationRequest = form.save(commit=False)
                TransportationRequestUtil.append_request_points(
                    tr_req, data.get('points', []))
                tr_req.save()

                TransportationHistory.objects.log_action(
                    tr_req,
                    ActionTypes.created.value,
                    user_by_id=request.user.id,
                    user_id=request.user.id,
                    comment='Заказ создан через систему управления заказами'
                )

            TransportationRequestUtil.send_ws_statistics_message(
                'tr_created',
                current_user=request.user
            )

            return Response({'uuid': tr_req.uuid})

        return Response(data={'errors': form.errors}, status=400)


class TransportationStatisticsAPIView(APIView):
    """
        Transportation Statistics APIView
        Get statistics about Transportation Requests
    """
    permission_classes = (IsAuthenticated, IsEmployeeManagerOrDriver, IsUserActive)
    authentication_classes = (BasicAuthentication, SessionAuthentication)

    def get(self, request: Request, *args, **kwargs) -> Response:
        """
        Get Transportation statistics
        :param request: HttpRequest instance
        :param args: arguments
        :param kwargs: key-word arguments
        :return: REST Response
        """
        return Response(TransportationRequestUtil.get_statistics(request.user))


class TransportationRequestStatistics(ViewSet):
    """
    User view set to get statistics:
    1. Placed orders of the current user
    2. Assigned to and resolved by the current user
    3. Assigned to and resolved by employee
    """
    permission_classes = (IsAuthenticated, IsUserActive)
    authentication_classes = (BasicAuthentication, SessionAuthentication)

    @staticmethod
    def get_person_statistics(request: Request, *args, **kwargs) -> Response:
        """
        Get statistics of the current logged-in user
        :param request: HttpRequest instance
        :param args: arguments
        :param kwargs: key-word arguments
        :return: REST Response
        """
        statistics: List[dict] = TransportationRequest.objects.get_person_statistics(request.user)

        return Response(statistics)

    def get_assigned_requests_statistics(self, request: Request, *args, **kwargs) -> Response:
        """
        Get statistics of the current logged-in user if the user is Employee
        :param request: HttpRequest instance
        :param args: arguments
        :param kwargs: key-word arguments
        :return: REST Response
        """
        if not IsEmployeeManagerOrDriver.has_permission(request, self):
            raise PermissionDenied(u'Только менеджеры (водитель, администратор, менеджер) '
                                   u'имеют статистику выполненных заказов')

        statistics: List[dict] = TransportationRequest.objects.get_person_statistics(request.user, is_employee=True)

        return Response(statistics)

    def get_employee_statistics(self, request: Request, username: str, *args, **kwargs) -> Response:
        """
        Get statistics of the current logged-in user if the user is Employee
        :param request: HttpRequest instance
        :param args: arguments
        :param username: User.username (str)
        :param kwargs: key-word arguments
        :return: REST Response
        """
        if not IsEmployeeManagerOrDriver.has_permission(request, self):
            raise PermissionDenied('Только менеджеры (водитель, администратор, менеджер) '
                                   'могут получить статистику выполненных заказов сотрудника')

        employee: DjangoUser = DjangoUser.objects.get(username=username)

        statistics: List[str] = TransportationRequest.objects.get_person_statistics(
            employee,
            is_employee=True
        )

        return Response(statistics)

    def get_organisation_statistics(self, request: Request, org_uuid: str, *args, **kwargs) -> Response:
        """
        Get Organisation's statistics of ordered Requests
        :param request: HttpRequest
        :param org_uuid: string (Organisation.uuid)
        :param args: arguments
        :param kwargs: key-word arguments
        :return: REST Response
        """
        organisation: Organisation = Organisation.objects.get_by_uuid(uuid_=org_uuid)

        if not IsEmployeeManagerOrDriver.has_permission(request, self) and \
                not IsOrganisationUserPermissionComponent.has_object_permission(None, request, self, organisation):
            raise PermissionDenied('Только менеджеры (администратор, менеджер) или Сотрудники Организации'
                                   'могут получить статистику заказов Организации')

        statistics: List[dict] = TransportationRequest.objects.get_organisation_statistics(
            organisation.id
        )

        return Response(statistics)
