# encoding: utf-8
"""
Transportation app config
"""
from __future__ import unicode_literals

from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class TransportationConfig(AppConfig):
    """
    Transportation Config
    """
    name = 'transportation'
    verbose_name = _('Транспортировка')
