# encoding:utf-8
"""
    Transportation Enums
"""
from enum import Enum

from django.utils.translation import ugettext_lazy as _


class RequestStatuses(Enum):
    """
        Request Statuses class
    """
    new = 'new'
    accepted = 'accepted'
    in_progress = 'in_progress'
    on_the_road = 'on_the_road'
    delivered = 'delivered'
    cancelled = 'cancelled'
    paid = 'paid'


REQUEST_STATUSES = (
    (RequestStatuses.new.value, _('Новый')),
    (RequestStatuses.accepted.value, _('Принят')),
    (RequestStatuses.in_progress.value, _('В процессе')),
    (RequestStatuses.on_the_road.value, _('В пути')),
    (RequestStatuses.delivered.value, _('Доставлен')),
    (RequestStatuses.cancelled.value, _('Отменен')),
    (RequestStatuses.paid.value, _('Оплачен')),
)

REQUEST_STATUSES_DICT = dict(REQUEST_STATUSES)


PORTERS_NUMBERS = (
    (1, _('1 грузчик')),
    (2, _('2 грузчика')),
    (3, _('3 грузчика')),
    (4, _('4 грузчика')),
    (5, _('5 грузчиков')),
    (6, _('6 грузчиков')),
    (7, _('7 грузчиков')),
    (8, _('8 и более грузчиков')),
)


class ActionTypes(Enum):
    """
    Action Types
    """
    created = 'created'
    change_info = 'change_info'
    change_price = 'change_price'
    change_status = 'change_status'
    assigned = 'assigned'
    on_the_road = 'on_the_road'
    started = 'started'
    delivered = 'delivered'
    paid = 'paid'
    cancelled = 'cancelled'


ACTION_TYPES = (
    (ActionTypes.created.value, _('Заказ Создан')),
    (ActionTypes.change_info.value, _('Заказ Изменен')),
    (ActionTypes.change_price.value, _('Изменена Цена')),
    (ActionTypes.change_status.value, _('Изменен Статус')),
    (ActionTypes.assigned.value, _('Заказ Назначен')),
    (ActionTypes.on_the_road.value, _('В Пути к начальной точке')),
    (ActionTypes.started.value, _('Перевозка Начата')),
    (ActionTypes.delivered.value, _('Груз Доставлен')),
    (ActionTypes.paid.value, _('Заказ Оплачен')),
    (ActionTypes.cancelled.value, _('Заказ Отменен'))
)
