# encoding: utf-8
"""
Transportation Forms
"""
from typing import Union, List

from django import forms
from django.conf import settings
from django.utils.translation import ugettext_lazy as _

from common.forms.fields import PhoneField
from common.forms.forms import BootstrapControlsForm, GoogleReCaptchaForm
from transportation.enums import PORTERS_NUMBERS
from transportation.models.transportation_location import TransportationLocation
from transportation.models.transportation_request import TransportationRequest


class TransportationRequestFormBase(forms.ModelForm):
    """
    Transportation Request Form Base class
    """
    phone = PhoneField(label=_('Номер телефона'), required=True)

    def clean_porters_number(self) -> Union[int, None]:
        """
        Clean porters number
        :return: porters number
        """
        porters_number = self.cleaned_data.get('porters_number')
        porters_number = int(porters_number) if porters_number else None

        return porters_number


class TransportationRequestForm(GoogleReCaptchaForm, TransportationRequestFormBase, BootstrapControlsForm):
    """ TransportationForm """
    date = forms.DateField(label=_(u'заказать на дату'), input_formats=settings.DATE_INPUT_FORMATS, required=False)
    porters_number = forms.ChoiceField(choices=PORTERS_NUMBERS, required=False)

    class Meta:
        """ Meta class """
        model = TransportationRequest
        exclude: List[str] = [
            'assignee', 'status', 'uuid', 'is_hourly_rate', 'porters_amount', 'porters_amount_max',
            'car_amount', 'car_amount_max', 'employee_comment', 'final_car_amount', 'final_porters_amount',
            'discount_amount', 'discount_amount_max', 'discount_amount_min'
        ]
        widgets: dict = {
            'route_length': forms.HiddenInput(),
            'phone': forms.TextInput(attrs={'minlength': 13})
        }

    def __init__(self, *args, **kwargs):
        """ initialize form """
        super(TransportationRequestForm, self).__init__(*args, **kwargs)
        self.fields['date'].widget.attrs['class'] += u' datepicker'
        self.fields['time'].widget.attrs['class'] += u' timepicker'
        self.fields['date'].widget.attrs['data-provide'] = u'datepicker'
        self.fields['time'].widget.attrs['data-provide'] = u'timepicker'
        self.fields['porters_number'].required = False
        self.fields['porters_number'].empty_label = None


class CreateTransportationRequestForm(TransportationRequestFormBase):
    """,
        Create Transportation Request Form
    """
    class Meta:
        """ Meta class """
        model = TransportationRequest
        exclude: List[str] = [
            'assignee', 'status', 'status_changed', 'uuid', 'car_amount_max', 'porters_amount_max', 'amount',
            'employee_comment'
        ]


class CreateTransportationRequestAsCustomerForm(TransportationRequestFormBase):
    """
        Create Transportation Request Form
    """
    class Meta:
        """ Meta class """
        model = TransportationRequest
        exclude: List[str] = [
            'assignee', 'status', 'status_changed', 'uuid', 'car_amount_max', 'porters_amount_max',
            'amount', 'car_amount', 'porters_amount',  'employee_comment', 'organisation',
            'action', 'porters_payment', 'is_hourly_rate',
            'discount_amount', 'discount_amount_min', 'discount_amount_max'
        ]


class EditTransportationRequestForm(TransportationRequestFormBase):
    """
        Create Transportation Request Form
    """
    class Meta:
        """ Meta class """
        model = TransportationRequest
        exclude: List[str] = [
            'assignee', 'created_by', 'status', 'uuid', 'car_amount', 'porters_amount',
            'car_amount_max', 'porters_amount_max', 'amount', 'is_hourly_rate', 'is_cashless_payment',
            'final_car_amount', 'final_porters_amount', 'porters_payment'
        ]


class TransportationLocationForm(forms.ModelForm):
    """
    Transportation Location Form
    """
    class Meta:
        """ Meta class """
        model = TransportationLocation
        exclude: List[str] = ['id', 'uuid', 'trans_req']
