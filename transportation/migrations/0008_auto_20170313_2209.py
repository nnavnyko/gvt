# -*- coding: utf-8 -*-
# Generated by Django 1.9.1 on 2017-03-13 19:09
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('transportation', '0007_auto_20160826_2240'),
    ]

    operations = [
        migrations.AddField(
            model_name='transportationlocation',
            name='floor',
            field=models.PositiveIntegerField(blank=True, default=None, null=True, verbose_name='\u044d\u0442\u0430\u0436'),
        ),
        migrations.AddField(
            model_name='transportationrequest',
            name='is_need_porters',
            field=models.BooleanField(default=False, verbose_name='\u043d\u0443\u0436\u043d\u044b \u0433\u0440\u0443\u0437\u0447\u0438\u043a\u0438'),
        ),
        migrations.AddField(
            model_name='transportationrequest',
            name='porters_number',
            field=models.IntegerField(choices=[(1, '1 \u0433\u0440\u0443\u0437\u0447\u0438\u043a'), (2, '2 \u0433\u0440\u0443\u0437\u0447\u0438\u043a\u0430'), (3, '3 \u0433\u0440\u0443\u0437\u0447\u0438\u043a\u0430'), (4, '4 \u0433\u0440\u0443\u0437\u0447\u0438\u043a\u0430'), (5, '5 \u0433\u0440\u0443\u0437\u0447\u0438\u043a\u043e\u0432'), (6, '6 \u0433\u0440\u0443\u0437\u0447\u0438\u043a\u043e\u0432'), (7, '7 \u0433\u0440\u0443\u0437\u0447\u0438\u043a\u043e\u0432'), (8, '8 \u0438 \u0431\u043e\u043b\u0435\u0435 \u0433\u0440\u0443\u0437\u0447\u0438\u043a\u043e\u0432')], default=2, verbose_name='\u043a\u043e\u043b\u0438\u0447\u0435\u0441\u0442\u0432\u043e \u0433\u0440\u0443\u0437\u0447\u0438\u043a\u043e\u0432'),
        ),
    ]
