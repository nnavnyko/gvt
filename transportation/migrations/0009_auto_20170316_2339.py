# -*- coding: utf-8 -*-
# Generated by Django 1.9.1 on 2017-03-16 20:39
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion
import django_extensions.db.fields


class Migration(migrations.Migration):
    atomic = False

    dependencies = [
        ('transportation', '0008_auto_20170313_2209'),
    ]

    operations = [
        migrations.CreateModel(
            name='TransportationType',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', django_extensions.db.fields.CreationDateTimeField(auto_now_add=True, verbose_name='created')),
                ('modified', django_extensions.db.fields.ModificationDateTimeField(auto_now=True, verbose_name='modified')),
                ('uuid', models.UUIDField(default=None, editable=False, null=True, verbose_name='\u0443\u043d\u0438\u043a\u0430\u043b\u044c\u043d\u044b\u0439 \u0438\u0434\u0435\u043d\u0442\u0438\u0444\u0438\u043a\u0430\u0442\u043e\u0440 \u043e\u0431\u044a\u0435\u043a\u0442\u0430')),
                ('sort_order', models.PositiveIntegerField(default=0, verbose_name='\u043f\u043e\u0440\u044f\u0434\u043e\u043a \u0441\u043e\u0440\u0442\u0438\u0440\u043e\u0432\u043a\u0438')),
            ],
            options={
                'db_table': 'transportation_type',
                'verbose_name': '\u0442\u0438\u043f \u0442\u0440\u0430\u043d\u0441\u043f\u043e\u0440\u0442\u0438\u0440\u043e\u0432\u043a\u0438',
                'verbose_name_plural': '\u0422\u0438\u043f\u044b \u0442\u0440\u0430\u043d\u0441\u043f\u043e\u0440\u0442\u0438\u0440\u043e\u0432\u043a\u0438',
            },
        ),
        migrations.CreateModel(
            name='TransportationTypeTranslation',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=255, verbose_name='\u043d\u0430\u0437\u0432\u0430\u043d\u0438\u0435')),
                ('description', models.TextField(blank=True, default=None, null=True, verbose_name='\u043e\u043f\u0438\u0441\u0430\u043d\u0438\u0435')),
                ('language_code', models.CharField(db_index=True, max_length=15)),
                ('master', models.ForeignKey(editable=False, on_delete=django.db.models.deletion.CASCADE, related_name='translations', to='transportation.TransportationType')),
            ],
            options={
                'managed': True,
                'abstract': False,
                'db_table': 'transportation_type_translation',
                'db_tablespace': '',
                'default_permissions': (),
            },
        ),
        migrations.AddField(
            model_name='transportationlocation',
            name='uuid',
            field=models.UUIDField(default=None, editable=False, null=True, verbose_name='\u0443\u043d\u0438\u043a\u0430\u043b\u044c\u043d\u044b\u0439 \u0438\u0434\u0435\u043d\u0442\u0438\u0444\u0438\u043a\u0430\u0442\u043e\u0440 \u043e\u0431\u044a\u0435\u043a\u0442\u0430'),
        ),
        migrations.AddField(
            model_name='transportationrequest',
            name='uuid',
            field=models.UUIDField(default=None, editable=False, null=True, verbose_name='\u0443\u043d\u0438\u043a\u0430\u043b\u044c\u043d\u044b\u0439 \u0438\u0434\u0435\u043d\u0442\u0438\u0444\u0438\u043a\u0430\u0442\u043e\u0440 \u043e\u0431\u044a\u0435\u043a\u0442\u0430'),
        ),
        migrations.AlterModelTable(
            name='transportationlocation',
            table='transportation_location',
        ),
        migrations.AlterModelTable(
            name='transportationrequest',
            table='transportation_request',
        ),
        migrations.AddField(
            model_name='transportationrequest',
            name='transportation_type',
            field=models.ForeignKey(blank=True, default=None, null=True, on_delete=django.db.models.deletion.CASCADE, to='transportation.TransportationType', verbose_name='\u0442\u0438\u043f \u0442\u0440\u0430\u043d\u0441\u043f\u043e\u0440\u0442\u0438\u0440\u043e\u0432\u043a\u0438'),
        ),
        migrations.AlterUniqueTogether(
            name='transportationtypetranslation',
            unique_together=set([('language_code', 'master')]),
        ),
    ]
