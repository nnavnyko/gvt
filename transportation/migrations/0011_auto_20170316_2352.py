# -*- coding: utf-8 -*-
# Generated by Django 1.9.1 on 2017-03-16 20:52
from __future__ import unicode_literals

from django.db import migrations, models
import uuid


class Migration(migrations.Migration):

    dependencies = [
        ('transportation', '0010_auto_20170316_2340'),
    ]

    operations = [
        migrations.AlterField(
            model_name='transportationlocation',
            name='uuid',
            field=models.UUIDField(default=uuid.uuid4, editable=False, verbose_name='\u0443\u043d\u0438\u043a\u0430\u043b\u044c\u043d\u044b\u0439 \u0438\u0434\u0435\u043d\u0442\u0438\u0444\u0438\u043a\u0430\u0442\u043e\u0440 \u043e\u0431\u044a\u0435\u043a\u0442\u0430'),
        ),
        migrations.AlterField(
            model_name='transportationrequest',
            name='uuid',
            field=models.UUIDField(default=uuid.uuid4, editable=False, verbose_name='\u0443\u043d\u0438\u043a\u0430\u043b\u044c\u043d\u044b\u0439 \u0438\u0434\u0435\u043d\u0442\u0438\u0444\u0438\u043a\u0430\u0442\u043e\u0440 \u043e\u0431\u044a\u0435\u043a\u0442\u0430'),
        ),
        migrations.AlterField(
            model_name='transportationtype',
            name='uuid',
            field=models.UUIDField(default=uuid.uuid4, editable=False, verbose_name='\u0443\u043d\u0438\u043a\u0430\u043b\u044c\u043d\u044b\u0439 \u0438\u0434\u0435\u043d\u0442\u0438\u0444\u0438\u043a\u0430\u0442\u043e\u0440 \u043e\u0431\u044a\u0435\u043a\u0442\u0430'),
        ),
    ]
