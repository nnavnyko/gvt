# -*- coding: utf-8 -*-
# Generated by Django 1.9.1 on 2017-03-18 16:53
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('transportation', '0011_auto_20170316_2352'),
    ]

    operations = [
        migrations.AddField(
            model_name='transportationtype',
            name='is_active',
            field=models.BooleanField(default=True, verbose_name='\u0430\u043a\u0442\u0438\u0432\u0435\u043d'),
        ),
    ]
