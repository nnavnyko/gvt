# -*- coding: utf-8 -*-
# Generated by Django 1.9.1 on 2017-05-24 11:00
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('transportation', '0014_auto_20170508_1745'),
    ]

    operations = [
        migrations.AddField(
            model_name='transportationrequest',
            name='price',
            field=models.FloatField(blank=True, default=None, null=True, verbose_name='\u0446\u0435\u043d\u0430'),
        ),
    ]
