# -*- coding: utf-8 -*-
# Generated by Django 1.9.1 on 2017-06-28 16:15
from __future__ import unicode_literals

from django.db import migrations
import model_utils.fields


class Migration(migrations.Migration):

    dependencies = [
        ('transportation', '0019_auto_20170617_1818'),
    ]

    operations = [
        migrations.AddField(
            model_name='transportationrequest',
            name='status_changed',
            field=model_utils.fields.MonitorField(blank=True, default=None, monitor='status', null=True, verbose_name='\u0438\u0437\u043c\u0435\u043d\u0435\u043d\u0438\u0435 \u0441\u0442\u0430\u0442\u0443\u0441\u0430'),
        ),
    ]
