# -*- coding: utf-8 -*-
# Generated by Django 1.9.1 on 2017-07-06 17:07
from __future__ import unicode_literals

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('transportation', '0021_auto_20170628_1917'),
    ]

    operations = [
        migrations.AlterField(
            model_name='transportationrequest',
            name='date',
            field=models.DateField(default=datetime.date.today, verbose_name='\u0437\u0430\u043a\u0430\u0437\u0430\u0442\u044c \u043d\u0430 \u0434\u0430\u0442\u0443'),
        ),
    ]
