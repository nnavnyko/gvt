# -*- coding: utf-8 -*-
# Generated by Django 1.9.1 on 2017-07-06 17:13
from __future__ import unicode_literals

from django.db import migrations, transaction, connection
from django.utils.decorators import method_decorator


class Migration(migrations.Migration):

    dependencies = [
        ('transportation', '0022_auto_20170706_2007'),
    ]

    operations = [
    ]

    @method_decorator(transaction.atomic())
    def apply(self, project_state, schema_editor, collect_sql=False):
        """
        Generate status_monitor field on TransportationRequest
        Set updated_at as TransportationRequest.status_changed
        :param project_state:
        :param schema_editor:
        :param collect_sql:
        :return:
        """
        with connection.cursor() as cursor:
            cursor.execute('''
                UPDATE transportation_request SET date = '2017-07-06' WHERE date IS NULL;
            ''')
            print('Rows dates set: ', cursor.rowcount)

        return project_state