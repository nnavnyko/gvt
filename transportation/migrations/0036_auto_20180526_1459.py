# Generated by Django 2.0.5 on 2018-05-26 11:59

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('transportation', '0035_transportationhistory'),
    ]

    operations = [
        migrations.AlterField(
            model_name='transportationhistory',
            name='action_type',
            field=models.CharField(choices=[('created', 'Заказ Создан'), ('change_info', 'Заказ Изменен'), ('change_price', 'Изменена Цена'), ('change_status', 'Изменен Статус'), ('assigned', 'Заказ Назначен'), ('on_the_road', 'В Пути к начальной точке'), ('started', 'Перевозка Начата'), ('delivered', 'Груз Доставлен'), ('paid', 'Заказ Оплачен'), ('cancelled', 'Заказ Отменен')], max_length=12, verbose_name='тип действия'),
        ),
        migrations.AlterField(
            model_name='transportationrequest',
            name='phone',
            field=models.CharField(max_length=255, verbose_name='номер телефона'),
        ),
    ]
