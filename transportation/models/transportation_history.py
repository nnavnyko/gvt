"""
TransportationHistory manager
"""
from typing import TYPE_CHECKING

from django.conf import settings
from django.db import models
from django.utils.translation import ugettext_lazy as _
from django_extensions.db.fields.json import JSONField
from django_extensions.db.models import TimeStampedModel

from common.managers.base_manager import BaseManager
from common.models import UUIDModel
from transportation.enums import ACTION_TYPES, ActionTypes

if TYPE_CHECKING:
    from transportation.models.transportation_request import TransportationRequest


class TransportationHistoryManager(BaseManager):
    """
    TransportationHistory DB Manager
    """

    def log_action(
            self,
            tr_request: 'TransportationRequest',
            action_type=ActionTypes.change_info.value,
            user_by_id: int = None,
            user_id: int = None,
            comment: str = None
    ) -> 'TransportationHistory':
        """
        Log action
        :param tr_request: TransportationRequest instance
        :param action_type: string (one of ActionTypes enum values)
        :param user_by_id: int (User.id, user who applied action)
        :param user_id: int (User.id, for example, assignee_id)
        :param comment: string
        :return: created TransactionHistory instance
        """
        from transportation.api.serializers import TransportationRequestHistorySerializer
        tr_request_data: dict = TransportationRequestHistorySerializer(tr_request, many=False).data

        return self.create(
            tr_request=tr_request,
            action_type=action_type,
            tr_state=tr_request_data,
            action_by_id=user_by_id,
            user_id=user_id,
            comment=comment
        )


class TransportationHistory(UUIDModel, TimeStampedModel):
    """
    Transportation History. Log all actions which were applied to the TransportationRequest
    """
    class Meta:
        """ Meta class """
        db_table = 'transportation_history'
        verbose_name = _(u'история транспортировки')
        verbose_name_plural = _(u'История Транспортировки')

    action_type = models.CharField(verbose_name=_('тип действия'), choices=ACTION_TYPES,
                                   null=False, blank=False, max_length=13)
    action_by = models.ForeignKey(settings.AUTH_USER_MODEL,
                                  related_name='action_by',
                                  verbose_name=_('пользователь, совершивший действие'),
                                  null=True, default=None, blank=True, on_delete=models.SET_NULL)
    tr_request = models.ForeignKey('transportation.TransportationRequest', verbose_name=_('заказ'),
                                   on_delete=models.CASCADE, null=False, related_name='history')
    tr_state = JSONField(verbose_name=_('данные заказа'), default=None, null=True, blank=True)
    comment = models.CharField(max_length=255, verbose_name=_('комментарий'),
                               null=True, blank=True, default=None)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, verbose_name=_('пользователь'),
                             related_name='user', on_delete=models.SET_NULL,
                             null=True, blank=True, default=None)
    objects = TransportationHistoryManager()

    def __str__(self) -> str:
        return 'Действие к Заказу #{}, {}'.format(self.tr_request_id, self.get_action_type_display())
