"""
TransportationLocation model - Map point with address and location
"""
from django.db import models
from django.utils.translation import ugettext_lazy as _
from django_extensions.db.models import TimeStampedModel

from common.managers.base_manager import BaseManager
from common.models import UUIDModel


class TransportationLocationManager(BaseManager):
    """
    TransportationLocation DB Manager
    """


class TransportationLocation(UUIDModel, TimeStampedModel):
    """ TransportationLocation """
    class Meta:
        """ Meta class """
        db_table = 'transportation_location'
        verbose_name = _(u'пункт доставки')
        verbose_name_plural = _(u'Пункты доставки')
        ordering = ['sort_order']

    sort_order = models.PositiveIntegerField(verbose_name=_(u'порядок сортировки'), default=1)
    address = models.TextField(verbose_name=_(u'адрес'))
    latitude = models.FloatField(verbose_name=_(u'широта'), null=True, blank=True, default=None)
    longitude = models.FloatField(verbose_name=_(u'долгота'), null=True, blank=True, default=None)
    previous = models.ForeignKey('self', verbose_name=_(u'предыдущая точка'),
                                 null=True, blank=True, default=None, on_delete=models.SET_NULL)
    trans_req = models.ForeignKey('transportation.TransportationRequest', verbose_name=_(u'заявка на транспортировку'),
                                  null=True, default=None, related_name='points', on_delete=models.CASCADE)
    floor = models.PositiveIntegerField(verbose_name=_(u'этаж'), default=None, blank=True, null=True)

    objects = TransportationLocationManager()

    def __str__(self):
        return '#{} Пункт назначения: {}'.format(self.sort_order, self.address)
