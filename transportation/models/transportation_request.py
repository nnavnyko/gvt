"""
TransportationRequest + DB Manager
"""
from datetime import date, timedelta
from typing import Dict, List, Tuple, TYPE_CHECKING

from django.conf import settings
from django.db import models
from django.db.models import Value as V, CharField, Count, QuerySet
from django.db.models.aggregates import Sum
from django.db.models.expressions import Case, When, F
from django.db.models.functions import Concat
from django.urls import reverse
from django.utils.translation import ugettext_lazy as _
from django_extensions.db.models import TimeStampedModel
from model_utils.fields import MonitorField

from common.db.expressions import DatePart
from common.managers.base_manager import BaseManager
from common.models import UUIDModel
from transportation.constants import TRANSPORTATION_REQUEST_DETAILS_URL
from transportation.enums import REQUEST_STATUSES, PORTERS_NUMBERS
from transportation.enums import RequestStatuses

ReportData = Tuple[List[dict], dict]

if TYPE_CHECKING:
    from accounts.models.user import DjangoUser


class TransportationRequestManager(BaseManager):
    """
    TransportationRequest DB Manager
    """

    def get_statistics(self, assignee_id: int = None) -> Dict[str, int]:
        """
        Return Transportation statistics
        :param assignee_id: int or None
        :return: dict
        """
        filter_params: dict = {}
        if assignee_id:
            filter_params['assignee_id'] = assignee_id

        return {
            RequestStatuses.new.value: self.filter(
                status=RequestStatuses.new.value, **filter_params
            ).count(),
            RequestStatuses.accepted.value: self.filter(
                status=RequestStatuses.accepted.value, **filter_params
            ).count(),
            RequestStatuses.delivered.value: self.filter(
                status__in=[
                    RequestStatuses.delivered.value,
                    RequestStatuses.paid.value
                ], **filter_params
            ).count(),
            RequestStatuses.in_progress.value: self.filter(
                status__in=[
                    RequestStatuses.in_progress.value,
                    RequestStatuses.on_the_road.value
                ], **filter_params
            ).count(),
            RequestStatuses.cancelled.value: self.filter(
                status=RequestStatuses.cancelled.value,
                **filter_params
            ).count()
        }

    def get_general_report_data(self, **kwargs):
        """
        Get Report Data
        :param kwargs: key-word arguments for filtration
        :return: list of dictionaries
        """
        fields: List[str] = [
            'uuid', 'created', 'created_url', 'date', 'phone_number', 'status',
            'price_amount', 'disc_amount', 'order_total_amount', 'final_car_amount', 'final_porters_amount',
            'assignee_name', 'route_length_km'
        ]

        query: QuerySet = self.select_related('assignee').annotate(
            assignee_name=Concat('assignee__first_name', V(' '), 'assignee__last_name')
        )

        return self._get_general_report_data(query, fields, **kwargs)

    def get_organisation_general_report_data(self, organisation_id: int, **kwargs) -> ReportData:
        """
        Get Report Data
        :param organisation_id: int (Organisation.id)
        :return: list of dictionaries
        """
        filtration = kwargs.copy()
        filtration['organisation_id'] = organisation_id

        fields: List[str] = [
            'uuid', 'created', 'created_url', 'date', 'status', 'price_amount', 'disc_amount',
            'order_total_amount', 'final_car_amount', 'final_porters_amount', 'route_length_km'
        ]

        return self._get_general_report_data(self, fields, **filtration)

    @classmethod
    def _get_general_report_data(cls, query: QuerySet, fields: List[str], **kwargs) -> ReportData:
        """
        Get General Report data
        :param query: QuerySet instance
        :param fields: list of strings (exported fields)
        :param kwargs: filtration dict
        :return: tuple of rows and total data dict
        """
        query = query.filter(**kwargs).order_by('date', 'created').annotate(
            phone_number=Concat(V('+375'), 'phone', output_field=CharField()),
            price_amount=Case(When(amount=None, then=0), default='amount'),
            disc_amount=Case(When(discount_amount=None, then=0), default='discount_amount'),
            route_length_km=Case(When(route_length=None, then=0), default=F('route_length') / 1000),
            created_url=Concat(
                V(settings.CURRENT_SITE), V(TRANSPORTATION_REQUEST_DETAILS_URL), 'uuid', output_field=CharField()
            )
        ).annotate(
            order_total_amount=F('price_amount') - F('disc_amount')
        )

        total_data: dict = query.aggregate(
            total_amount=Sum('order_total_amount'),
            total_route_length=Sum('route_length_km'),
            total_car_amount=Sum('final_car_amount'),
            total_porters_amount=Sum('final_porters_amount'),
            final_total_amount=Sum(F('final_car_amount') + F('final_porters_amount'))
        )

        return query.values(*fields), total_data

    def get_person_statistics(self, user: 'DjangoUser', is_employee: bool = False) -> List[dict]:
        """
        Get person/employee statistics
        Get statistics for each month of the previous six months

        :param user: auth.User instance
        :param is_employee: boolean (if need to get statistics for Employee)
        :return: list of dicts
        """
        queryset: QuerySet
        if not is_employee:
            queryset = self.filter(created_by_id=user.id)
        else:
            queryset = self.filter(assignee_id=user.id).exclude(status=RequestStatuses.cancelled.value)

        return self._get_statistics(queryset)

    @staticmethod
    def _get_statistics(queryset: QuerySet) -> List[dict]:
        """
        Get Organisation's statistics
        :return: list of dicts
        """
        from django.utils import dateformat
        current_date = date.today().replace(day=1)
        previous_date = current_date

        months: List[dict] = []

        for i in range(6):
            if i:
                previous_date: date = (previous_date - timedelta(days=1)).replace(day=1)

            months.append({'name': dateformat.format(previous_date, 'F'), 'index': previous_date.month})

        queryset = queryset.filter(date__gte=previous_date) \
            .annotate(orders_count=Count('id', distinct=True)) \
            .values('orders_count') \
            .annotate(month=DatePart('month', 'date'), year=DatePart('year', 'date')) \
            .order_by('year', 'month')

        queryset.query.group_by = ['year', 'month']

        result: List[dict] = []

        for month in reversed(months):
            stat = next((s for s in queryset if int(s['month']) == month['index']), {})

            result.append({
                'month': month['name'],
                'orders_count': stat.get('orders_count', 0)
            })

        return result

    def get_organisation_statistics(self, organisation_id: int) -> List[dict]:
        """
        Get Organisation's statistics
        :param organisation_id: int (Organisation.id)
        :return: list of dicts (statistics)
        """
        return self._get_statistics(self.filter(organisation_id=organisation_id))


class TransportationRequest(UUIDModel, TimeStampedModel):
    """ TransportationRequest """
    class Meta:
        """ Meta class """
        db_table = 'transportation_request'
        verbose_name = _('заявка на транспортировку')
        verbose_name_plural = _('Заявки на транспортировку')

    assignee_id: int
    discount_id: int

    text = models.TextField(verbose_name=_('текст'), )
    phone = models.CharField(max_length=255, verbose_name=_('номер телефона'))
    email = models.EmailField(verbose_name='email', null=True, blank=True, default=None)
    status = models.CharField(choices=REQUEST_STATUSES, verbose_name=_('статус'),
                              default='new', max_length=20, blank=True)
    status_changed = MonitorField(verbose_name=_('изменение статуса'),
                                  monitor='status', blank=True, default=None, null=True)
    date = models.DateField(verbose_name=_('заказать на дату'), null=False, blank=False, default=date.today)
    time = models.TimeField(verbose_name=_('указать время'), null=True, blank=True, default=None)
    assignee = models.ForeignKey(settings.AUTH_USER_MODEL, verbose_name=_('назначен на'),
                                 null=True, blank=True, default=None, on_delete=models.SET_NULL)
    created_by = models.ForeignKey(settings.AUTH_USER_MODEL, verbose_name=_('создан'),
                                   null=True, blank=True, default=None, related_name='tr_requests',
                                   on_delete=models.SET_NULL)
    accepted_by_customer = models.BooleanField(verbose_name=_('одобрен клиентом'), default=False)
    route_length = models.FloatField(verbose_name=_('расстояние (в метрах)'), default=None, null=True, blank=True)
    is_need_porters = models.BooleanField(verbose_name=_('нужны грузчики'), default=False)
    porters_number = models.IntegerField(verbose_name=_('количество грузчиков'), default=2,
                                         choices=PORTERS_NUMBERS, blank=True, null=True)
    transportation_type = models.ForeignKey('transportation.TransportationType', verbose_name=_('тип транспортировки'),
                                            blank=True, null=True, default=None, on_delete=models.SET_NULL)
    is_hourly_rate = models.BooleanField(_('почасавая оплата'), default=False)
    is_cashless_payment = models.BooleanField(_('безналичный расчет'), default=False)

    amount = models.FloatField(verbose_name=_('окончательная цена заказа'), default=None, blank=True, null=True)
    discount_amount_min = models.FloatField(verbose_name=_('минимальная сумма скидки по заказу'),
                                            default=None, blank=True, null=True)
    discount_amount_max = models.FloatField(verbose_name=_('максимальная сумма скидки по заказу'),
                                            default=None, blank=True, null=True)
    discount_amount = models.FloatField(verbose_name=_('сумма скидки по заказу'),
                                        default=None, blank=True, null=True)

    car_amount = models.FloatField(verbose_name=_('цена автомобиля'),
                                   default=None, blank=True, null=True)
    porters_amount = models.FloatField(verbose_name=_('цена грузчиков'),
                                       default=None, blank=True, null=True)

    car_amount_max = models.FloatField(verbose_name=_('максимальная цена автомобиля'),
                                       default=None, blank=True, null=True)
    porters_amount_max = models.FloatField(verbose_name=_('максимальная цена грузчиков'),
                                           default=None, blank=True, null=True)

    final_car_amount = models.FloatField(verbose_name=_('окончательная цена автомобиля'),
                                         default=0, blank=True, null=True)
    final_porters_amount = models.FloatField(verbose_name=_('окончательная цена грузчиков'),
                                             default=0, blank=True, null=True)
    porters_payment = models.FloatField(verbose_name=_('оплата грузчикам'),
                                        default=0, blank=True, null=True)

    employee_comment = models.TextField(verbose_name=_('комментарий сотрудника'),
                                        default=None, blank=True, null=True)
    user_remote_ip = models.CharField(verbose_name=_('IP пользователя'), max_length=255,
                                      default=None, blank=True, null=True)
    action = models.ForeignKey('discounts.Action', verbose_name=_('по акции'),
                               null=True, blank=True, default=None, related_name='tr_requests',
                               on_delete=models.SET_NULL)
    discount = models.ForeignKey(
        'discounts.Discount',
        verbose_name=_('по акции'),
        null=True, blank=True, default=None,
        related_name='tr_requests',
        on_delete=models.SET_NULL
    )
    organisation = models.ForeignKey(
        'organisations.Organisation',
        verbose_name=_('организация'),
        null=True, blank=True, default=None, on_delete=models.SET_NULL
    )

    objects = TransportationRequestManager()

    def __str__(self):
        return 'Заказ №%s, на дату %s' % (self.pk, self.date)

    @property
    def admin_url(self):
        """
        URL in admin site
        :return:
        """
        return reverse('admin:transportation_transportationrequest_change', args=(self.pk,))

    @property
    def min_amount(self):
        """
        Minimal amount of the order
        :return:
        """

        return (self.car_amount or 0) + (self.porters_amount or 0)

    @property
    def max_amount(self):
        """
        Minimal amount of the order
        :return:
        """
        return (self.car_amount_max or self.car_amount or 0) + (self.porters_amount_max or self.porters_amount or 0)
