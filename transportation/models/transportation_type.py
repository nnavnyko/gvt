"""
TransportationType model + Manager
"""
from django.db import models
from django.utils.translation import ugettext_lazy as _
from django_extensions.db.models import TimeStampedModel

from common.managers.base_manager import BaseManager
from common.models import UUIDModel


class TransportationTypeManager(BaseManager):
    """
    TransportationType DB Manager
    """
    pass


class TransportationType(UUIDModel, TimeStampedModel):
    """
        Transportation Type
    """
    class Meta:
        """ Meta class """
        db_table = 'transportation_type'
        verbose_name = _(u'тип транспортировки')
        verbose_name_plural = _(u'Типы транспортировки')

    name = models.CharField(verbose_name=_(u'название'), max_length=255, default='Transportation Type')
    description = models.TextField(verbose_name=_(u'описание'), blank=True, null=True, default=None)
    sort_order = models.PositiveIntegerField(verbose_name=_(u'порядок сортировки'), default=0)
    is_active = models.BooleanField(verbose_name=_(u'активен'), default=True)

    objects = TransportationTypeManager()

    def __str__(self):
        """
        String representation
        :return:
        """
        return self.name
