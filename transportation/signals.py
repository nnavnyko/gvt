# encoding: utf-8
"""
Transportation signals
"""
from typing import TYPE_CHECKING

from django.contrib.auth import get_user_model
from django.template.loader import render_to_string
import django.dispatch
from django.utils.translation import ugettext_lazy as _

from common.email_notifications.email_sender import EmailSender

if TYPE_CHECKING:
    from transportation.models.transportation_request import TransportationRequest

request_created_signal = django.dispatch.Signal(providing_args=['instance'])


def request_created(sender, instance: 'TransportationRequest', **kwargs):
    """
    Send emails to admins after request created
    :param sender: class object
    :param instance: instance (TransportationRequest)
    :param kwargs: key-word arguments
    :return: None
    """
    phone = instance.phone
    email = instance.email
    context = {'req': instance, 'phone': phone, 'email': email}

    recipients_admins = get_user_model().objects.filter(is_superuser=True).values_list('email', flat=True)
    subject_admins = str(_('[Грузи в ТАКСИ] Была оформлена заявка №%s')) % instance.id
    message_admins = render_to_string('emails/request_created_admin.html', context)

    EmailSender.send_message(
        subject_admins,
        message_admins,
        recipients_admins
    )

    # Send email if user has fill email field
    if email:
        recipients = [email]
        subject = str(_(u'[Грузи в ТАКСИ] Оформлена заявка на перевоз груза'))
        message = render_to_string('emails/request_created_customer.html', context)

        EmailSender.send_message(
            subject,
            message,
            recipients,
            onerror=lambda: EmailSender.send_message(
                'Error',
                'Error occurred when tried to deliver message to client',
                recipients_admins
            )
        )


request_created_signal.connect(receiver=request_created)
