# encoding: utf-8
"""
    Transportation Request Tests
"""
from django.contrib.auth import get_user_model
from django.test import TestCase, Client
import time

from django.urls import reverse

from accounts.models.employee_card import EmployeeCard
from accounts.models.user_card import UserCard
from accounts.enums import EmployeeRoles
from transportation.models.transportation_request import TransportationRequest
from transportation.enums import RequestStatuses
from transportation.forms.forms import TransportationRequestForm
from transportation.utils.transportation_request_util import TransportationRequestUtil


SIMPLE_PASSWORD = '12345678*Aa'
REQUEST_ERROR = 400
PERMISSION_DENIED_STATUS = 403
SUCCESS_STATUS = 200
REDIRECT_STATUS = 302   # Used for Log-in requests


class TransportationRequestTestCase(TestCase):
    """
        Transportation Request Test Case
    """

    request_data = {
        'text': 'Need to transport closet from A point to B point then to C point',
        'phone': '333333333',
        'date': '2017-07-12',
        'points': [
            {'floor': 2, 'longitude': '5.55', 'latitude': '6.66', 'address': 'Mozyr, B.Unosti 18, 21'},
            {'floor': 5, 'longitude': '5.56', 'latitude': '7.77', 'address': 'Mozyr, Sovetskaya, 14'},
            {'floor': 6, 'longitude': '5.58', 'latitude': '7.74', 'address': 'Mozyr, Sovetskaya, 51'}
        ],
        'g-recaptcha-response': 'GOOGLE_RECAPTCHA_RESPONSE'
    }

    def test_no_required_data(self):
        """
        Test 'text' and 'phone' parameters missed
        :return: None
        """
        client = Client()
        response = client.post(reverse('transportation:api:add_transportation_request'), {
            'g-recaptcha-response': 'GOOGLE_RECAPTCHA_RESPONSE'
        })
        time.sleep(3)
        # Response status is 400, error
        self.assertEqual(response.status_code, REQUEST_ERROR, 'Response Code is 400, Bad request')
        # 'phone' and 'text' are required params and missing
        self.assertIn('text', response.data['errors'])

        response = client.post(reverse('transportation:api:add_transportation_request'), {
            'g-recaptcha-response': 'GOOGLE_RECAPTCHA_RESPONSE',
            'text': 'Some text'
        })
        time.sleep(3)
        # Response status is 400, error
        self.assertEqual(response.status_code, REQUEST_ERROR, 'Response Code is 400, Bad request')
        # 'phone' and 'text' are required params and missing
        self.assertIn('phone', response.data['errors'])

    # Google recaptcha always returns valid response in test mode
    # def test_create_via_site(self):
    #     """
    #     Test Google Recaptcha missing or incorrect
    #     :return: None
    #     """
    #     client = Client()
    #
    #     data = self.request_data.copy()
    #     data['g-recaptcha-response'] = 'asdasdasd'
    #
    #     response = client.post(reverse('transportation:api:add_transportation_request'), data, content_type='application/json')
    #     time.sleep(3)
    #     # Response status is 400, error
    #     self.assertEqual(response.status_code, REQUEST_ERROR)

    def test_create_3_points_in_request(self):
        """
        Test to create Transportation Request having 3 transportation points
        :return: None
        """

        form = TransportationRequestForm(self.request_data)
        is_valid = form.is_valid()
        # Test Form is valid
        self.assertTrue(is_valid, 'Form is valid')

        tr_request = form.save()
        TransportationRequestUtil.append_request_points(tr_request, self.request_data.get('points', []))

        tr_request = TransportationRequest.objects.first()

        self.assertIsNotNone(tr_request)
        # 3 points created
        self.assertEqual(tr_request.points.count(), 3, '3 Transportation Points created')
        # The transportation request status is 'new'
        self.assertEqual(tr_request.status, 'new', '3 Transportation Points created')


# class TestTransportationRequestsPermissions(TestCase):  # TODO: remove comment after create Customer Application
class TestTransportationRequestsPermissions(object):
    """
        TestTransportationRequestsPermissions
    """

    def setUp(self):
        """
        Set Up Test Case
        Add users and Transportation Requests
        :return:
        """
        user_model = get_user_model()

        self.manager = user_model(username='manager_user', first_name='Alex',
                                  last_name='Bolduing', email='admin_user_aa@yopmail.com')
        self.manager.set_password(SIMPLE_PASSWORD)
        self.manager.save()
        user_card = UserCard(phone_number=444444444)
        employee_card = UserCard(role=EmployeeRoles.manager.value)

        user_card.user_id = self.manager.id
        employee_card.user_id = self.manager.id

        self.manager.user_card = user_card
        self.manager.employee_card = employee_card

        user_card.save()
        employee_card.save()

        self.simple_user = user_model(username='simple_user', first_name='Alex',
                                      last_name='Bolduing', email='gvtmozyr@gmail.com')
        self.simple_user.set_password(SIMPLE_PASSWORD)
        self.simple_user.save()

        user_card = UserCard(phone_number=555555555)
        user_card.user_id = self.simple_user.id

        self.simple_user.user_card = user_card
        user_card.save()

        self.simple_user2 = user_model(username='simple_user2', first_name='Will',
                                       last_name='Smith', email='test@example.com')
        self.simple_user2.set_password(SIMPLE_PASSWORD)
        self.simple_user2.save()

        user_card = UserCard(phone_number=666666666)
        user_card.user_id = self.simple_user2.id

        self.simple_user2.user_card = user_card
        user_card.save()

    def test_assign_transportation_request(self):
        """
        Test Change status permission
        The simple user has no permissions
        :return:
        """
        client = Client()
        response = client.post(reverse('accounts:auth_login'), data={'username': self.simple_user.username, 'password': SIMPLE_PASSWORD})

        # Login success
        self.assertEqual(response.status_code, REDIRECT_STATUS)

        response = client.post(reverse('transportation:api:create_transportation_request_api'),
                               data={'phone': self.simple_user.user_card.phone_number, 'text': 'Comment'})
        # Create Transportation Request success
        self.assertEqual(response.status_code, SUCCESS_STATUS)

        tr_request = TransportationRequest.objects.filter(created_by_id=self.simple_user.id).first()
        self.assertIsNotNone(tr_request)

        response = client.post(reverse('transportation:api:change_request_status', args=(tr_request.uuid,)),
                               data={'type': 'accept'})
        # The Simple User does not have necessary permissions
        self.assertEqual(response.status_code, PERMISSION_DENIED_STATUS)

        response = client.post(reverse('accounts:auth_login'), data={'username': self.manager.username,
                                                            'password': SIMPLE_PASSWORD})
        # Login success
        self.assertEqual(response.status_code, REDIRECT_STATUS)

        response = client.post(reverse('transportation:api:change_request_status', args=(tr_request.uuid,)),
                               data={'type': 'assign'})
        # The Manager User has permissions
        self.assertEqual(response.status_code, SUCCESS_STATUS)

        # After accept Transportation Request the self.manager is assigned to Transportation Request
        tr_request = TransportationRequest.objects.filter(created_by_id=self.simple_user.id).first()
        self.assertEqual(tr_request.assignee_id, self.manager.id)
        self.assertEqual(tr_request.status, RequestStatuses.accepted.value)

    def test_assign_to_user_transportation_request(self):
        """
        Test Assign To User Permissions
        :return:
        """
        client = Client()
        response = client.post(reverse('accounts:auth_login'),
                               data={'username': self.simple_user.username, 'password': SIMPLE_PASSWORD})

        # Login success
        self.assertEqual(response.status_code, REDIRECT_STATUS)
        response = client.post(reverse('transportation:api:create_transportation_request_api'),
                               data={'phone': self.simple_user.user_card.phone_number, 'text': 'Comment'})
        # Create Transportation Request success
        self.assertEqual(response.status_code, SUCCESS_STATUS)

        tr_request = TransportationRequest.objects.filter(created_by_id=self.simple_user.id).first()
        self.assertIsNotNone(tr_request)

        response = client.post(reverse('transportation:api:assign_request', args=(tr_request.uuid,)),
                               data={'assignee_id': self.manager.id})

        # The Simple User does not have necessary permissions
        self.assertEqual(response.status_code, PERMISSION_DENIED_STATUS)

        response = client.post(reverse('accounts:auth_login'), data={'username': self.manager.username,
                                                            'password': SIMPLE_PASSWORD})

        # Login success
        self.assertEqual(response.status_code, REDIRECT_STATUS)

        response = client.post(reverse('transportation:api:assign_request', args=(tr_request.uuid,)),
                               data={'assignee_id': self.manager.id})
        # The Manager User has permissions
        self.assertEqual(response.status_code, SUCCESS_STATUS)

        # After accept Transportation Request the self.manager is assigned to Transportation Request
        tr_request = TransportationRequest.objects.filter(created_by_id=self.simple_user.id).first()
        self.assertEqual(tr_request.assignee_id, self.manager.id)
        self.assertEqual(tr_request.status, RequestStatuses.accepted.value)

    def test_cancel_request(self):
        """
        Test cancel Transportation Request Permissions
        Only Transportation Request Creator or Manager user can cancel the Transportation Reuqest
        :return:
        """
        client = Client()
        response = client.post(reverse('accounts:auth_login'),
                               data={'username': self.simple_user.username, 'password': SIMPLE_PASSWORD})

        # Login success
        self.assertEqual(response.status_code, REDIRECT_STATUS)
        response = client.post(reverse('transportation:api:create_transportation_request_api'),
                               data={'phone': self.simple_user.user_card.phone_number, 'text': 'Comment'})
        # Create Transportation Request success
        self.assertEqual(response.status_code, SUCCESS_STATUS)

        tr_request = TransportationRequest.objects.filter(created_by_id=self.simple_user.id).first()
        self.assertIsNotNone(tr_request)

        # Login another user and try to Cancel Request
        response = client.post(reverse('accounts:auth_login'), data={'username': self.simple_user2.username,
                                                            'password': SIMPLE_PASSWORD})

        # Login success
        self.assertEqual(response.status_code, REDIRECT_STATUS)

        response = client.post(reverse('transportation:api:change_request_status', args=(tr_request.uuid,)),
                               data={'type': 'cancel'})

        # The Simple User2 does not have necessary permissions
        self.assertEqual(response.status_code, PERMISSION_DENIED_STATUS)

        # Log-In as request Owner and try to cancel request
        response = client.post(reverse('accounts:auth_login'),
                               data={'username': self.simple_user.username, 'password': SIMPLE_PASSWORD})

        # Login success
        self.assertEqual(response.status_code, REDIRECT_STATUS)
        response = client.post(reverse('transportation:api:change_request_status', args=(tr_request.uuid,)),
                               data={'type': 'cancel'})
        self.assertEqual(response.status_code, SUCCESS_STATUS)

        tr_request = TransportationRequest.objects.filter(created_by_id=self.simple_user.id).first()
        # Test TransportationRequest status is cancelled
        self.assertEqual(tr_request.status, RequestStatuses.cancelled.value)

    def test_finish_transportation_request(self):
        """
        Test Finish Transportation Request
        :return: None
        """
        client = Client()
        response = client.post(reverse('accounts:auth_login'),
                               data={'username': self.simple_user.username, 'password': SIMPLE_PASSWORD})

        # Login success
        self.assertEqual(response.status_code, REDIRECT_STATUS)
        response = client.post(reverse('transportation:api:create_transportation_request_api'),
                               data={'phone': self.simple_user.user_card.phone_number, 'text': 'Comment'})
        # Create Transportation Request success
        self.assertEqual(response.status_code, SUCCESS_STATUS)

        tr_request = TransportationRequest.objects.filter(created_by_id=self.simple_user.id).first()
        self.assertIsNotNone(tr_request)

        response = client.post(reverse('accounts:auth_login'), data={'username': self.manager.username,
                                                            'password': SIMPLE_PASSWORD})

        # Login success
        self.assertEqual(response.status_code, REDIRECT_STATUS)

        response = client.post(reverse('transportation:api:assign_request', args=(tr_request.uuid,)),
                               data={'assignee_id': self.manager.id})
        # The Manager User has permissions to assign Transportation Request
        self.assertEqual(response.status_code, SUCCESS_STATUS)

        # Log-In as simple user and try to finish Transportation Request and change final amount
        response = client.post(reverse('accounts:auth_login'),
                               data={'username': self.simple_user.username, 'password': SIMPLE_PASSWORD})

        # Login success
        self.assertEqual(response.status_code, REDIRECT_STATUS)

        response = client.post(reverse('transportation:api:change_request_status', args=(tr_request.uuid,)),
                               data={'type': 'finish', 'amount': 50})
        # The Simple User Cannot finish Transportation Request
        self.assertEqual(response.status_code, PERMISSION_DENIED_STATUS)

        # Log-In as simple user2 and try to finish Transportation Request and change final amount
        response = client.post(reverse('accounts:auth_login'),
                               data={'username': self.simple_user2.username, 'password': SIMPLE_PASSWORD})

        # Login success
        self.assertEqual(response.status_code, REDIRECT_STATUS)

        response = client.post(reverse('transportation:api:change_request_status', args=(tr_request.uuid,)),
                               data={'type': 'finish', 'amount': 100})
        # The Simple User2 Cannot finish Transportation Request
        self.assertEqual(response.status_code, PERMISSION_DENIED_STATUS)

        # Log-In as simple user2 and try to finish Transportation Request and change final amount
        response = client.post(reverse('accounts:auth_login'),
                               data={'username': self.manager.username, 'password': SIMPLE_PASSWORD})

        # Login success
        self.assertEqual(response.status_code, REDIRECT_STATUS)

        final_amount = 300
        response = client.post(reverse('transportation:api:change_request_status', args=(tr_request.uuid,)),
                               data={'type': 'finish', 'amount': final_amount})
        # The Simple User2 Cannot finish Transportation Request
        self.assertEqual(response.status_code, SUCCESS_STATUS)

        tr_request = TransportationRequest.objects.filter(created_by_id=self.simple_user.id).first()
        # Test Final amount and Transportation Request status
        self.assertEqual(tr_request.amount, final_amount)
        self.assertEqual(tr_request.status, RequestStatuses.delivered.value)


class TestTransportationReports(TestCase):
    """
    Test Transportation Reports
    """

    def setUp(self):
        """
        Initialize TestCase
        :return:
        """
        self.client = Client()
        user_model = get_user_model()

        self.manager = user_model(username='manager_user', first_name='Alex',
                                  last_name='Bolduing', email='admin_user_aa@yopmail.com')
        self.manager.set_password(SIMPLE_PASSWORD)
        self.manager.save()
        user_card = UserCard(phone_number=444444444)
        employee_card = EmployeeCard(role=EmployeeRoles.manager.value)

        user_card.user_id = self.manager.id
        employee_card.user_id = self.manager.id

        self.manager.user_card = user_card
        self.manager.employee_card = employee_card

        user_card.save()
        employee_card.save()

        self.simple_user = user_model(username='simple_user', first_name='Alex',
                                      last_name='Bolduing', email='gvtmozyr@gmail.com')
        self.simple_user.set_password(SIMPLE_PASSWORD)
        self.simple_user.save()

        user_card = UserCard(phone_number=555555555)
        user_card.user_id = self.simple_user.id

        self.simple_user.user_card = user_card
        user_card.save()

        self.driver = user_model(username='driver', first_name='Alex',
                                 last_name='Bolduing', email='driver@gmail.com')
        self.driver.set_password(SIMPLE_PASSWORD)
        self.driver.save()

        user_card = UserCard(phone_number=555555555)
        user_card.user_id = self.driver.id

        self.driver.user_card = user_card
        user_card.save()

        self.transportation_requests_count = 3
        for i in range(self.transportation_requests_count):
            form = TransportationRequestForm({'phone': 666666666, 'text': 'TEST',
                                              'g-recaptcha-response': 'recaptcha_response'})
            is_valid = form.is_valid()
            # Test Form is valid
            self.assertTrue(is_valid, 'Form is valid')

            form.save()

    def login_user(self, user):
        """
        Login user
        :param user:
        :return:
        """
        response = self.client.post(reverse('accounts:auth_login'),
                                    data={'username': user.username, 'password': SIMPLE_PASSWORD})

        # Login success
        self.assertEqual(response.status_code, REDIRECT_STATUS)

    def test_permissions_to_get_report(self):
        """
        Test permissions
        Only managers can get report
        :return:
        """

        transportation_requests = TransportationRequest.objects.all()

        self.assertEqual(len(transportation_requests), self.transportation_requests_count)

        self.login_user(self.simple_user)
        response = self.client.get(reverse('transportation:reports:download_transportation_general_report'))

        self.assertEqual(response.status_code, PERMISSION_DENIED_STATUS)

        self.login_user(self.driver)
        response = self.client.get(reverse('transportation:reports:download_transportation_general_report'))

        self.assertEqual(response.status_code, PERMISSION_DENIED_STATUS)

        self.login_user(self.manager)
        response = self.client.get(reverse('transportation:reports:download_transportation_general_report'))

        self.assertEqual(response.status_code, SUCCESS_STATUS)

    def test_generated_content(self):
        """
        Test Generated Content
        :return:
        """
        total_price = 100

        tr_request = TransportationRequest.objects.first()
        TransportationRequestUtil.finish_transportation_request(tr_request, self.manager, {'final_car_amount': total_price})

        rows, total_data = TransportationRequest.objects.get_general_report_data()

        self.assertEqual(len(rows), self.transportation_requests_count)

        self.assertEqual(total_price, total_data['total_amount'])
