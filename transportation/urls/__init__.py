# encoding: utf-8
"""
Transportation Urls
"""
from django.conf.urls import url, include
from transportation.api import urls as api_urls
from transportation.urls import reports as reports_urls


app_name = 'transportation'

urlpatterns = [
    url(r'^api/', include(api_urls, namespace='api')),
    url(r'^reports/', include(reports_urls, namespace='reports')),
]
