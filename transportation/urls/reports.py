# encoding: utf-8
"""
Transportation Reports Urls
"""
from django.conf.urls import url
from transportation.views.reports import download_transportation_general_report, \
    download_transportation_employee_general_report, download_transportation_organisation_general_report

app_name = 'transportation_reports'

urlpatterns = [
    url(r'^general/download$', download_transportation_general_report,
        name='download_transportation_general_report'),
    url(r'^general/employee/download$', download_transportation_employee_general_report,
        name='download_transportation_general_report'),
    url(r'^general/organisation/(?P<org_uuid>[a-z0-9\-]+)/download$',
        download_transportation_organisation_general_report,
        name='download_organisation_general_report'),
]
