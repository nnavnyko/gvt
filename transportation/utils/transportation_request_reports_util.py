# encoding: utf-8
"""
Transportation Requests Reports Utility
"""
from typing import List

from django.http import HttpResponse
from django.utils.translation import ugettext_lazy as _

from common.file_builders.enums import ValuesTypes
from common.file_builders.xls.xls_builder import XlsBuilder
from common.file_builders.xls.xls_builder_formatter import XlsBuilderFormatter
from transportation.enums import REQUEST_STATUSES_DICT

DATE_FIELD_KEY = 'date'
UUID_FIELD_KEY = 'uuid'

TOTAL_VALUE_LABEL_COLUMN = 0
TOTAL_VALUE_COLUMN = 1


class TransportationRequestsReportsUtil:
    """
    Transportation Request Reports Utility class
    """

    @classmethod
    def _generate_general_report_response(
            cls,
            general_fields: List[tuple],
            rows: List[dict],
            total_data: dict,
            filename: str = None,
            add_summary: bool = True
    ) -> HttpResponse:
        """
        Generate Report HTTP Response
        :param general_fields: list of fields (Excel columns)
        :param rows: list of dictionaries
        :param total_data: dictionary of total amount and total route length
        :param filename: string or None
        :param add_summary: if need to add summary
        :return: HttpResponse instance
        """
        columns = [XlsBuilderFormatter.get_column(*field) for field in general_fields]

        xls_builder = XlsBuilder(filename, columns)

        rows = cls._prepare_general_report_data(rows)
        orders_length = len(rows)

        if orders_length:
            row = 1
            for row_dict in rows:
                xls_builder.write_row(row, row_dict)
                row += 1

            if add_summary:
                # Write Total Amount and Total Route lengnt
                # Make indent 2 rows for each total value
                cls._add_summary(xls_builder, orders_length, total_data)

        return xls_builder.make_http_response()

    @classmethod
    def _add_summary(cls, xls_builder: XlsBuilder, orders_length: int, total_data: dict):
        """
        Add Summary to Report Excel file
        :param xls_builder: XlsBuilder
        :param orders_length: count of orders
        :param total_data: dict (summary data)
        :return: None
        """
        # Write Total Amount and Total Route lengnt
        # Make indent 2 rows for each total value
        total_amount_row = orders_length + 3
        total_route_length_row = orders_length + 5
        total_car_amount_length_row = orders_length + 7
        total_porters_amount_length_row = orders_length + 9
        final_total_amount_length_row = orders_length + 13

        total_data_fields: dict = {
            'total_amount': {'label': _('ОБЩАЯ СТОИМОСТЬ ПЕРЕВОЗОК'),
                             'value_type': ValuesTypes.MONEY_TYPE,
                             'row': total_amount_row,
                             'cell_format': xls_builder.bold_format},
            'total_route_length': {'label': _('Общий километраж'),
                                   'value_type': ValuesTypes.NUMBER_TYPE,
                                   'row': total_route_length_row,
                                   'cell_format': xls_builder.bold_format},
            'total_car_amount': {'label': _('Общая стоимость авто'),
                                 'value_type': ValuesTypes.NUMBER_TYPE,
                                 'row': total_car_amount_length_row,
                                 'cell_format': xls_builder.bold_format},
            'total_porters_amount': {'label': _('Общая стоимость грузчиков'),
                                     'value_type': ValuesTypes.NUMBER_TYPE,
                                     'row': total_porters_amount_length_row,
                                     'cell_format': xls_builder.bold_format},
            'final_total_amount': {'label': _('ОБЩАЯ СУММА'),
                                   'value_type': ValuesTypes.NUMBER_TYPE,
                                   'row': final_total_amount_length_row,
                                   'cell_format': xls_builder.bold_format},
        }
        for key, field in total_data_fields.items():
            # Write Total field Label
            xls_builder.write(field['row'], TOTAL_VALUE_LABEL_COLUMN, str(field['label']),
                              cell_format=field['cell_format'], value_type=ValuesTypes.STRING_TYPE)
            # Write Total field Value
            xls_builder.write(field['row'], TOTAL_VALUE_COLUMN, total_data[key],
                              cell_format=field['cell_format'], value_type=field['value_type'])

    @classmethod
    def generate_general_report_response(
            cls,
            rows: List[str],
            total_data: dict,
            filename: str = None
    ) -> HttpResponse:
        """
        Generate Report HTTP Response
        :param rows: list of dictionaries
        :param total_data: dictionary of total amount and total route length
        :param filename: string or None
        :return: HttpResponse instance
        """
        # Need to follow specific order to have the same order of columns in file
        general_fields: List[tuple] = [
            (str(_('Дата Создания')), 'created', ValuesTypes.URL_TYPE),
            (str(_('Заказано На Дату')), 'date', ValuesTypes.DATE_TYPE),
            (str(_('Номер Телефона')), 'phone_number', ValuesTypes.STRING_TYPE),
            (str(_('Статус')), 'status', ValuesTypes.STRING_TYPE),
            (str(_('Стоимость Авто (BYN)')), 'final_car_amount', ValuesTypes.NUMBER_TYPE),
            (str(_('Стоимость Грузчиков (BYN)')), 'final_porters_amount', ValuesTypes.NUMBER_TYPE),
            (str(_('Стоимость (BYN)')), 'price_amount', ValuesTypes.NUMBER_TYPE),
            (str(_('Скидка (BYN)')), 'disc_amount', ValuesTypes.NUMBER_TYPE),
            (str(_('Сумма (BYN)')), 'order_total_amount', ValuesTypes.NUMBER_TYPE),
            (str(_('Сотрудник')), 'assignee_name', ValuesTypes.STRING_TYPE),
            (str(_('Длина Маршрута (км)')), 'route_length_km', ValuesTypes.NUMBER_TYPE)
        ]

        return cls._generate_general_report_response(general_fields, rows, total_data, filename)

    @classmethod
    def generate_general_organisation_report_response(
            cls,
            rows: List[dict],
            total_data: dict,
            filename: str = None
    ) -> HttpResponse:
        """
        Generate Report HTTP Response
        :param rows: list of dictionaries
        :param total_data: dictionary of total amount and total route length
        :param filename: string or None
        :return: HttpResponse instance
        """
        # Need to follow specific order to have the same order of columns in file
        general_fields: List[tuple] = [
            (str(_('Дата Создания')), 'created', ValuesTypes.URL_TYPE),
            (str(_('Заказано На Дату')), 'date', ValuesTypes.DATE_TYPE),
            (str(_('Статус')), 'status', ValuesTypes.STRING_TYPE),
            (str(_('Стоимость Авто (BYN)')), 'final_car_amount', ValuesTypes.NUMBER_TYPE),
            (str(_('Стоимость Грузчиков (BYN)')), 'final_porters_amount', ValuesTypes.NUMBER_TYPE),
            (str(_('Стоимость (BYN)')), 'price_amount', ValuesTypes.NUMBER_TYPE),
            (str(_('Скидка (BYN)')), 'disc_amount', ValuesTypes.NUMBER_TYPE),
            (str(_('Сумма (BYN)')), 'order_total_amount', ValuesTypes.NUMBER_TYPE),
            (str(_('Длина Маршрута (км)')), 'route_length_km', ValuesTypes.NUMBER_TYPE)
        ]

        return cls._generate_general_report_response(general_fields, rows, total_data, filename, add_summary=False)

    @classmethod
    def _prepare_general_report_data(cls, rows: List[dict]) -> List[dict]:
        """
        Prepare General Report Data
        :param rows: list of dictionaries
        :return: rows (formatted list of dicts)
        """
        for row in rows:
            row['status'] = str(REQUEST_STATUSES_DICT.get(row['status'], row['status']))

        return rows
