# encoding: utf-8
"""
    Transportation Utils
"""
from typing import List, Dict, Callable, Union

from django.core.exceptions import SuspiciousOperation

from accounts.models.user import DjangoUser
from accounts.utils.employees_util import EmployeesUtil
from discounts.utils.discount_calculator import DiscountCalculator
from gvt.gvt_websockets.websockets_publisher import WebSocketsPublisher
from transportation.enums import RequestStatuses, ActionTypes
from transportation.models.transportation_history import TransportationHistory
from transportation.models.transportation_request import TransportationRequest

POINT_LATITUDE_KEY = 'point{}_lat'
POINT_LONGITUDE_KEY = 'point{}_lng'
POINT_FLOOR_KEY = 'point{}_floor'
POINT_KEY = 'point{}'

START_POINT_LATITUDE_KEY = 'start_lat'
START_POINT_LONGITUDE_KEY = 'start_lng'
START_FLOOR_KEY = 'start_floor'
START_KEY = 'start'

NEXT_POINTS_COUNTER_KEY = 'next-points-counter'
COMPLETED_STATUSES = ['cancelled', 'delivered']

TRANSPORTATION_APP_WS_PREFIX = 'transportation_requests_app'
TRANSPORTATION_WS_PREFIX = 'transportation_'
TRANSPORTATION_REQUESTS_WS_PREFIX = ''.join([TRANSPORTATION_WS_PREFIX, 'requests_'])


class TransportationRequestUtil:
    """
        TransportationRequestUtil
        Manage Transportation Requests
    """

    status_map: dict = {
        RequestStatuses.new.value: [RequestStatuses.accepted.value, RequestStatuses.cancelled.value],
        RequestStatuses.accepted.value: [RequestStatuses.on_the_road.value, RequestStatuses.in_progress.value,
                                         RequestStatuses.cancelled.value, RequestStatuses.accepted.value],
        RequestStatuses.on_the_road.value: [RequestStatuses.in_progress.value, RequestStatuses.cancelled.value],
        RequestStatuses.in_progress.value: [RequestStatuses.delivered.value, RequestStatuses.cancelled.value],
        RequestStatuses.delivered.value: [RequestStatuses.paid.value],
        RequestStatuses.paid.value: None,
        RequestStatuses.cancelled.value: None
    }

    @classmethod
    def append_request_points(cls, tr_req: TransportationRequest, points: List[dict]) -> TransportationRequest:
        """
        Create Request Points
        Format of points should be:
        points = [
            {
                'address': 'Беларусь, Мозырь, Притыцкого 22',
                'longitude': 3.33333333,
                'latitude': 3.33333333,
                'floor': 2,
                'sort_order': 1
            }
        ]
        :param tr_req: TransportationRequest instance
        :param points: list of dictionaries
        :return: created TransportationRequest instance
        """
        from transportation.forms.forms import TransportationLocationForm

        if points:
            for point in points:
                point['sort_order'] = point.get('sort_order', points.index(point))
                point_form = TransportationLocationForm(point)

                if point_form.is_valid():
                    tr_req.points.add(point_form.save(commit=False), bulk=False)

        return tr_req

    @classmethod
    def _can_change_status(cls, tr_request: TransportationRequest, status: str) -> bool:
        """
        Check if tr_request.status can be switched to status
        :param tr_request: TransportationRequest instance
        :param status: string
        :return: boolean
        """
        return cls.status_map.get(status) is None or status in cls.status_map[tr_request.status]

    @classmethod
    def validate_status(cls, tr_request: TransportationRequest, status: str) -> None:
        """
        Validate status on change
        :param tr_request: TransportationRequest instance
        :param status: string
        :return: None
        :raise: SuspiciousOperation if tr_request.status can be switched to status
        """
        if not cls._can_change_status(tr_request, status):
            raise SuspiciousOperation(u'Заказ не может быть переведен в этот статус')

    @classmethod
    def switch_status(cls, tr_request: TransportationRequest, user: DjangoUser, status: str) -> TransportationRequest:
        """
        Change status. Check if tr_request.status can be changed to status
        :param tr_request: TransportationRequest instance
        :param user: User instance (currently logged-in user)
        :param status: string
        :return: updated TransportationRequest instance
        """
        cls.validate_status(tr_request, status)

        TransportationRequest.objects.update_model(tr_request, status=status)

        TransportationHistory.objects.log_action(
            tr_request,
            ActionTypes.change_status.value,
            user_by_id=user.id,
            comment='Изменен статус заказа'
        )

        return tr_request

    @classmethod
    def assign_transportation_request(
            cls,
            tr_request: TransportationRequest,
            user: DjangoUser,
            data: dict = None
    ) -> TransportationRequest:
        """
        Assign Transportation Request to User and change status to Accepted
        :param tr_request: TransportationRequest instance
        :param user: auth.User instance (current session user)
        :param data: dict (request.data)
        :return: updated TransportationRequest
        """
        if tr_request.status == RequestStatuses.new.value:
            cls.validate_status(tr_request, RequestStatuses.accepted.value)
            tr_request.status = RequestStatuses.accepted.value

        TransportationRequest.objects.update_model(tr_request, assignee=user)

        current_user: DjangoUser
        current_user = data.get('current_user') if data else None
        current_user = current_user or user

        comment: str = 'Заказ был назначен на сотрудника'
        if current_user.id == user.id:
            comment = 'Сотрудник назначил заказ на себя'

        TransportationHistory.objects.log_action(
            tr_request,
            ActionTypes.assigned.value,
            user_by_id=current_user.id,
            user_id=user.id,
            comment=comment
        )

        return tr_request

    @classmethod
    def cancel_transportation_request(
            cls,
            tr_request: TransportationRequest,
            user: DjangoUser,
            *_,
            **__
    ) -> TransportationRequest:
        """
        Cancel Transportation Request
        :param tr_request: TransportationRequest instance
        :param user: auth.User (current session user)
        :return: updated TransportationRequest:
        :raises: ValueError
        """
        cls.validate_status(tr_request, RequestStatuses.cancelled.value)

        TransportationRequest.objects.update_model(
            tr_request,
            **{'status': RequestStatuses.cancelled.value, 'assignee': user}
        )
        comment: str = 'Сотрудник отменил заказ'
        if tr_request.created_by_id == user.id and not EmployeesUtil.is_manager(user):
            comment = 'Клиент отменил заказ'

        TransportationHistory.objects.log_action(
            tr_request,
            ActionTypes.cancelled.value,
            user_by_id=user.id,
            comment=comment
        )

        return tr_request

    @classmethod
    def start_transportation(
            cls,
            tr_request: TransportationRequest,
            user: DjangoUser,
            *_,
            **__
    ) -> TransportationRequest:
        """
        Start Transportation Request
        :param tr_request: TransportationRequest instance
        :param user: auth.User (current session user)
        :return: updated TransportationRequest:
        :raises: ValueError
        """
        cls.validate_status(tr_request, RequestStatuses.in_progress.value)

        TransportationRequest.objects.update_model(
            tr_request,
            **{'status': RequestStatuses.in_progress.value}
        )

        TransportationHistory.objects.log_action(
            tr_request, ActionTypes.started.value, user_by_id=user.id,
            comment='Сотрудник начал заказ. Автомобиль находится в пути')

        return tr_request

    @classmethod
    def transportation_request_paid(
            cls,
            tr_request: TransportationRequest,
            user: DjangoUser,
            *_,
            **__
    ) -> TransportationRequest:
        """
        Transportation Request has been paid
        :param tr_request: TransportationRequest instance
        :param user: auth.User (current session user)
        :return: updated TransportationRequest:
        :raises: ValueError
        """
        cls.validate_status(tr_request, RequestStatuses.paid.value)

        TransportationRequest.objects.update_model(
            tr_request,
            **{'status': RequestStatuses.paid.value}
        )

        TransportationHistory.objects.log_action(
            tr_request,
            ActionTypes.paid.value,
            user_by_id=user.id,
            comment='Заказ был оплачен'
        )

        return tr_request

    @classmethod
    def set_on_the_road(
            cls,
            tr_request: TransportationRequest,
            user: DjangoUser,
            *_,
            **__
    ) -> TransportationRequest:
        """
        Cancel Transportation Request
        :param tr_request: TransportationRequest instance
        :param user: auth.User (current session user)
        :param data: dict (request.data)
        :return: updated TransportationRequest:
        :raises: ValueError
        """
        cls.validate_status(tr_request, RequestStatuses.on_the_road.value)

        TransportationRequest.objects.update_model(
            tr_request,
            **{'status': RequestStatuses.on_the_road.value}
        )

        TransportationHistory.objects.log_action(
            tr_request,
            ActionTypes.on_the_road.value,
            user_by_id=user.id,
            comment='Сотрудник выехал к пункту отправки'
        )

        return tr_request

    @classmethod
    def finish_transportation_request(
            cls,
            tr_request: TransportationRequest,
            user: DjangoUser,
            data: dict = None
    ) -> TransportationRequest:
        """
        Finish Transportation Request (set status to Delivered)
        :param tr_request: TransportationRequest instance
        :param user: User instance (current logged-in user)
        :param data: dict (request.data)
        :return: updated TransportationRequest
        """
        data = data or {}

        amount_fields = ['final_car_amount', 'final_porters_amount', 'porters_payment']

        update_data: dict = {}
        for key in amount_fields:
            value = data.get(key) or 0
            value = abs(float(value))

            update_data[key] = value

        update_data.update({
            'status': RequestStatuses.delivered.value,
            'amount': update_data['final_car_amount'] + update_data['final_porters_amount']
        })

        if tr_request.action:
            update_data['discount_amount'] = DiscountCalculator.calculate_action_discount_amount(
                update_data['amount'],
                tr_request.discount
            )

        TransportationRequest.objects.update_model(
            tr_request,
            **update_data
        )

        TransportationHistory.objects.log_action(
            tr_request,
            ActionTypes.delivered.value,
            user_by_id=user.id,
            comment='Груз доставлен в последний пункт'
        )

        return tr_request

    @classmethod
    def change_status(
            cls,
            tr_request: TransportationRequest,
            user: DjangoUser,
            type_: str,
            data: dict
    ) -> TransportationRequest:
        """
        Change transportation request status
        :param tr_request: TransportationRequest instance
        :param user: auth.User (current session user)
        :param type_: string, type of the request
        :param data: dict (request.data)
        :return: updated TransportationRequest
        """
        handlers: Dict[str, Callable] = {
            'assign': cls.assign_transportation_request,
            'on_the_road': cls.set_on_the_road,
            'in_progress': cls.start_transportation,
            'cancel': cls.cancel_transportation_request,
            'paid': cls.transportation_request_paid,
            'finish': cls.finish_transportation_request
        }
        if type_ in handlers:
            return handlers[type_](tr_request, user, data)
        else:
            return cls.switch_status(tr_request, user, type_)

    @classmethod
    def set_price_amount(
            cls,
            tr_request: TransportationRequest,
            user: DjangoUser,
            data: dict
    ) -> TransportationRequest:
        """
        Set TransportationRequest car_amount and porters_amount
        :param tr_request: TransportationRequest instance
        :param user: User instance (currently logged-in user)
        :param data: dictionary (request.data)
        :return: updated TransportationRequest
        :raises: ValueError (if TransportationRequest is completed)
        """
        if tr_request.status in COMPLETED_STATUSES:
            raise ValueError(u'Заявка в данном статусе не может быть изменена')

        validated_data: dict = cls._get_validated_price_data(data)
        TransportationRequest.objects.update_model(
            tr_request,
            commit=False,
            **validated_data
        )

        if tr_request.action:
            tr_request.discount_amount_min = DiscountCalculator.calculate_action_discount_amount(
                tr_request.min_amount,
                tr_request.discount
            )

            tr_request.discount_amount_max = DiscountCalculator.calculate_action_discount_amount(
                tr_request.max_amount,
                tr_request.discount
            )

        tr_request.save()

        TransportationHistory.objects.log_action(
            tr_request,
            ActionTypes.change_price.value,
            user_by_id=user.id,
            comment='Цена изменена сотрудником'
        )

        return tr_request

    @classmethod
    def _get_validated_price_data(cls, data: dict) -> dict:
        """
        Validate prices
        Max price must be greater than general amount. If it's the same or less,
        exclude Max price and leave only general price
        :param data: dict
        :return: dict
        """
        validated_data: dict = {
            'car_amount': abs(float(data.get('car_amount') or 0)),
            'porters_amount': abs(float(data.get('porters_amount') or 0)),
            'car_amount_max': abs(float(data.get('car_amount_max') or 0)),
            'porters_amount_max': abs(float(data.get('porters_amount_max') or 0)),
            'is_hourly_rate': data.get('is_hourly_rate', False),
            'is_cashless_payment': data.get('is_cashless_payment', False)
        }

        if validated_data['car_amount_max'] <= validated_data['car_amount']:
            validated_data['car_amount_max'] = None
        elif not validated_data['car_amount']:
            validated_data['car_amount'] = validated_data['car_amount_max']
            validated_data['car_amount_max'] = None

        if validated_data['porters_amount_max'] <= validated_data['porters_amount']:
            validated_data['porters_amount_max'] = None
        elif not validated_data['porters_amount']:
            validated_data['porters_amount'] = validated_data['porters_amount_max']
            validated_data['porters_amount_max'] = None

        return validated_data

    @classmethod
    def get_statistics(cls, user: DjangoUser) -> str:
        """
        Return Transportation statistics
        :param user: auth.User instance
        :return: dict
        """
        assignee_id: Union[int, None] = None
        if user.employee_card.role == 'driver':
            assignee_id = user.id
        
        return TransportationRequest.objects.get_statistics(assignee_id)

    @classmethod
    def send_ws_message(
            cls,
            tr_request: TransportationRequest,
            message: str = 'tr_changed',
            current_user: DjangoUser = None
    ):
        """
        Send WebSocket message about Transportation request has been changed
        :param tr_request: Transportation Request
        :param message: string
        :param current_user: current logged in user
        :return: None
        """
        try:
            WebSocketsPublisher.publish_tr_request_changed(tr_request, message, current_user)
        except Exception as e:
            print(e)

    @classmethod
    def send_ws_statistics_message(
            cls,
            message: str,
            current_user: DjangoUser = None,
            recipients: List[str] = None
    ):
        """
        Send WebSocket statistics message (request created, request assigned to user
        :param message: string
        :param current_user: current logged-in user
        :param recipients: list of string
        :return: None
        """
        try:
            WebSocketsPublisher.publish_statistics(message, current_user, recipients)
        except Exception as e:
            print(e)
