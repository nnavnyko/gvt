"""
Transportation Reports Views
"""
from typing import List

from django.contrib.auth.decorators import login_required
from django.http import HttpRequest, HttpResponse
from django.utils.decorators import method_decorator
from django.utils.translation import ugettext as _
from django.views.generic.base import View

from accounts.decorators import role_required, status_active_required
from accounts.enums import EMPLOYEE_MANAGERS_ROLES, EMPLOYEES_ROLES
from common.utilities.common import normalize_filename
from organisations.models.organisation import Organisation
from transportation.models.transportation_request import TransportationRequest
from transportation.utils.transportation_request_reports_util import TransportationRequestsReportsUtil


@method_decorator([login_required(login_url='auth_login'), role_required(*EMPLOYEE_MANAGERS_ROLES),
                   status_active_required],
                  name='dispatch')
class TransportationRequestsGeneralReportView(View):
    """
    transportation Request reports api ViewSet
    """

    @staticmethod
    def get(request: HttpRequest, *__, **___) -> HttpResponse:
        """
        Generate General Report
        :param request: HttpRequest instance
        :return: xls file response
        """

        rows, total_data = TransportationRequest.objects.get_general_report_data(**dict(request.GET.items()))
        http_response = TransportationRequestsReportsUtil.generate_general_report_response(rows, total_data)

        return http_response


@method_decorator([login_required(login_url='auth_login'), role_required(*EMPLOYEES_ROLES), status_active_required],
                  name='dispatch')
class TransportationRequestsEmployeeReportView(View):
    """
    transportation Request reports api ViewSet
    """

    @staticmethod
    def get(request: HttpRequest, *__, **___) -> HttpResponse:
        """
        Generate General Report
        :param request: HttpRequest instance
        :return: xls file response
        """
        data = dict(request.GET.items())
        data['assignee_id'] = request.user.id
        
        rows, total_data = TransportationRequest.objects.get_general_report_data(**data)
        http_response = TransportationRequestsReportsUtil.generate_general_report_response(rows, total_data)

        return http_response


# TODO: fix role required - must be either Employee or Organisation member
@method_decorator([
    login_required(login_url='auth_login'),
    role_required(*EMPLOYEE_MANAGERS_ROLES),
    status_active_required
], name='dispatch')
class TransportationRequestsOrganisationReportView(View):
    """
    transportation Request reports api ViewSet
    """

    @staticmethod
    def get(request: HttpRequest, org_uuid: str, *__, **___) -> HttpResponse:
        """
        Generate General Report
        :param request: HttpRequest instance
        :param org_uuid: string (Organisation.uuid)
        :return: xls file response
        """
        data: dict = dict(request.GET.items())
        organisation: Organisation = Organisation.objects.get_by_uuid(org_uuid)

        rows: List[dict]
        total_data: dict
        rows, total_data = TransportationRequest.objects.get_organisation_general_report_data(
            organisation.id,
            **data
        )

        filename: str = ''.join([normalize_filename(organisation.name), '_', _('Отчет')])
        http_response = TransportationRequestsReportsUtil.\
            generate_general_organisation_report_response(rows, total_data, filename)

        return http_response


download_transportation_general_report = TransportationRequestsGeneralReportView.as_view()
download_transportation_employee_general_report = TransportationRequestsEmployeeReportView.as_view()
download_transportation_organisation_general_report = TransportationRequestsOrganisationReportView.as_view()
